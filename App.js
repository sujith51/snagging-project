import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Modal } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import store, { persistor } from './src/redux/store';
import * as Updates from 'expo-updates';
import Button from './src/components/Button';
import { createTheming } from '@callstack/react-theme-provider';
import { theme } from './src/utils/theme';
import { NavigationContainer } from '@react-navigation/native';
import { ThemeProvider, withTheme } from './src/utils';
import { AppStart } from '_navigations';
import * as Font from 'expo-font';
console.disableYellowBox = true;

// const customFonts = {
//   'Montserrat-ExtraLight': require('./assets/fonts/Montserrat/Montserrat-ExtraLight.ttf'),
//   'Montserrat-Light': require('./assets/fonts/Montserrat/Montserrat-Light.ttf'),
//   'Montserrat-Thin': require('./assets/fonts/Montserrat/Montserrat-Thin.ttf'),
//   'Montserrat-Regular': require('./assets/fonts/Montserrat/Montserrat-Regular.ttf'),
//   'Montserrat-SemiBold': require('./assets/fonts/Montserrat/Montserrat-SemiBold.ttf'),
//   'Montserrat-Black': require('./assets/fonts/Montserrat/Montserrat-Black.ttf'),
//   'Montserrat-Bold': require('./assets/fonts/Montserrat/Montserrat-Bold.ttf'),
//   'Montserrat-ExtraBold': require('./assets/fonts/Montserrat/Montserrat-ExtraBold.ttf'),
//   'Montserrat-Medium': require('./assets/fonts/Montserrat/Montserrat-Medium.ttf'),
// };

const loadFontsAsync = async () => {
  // await Font.loadAsync(customFonts);
  // return true;
};
// const { ThemeProvider,  withTheme, useTheme } = createTheming(theme);
export default function App() {
  const [fontLoad, setFontLoaded] = useState(false);
  const [isUpdateAvl, setIsUpdateAvl] = useState(false);
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  loadFontsAsync().then(() => {
    setFontLoaded(true);
  });

  const CheckUpdate = async () => {
    if (!__DEV__) {
      try {
        const update = await Updates.checkForUpdateAsync();
        if (update.isAvailable) {
          setIsUpdateAvl(true);
          // toggleModal({
          //   title: 'Updating...',
          //   onBgPress: () => {},
          // });
          // alert('update available');
          await Updates.fetchUpdateAsync();
          await Updates.reloadAsync();
          // ... notify user of update ...
        }
      } catch (e) {
        setIsUpdateAvl(false);
        // handle or log error
      }
    }
    setIsUpdateAvl(false);
  };

  useEffect(() => {
    CheckUpdate();
  }, []);

  return (
    // <View style={styles.container}>
    //   <StatusBar style="auto" />
    !fontLoad ? null : (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ThemeProvider>
            <NavigationContainer>
              <AppStart />
              {isUpdateAvl && (
                <Modal
                  isVisible={isUpdateAvl}
                  useNativeDriver={true}
                  style={{ margin: 0, flex: 1 }}
                  onBackdropPress={() => {}}
                  // backdropColor={'red'}
                  backdropOpacity={0.8}
                  transparent
                >
                  <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                    <View
                      style={{
                        flex: 0,
                        backgroundColor: 'lightgray',
                        padding: 80,
                        // paddingBottom: 100,
                        borderTopEndRadius: 20,
                        borderTopStartRadius: 20,
                        alignContent: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      <View
                        style={{
                          alignContent: 'center',
                          justifyContent: 'center',
                          flexDirection: 'row',
                        }}
                      >
                        {/* <Image
                        source={require('../../../assets/success-pop-image.png')}
                      /> */}
                      </View>
                      <View style={{ marginTop: 30 }}>
                        <Text
                          style={{
                            textAlign: 'center',
                            // ...theme.typography.headH2,
                            // color: theme.colors.primary,
                          }}
                        >
                          Updating the App please wait...
                        </Text>
                      </View>
                    </View>
                  </View>
                </Modal>
              )}
            </NavigationContainer>
          </ThemeProvider>
        </PersistGate>
      </Provider>
    )

    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
