import * as Notifications from 'expo-notifications';


export default async function registerForPushNotificationsAsync() {
  
  let experienceId = '@realcube/snagging';
  let status = await Notifications.getPermissionsAsync();
  console.log('stat',status)
  if(!(status.granted || status.ios?.status === Notifications.IosAuthorizationStatus.PROVISIONAL)) {
  status = await Notifications.requestPermissionsAsync({
      ios: {
        allowAlert: true,
        allowBadge: true,
        allowSound: true,
        allowAnnouncements: true,
      },
    });
  } 
  if(!(status.granted || status.ios?.status === Notifications.IosAuthorizationStatus.PROVISIONAL)) {
    return;
  }
  let expoPushToken = await Notifications.getExpoPushTokenAsync({
    experienceId,
  });
  // console.log('tookennnnnnnnnnnn after',expoPushToken)
  return Promise.resolve(expoPushToken.data);


  // if (userData) {
  //   payload.id_user = userData.id_user;
  //   return fetch(PUSH_ENDPOINT, {
  //     method: 'POST',
  //     headers: {
  //       Accept: 'application/json',
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify(payload),
  //   });
  // }
  //http://provis.realcube.estate/api/mobilenotification/updatedevicetoken

  //  device_token: "ExponentPushToken[DK1imHEWFzoee3KyQgj-PP]"
  // device_type: "android"
  // id_user: "11964"
}

