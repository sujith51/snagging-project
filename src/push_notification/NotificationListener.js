import React,{useEffect,useState,useRef, } from 'react';
import { Linking,Text,TouchableWithoutFeedback,View } from 'react-native';
import * as Notifications from 'expo-notifications';
import {
	useNavigation
} from '@react-navigation/native';

 const NotificationListener =  ()  =>{
	const { navigate } = useNavigation();
	const notificationListener = useRef();
  	const responseListener = useRef();		

	useEffect(() => {
		notificationListener.current = Notifications.addNotificationReceivedListener(
		  handleNotifications,
		);
	
		responseListener.current = Notifications.addNotificationResponseReceivedListener(
		  (response) => {
			handleNotifications(response.notification, false);
		  },
		);
	
		return () => {
		  Notifications.removeNotificationSubscription(notificationListener);
		  Notifications.removeNotificationSubscription(responseListener);
		};
	}, []);
  	navigateTo =(data) => {
		switch (data.screen) {
			case 'mrp_key_collection':
				navigate('Quotations');
				break;
			case 'mrp_item_requested':
				navigate('TaskLog');
				break;
			case 'Quotation':
				navigate('Quotations');
				break;
			default:
				break;
		}
	}
	const handleNotifications = (notifications, showNotifications = true) => {
		// console.log('1111111111111',notifications, showNotifications)
		let { data = null } = notifications.request.content;
		data = Platform.OS === 'ios' ? data.body : data;
		// console.log('22222222222222',data)
		if (showNotifications === false) {
			navigateTo(data);
		}
	};
  return (
    // Your app content
    <TouchableWithoutFeedback >
			<View style={{ flexDirection: 'row' }}>
				<View style={{ flex: 1 }}>
					{/* <Text style={{ fontSize: 16, fontWeight: 'bold' }}>
						{'bodyData.title'}
					</Text>
					<Text>{'bodyData.body'}</Text> */}
				</View>
			</View>
		</TouchableWithoutFeedback>
  );
}
export default NotificationListener;