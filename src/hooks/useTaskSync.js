/* eslint-disable no-prototype-builtins */
/* eslint-disable no-useless-return */
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as actions from '../redux/actions';
import { services, getFullUrl } from '../utils';
// import useTask from './useTask';

export default function useTaskSync() {
  // :Hooks
  const dispatch = useDispatch();
  const { taskQueue } = useSelector((state) => state.taskQueue);
  const {
    netInfo: { isConnected },
  } = useSelector((state) => state.user);

  // const { getTaskForced } = useTask('newtask');

  // :Effects
  useEffect(() => {
    if (isConnected) {
      apiSyncFn();
      const check = taskQueue.some((i) => i.count === 0);
      if (!check) {
        retrySyncFn();
      }
    }
  }, [taskQueue, isConnected]);

  // :fn
  const apiSyncFn = async () => {
    if (taskQueue?.length > 0) {
      taskQueue.forEach((task) => {
        if (
          (task.status === 'fail' && task.count !== 0) ||
          task.hasOwnProperty('id') === false
        ) {
          return;
        }
        services
          .apiSync({ data: task, id: task.id, url: task.url })
          .then((res) => {
            if (res?.response?.data?.status === 'success') {
              setTimeout(() => {
                dispatch(actions.removeFromSyncQueue(res.id));
                // dispatch(
                //   actions.showToast(
                //     res?.response?.data?.msg
                //       ? res.response.data.msg
                //       : 'Allocated successfully',
                //   ),
                // );
              }, 5000);
              // getTaskForced();
            } else {
              setTimeout(() => {
                dispatch(actions.updateCountSyncQueue(res.id));
                // dispatch(actions.showToast('Failed, Auto re-trying'));
              }, 50000);
            }
          });
      });
    }
  };

  const retrySyncFn = async () => {
    if (taskQueue.length > 0) {
      taskQueue.forEach((task) => {
        if (task.status != null && task.count !== 0) {
          dispatch(actions.resetFailedSyncQueue(task.id));
        } else {
          return;
        }
      });
    }
  };
  return null;
}
