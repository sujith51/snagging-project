import * as types from '../types';
import { services, endpoints, getFullUrlPM } from '../../utils';

export const getDashboardListing = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.DASHBOARD_DATA_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.snagInspectionUnitList), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.DASHBOARD_DATA_SUCCESS,
            payload: res,
          });
          // services.localSet("@userData",res);
        } else {
          dispatch({
            type: types.DASHBOARD_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const getOwnerDashboardListing = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.DASHBOARD_OWNER_DATA_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.ownerInspectionUnitList), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.DASHBOARD_OWNER_DATA_SUCCESS,
            payload: res,
          });
          // services.localSet("@userData",res);
        } else {
          dispatch({
            type: types.DASHBOARD_OWNER_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getBarGraphData = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.DASHBOARD_BARGRAPH_DATA_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.unitsInspectionGraph), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.DASHBOARD_BARGRAPH_DATA_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.DASHBOARD_BARGRAPH_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getCircularGraphData = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.DASHBOARD_CIRCULARGRAPH_DATA_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.unitsListForInspection), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.DASHBOARD_CIRCULARGRAPH_DATA_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.DASHBOARD_CIRCULARGRAPH_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const searchUnits = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.SEARCH_UNIT_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.getinspectionList), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.SEARCH_UNIT_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.SEARCH_UNIT_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const saveLocationProperty = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_PROPERTY_LOCATION_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.addPropertyGeocodes), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.SAVE_PROPERTY_LOCATION_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.SAVE_PROPERTY_LOCATION_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getReviewVendor = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.GET_REVIEW_VENDOR_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.getreviewVendor), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.GET_REVIEW_VENDOR_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.GET_REVIEW_VENDOR_FAIL,
            payload: res,
          });
        }
      });
  };
};
