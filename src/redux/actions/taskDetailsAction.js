/* eslint-disable import/prefer-default-export */
import * as types from '../types';
import { services, endpoints, getFullUrlPM } from '../../utils';

export const getTaskDetails = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.UNIT_INSPECTION_DETAILS_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.unitsInspectionDetails), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.UNIT_INSPECTION_DETAILS_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.UNIT_INSPECTION_DETAILS_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const getTaskDetailsMrd = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.UNIT_INSPECTION_DETAILS_MRD_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.taskDetailsMrd), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.UNIT_INSPECTION_DETAILS_MRD_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.UNIT_INSPECTION_DETAILS_MRD_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const getHandoverDetails = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.GET_HANDOVER_DETAILS_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.ownerInspectionDetails), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.GET_HANDOVER_DETAILS_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.GET_HANDOVER_DETAILS_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getTaskData = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.TASK_DATA_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.unitsListForInspectionMRD), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.TASK_DATA_SUCCESS,
            payload: { res, page: data.page },
          });
        } else {
          dispatch({
            type: types.TASK_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getMaterialsList = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.GET_MATERIAL_START,
      payload: null,
    });
    services
      .postWithOutKey(getFullUrlPM(endpoints.getMaterial), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'success' && res.data) {
          dispatch({
            type: types.GET_MATERIAL_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.GET_MATERIAL_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getListDamage = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.LIST_DAMAGE_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.listDamages), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.LIST_DAMAGE_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.LIST_DAMAGE_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const getOwnerListDamage = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.OWNER_LIST_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.ownerWorkStatusDetails), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.OWNER_LIST_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.OWNER_LIST_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const savechklistquesctions = (idInsBooking, data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_LIST_DAMAGE_QUESCTIONS,
      payload: { idInsBooking, data },
    });
  };
};
export const saveYValues = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_Y_VALUES,
      payload: { data },
    });
  };
};
export const saveMaterialDetails = (id, data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_NEW_MATERIAL_DETAILS,
      payload: { id, data },
    });
  };
};
export const deleteMaterialDetails = (id) => {
  return (dispatch) => {
    dispatch({
      type: types.DELETE_NEW_MATERIAL_DETAILS,
      payload: { id },
    });
  };
};

export const saveReviewAnswers = (idInsBooking, data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_REVIEW_ANSWERS,
      payload: { idInsBooking, data },
    });
  };
};
export const saveExtraReviewData = (idInsBooking, data) => {
  console.log('inde', idInsBooking, data)
  return (dispatch) => {
    dispatch({
      type: types.SAVE_REVIEW_EXTRA_DATA,
      payload: { idInsBooking, data },
    });
  };
};
export const deleteExtraReviewData = (idInsBooking,id, data) => {
  return (dispatch) => {
    dispatch({
      type: types.DELETE_REVIEW_EXTRA_DATA,
      payload: { idInsBooking,id, data },
    });
  };
};
export const deleteWholeReviewData = (idInsBooking) => {
  return (dispatch) => {
    dispatch({
      type: types.DELETE_WHOLE_MATERIAL_DETAILS,
      payload: { idInsBooking },
    });
  };
};
export const getRateCardList = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.RATECARD_DATA_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.listRateCard), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.RATECARD_DATA_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.RATECARD_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getInspectionHistory = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.INSPECTION_HISTORY_DATA_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.unitInspectionHistory), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.INSPECTION_HISTORY_DATA_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.INSPECTION_HISTORY_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getTaskLogs = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.GET_TASK_LOGS_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.taskLogs), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.GET_TASK_LOGS_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.GET_TASK_LOGS_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const saveDamageInspection = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_DAMAGE_INSPECTION_START,
      payload: null,
    });
    services
      .postWithOutKey(getFullUrlPM(endpoints.saveDamageInspection), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.SAVE_DAMAGE_INSPECTION_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.SAVE_DAMAGE_INSPECTION_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const keyNotCollected = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.KEY_NOT_COLLECTED_START,
      payload: null,
    });
    services.post(getFullUrlPM(endpoints.keyNotCollected), data).then((res) => {
      if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
        dispatch({
          type: types.KEY_NOT_COLLECTED_SUCCESS,
          payload: res,
        });
      } else {
        dispatch({
          type: types.KEY_NOT_COLLECTED_FAIL,
          payload: res,
        });
      }
    });
  };
};

export const deletesubmittedIns = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.DELETE_COMPLETED_INSPECTION,
      payload: { data },
    });
  };
};

export const deleteReviewAnswers = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.DELETE_COMPLETED_REVIEW_ANS,
      payload: { data },
    });
  };
};

export const getWorkDetails = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.GET_WORK_DETAILS_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.itemWorkStatusDetails), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res?.data) {
          dispatch({
            type: types.GET_WORK_DETAILS_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.GET_WORK_DETAILS_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const getItemWork = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.GET_ITEM_WORK_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.itemWork), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res?.data) {
          dispatch({
            type: types.GET_ITEM_WORK_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.GET_ITEM_WORK_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const saveWorkDetails = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_WORK_DETAILS_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.saveWorkStatusDetails), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res?.data) {
          dispatch({
            type: types.SAVE_WORK_DETAILS_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.SAVE_WORK_DETAILS_FAIL,
            payload: res,
          });
        }
      });
  };
};
export const saveOwnerWorkStatus = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.SAVE_WORK_STATUS_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.saveOwnerWorkStatus), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res?.data) {
          dispatch({
            type: types.SAVE_WORK_STATUS_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.SAVE_WORK_STATUS_FAIL,
            payload: res,
          });
        }
      });
  };
};

export const getContractorRate = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.LIST_CONTRACTOR_RATE_START,
      payload: null,
    });
    services
      .getWithoutKey(getFullUrlPM(endpoints.listContractorRateCard), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.LIST_CONTRACTOR_RATE_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.LIST_CONTRACTOR_RATE_FAIL,
            payload: res,
          });
        }
      });
  };
};
