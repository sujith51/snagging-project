/* eslint-disable import/prefer-default-export */
import * as types from '../types';
import { services, endpoints, getFullUrl, getFullUrlPM } from '../../utils';

// export const getUserUnit = (data) => {
//   return (dispatch) => {
//     dispatch({
//       type: types.USER_UNIT_START,
//       payload: null,
//     });
//     services.post(getFullUrl(endpoints.getuserunits), data).then((res) => {
//       if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
//         dispatch({
//           type: types.USER_UNIT_SUCCESS,
//           payload: res,
//         });
//       } else {
//         dispatch({
//           type: types.USER_UNIT_FAIL,
//           payload: res,
//         });
//       }
//     });
//   };
// };
export const getQuotationList = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.QUOTATION_LIST_START,
      payload: null,
    });
    services.post(getFullUrlPM(endpoints.itemRequested), data).then((res) => {
      if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
        dispatch({
          type: types.QUOTATION_LIST_SUCCESS,
          payload: res,
        });
      } else {
        dispatch({
          type: types.QUOTATION_LIST_FAIL,
          payload: res,
        });
      }
    });
  };
};
// : generate segregation report
export const genSegReport = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.GEN_SEG_REPORT_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.generateSegregationReport), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.GEN_SEG_REPORT_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.GEN_SEG_REPORT_FAIL,
            payload: res,
          });
        }
      });
  };
};

// : submit segregation report
export const submitSegReport = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.SUBMIT_SEG_REPORT_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.submitSegregationReport), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.SUBMIT_SEG_REPORT_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.SUBMIT_SEG_REPORT_FAIL,
            payload: res,
          });
        }
      });
  };
};
