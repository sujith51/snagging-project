import * as types from '../types';
import { services, endpoints, getFullUrlPM } from '../../utils';

export const login = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.CHECK_LOGIN_START,
      payload: null,
    });
    services.post(getFullUrlPM(endpoints.login), data).then((res) => {
      if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
        dispatch({
          type: types.CHECK_LOGIN_END_SUCCESS,
          payload: res,
        });
        // services.localSet("@userData",res);
      } else {
        dispatch({
          type: types.CHECK_LOGIN_END_FAIL,
          payload: res,
        });
      }
    });
  };
};

export const logOut = () => {
  return (dispatch) => {
    dispatch({
      type: types.LOGOUT,
      payload: null,
    });
  };
};

export const updateNetinfo = (data) => (dispatch) => {
  dispatch({
    type: types.UPDATE_NET_INFO,
    payload: data,
  });
};

export const saveSelectedCommunity = (data,type) => (dispatch) => {
  dispatch({
    type: types.SAVE_SELECTED_COMMUNITY,
    payload: {data,type},
  });
};

export const saveSelectedUnit = (data) => (dispatch) => {
  dispatch({
    type: types.SAVE_SELECTED_UNIT,
    payload: data,
  });
};

export const getCommunityList = (data) => (dispatch) => {
  dispatch({
    type: types.GET_COMMUNITY_LIST_START,
    payload: null,
  });
  services.post(getFullUrlPM(endpoints.getCommunities), data).then((res) => {
    if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
      dispatch({
        type: types.GET_COMMUNITY_LIST_SUCCESS,
        payload: res,
      });
    } else {
      dispatch({
        type: types.GET_COMMUNITY_LIST_FAIL,
        payload: res,
      });
    }
  });
};

export const updateDeviceToken = (data) => (dispatch) => {
  dispatch({
    type: types.UPDATE_DEVICE_TOKEN_START,
    payload: null,
  });
  services.post(getFullUrlPM(endpoints.updateDeviceToken), data).then((res) => {
    if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
      dispatch({
        type: types.UPDATE_DEVICE_TOKEN_SUCCESS,
        payload: res,
      });
    } else {
      dispatch({
        type: types.UPDATE_DEVICE_TOKEN_FAIL,
        payload: res,
      });
    }
  });
};

export const saveUserPushToken = (data) => (dispatch) => {
  dispatch({
    type: types.SAVE_USER_PUSH_TOKEN,
    payload: data,
  });
};
