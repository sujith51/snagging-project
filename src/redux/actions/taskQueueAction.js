import * as types from '../types';

export const addToSyncQueue = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.ADD_TASK_TO_QUEUE,
      payload: data,
    });
  };
};
export const removeFromSyncQueue = (id) => {
  return (dispatch) => {
    dispatch({
      type: types.REMOVE_TASK_FROM_QUEUE,
      payload: id,
    });
  };
};

export const updateCountSyncQueue = (id) => {
  return (dispatch) => {
    dispatch({
      type: types.UPDATE_FAIL_COUNT_TASK_QUEUE,
      payload: id,
    });
  };
};

export const resetFailedSyncQueue = (id) => {
  return (dispatch) => {
    dispatch({
      type: types.RESET_FAIL_SYNC_TASK_QUEUE,
      payload: id,
    });
  };
};
