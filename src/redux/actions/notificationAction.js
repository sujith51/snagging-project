/* eslint-disable import/prefer-default-export */
import * as types from '../types';
import { services, endpoints, getFullUrlPM } from '../../utils';

// : push notification data
export const pushNotificationData = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.PUSH_NOTIFICATION_DATA_START,
      payload: null,
    });
    services
      .post(getFullUrlPM(endpoints.pushNotification), data)
      .then((res) => {
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          dispatch({
            type: types.PUSH_NOTIFICATION_DATA_SUCCESS,
            payload: res,
          });
        } else {
          dispatch({
            type: types.PUSH_NOTIFICATION_DATA_FAIL,
            payload: res,
          });
        }
      });
  };
};

// : push notification detail data
export const notificationDetailData = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.NOTIFICATION_DETAIL_START,
      payload: null,
    });
    services.post(getFullUrlPM(endpoints.notification), data).then((res) => {
      if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
        dispatch({
          type: types.NOTIFICATION_DETAIL_SUCCESS,
          payload: res,
        });
      } else {
        dispatch({
          type: types.NOTIFICATION_DETAIL_FAIL,
          payload: res,
        });
      }
    });
  };
};
