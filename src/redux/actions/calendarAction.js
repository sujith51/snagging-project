import * as types from '../types';
import { services, endpoints, getFullUrlPM } from '../../utils';

export const getCalendarData = (data) =>{
    return (dispatch) =>{
      dispatch({
        type : types.CALENDAR_DATA_START,
        payload : null
      });
      services.getWithoutKey(getFullUrlPM(endpoints.unitInspectionsCalendar), data).then((res)=>{
        if(res?.status?.toUpperCase() === 'SUCCESS' && res.data){
          dispatch({
            type : types.CALENDAR_DATA_SUCCESS,
            payload : res
          })
        }else{
          dispatch({
            type : types.CALENDAR_DATA_FAIL,
            payload : res
          })
        }
      })
    }
  }