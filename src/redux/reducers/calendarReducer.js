import * as types from '../types';

const init_state = {
    calendarData: {
        data: null,
        loading: false,
        error: {
            status: false,
            message: ''
        }
    }
}


const calendarReducer = (state = init_state, action) => {
    switch (action.type) {

        case types.CALENDAR_DATA_START: {
            return {
                ...state,
                calendarData: {
                    ...state.calendarData,
                    loading: true
                }
            }
        }

        case types.CALENDAR_DATA_SUCCESS: {
            return {
                ...state,
                calendarData: {
                    data: action.payload.data,
                    error: {
                        status: false,
                        message: ''
                    },
                    loading: false
                }
            }
        }

        case types.CALENDAR_DATA_FAIL: {
            return {
                ...state,
                calendarData: {
                    data: action.payload.data,
                    error: {
                        status: true,
                        message: ''
                    },
                    loading: false
                }
            }
        }
    }
    return state;
}

export default calendarReducer;