import * as types from '../types';

const init_state = {
    unitData: {
        loginUnit: null,
        unitLoading: false,
        unitError: {
            status: false,
            message: ''
        }
    },
    quotationList:{
        listData: null,
        quotationLoading: false,
        quotationListError: {
            status: false,
            message: ''
        }
    },
    geneSegReport:{
        segData: null,
        geneSegReportLoading: false,
        geneSegReportError: {
            status: false,
            message: ''
        }
    },
    subitSegReport:{
        submitSegData: null,
        subitSegReportLoading: false,
        subitSegReportError: {
            status: false,
            message: ''
        }
    }
}


const quotationReducer = (state = init_state, action) => {
    switch (action.type) {
        case types.USER_UNIT_START: {
            return {
                ...state,
                unitData: {
                    ...state.unitData,
                    unitLoading: true
                }
            }
        }
        case types.USER_UNIT_SUCCESS: {
            return {
                ...state,
                unitData: {
                    loginUnit: action.payload.data,
                    // unitError: {
                    //     status: false,
                    //     message: ''
                    // },
                    unitLoading: false
                }
            }
        }

        case types.USER_UNIT_FAIL: {
            return {
                ...state,
                unitData: {
                    loginUnit: action.payload.data,
                    unitError: {
                        status: true,
                        message: ''
                    },
                    unitLoading: false
                }
            }
        }
        case types.QUOTATION_LIST_START: {
            return {
                ...state,
                quotationList: {
                    ...state.quotationList,
                    quotationLoading: true
                }
            }
        }
        case types.QUOTATION_LIST_SUCCESS: {
            return {
                ...state,
                quotationList: {
                    listData: action.payload.data,
                    // quotationListError: {
                    //     status: false,
                    //     message: ''
                    // },
                    quotationLoading: false
                }
            }
        }
        case types.QUOTATION_LIST_FAIL: {
            return {
                ...state,
                quotationList: {
                    listData: action.payload.data,
                    quotationListError: {
                        status: true,
                        message: ''
                    },
                    quotationLoading: false
                }
            }
        }

        case types.GEN_SEG_REPORT_START: {
            return {
                ...state,
                geneSegReport:{
                    ...state.geneSegReport,                   
                    geneSegReportLoading: true,
                },
            }
        }
        case types.GEN_SEG_REPORT_SUCCESS: {
            return {
                ...state,
                geneSegReport:{
                    segData: action.payload.data,
                    geneSegReportLoading: false,
                },
            }
        }
        case types.GEN_SEG_REPORT_FAIL: {
            return {
                ...state,
                geneSegReport:{
                    geneSegReportLoading: false,
                    geneSegReportError: {
                        status: action.payload.data,
                        message: action.payload.data
                    }
                },
            }
        }
        case types.SUBMIT_SEG_REPORT_START: {
            return {
                ...state,
                subitSegReport:{
                    // ...state.subitSegReport, 
                    submitSegData:{},                  
                    subitSegReportLoading: true,
                },
            }
        }
        case types.SUBMIT_SEG_REPORT_SUCCESS: {
            // console.log('SUBMIT_SEG_REPORT_SUCCESS', action.payload);
            return {
                ...state,
                subitSegReport:{
                    submitSegData: action.payload,
                    subitSegReportLoading: false,
                },
            }
        }
        case types.SUBMIT_SEG_REPORT_FAIL: {
            // console.log('SUBMIT_SEG_REPORT_FAIL', action.payload);
            return {
                ...state,
                subitSegReport:{
                    subitSegReportLoading: false,
                    subitSegReportError: {
                        status: action.payload.data,
                        message: action.payload.data
                    }
                },
            }
        }
            case types.LOGOUT: {
           return {
             ...init_state,
           };
    }
    }
    return state;
}

export default quotationReducer;