import * as types from '../types';

const init_state = {
  dashboardData: {
    data: null,
    count: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  ownerDashboard: {
    data: null,
    count: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  barGraphData: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  circularGraphData: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  searchData: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  savePropertyLocation: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  reviewVendorList: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
};

const dashboardReducer = (state = init_state, action) => {
  switch (action.type) {
    case types.DASHBOARD_DATA_START: {
      return {
        ...state,
        dashboardData: {
          ...state.dashboardData,
          loading: true,
        },
      };
    }

    case types.DASHBOARD_DATA_SUCCESS: {
      return {
        ...state,
        dashboardData: {
          data: action.payload.data,
          count: action.payload.count,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.DASHBOARD_DATA_FAIL: {
      return {
        ...state,
        ddashboardData: {
          data: action.payload.data,
          count: null,
          loading: false,
          error: {
            status: true,
            message: '',
          },
        },
      };
    }
    case types.DASHBOARD_OWNER_DATA_START: {
      return {
        ...state,
        ownerDashboard: {
          ...state.ownerDashboard,
          loading: true,
        },
      };
    }

    case types.DASHBOARD_OWNER_DATA_SUCCESS: {
      return {
        ...state,
        ownerDashboard: {
          data: action.payload.data,
          count: action.payload.count,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.DASHBOARD_OWNER_DATA_FAIL: {
      return {
        ...state,
        ownerDashboard: {
          data: action.payload.data,
          count: null,
          loading: false,
          error: {
            status: true,
            message: '',
          },
        },
      };
    }

    case types.DASHBOARD_BARGRAPH_DATA_START: {
      return {
        ...state,
        barGraphData: {
          ...state.barGraphData,
          loading: true,
        },
      };
    }

    case types.DASHBOARD_BARGRAPH_DATA_SUCCESS: {
      return {
        ...state,
        barGraphData: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.DASHBOARD_BARGRAPH_DATA_FAIL: {
      return {
        ...state,
        barGraphData: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    // Corcular graph
    case types.DASHBOARD_CIRCULARGRAPH_DATA_START: {
      return {
        ...state,
        circularGraphData: {
          ...state.barGrcircularGraphDataaphData,
          loading: true,
        },
      };
    }

    case types.DASHBOARD_CIRCULARGRAPH_DATA_SUCCESS: {
      return {
        ...state,
        circularGraphData: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.DASHBOARD_CIRCULARGRAPH_DATA_FAIL: {
      return {
        ...state,
        circularGraphData: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    // search data
    case types.SEARCH_UNIT_START: {
      return {
        ...state,
        searchData: {
          ...state.searchData,
          loading: true,
        },
      };
    }

    case types.SEARCH_UNIT_SUCCESS: {
      return {
        ...state,
        searchData: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.SEARCH_UNIT_FAIL: {
      return {
        ...state,
        searchData: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    // location data
    case types.SAVE_PROPERTY_LOCATION_START: {
      return {
        ...state,
        savePropertyLocation: {
          ...state.savePropertyLocation,
          loading: true,
        },
      };
    }

    case types.SAVE_PROPERTY_LOCATION_SUCCESS: {
      return {
        ...state,
        savePropertyLocation: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.SAVE_PROPERTY_LOCATION_FAIL: {
      return {
        ...state,
        savePropertyLocation: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
      
       // review vendor list
    case types.GET_REVIEW_VENDOR_START: {
      return {
        ...state,
        reviewVendorList: {
          ...state.reviewVendorList,
          loading: true,
        },
      };
    }

    case types.GET_REVIEW_VENDOR_SUCCESS: {
      return {
        ...state,
        reviewVendorList: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.GET_REVIEW_VENDOR_FAIL: {
      return {
        ...state,
        reviewVendorList: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.LOGOUT: {
      return {
        ...init_state,
      };
    }
  }
  return state;
};

export default dashboardReducer;
