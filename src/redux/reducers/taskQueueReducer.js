/* eslint-disable default-case */
import * as types from '../types';

const init_state = {
  taskQueue: [],
};

const taskQueueReducer = (state = init_state, action) => {
  switch (action.type) {
    // task list
    case types.ADD_TASK_TO_QUEUE: {
      let newTask = [...state.taskQueue];
      const checkTask = newTask.findIndex((i) => i.id === action.payload.id);
      if (checkTask >= 0) {
        if (action.payload?.data?.work_status !== 8) {
          newTask[checkTask] = action.payload;
        }
      } else {
        newTask = [...newTask, action.payload];
      }
      return {
        ...state,
        taskQueue: newTask,
      };
    }
    case types.REMOVE_TASK_FROM_QUEUE: {
      return {
        taskQueue: state.taskQueue.filter((que) => {
          return que.id !== action.payload;
        }),
      };
    }

    case types.UPDATE_FAIL_COUNT_TASK_QUEUE: {
      const newTask = [...state.taskQueue];
      const check = newTask.findIndex((i) => i.id === action.payload);
      if (check >= 0) {
        newTask[check].count = 1;
        newTask[check].status = 'fail';
      }
      return {
        ...state,
        taskQueue: newTask,
      };
    }

    case types.RESET_FAIL_SYNC_TASK_QUEUE: {
      const newTask = [...state.taskQueue];
      const check = newTask.findIndex((i) => i.id === action.payload);
      if (check >= 0) {
        newTask[check].count = 0;
        newTask[check].status = null;
      }
      return {
        ...state,
        taskQueue: newTask,
      };
    }
  }
  return state;
};
export default taskQueueReducer;
