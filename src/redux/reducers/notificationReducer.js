/* eslint-disable default-case */
import * as types from '../types';

const init_state = {
  pushNotification: {
    data: null,
    idIns: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },

  notificationDetail: {
    detailData: null,
    nDetailLoading: false,
    nDetailError: {
      status: false,
      message: '',
    },
  },
};

const notificationReducer = (state = init_state, action) => {
  switch (action.type) {
    // : push notification data
    case types.PUSH_NOTIFICATION_DATA_START: {
      return {
        ...state,
        pushNotification: {
          ...state.pushNotification,
          loading: true,
        },
      };
    }
    case types.PUSH_NOTIFICATION_DATA_SUCCESS: {
      return {
        ...state,
        pushNotification: {
          data: action.payload.data,
          idIns: action.payload.id_snag_property,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.PUSH_NOTIFICATION_DATA_FAIL: {
      return {
        ...state,
        pushNotification: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    // : push notification detail data
    case types.NOTIFICATION_DETAIL_START: {
      return {
        ...state,
        notificationDetail: {
          // ...state.notificationDetail,
          detailData:null,
          nDetailLoading: true,
          nDetailError: {
            status: false,
            message: '',
          },
        },
      };
    }
    case types.NOTIFICATION_DETAIL_SUCCESS: {
      return {
        ...state,
        notificationDetail: {
          detailData: action.payload.data,
          nDetailLoading: false,
        },
      };
    }
    case types.NOTIFICATION_DETAIL_FAIL: {
      return {
        ...state,
        notificationDetail: {
          nDetailLoading: false,
          nDetailError: {
            status: action.payload.status,
            message: action.payload.message,
          },
        },
      };
    }
  }
  return state;
};

export default notificationReducer;




