/* eslint-disable default-case */
import * as types from '../types';
import { getFormattedDate } from '../../utils';

const init_state = {
  data: null,
  loading: false,
  loginTime: null,
  error: {
    status: false,
    message: '',
  },
  netInfo: {
    isConnected: false,
  },
  selectCommunity: [],
  selectUnit: {
    data: null,
  },
  communityList: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  updateToken: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  pushToken: {
    deviceToken: null,
  },
};

const userReducer = (state = init_state, action) => {
  switch (action.type) {
    // CHECK USER
    case types.LOGOUT: {
      return {
        data: null,
        loading: false,
        loginTime: null,
        error: {
          status: false,
          message: '',
        },
      };
    }
    // LOGIN
    case types.CHECK_LOGIN_START: {
      return {
        ...state,
        data: null,
        loading: true,
        error: { status: false },
      };
    }
    case types.CHECK_LOGIN_END_SUCCESS: {
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        loginTime: getFormattedDate(),
        error: {
          status: false,
        },
      };
    }
    case types.CHECK_LOGIN_END_FAIL: {
      return {
        ...state,
        loading: false,
        error: {
          status: true,
          message: action.payload.msg,
        },
      };
    }
    case types.UPDATE_NET_INFO: {
      return {
        ...state,
        netInfo: action.payload,
      };
    }

    case types.SAVE_SELECTED_COMMUNITY: {
      const copy = state.selectCommunity;
      const { data, type } = action.payload;
      let arr;
      if (type == 'All') {
        // const ifAllExist = copy.some((val) => val?.value == 'All');
        // if (ifAllExist) {
        //   arr = [];
        // } else {
        //   arr = [...data];
        // }
        arr = [...data];
      } else {
        const checkExist = copy.some((val) => val?.value == data?.value);

        if (checkExist) {
          arr = copy.filter(
            (val) => val?.value != data?.value && val.value != 'All',
          );
        } else {
          arr = [...copy, data];
        }
      }

      return {
        ...state,
        selectCommunity: arr,
      };
    }

    case types.SAVE_SELECTED_UNIT: {
      return {
        ...state,
        selectUnit: action.payload,
      };
    }
    // get community list..
    case types.GET_COMMUNITY_LIST_START: {
      return {
        ...state,
        communityList: {
          ...init_state.inspectionHistory,
          loading: true,
        },
      };
    }

    case types.GET_COMMUNITY_LIST_SUCCESS: {
      return {
        ...state,
        communityList: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.GET_COMMUNITY_LIST_FAIL: {
      return {
        ...state,
        communityList: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    // update device token..
    case types.UPDATE_DEVICE_TOKEN_START: {
      return {
        ...state,
        updateToken: {
          ...init_state.updateToken,
          loading: true,
        },
      };
    }

    case types.UPDATE_DEVICE_TOKEN_SUCCESS: {
      return {
        ...state,
        updateToken: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.UPDATE_DEVICE_TOKEN_FAIL: {
      return {
        ...state,
        updateToken: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.SAVE_USER_PUSH_TOKEN: {
      return {
        ...state,
        pushToken: action.payload,
      };
    }
  }
  return state;
};

export default userReducer;
