import { combineReducers } from 'redux';
import dashboardReducer from './dashboardReducer';
import taskDetailsReducer from './taskDetailsReducer';
import userReducer from './userReducer';
import calendarReducer from './calendarReducer';
import taskQueueReducer from './taskQueueReducer';
import quotationReducer from './quotationReducer';
import notificationReducer from './notificationReducer';

const combinedReducer = combineReducers({
  user: userReducer,
  dashboard: dashboardReducer,
  taskdetails: taskDetailsReducer,
  calendar: calendarReducer,
  taskQueue: taskQueueReducer,
  quotation: quotationReducer,
  notificationData: notificationReducer,
});

const rootReducer = (state, action) => {
  return combinedReducer(state, action);
};

export default rootReducer;
