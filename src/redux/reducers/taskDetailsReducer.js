/* eslint-disable default-case */
import * as types from '../types';

const init_state = {
  taskDetails: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  taskDetailsMrd: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  ownerTaskDetails: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  taskData: {
    data: null,
    count: null,
    loading: false,
    page: 1,
    endOfData: false,
    error: {
      status: false,
      message: '',
    },
  },
  listDamage: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  ownerListDamage: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  chklistQuesData: {
    data: [],
  },
  positionData: {},
  newMaterialArr: {},
  rateCardList: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  inspectionHistory: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  taskLogs: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  itemWork: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  saveDamage: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  keyNotCollected: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  workStatus: {
    data: null,
    loading: false,
    isResopnse: null,
    percentage: null,
    vendor_percentage: null,
    error: {
      status: false,
      message: '',
    },
  },
  saveReviewAnswer: {
    data: null,
  },
  saveReviewExtraDatas:{
    data: null,
  },
  saveWorkDetail: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  saveWorkStatus: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  contractorRate: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
  materialLists: {
    data: null,
    loading: false,
    error: {
      status: false,
      message: '',
    },
  },
};

const taskDetailsReducer = (state = init_state, action) => {
  switch (action.type) {
    case types.UNIT_INSPECTION_DETAILS_START: {
      return {
        ...state,
        taskDetails: {
          ...init_state.taskDetails,
          loading: true,
        },
      };
    }
    case types.UNIT_INSPECTION_DETAILS_SUCCESS: {
      return {
        ...state,
        taskDetails: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.UNIT_INSPECTION_DETAILS_FAIL: {
      return {
        ...state,
        taskDetails: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.UNIT_INSPECTION_DETAILS_MRD_START: {
      return {
        ...state,
        taskDetailsMrd: {
          ...init_state.taskDetailsMrd,
          loading: true,
        },
      };
    }
    case types.UNIT_INSPECTION_DETAILS_MRD_SUCCESS: {
      return {
        ...state,
        taskDetailsMrd: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.UNIT_INSPECTION_DETAILS_MRD_FAIL: {
      return {
        ...state,
        taskDetailsMrd: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.GET_HANDOVER_DETAILS_START: {
      return {
        ...state,
        ownerTaskDetails: {
          ...init_state.ownerTaskDetails,
          loading: true,
        },
      };
    }
    case types.GET_HANDOVER_DETAILS_SUCCESS: {
      return {
        ...state,
        ownerTaskDetails: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.GET_HANDOVER_DETAILS_FAIL: {
      return {
        ...state,
        ownerTaskDetails: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.GET_MATERIAL_START: {
      return {
        ...state,
        materialLists: {
          ...init_state.materialLists,
          loading: true,
        },
      };
    }
    case types.GET_MATERIAL_SUCCESS: {
      return {
        ...state,
        materialLists: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.GET_MATERIAL_FAIL: {
      return {
        ...state,
        materialLists: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.TASK_DATA_START: {
      return {
        ...state,
        taskData: {
          ...state.taskData,
          loading: true,
          endOfData: false,
        },
      };
    }
    case types.TASK_DATA_SUCCESS: {
      const data =
        action.payload.page === 1
          ? action.payload.res.data
          : [...state.taskData.data, ...action.payload.res.data];
      let endOfData = false;
      if (action.payload.res.data?.length === 0) {
        endOfData = true;
      }
      return {
        ...state,
        taskData: {
          data,
          count: action.payload.res.count,
          page: action.payload.page,
          error: {
            status: false,
            message: '',
          },
          loading: false,
          endOfData,
        },
      };
    }
    case types.TASK_DATA_FAIL: {
      return {
        ...state,
        taskData: {
          data: action.payload.data,
          count: null,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.LIST_DAMAGE_START: {
      return {
        ...state,
        listDamage: {
          ...state.listDamage,
          loading: true,
        },
      };
    }
    case types.LIST_DAMAGE_SUCCESS: {
      return {
        ...state,
        listDamage: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.LIST_DAMAGE_FAIL: {
      return {
        ...state,
        listDamage: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.OWNER_LIST_START: {
      return {
        ...state,
        ownerListDamage: {
          ...state.ownerListDamage,
          loading: true,
        },
      };
    }
    case types.OWNER_LIST_SUCCESS: {
      return {
        ...state,
        ownerListDamage: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.OWNER_LIST_FAIL: {
      return {
        ...state,
        ownerListDamage: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.SAVE_LIST_DAMAGE_QUESCTIONS: {
      console.log({action})
      const check =
        state.chklistQuesData[action.payload.idInsBooking] &&
        state.chklistQuesData[action.payload.idInsBooking].answer;
      return check
        ? {
            ...state,
            chklistQuesData: {
              ...state.chklistQuesData,
              [action.payload.idInsBooking]: {
                ...state.chklistQuesData[action.payload.idInsBooking],
              
                answer: {
                  ...state.chklistQuesData[action.payload.idInsBooking].answer,
                  [action.payload.data.details]: {
                    ...state.chklistQuesData[action.payload.idInsBooking]
                      .answer[action.payload.data.details],
                    [action.payload.data.numberOf]: {
                      // ...state.chklistQuesData[action.payload.idInsBooking]
                      //   .answer[action.payload.data.details][
                      //   action.payload.data.numberOf
                      // ],
                      ...action.payload.data.quesction,
                    },
                  },
                },
              },
            },
          }
        : {
            ...state,
            chklistQuesData: {
              ...state.chklistQuesData,
              [action.payload.idInsBooking]: {
                ...state.chklistQuesData[action.payload.idInsBooking],
                answer: {
                  ...action.payload.data,
                },
              },
            },
          };
    }

    case types.SAVE_Y_VALUES: {
      return {
        ...state,
        positionData: {
          ...state.positionData,
          ...action.payload.data,
        },
      };
    }
    case types.SAVE_NEW_MATERIAL_DETAILS: {
      return {
        ...state,
        newMaterialArr: {
          ...state.newMaterialArr,
          [action.payload.id]: [...action.payload.data],
        },
      };
    }
    case types.DELETE_NEW_MATERIAL_DETAILS: {
      const copy = state.newMaterialArr;
      if (copy[action.payload.id]) {
        delete copy[action.payload.id];
      }
      return {
        ...state,
        newMaterialArr: {
          copy,
        },
      };
    } 
  
    case types.SAVE_REVIEW_ANSWERS: {
      const check =
        state.saveReviewAnswer[action.payload.idInsBooking] &&
        state.saveReviewAnswer[action.payload.idInsBooking].answer;
      return check
        ? {
            ...state,
            saveReviewAnswer: {
              ...state.saveReviewAnswer,
              [action.payload.idInsBooking]: {
                ...state.saveReviewAnswer[action.payload.idInsBooking],
                // idInsBooking: action.payload.idInsBooking,
                answer: {
                  ...state.saveReviewAnswer[action.payload.idInsBooking].answer,
                  [action.payload.data.details]: {
                    ...state.saveReviewAnswer[action.payload.data.details],
                    ...action.payload.data.quesction,
                  },
                },
              },
            },
          }
        : {
            ...state,
            saveReviewAnswer: {
              ...state.saveReviewAnswer,
              [action.payload.idInsBooking]: {
                ...state.saveReviewAnswer[action.payload.idInsBooking],
                answer: {
                  ...action.payload.data,
                },
              },
            },
          };
    }
    
    case types.SAVE_REVIEW_EXTRA_DATA: {
      console.log({action})
      const check =
        state.saveReviewExtraDatas[action.payload.idInsBooking] &&
        state.saveReviewExtraDatas[action.payload.idInsBooking].answer;
      return check
        ? {
            ...state,
            saveReviewExtraDatas: {
              ...state.saveReviewExtraDatas,
              [action.payload.idInsBooking]: {
                ...state.saveReviewExtraDatas[action.payload.idInsBooking],
              
                answer: {
                  ...state.saveReviewExtraDatas[action.payload.idInsBooking].answer,
                  [action.payload.data.details]: {
                    ...state.saveReviewExtraDatas[action.payload.idInsBooking]
                      .answer[action.payload.data.details],
                    [action.payload.data.numberOf]: {
                      // ...state.saveReviewExtraDatas[action.payload.idInsBooking]
                      //   .answer[action.payload.data.details][
                      //   action.payload.data.numberOf
                      // ],
                      ...action.payload.data.quesction,
                    },
                  },
                },
              },
            },
          }
        : {
            ...state,
            saveReviewExtraDatas: {
              ...state.saveReviewExtraDatas,
              [action.payload.idInsBooking]: {
                ...state.saveReviewExtraDatas[action.payload.idInsBooking],
                answer: {
                  ...action.payload.data,
                },
              },
            },
          };
    }
    case types.DELETE_REVIEW_EXTRA_DATA: {
      const copy = state.saveReviewExtraDatas[action.payload.idInsBooking]?.answer;
      if(copy?.[action.payload.id][action.payload.data]){
        delete copy?.[action.payload.id][action.payload.data]
      
      }
     
      return {
        ...state,
        saveReviewExtraDatas: {
          ...state.saveReviewExtraDatas,
          [action.payload.idInsBooking]:{
            answer:{
              ...state.saveReviewExtraDatas[action.payload.idInsBooking].answer,
              ...copy
            }
           
          }
        },
      };
    }

    case types.DELETE_WHOLE_MATERIAL_DETAILS: {
      const copy = state.saveReviewExtraDatas;
      if (copy[action.payload.id]) {
        delete copy[action.payload.id];
      }
      return {
        ...state,
        saveReviewExtraDatas: {
          copy,
        },
      };
    } 
    // Material rate card
    case types.RATECARD_DATA_START: {
      return {
        ...state,
        rateCardList: {
          ...state.rateCardList,
          loading: true,
        },
      };
    }

    case types.RATECARD_DATA_SUCCESS: {
      return {
        ...state,
        rateCardList: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.RATECARD_DATA_FAIL: {
      return {
        ...state,
        rateCardList: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    // Inspection History..
    case types.INSPECTION_HISTORY_DATA_START: {
      return {
        ...state,
        inspectionHistory: {
          ...init_state.inspectionHistory,
          loading: true,
        },
      };
    }

    case types.INSPECTION_HISTORY_DATA_SUCCESS: {
      return {
        ...state,
        inspectionHistory: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.INSPECTION_HISTORY_DATA_FAIL: {
      return {
        ...state,
        inspectionHistory: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    // task logs
    case types.GET_TASK_LOGS_START: {
      return {
        ...state,
        taskLogs: {
          ...init_state.taskLogs,
          loading: true,
        },
      };
    }

    case types.GET_TASK_LOGS_SUCCESS: {
      return {
        ...state,
        taskLogs: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.GET_TASK_LOGS_FAIL: {
      return {
        ...state,
        taskLogs: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    } 
    // item Work 
    case types.GET_ITEM_WORK_START: {
      return {
        ...state,
        itemWork: {
          ...init_state.itemWork,
          loading: true,
        },
      };
    }

    case types.GET_ITEM_WORK_SUCCESS: {
      return {
        ...state,
        itemWork: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.GET_ITEM_WORK_FAIL: {
      return {
        ...state,
        itemWork: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.GET_WORK_DETAILS_START: {
      return {
        ...state,
        workStatus: {
          ...init_state.workStatus,
          loading: true,
        },
      };
    }

    case types.GET_WORK_DETAILS_SUCCESS: {
      return {
        ...state,
        workStatus: {
          data: action.payload.data,
          isResopnse: action.payload.isResopnse,
          percentage: action.payload.percentage,
          vendor_percentage: action.payload.vendor_percentage,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.GET_WORK_DETAILS_FAIL: {
      return {
        ...state,
        workStatus: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    // save work detail
    case types.SAVE_WORK_DETAILS_START: {
      return {
        ...state,
        saveWorkDetail: {
          ...init_state.saveWorkDetail,
          loading: true,
        },
      };
    }

    case types.SAVE_WORK_DETAILS_SUCCESS: {
      return {
        ...state,
        saveWorkDetail: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.SAVE_WORK_DETAILS_FAIL: {
      return {
        ...state,
        saveWorkDetail: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    } 
    case types.SAVE_WORK_STATUS_START: {
      return {
        ...state,
        saveWorkStatus: {
          ...init_state.saveWorkStatus,
          loading: true,
        },
      };
    }

    case types.SAVE_WORK_STATUS_SUCCESS: {
      return {
        ...state,
        saveWorkStatus: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }

    case types.SAVE_WORK_STATUS_FAIL: {
      return {
        ...state,
        saveWorkStatus: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }

    // check list quesctions
    case types.SAVE_DAMAGE_INSPECTION_START: {
      return {
        ...state,
        saveDamage: {
          ...init_state.saveDamage,
          loading: true,
          error: {
            status: false,
            message: '',
          },
        },
      };
    }
    case types.SAVE_DAMAGE_INSPECTION_SUCCESS: {
      return {
        ...state,
        saveDamage: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.SAVE_DAMAGE_INSPECTION_FAIL: {
      return {
        ...state,
        saveDamage: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    // Key not collected
    case types.KEY_NOT_COLLECTED_START: {
      return {
        ...state,
        keyNotCollected: {
          ...init_state.keyNotCollected,
          loading: true,
        },
      };
    }
    case types.KEY_NOT_COLLECTED_SUCCESS: {
      return {
        ...state,
        keyNotCollected: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.KEY_NOT_COLLECTED_FAIL: {
      return {
        ...state,
        keyNotCollected: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.DELETE_COMPLETED_INSPECTION: {
      return {
        ...state,
        chklistQuesData: {
          ...action.payload.data,
        },
      };
    }
    case types.DELETE_COMPLETED_REVIEW_ANS: {
      return {
        ...state,
        saveReviewAnswer: {
          ...action.payload.data,
        },
      };
    }
    // vendor rate card
    case types.LIST_CONTRACTOR_RATE_START: {
      return {
        ...state,
        contractorRate: {
          ...init_state.contractorRate,
          loading: true,
        },
      };
    }
    case types.LIST_CONTRACTOR_RATE_SUCCESS: {
      return {
        ...state,
        contractorRate: {
          data: action.payload.data,
          error: {
            status: false,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.LIST_CONTRACTOR_RATE_FAIL: {
      return {
        ...state,
        contractorRate: {
          data: action.payload.data,
          error: {
            status: true,
            message: '',
          },
          loading: false,
        },
      };
    }
    case types.LOGOUT: {
      return {
        ...init_state,
      };
    }
  }
  return state;
};

export default taskDetailsReducer;
