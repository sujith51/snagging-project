import React from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import TaskStatus from '../components/TaskStatus';
import CircularGraph from '../components/ui-elements/CircularGraph';
import { getAppFont } from '../utils';

const MoveinInspection = (props) => {
  const { selectCommunity } = useSelector((state) => state.user);
  const {
    onTabPress = () => {},
    inspStatus = [],
    selectedId,
    graphValue,
    totalFill,
    completedData,
    totalCount = 0,
    value = 0,
  } = props;
  return (
    <View style={styles.container}>
      <View style={styles.tabStatus}>
        {inspStatus.map((item, index) => {
          return (
            <TaskStatus
              id={item}
              handleOnSelectPress={onTabPress}
              inspStatusColor={item.color}
              inspTextColor={item.textColor}
              selectedId={selectedId === item.id}
              key={index}
              inspTitle={item.title}
            />
          );
        })}
      </View>
      {selectCommunity?.length > 0 && totalCount > 0 && (
        <View style={styles.chartView}>
          <CircularGraph
            selectedId={selectedId}
            graphColor={graphValue}
            totalFill={null}
            completedData={null}
            totalCount={totalCount}
            value={value}
          />
        </View>
      )}
      {(selectCommunity?.length === 0 || totalCount === 0) && (
        <View style={styles.nodatas}>
          <Text style={styles.txtStyl}>No Data Found</Text>
        </View>
      )}
    </View>
  );
};

export default MoveinInspection;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },
  tabStatus: {
    flexDirection: 'row',
  },
  chartView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 180,
  },
  txtStyl: {
    ...getAppFont('Bold'),
    fontSize: Platform.isPad ? 18 : 14,
  },
  nodatas: {
    alignItems: 'center',
    paddingTop: 50,
  },
});
