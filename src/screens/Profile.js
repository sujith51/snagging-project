/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { CommonContainer, SignoutPopup } from '_components';
import { logOut } from '_actions';
import { NotificationIcon } from '_svg';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { getAppFont, useTheme } from '../utils';

const Profile = () => {
  const theme = useTheme();
  const { navigate } = useNavigation();
  const dispatch = useDispatch();

  // :states
  const [modalVisible, setModalVisible] = useState(false);

  const { data: userData } = useSelector((state) => state.user);

  const containerProps = {
    navbar: {
      type: 'outer',
      title: '   ',
      actions: [
        {
          icon: <NotificationIcon />,
          onPress: () => navigate('Notifications'),
        },
      ],
    },
    scrollEnabled: true,
    onModalPress: () => {
      // onRefresh();
    },
  };
  const logOutClicked = () => {
    setModalVisible(true);
  };

  const yesClicked = () => {
    setModalVisible(false);
    navigate('log');
    dispatch(logOut());
  };

  return (
    <CommonContainer {...containerProps}>
      {userData && (
        <View
          style={[styles.container, { backgroundColor: theme.color.primary }]}
        >
          <View style={styles.subCont}>
            <Image
              source={{ uri: userData.user_icon }}
              style={[
                styles.imgCont,
                {
                  borderColor: theme.color.pink,
                  backgroundColor: theme.color.darkGray2,
                },
              ]}
            />
          </View>
          <View style={styles.fullNameCont}>
            <Text
              style={{
                ...getAppFont('Bold'),
                color: theme.color.primaryWhite,
                fontSize: 30,
              }}
            >
              {`${userData.first_name}`}
            </Text>
          </View>

          {/* <View style={styles.roleCont}>
            <Text
              style={{
                ...getAppFont('Regular'),
                color: theme.color.primaryWhite,
                fontSize: 14,
              }}
            >
              {userData.id_role}
            </Text>
          </View> */}

          <View style={styles.profileDataCont}>
            <View style={styles.eachRow}>
              <Text style={[styles.title, { color: theme.color.primaryWhite }]}>
                Name
              </Text>
              <Text style={[styles.desc, { color: theme.color.primaryWhite }]}>
                {userData.first_name}
              </Text>
              <View
                style={{ borderWidth: 0.6, borderColor: theme.color.darkGray2 }}
              />
            </View>

            <View style={styles.eachRow}>
              <Text style={[styles.title, { color: theme.color.primaryWhite }]}>
                Designation
              </Text>
              <Text style={[styles.desc, { color: theme.color.primaryWhite }]}>
                {userData.role_name}
              </Text>
              <View
                style={{ borderWidth: 1, borderColor: theme.color.darkGray2 }}
              />
            </View>

            <View style={styles.eachRow}>
              <Text style={[styles.title, { color: theme.color.primaryWhite }]}>
                Phone Number
              </Text>
              <Text style={[styles.desc, { color: theme.color.primaryWhite }]}>
                {userData.phone !== '' ? userData.phone : 'N/A'}
              </Text>
              <View
                style={{ borderWidth: 1, borderColor: theme.color.darkGray2 }}
              />
            </View>

            <View style={styles.eachRow}>
              <Text style={[styles.title, { color: theme.color.primaryWhite }]}>
                Email Address
              </Text>
              <Text style={[styles.desc, { color: theme.color.primaryWhite }]}>
                {userData.email}
              </Text>
              <View
                style={{ borderWidth: 1, borderColor: theme.color.darkGray2 }}
              />
            </View>

            <View style={{ paddingBottom: 100 }}>
              <TouchableWithoutFeedback onPress={logOutClicked}>
                <Text
                  style={[styles.desc, { color: theme.color.primaryWhite }]}
                >
                  Logout
                </Text>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </View>
      )}
      {modalVisible && (
        <SignoutPopup
          visible={modalVisible}
          title="Sign Out"
          desc="Are you sure you wish to sign out?"
          yes={() => yesClicked()}
          no={() => setModalVisible(false)}
          close={() => setModalVisible(false)}
        />
      )}
    </CommonContainer>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subCont: {
    alignItems: 'center',
    marginTop: 50,
  },
  imgCont: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    borderWidth: 1,
  },
  fullNameCont: {
    alignItems: 'center',
    paddingTop: 15,
  },
  roleCont: {
    alignItems: 'center',
    paddingTop: 10,
  },

  profileDataCont: {
    marginLeft: 44,
    marginRight: 44,
    paddingTop: 30,
  },
  eachRow: {
    paddingBottom: 31,
    paddingVertical: 4,
  },
  title: {
    fontSize: 14,
    ...getAppFont('Regular'),
  },
  desc: {
    fontSize: 21,
    ...getAppFont('Bold'),
  },
});
