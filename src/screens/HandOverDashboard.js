/* eslint-disable no-nested-ternary */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-shadow */
/* eslint-disable camelcase */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable import/no-unresolved */
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ActivityIndicator, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { SearchIcon, NotificationIcon } from '_svg';

import {
  CommonContainer,
  DropDown,
  OwnerCard,
  TaskList,
  Text,
  BarGraph,
  Spinner,
  SelectCommunity,
} from '_components';
import { useRoute, useNavigation } from '@react-navigation/native';
import {
  getBarGraphData,
  getOwnerDashboardListing,
  getHandoverDetails,
  getCommunityList,
} from '_actions';
import { usePrevious } from '_hooks';
import MoveinInspection from './MoveinInspection';
import {
  useTheme,
  getAppFont,
  getColor,
  theme as themeIn,
  returnWithoutStatic,
} from '../utils';
import { cos } from 'react-native-reanimated';


const DropDownValues = [
  { value: 'Weekly', label: 'Weekly' },
  { value: 'Monthly', label: 'Monthly' },
  { value: 'Yearly', label: 'Yearly' },
];

const statusValues = [
  { value: 0, label: 'Pending' },
  { value: 2, label: 'Completed' },
  // { value: 3, label: 'Review' },
];
const all_inpection = 'All Inspection';
const DropDownInspection = [
  { value: 'All', label: all_inpection },
  { value: 'move_in_inspection', label: 'Move-in Inspection' },
  { value: 'move_out_inspection', label: 'Move-out Inspection' },
  // { value: 'makeready_inspection', label: 'Make Ready Inspection' },
  { value: 'vacant', label: 'Vacant Inspection' },
];

const tabStatus = [
  {
    title: 'Completed',
    id: 1,
    id_tab: 'completed',
    color: themeIn.color.primary,
    textColor: themeIn.color.primaryWhite,
  },
  {
    title: 'In Progress',
    id: 2,
    id_tab: 'in_progress',
    color: themeIn.color.secondary,
    textColor: themeIn.color.primaryWhite,
  },
  {
    title: 'Upcoming',
    id: 3,
    id_tab: 'upcoming',
    color: themeIn.color.lime,
    textColor: themeIn.color.primary,
  },
];

const HandOverDashboard = () => {
  // :States
  const theme = useTheme();
  const dispatch = useDispatch();
  const [duration, setDuration] = useState('Weekly');
  const [status, setStatus] = useState(0);
  const [inspectionType, setInspectionType] = useState('All');
  const [selected, setSelected] = useState(3);
  const [selectedTab, setSelectedTab] = useState('upcoming');
  const [tabTitle, setTabTitle] = useState('Owner Inspection Snagging ');
  const [communities, setCommunities] = useState([]);
  const [units, setUnits] = useState([]);
  // used to send as props to selectcommunity to load or not
  const [loadAllData, setLoadAllData] = useState(true);
  const [reloadBar, setReloadBar] = useState(true);
  // used to load if we come to dashboard or come from tasks
  const [loadOnlyFirstTime, setLoadOnlyFirstTime] = useState(true);
  const [fromTaskList, setFromTaskList] = useState(false);
  const [statusTitle, setStatusTitle] = useState(statusValues[0].label);
  // new api changes


  const { navigate } = useNavigation();

  const { params } = useRoute();
  const loaded = params?.loaded ?? false;
  const [graphValue, setGraphValue] = useState({
    color: theme.color.lime,
    textColor: theme.color.primary,
  });
  const {
    ownerDashboard: {
      data: dashboardData,
      count: dataCount,
      loading: dashboardLoading,
    },
    barGraphData: { data: barData, loading: barLoading },
    circularGraphData: { data: circularData, loading: circiularLoading },
  } = useSelector((state) => state.dashboard);
  const {
    ownerTaskDetails: {
      data: taskDetailsData,
      loading: taskDetailsloading,
      error: { status: taskDetailsError },
    },
    saveWorkStatus: {
      data: saveDamageData,
      loading: saveDamageloading,
      error: {status: saveDamageError},
    },
  } = useSelector((state) => state.taskdetails);

  const {
    data,
    selectCommunity = [],
    selectUnit,
    communityList,
    // communityList: {
    //   data: communityListData,
    //   loading: communityListloading,
    //   error,
    // },
  } = useSelector((state) => state.user);

  const prevtaskDetailsloading = usePrevious(taskDetailsloading);

  const finalBarGraph =
    barData?.length > 0
      ? barData.map((item) => ({
          ...item,
          // eslint-disable-next-line radix
          value: parseInt(item.value),
          svg: { fill: getColor(item.id) },
        }))
      : [];
  // :Effects.

  useEffect(() => {
    if (data?.id_user && data?.id_user_type) {
      dispatch(
        getCommunityList({
          id_user: data?.id_user,
          id_user_type: data?.id_user_type,
        }),
      );
    }
  }, [data?.id_user]);

  useEffect(() => {
    const commun = [];
    if (communityList?.data) {
      Object.keys(communityList?.data).forEach((key) =>
        commun.push({
          value: key,
          label: communityList?.data[key],
        }),
      );
      setCommunities(commun);
    }
  }, [communityList?.data]);

  useEffect(() => {
    if (saveDamageData) {
      refreshClicked()
    }
  }, [saveDamageData]);

  useEffect(() => {
    const allCommunity =
      communityList?.data && Object.keys(communityList?.data);
    if (
      allCommunity &&
      selectCommunity?.length === 0 &&
      (loadOnlyFirstTime || fromTaskList)
    ) {
      // load in selectcommunity
      setLoadAllData(true);
      setLoadOnlyFirstTime(false);
      setFromTaskList(false);
      callDashboardListing()
    }
    if (selectCommunity?.length > 0) {
      setLoadAllData(false);
      callDashboardListing()
     
    }
   
  }, [
    inspectionType,
    selectCommunity,
    selectUnit,
    selectedTab,
    communityList,
    status,
    fromTaskList,

  ]);

  useEffect(() => {
    if (reloadBar) {
      if (inspectionType === 'All') {
        dispatch(getBarGraphData({ duration, id_user: data?.id_user }));
        callDashboardListing()
       
        setReloadBar(false);
      } else {
        setSelected(1);
        setSelectedTab('completed');
      
        setReloadBar(false);
      }
    }
  }, [duration, reloadBar]);

  useEffect(() => {
    if (
      prevtaskDetailsloading === true &&
      taskDetailsloading === false &&
      taskDetailsData?.length > 0
    ) {
      navigate('HandOverDetails', {
        taskDetail: taskDetailsData,
       
      });
    } else if (
      (prevtaskDetailsloading === true &&
        taskDetailsloading === false &&
        taskDetailsData?.length === 0) ||
      taskDetailsError
    ) {
      Alert.alert('Something went wrong!');
    }
  }, [taskDetailsData, taskDetailsloading, taskDetailsError]);

  const handleDropDown = (data) => {
    if (data?.value) {
      setDuration(data.value);
      setReloadBar(true);
    }
  };

  const handleDropDownStatus = (data) => {
    // if (data?.value) {
    setStatus(data.value);
    setStatusTitle(data.label);
   

    // }
  };

  const handleDropDownInspections = (data) => {
    if (data?.value) {
      setInspectionType(data.value);
      setReloadBar(data.value === 'All');
      // if (data.label === all_inpection) {
      //   setTabTitle('Move In/ Move Out/ Vacant Inspection');
      // } else {
      //   setTabTitle(data.label);
      // }
    }
  };

  const taskClicked = (_item) => {
    // console.log('item',_item)
    const data = {
        id_snag_property: _item?.id_snag_property,

    };
    dispatch(getHandoverDetails(data));
  };
  const callDashboardListing=()=>{
    dispatch(
        getOwnerDashboardListing({
        id_user: data?.id_user,
        page: 1,
        id_community: returnWithoutStatic(selectCommunity),         
      }),
    )
  }

  const taskRender = ({ item, index }) => {
    return (
      <View>
        <OwnerCard
          {...item}
         
          index={index}
          onPress={() => taskClicked(item)}
          cardStyle={[styles.cardStyle, { padding: theme.spacing.sm }]}
        />
      </View>
    );
  };

  const listHeader = () => {
    return (
      <View style={styles.listHeadCont}>
        <Text variant="taskHeading" color="primary" style={{marginTop:10}}>
          {tabTitle}
        </Text>
      </View>
    );
  };

  // Refresh Added+++
  const refreshClicked = () => {
  
    if (selectCommunity?.length > 0) {
      callDashboardListing()
    }
   
  };

  const containerProps = {
    navbar: {
      type: 'outer',
      title: 'Dashboard',
      actions: [
        // {
        //   icon: <SearchIcon />,
        //   onPress: () => navigate('Search'),
        // },
        {
          icon: <NotificationIcon />,
          onPress: () => navigate('Notifications'),
        },
      ],
    },
    style: { backgroundColor: theme.color.secondaryLight },
    scrollEnabled: true,
    // style: { flex: 1 },
    onModalPress: () => {
      // onRefresh();
    },
    onRefresh: () => {
      // alert("refresh called")
      refreshClicked();
    },
  };

  const onSelectPress = (item) => {
    setSelected(item.id);
    setSelectedTab(item.id_tab);
    // setTabTitle(item.title);
    setGraphValue({ color: item.color, textColor: item.textColor });
  };

  return (
    <CommonContainer {...containerProps}>
      <View
        style={[
          styles.container,
          { backgroundColor: theme.color.secondaryLight },
        ]}
      >
        <View
          // style={{
          //   margin: 10,
          //   shadowColor: '#000',
          //   shadowOffset: {
          //     width: 0,
          //     height: 2,
          //   },
          //   shadowOpacity: 0.2,
          //   shadowRadius: 20,
          //   elevation: 3,
          //   backgroundColor: theme.color.primaryWhite,
          //   borderRadius: 20,
          //   paddingBottom:20
          // }}
        >
          {communities?.length > 0 && (
            <SelectCommunity
              units={units}
              communities={communities}
              loadInit={loadAllData}
            />
          )}
          {/* <View style={styles.dropDownCont}>
            <DropDown
              value={DropDownInspection[0].label}
              count={dataCount}
              options={DropDownInspection}
              placeHolderStyle={{
                color: theme.color.primary,
                ...getAppFont('Bold'),
              }}
              onClose={handleDropDownInspections}
              communityExists={selectCommunity?.length > 0}
            /> 
           <DropDown
              value={statusTitle}
              options={statusValues}
              placeHolderStyle={{
                color: theme.color.primary,
                ...getAppFont('Bold'),
              }}
              onClose={handleDropDownStatus}
            /> 
            <DropDown
              value={DropDownValues[0].label}
              options={DropDownValues}
              placeHolderStyle={{
                color: theme.color.primary,
                ...getAppFont('Bold'),
              }}
              onClose={handleDropDown}
            />
          </View> */}
          {/* <View style={{ paddingHorizontal: 20 }}>
            {
              inspectionType === 'All' ? (
                barLoading ? (
                  <ActivityIndicator style={{ height: 100 }} />
                ) : (
                  barData?.length > 0 && <View style={{ height: 50 }} />
                )
              ) : circiularLoading ? (
                <ActivityIndicator style={{ height: 50 }} />
              ) : (
                <View style={{ height: 50 }} />
              )
              (
              dashboardData &&
              circularData && (
                <View style={{ flex: 1, paddingBottom: 200 }}>
                  <MoveinInspection
                    onTabPress={onSelectPress}
                    inspStatus={tabStatus}
                    selectedId={selected}
                    graphValue={graphValue}
                    totalCount={dataCount}
                    value={
                      circularData?.length > 0
                        ? circularData.filter(
                            (item) => item.id_tab === selectedTab,
                          )[0]?.value
                        : 0
                    }
                  />
                </View>
              )
            )
            }
          </View> */}
        </View>

        <TaskList
          data={dashboardData || []}
          renderItem={taskRender}
          listHeaderComponent={listHeader}
          loading={dashboardLoading}
        />
        <Spinner
          visible={taskDetailsloading || dashboardLoading}
          size="large"
          animationType="none"
        />
      </View>
    </CommonContainer>
  );
};

export default HandOverDashboard;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingHorizontal:10
  },
  listHeadCont: {
    paddingLeft: 20,
    paddingBottom: 20,
  },
  dropDownCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 22,
    paddingLeft: 22,
  },
  cardStyle: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    borderRadius: 30,
  },
});
