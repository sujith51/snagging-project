import React, {useState} from 'react';
import { View, Text } from 'react-native';
import { CommonContainer, DropDownCheckQty } from '_components';
import { getAppFont, useTheme } from '../utils';

const Quotations = () =>{
    const theme = useTheme();
    return (
        <CommonContainer navbar={{ title: 'Quotations' }}>
            <Text>
                Quotations screen
            </Text>
        </CommonContainer>
    )
}

export default Quotations;