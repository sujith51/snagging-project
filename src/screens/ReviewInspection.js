import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList, Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useRoute, useNavigation} from '@react-navigation/native';
import {
  CommonContainer,
  DropDown,
  Spinner,
  Text,
  InputText,
  ValidationReview,
  SuccessModal,
} from '_components';
import {
  getWorkDetails,
  getListDamage,
  saveReviewAnswers,
  saveWorkDetails,
  deleteReviewAnswers,
  getReviewVendor,
  getItemWork,
  saveExtraReviewData,
  deleteWholeReviewData,
} from '_actions';
import {usePrevious} from '_hooks';
import {useTheme, getAppFont} from '../utils';
import Button from '../components/Button';
import ImageUpload from '../components/ui-elements/ImageUpload';
import {ReviewAccordian} from '_components';

const ReviewInspection = () => {
  const dispatch = useDispatch();
  const {params} = useRoute();
  const theme = useTheme();
  const {navigate} = useNavigation();
  const {idIns, id_property_quotation = 0} = params;
  const additionalCommentId = 205;
  const {
    workStatus,
    // listDamage,
    saveReviewAnswer,
    saveReviewExtraDatas,
    saveWorkDetail,
    itemWork,
  } = useSelector(state => state.taskdetails);

  const {data: userData} = useSelector(state => state.user);
  const {
    reviewVendorList: {
      data: reviewVendorListData,
      loading: reviewVendorListLoading,
      error: {status: reviewVendorListError},
    },
  } = useSelector(state => state.dashboard);
  const {
    data: workStatusData,
    loading: workStatusLoading,
    error: workStatusError,
    vendor_percentage,
  } = workStatus;
  const {
    data: itemWorkData,
    loading: itemWorkLoading,
    error: itemWorkError,
  } = itemWork;
  const {data: saveWorkDetailData, loading: saveWorkDetailLoading} =
    saveWorkDetail;
  const prevsaveWorkDetailloading = usePrevious(saveWorkDetailLoading);
  // const { loading: contractLoading } = listDamage;
  const [selectVendor, setSelectVendor] = useState(null);
  const [validErr, setValidErr] = useState(false);
  const [success, setSuccess] = useState(false);
  const [isDataLoaded, setIsDataLoaded] = useState(false);
  const [validErrData, setValidErrData] = useState(null);
  const [primaryVendor, setPrimaryVendor] = useState([]);
  const [reload, setReload] = useState(false);
  const answerType = ['Yes', 'No'];
  const checjlistObj = saveReviewAnswer[idIns];

  const pendingQuesctions = itemWorkData ?? [];
  let count = saveReviewExtraDatas[idIns]
    ? saveReviewExtraDatas[idIns]?.answer?.sub_categery?.additional?.data
    : null;
  console.log({count});
  useEffect(() => {
    if (idIns) {
      dispatch(
        getItemWork({
          id_snag_property: idIns,
        }),
      );
    }
  }, []);
  useEffect(() => {
    if (idIns && reload) {
      // const first = saveWorkDetailData?.work_staus_details;
      dispatch(deleteWholeReviewData(idIns));
      // if (first == 1) {
      //   navigate('Tasks', { loaded: true });
      // } else {
      //   dispatch(
      //     getReviewVendor({
      //       id_snag_property: idIns,
      //       id_kh_user: userData.id_user,
      //     }),
      //   );
      // }
      navigate('Tasks', {loaded: true});
      setReload(false);
    }
  }, [reload]);
  useEffect(() => {
    if (!saveReviewExtraDatas[idIns]) {
      const data = {
        // vendor: selectVendor?.value,
        id_snag_property: idIns,
      };
      dispatch(saveExtraReviewData(idIns, data));
    }
  }, [dispatch, idIns]);
  useEffect(() => {
    if (!count) {
      console.log('here');
      const saveRes = {
        details: 'sub_categery',
        numberOf: 'additional',
        quesction: {
          data: [],
        },
      };
      dispatch(saveExtraReviewData(idIns, saveRes));
    }
  }, [count]);
  useEffect(() => {
    if (
      saveWorkDetailData &&
      prevsaveWorkDetailloading === true &&
      saveWorkDetailLoading === false
    ) {
      setSuccess(true);
    }
  }, [
    dispatch,
    prevsaveWorkDetailloading,
    saveWorkDetailData,
    saveWorkDetailLoading,
  ]);
  const incrementClicked = () => {
    const saveRes = {
      details: 'sub_categery',
      numberOf: 'additional',
      quesction: {
        data: [
          ...saveReviewAnswer[idIns]?.sub_categery['additional']?.data,
          saveReviewAnswer[idIns]?.sub_categery['additional']?.data.length + 1,
        ],
      },
    };
    console.log({saveRes});
    // dispatch(saveReviewAnswers(idIns, saveRes));
  };

  const changeValue = (ques, ans) => {
    if (ans === 'No') {
      const saveRes = {
        details: ques,
        quesction: {
          damage: 'No',
        },
      };
      dispatch(saveReviewAnswers(idIns, saveRes));
    } else {
      const saveRes = {
        details: ques,
        quesction: {
          damage: 'Yes',
        },
      };
      dispatch(saveReviewAnswers(idIns, saveRes));
    }
  };

  const handleAnswers = (ques, key, value) => {
    const idTemplateMapping = checjlistObj.answer[ques];
    if (idTemplateMapping) {
      const saveRes = {
        details: ques,
        quesction: {
          ...idTemplateMapping,
          [key]: value,
        },
      };
      dispatch(saveReviewAnswers(idIns, saveRes));
    }
  };

  const validErrModal = () => {
    setValidErr(false);
  };
  const renderItem = ({item}) => {
    return (
      <>
        <View style={styles.wrapper}>
        <View style={[styles.borderBottom]}>
        <Text variant="taskBold" size={20} style={styles.quesSty} color='primary'>
            {item?.id_template_mapping === '205'
              ? item?.additional_comment
              : item?.question}
            *
          </Text>
        </View>
          
          {item?.damage_image ? (
            <View style={[styles.borderBottom,{paddingBottom: 15}]}>
              <Text variant="bodyLight" size={16} style={{paddingBottom: 10}}>
                Damage Image -
              </Text>
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                <Image
                  style={styles.tinyLogo}
                  source={{
                    uri: item.damage_image,
                  }}
                />
                {item?.repaired_image && (
                  <Image
                    style={styles.tinyLogo}
                    source={{
                      uri: item.repaired_image,
                    }}
                  />
                )}
              </View>
            </View>
          ) : null}
          {item?.inspection_images?.length > 0 && (
            <>
              {/* <View style={{marginTop:10,borderBorderWidth:1,orderBorderColor:'red',width:100}}/> */}
              <Text variant="bodyLight" size={16} style={{paddingBottom: 10}}>
                Contractor Repair Image -
              </Text>
            </>
          )}
          {item?.inspection_images?.length > 0 &&
            item.inspection_images.map(image => {
              return (
                <>
                  <Text variant="taskBold" size={14} style={styles.quesSty}>
                    {image?.contractor_remark}
                  </Text>
                  <Image
                    style={styles.tinyLogo}
                    source={{
                      uri: image?.contractor_image,
                    }}
                  />
                </>
              );
            })}
          {item?.description ? (
            <View style={[styles.borderBottom,{paddingBottom: 15}]}>
           
              <Text variant="bodyLight" size={14} style={{paddingBottom: 10}}>
                Description -
              </Text>
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                <Text variant="bodyLight" size={14} style={{paddingBottom: 10}}>
                  {item?.description}
                </Text>
              </View>
            </View>
          ) : null}

          <View style={styles.btRow}>
            {answerType.map(btTxt => {
              return (
                <Button
                  key={btTxt}
                  title={btTxt}
                  type={
                    (checjlistObj?.answer[item.snag_inspection_id] &&
                      checjlistObj.answer[item.snag_inspection_id]?.damage ===
                        btTxt &&
                      checjlistObj.answer[item.snag_inspection_id]?.damage) ||
                    'default'
                  }
                  style={styles.button1}
                  titleStyle={styles.appFontReg}
                  onPress={() => changeValue(item.snag_inspection_id, btTxt)}
                />
              );
            })}
          </View>
          {checjlistObj?.answer[item.snag_inspection_id] && (
            <View>
              <ImageUpload
                onImagesUpdate={img =>
                  img && handleAnswers(item.snag_inspection_id, 'image', img)
                }
                preImages={
                  checjlistObj?.answer[item.snag_inspection_id] &&
                  checjlistObj.answer[item.snag_inspection_id].image
                }
                style={{marginTop: -5}}
                renderImage={
                  checjlistObj?.answer[item.snag_inspection_id] &&
                  checjlistObj.answer[item.snag_inspection_id].image
                }
                multiple
              />
              <View>
                <InputText
                  placeholder="Comment"
                  onChangeText={text =>
                    handleAnswers(item.snag_inspection_id, 'description', text)
                  }
                  value={
                    checjlistObj?.answer[item.snag_inspection_id] &&
                    checjlistObj.answer[item.snag_inspection_id].description
                  }
                  style={[
                    styles.commentbox,
                    {
                      borderColor: theme.color.secondaryLight2,
                      padding: theme.spacing.sm,
                      borderRadius: theme.borderRadius.x,
                      backgroundColor: theme.color.primaryWhite,
                    },
                  ]}
                  textFieldStyle={styles.txtFieldStyle}
                  blurOnSubmit={false}
                  multiline
                  numberOfLines={2}
                  textFieldContainerStyle={styles.txtfldcontainstyle}
                />
              </View>
            </View>
          )}
        </View>
      </>
    );
  };
  const containerProps = {
    navbar: {
      title: 'Review',
    },
    style: {backgroundColor: theme.color.secondaryLight},
    onModalPress: () => {
      // onRefresh();
    },
  };
  const returnAddtionalComment = () => {
    const addEmptyArr = [];
    const additionalDatas =
      saveReviewExtraDatas[idIns]?.answer?.sub_categery?.additional?.data;
    if (additionalDatas?.length > 0) {
      saveReviewExtraDatas[idIns]?.answer?.sub_categery?.additional?.data?.map(
        sub => {
          // console.log(saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId][sub])
          if (
            saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId]?.[sub]
              ?.damage == 0
          ) {
            let {comment, image, taskType} =
              saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId][sub];
            if (comment === null || image == null || taskType == 0) {
              addEmptyArr.push(`Additional Comment ${sub}`);
            }
          }
        },
      );
    }
    return addEmptyArr;
  };
  const handleSubmit = () => {
    const isValid = pendingQuesctions.map(item => {
      if (
        idIns in saveReviewAnswer &&
        item.snag_inspection_id in saveReviewAnswer[idIns]?.answer
      ) {
        return true;
      }
      return item?.id_template_mapping === '205'
        ? item?.additional_comment
        : item?.question;
    });

    const addEmptyArr = returnAddtionalComment();
    const combineArr = [...isValid, ...addEmptyArr];
    // console.log('add',combineArr)
    const queErr = combineArr.filter(f => f !== true);
    console.log('first', {isValid, queErr});
    if (queErr.length > 0) {
      setValidErrData(queErr);
      setValidErr(true);
    } else {
      const isValidField = pendingQuesctions
        .map(item => {
          const check = ['damage', 'image'];
          const all = check.map(va => {
            return va in
              saveReviewAnswer[idIns]?.answer[item.snag_inspection_id]
              ? true
              : item?.id_template_mapping === '205'
              ? item?.additional_comment
              : item?.question;
          });
          return all;
        })
        .flat();
      console.log('isValidField', isValidField);
      const addEmptyArr = returnAddtionalComment();
      const combineArr = [...isValidField, ...addEmptyArr];
      const allfielderr = combineArr.filter(f => f !== true);
      if (allfielderr.length > 0) {
        setValidErrData(allfielderr);
        setValidErr(true);
      } else {
        const data = {};
        data.reviewData = saveReviewAnswer[idIns];
        data.id_user = userData.id_user;

        data.id_snag_property = idIns;
        if (
          saveReviewExtraDatas[idIns]?.answer &&
          saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId] &&
          Object.keys(
            saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId],
          )?.length > 0
        ) {
          // const addDatas=saveReviewExtraDatas[idIns]?.answer?.sub_categery?.additional?.data;
          // let obj={};
          // if(addDatas?.length>0){
          //   saveReviewExtraDatas[idIns]?.answer?.sub_categery?.additional?.data?.map(sub=>{
          //     // console.log(saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId][sub])
          //     if(saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId]?.[sub]?.damage==0){
          //      console.log(saveReviewExtraDatas[idIns]?.answer?.[additionalCommentId]?.[sub])
          //     }
          //   })
          // }
          data.questionData = saveReviewExtraDatas[idIns];
        }

        // data.vendor = selectVendor.value;
        // data.pay_for = selectVendor?.tenant ? 4 : 3;
        // data.po_number = selectVendor?.label;
        // if (selectVendor?.value == 3) {
        //   data.owner_inspection = 3;
        // }
        dispatch(saveWorkDetails(data));
      }
    }
  };

  const handleSuccess = () => {
    setSuccess(false);
    // navigate('Dashboard');
    setReload(true);
    setSelectVendor(null);
    setValidErr(false);
    setValidErrData(null);
    setPrimaryVendor([]);

    const updatedData = saveReviewAnswer;
    delete updatedData[idIns];
    dispatch(deleteReviewAnswers(updatedData));
  };
  return (
    <CommonContainer {...containerProps}>
      <View style={styles.container}>
        <View style={{paddingTop: 15}}>
          <FlatList
            data={pendingQuesctions}
            key={item => item.snag_inspection_id}
            renderItem={renderItem}
          />
          {pendingQuesctions?.length > 0 && (
            <>
              <ReviewAccordian count={count} idIns={idIns} />
              <View style={styles.subBtn}>
                <Button
                  title="Submit"
                  style={styles.subBtntxt}
                  titleStyle={styles.subtletxt}
                  onPress={() => handleSubmit()}
                />
              </View>
            </>
          )}
        </View>

        <Spinner
          visible={workStatusLoading || reviewVendorListLoading}
          size="large"
          animationType="none"
        />
        <Spinner
          visible={saveWorkDetailLoading}
          size="large"
          animationType="none"
        />
        <ValidationReview
          open={validErr}
          close={() => validErrModal()}
          title="Please answer all the questions"
          data={validErrData}
        />
        <SuccessModal
          open={success}
          close={() => handleSuccess()}
          title={'Inspection Review Successfully Submitted'}
          link={
            saveWorkDetailData?.wcr_report !== ''
              ? saveWorkDetailData?.wcr_report
              : null
          }
        />
      </View>
    </CommonContainer>
  );
};

export default ReviewInspection;

const styles = StyleSheet.create({
  dropDownContainer: {
    justifyContent: 'space-between',
    left: 0,
    height: 46,
    borderRadius: 10,
  },
  container: {
    padding: 15,
    flex: 1,
  },
  pad: {
    paddingBottom: 5,
  },
  button1: {
    width: 100,
    marginRight: 5,
    padding: 10,
    borderRadius: 10,
  },
  appFontReg: {
    ...getAppFont('SemiBold'),
  },
  btRow: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  commentbox: {
    borderWidth: 1,
    height: 70,
    // marginLeft: 15,
    // marginRight: 15,
    // marginBottom: 30,
    marginTop: 20,
    alignItems: 'flex-start',
  },
  txtFieldStyle: {
    textAlignVertical: 'top',
    alignItems: 'flex-start',
    ...getAppFont('Regular'),
  },
  borderHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',

    borderBottomWidth: 0.5,
    marginBottom: 10,
  },
  txtfldcontainstyle: {
    borderBottomWidth: 0,
  },
  quesSty: {
    paddingTop: 15,
    paddingBottom: 15,
  },
  subBtn: {
    paddingBottom: 50,
    alignItems: 'center',
  },
  subBtntxt: {
    width: 200,
    borderRadius: 10,
  },
  subtletxt: {
    ...getAppFont('Bold'),
    fontSize: 18,
  },
  centerView: {
    height: 400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  workStatus: {
    color: 'orange',
  },
  tinyLogo: {
    width: 100,
    height: 100,
    marginTop: 5,
    marginRight: 5,
  },
  wrapper: {
    paddingBottom: 10,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginBottom: 15,
    borderWidth: 0.3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20,
    elevation: 3,
    paddingTop: 0,
  },
  borderBottom:{
    borderBottomColor:'#ddd',
    borderBottomWidth:1,
    marginBottom:10
  }
});
