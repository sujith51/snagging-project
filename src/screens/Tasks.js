/* eslint-disable react-native/no-inline-styles */
/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Alert, Platform, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { SearchIcon, MoreIcon, NotificationIcon } from '_svg';
import {
  CommonContainer,
  DropDown,
  TaskCard,
  MrdCard,
  TaskList,
  Spinner,
  MoreMenu,
  SelectCommunity,
} from '_components';
import { useNavigation, useRoute } from '@react-navigation/native';
import { getTaskData, getTaskDetailsMrd, getCommunityList } from '_actions';
import { usePrevious } from '_hooks';
import { useTheme, getAppFont, returnWithoutStatic } from '../utils';

const DropDownInspection = [
  // { value: 'All', label: 'Total Inspection' },

  { value: 'move_out_inspection', label: 'Make Ready Inspection' },
  { value: 'move_in_inspection', label: 'Move In Inspection' },
  { value: 'vacant', label: 'Vacant Inspection' },
];
const MoreMenuOptions = [
  { value: 'completed', label: 'Completed' },
  { value: 'inprogress', label: 'In Progress' },
  { value: 'upcoming', label: 'Upcoming' },
];
const statusValues = [
  { value: 'upcoming', label: 'Pending' },
  // { value: 3, label: 'Review' },
  { value: 'completed', label: 'Completed' },
 
];

const Tasks = () => {
  // :States
  const { params } = useRoute();
  let loaded = params?.loaded ?? false;
  const [inspectionType, setInspectionType] = useState(DropDownInspection[0]);
  // const [moreMenu, setMoreMenu] = useState(false);
  const [communities, setCommunities] = useState([]);
  const [units, setUnits] = useState([]);
  const [moreMenuData, setMoreMenuData] = useState(null);
  const [status, setStatus] = useState('upcoming');
  const [statusTitle, setStatusTitle] = useState(statusValues[0].label);
  const review = 3;
  const dispatch = useDispatch();
  const theme = useTheme();
  const { navigate } = useNavigation();
  const {
    taskDetailsMrd: {
      data: taskDetailsData,
      loading: taskDetailsloading,
      error: { status: taskDetailsError },
    },
    taskData: {
      data: fullTaskData,
      count: taskCount,
      loading: taskDataLoading,
      page: taskPage,
      endOfData: taskDataEnd,
    },
    saveWorkDetail:{
      data: saveWorkDetailData,
       loading: saveWorkDetailLoading
    }
  } = useSelector((state) => state.taskdetails);

  const {
    data,
    selectCommunity = [],
    communityList,
  } = useSelector((state) => state.user);

  const prevtaskDetailsloading = usePrevious(taskDetailsloading);

  // :Effects.
  useEffect(() => {
    if (data?.id_user && data?.id_user_type) {
      dispatch(
        getCommunityList({
          id_user: data?.id_user,
          id_user_type: data?.id_user_type,
        }),
      );
    }
  }, []);
  useEffect(() => {
    if (saveWorkDetailData) {
      refreshData()
    }
  }, [saveWorkDetailData]);
  const refreshData = () => {
    // console.log('refresh data')
    const allCommunity =
      communityList?.data && Object.keys(communityList?.data);
    // if (allCommunity && selectCommunity?.length === 0) {
    //   callData();
    // }
    if (selectCommunity?.length > 0) {
      callData();
    }
  };

  useEffect(() => {
    const commun = [];
    if (communityList?.data) {
      Object.keys(communityList?.data).forEach((key) =>
        commun.push({
          value: key,
          label: communityList?.data[key],
        }),
      );
      setCommunities(commun);
    }
  }, [communityList?.data]);
  const callData = () => {
    dispatch(
      getTaskData({
        id_community: returnWithoutStatic(selectCommunity),
        id_user: data?.id_user,
        page: 1,

        inspection_status: status || 0,
      }),
    );
  };
  useEffect(() => {
    const allCommunity =
      communityList?.data && Object.keys(communityList?.data);
    // if (allCommunity && selectCommunity?.length == 0) {
    //   callData();
    // }
    if (selectCommunity?.length > 0) {
      callData();
    }
  }, [dispatch, selectCommunity, status]);

  useEffect(() => {
    if (
      prevtaskDetailsloading === true &&
      taskDetailsloading === false &&
      taskDetailsData?.length > 0
    ) {
      navigate('TaskDetails', {
        taskDetail: taskDetailsData,
        type:'MRD'
      });
    } else if (
      (prevtaskDetailsloading === true &&
        taskDetailsloading === false &&
        taskDetailsData?.length === 0) ||
      taskDetailsError
    ) {
      Alert.alert('Something went wrong!');
    }
  }, [
    taskDetailsData,
    taskDetailsloading,
    taskDetailsError,
    prevtaskDetailsloading,
    navigate,
  ]);

  const handleDropDownInspections = (data) => {
    if (data) {
      setInspectionType(data);
      setMoreMenuData(null);
    }
  };

  const onReachEnd = () => {
    if (!taskDataLoading && !taskDataEnd) {
      const allCommunity =
        communityList?.data && Object.keys(communityList?.data);
      // if (allCommunity && selectCommunity?.length === 0) {
      //   callData();
      // }
      if (selectCommunity?.length > 0) {
        dispatch(
          getTaskData({
            id_community: allCommunity,
            id_user: data?.id_user,
            page: taskPage + 1,
            inspection_status: status || 0,
          }),
        );
      }
    }
  };

  const closeMoreMenu = (data) => {
    setMoreMenuData(data);
    // setMoreMenu(false);
  };

  const taskClicked = (_item) => {
    const data = {
      id_snag_property: _item?.id_snag_property,
      id_snag_inspection: _item?.id_snag_inspection
    };
    dispatch(getTaskDetailsMrd(data));
  };

  const taskRender = ({ item, index }) => {
    return (
      <View style={{ paddingHorizontal: Platform.isPad ? 10 : 1 }}>
        <MrdCard
          {...item}
          index={index}
          status={status === review}
          onPress={() => taskClicked(item)}
          cardStyle={[styles.cardStyle, { padding: theme.spacing.sm }]}
        />
      </View>
    );
  };

  const containerProps = {
    navbar: {
      type: 'outer',
      title: 'Desnagging',
      actions: [
        // {
        //   icon: <SearchIcon />,
        //   onPress: () => navigate('Search'),
        // },
        {
          icon: <NotificationIcon />,
          onPress: () => navigate('Notifications'),
        },
      ],
    },
    style: { backgroundColor: theme.color.secondaryLight },
    scrollEnabled: true,
    // style: { flex: 1 },
    onModalPress: () => {
      // onRefresh();
    },
    onRefresh: () => {
      refreshData();
    },
  };

  const handleDropDownStatus = (data) => {
    // if (data?.value) {
    setStatus(data.value);
    setStatusTitle(data.label);
    // }
  };

  const listHeader = () => {
    return (
      <>
        <View>
          {communities?.length > 0 && (
            <SelectCommunity units={units} communities={communities} />
          )}
        </View>
        <View style={styles.dropDownCont}> 
          <Text
            style={[
              styles.label,
              { color: theme.color.primary, ...getAppFont('Bold') },
            ]}
          >
           De-Snagging Inspection
          </Text>
          <DropDown
            value={statusTitle}
            options={statusValues}
            placeHolderStyle={{
              color: theme.color.primary,
              ...getAppFont('Bold'),
            }}
            onClose={handleDropDownStatus}
          />
         
        </View>
      </>
    );
  };

  return (
    <CommonContainer {...containerProps}>
      <View
        style={[
          styles.container,
          { backgroundColor: theme.color.secondaryLight },
        ]}
      >
        <View style={styles.taskcardstyle}>
          <TaskList
            data={fullTaskData || []}
            renderItem={taskRender}
            loading={taskDataLoading}
            onEndReached={onReachEnd}
            listHeaderComponent={listHeader}
          />
        </View>
        <Spinner
          visible={taskDetailsloading || taskDataLoading}
          size="large"
          animationType="none"
        />
      </View>
    </CommonContainer>
  );
};

export default Tasks;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // borderTopLeftRadius: 30,
    // borderTopRightRadius: 30,
  },
  dropDownCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20,
    paddingLeft: 22,
    paddingBottom: 20,
  },
  taskcardstyle: {
    // paddingTop: 15,
    flex: 1,
    paddingHorizontal: 0,
  },
  cardStyle: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
  },
  label: {
    textAlign: 'center',
    paddingTop: 5,
    fontSize: Platform.isPad ? 20 : 14,
  },
  marTop: {
    // marginTop: 15,
  },
});
