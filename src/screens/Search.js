import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Alert,
  ActivityIndicator,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { CommonContainer, TaskList, TaskCard, Text } from '_components';
import { searchUnits, getTaskDetails } from '_actions';
import { usePrevious } from '_hooks';
import { SearchIcon } from '_svg';
import { useTheme, getAppFont } from '../utils';

const Search = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const [searchUnit, setSearchUnit] = useState(null);

  const { searchData } = useSelector((state) => state.dashboard);
  const {
    taskDetails: {
      data: taskDetailsData,
      loading: taskDetailsloading,
      error: { status: taskDetailsError },
    },
  } = useSelector((state) => state.taskdetails);

  const prevtaskDetailsloading = usePrevious(taskDetailsloading);

  useEffect(() => {
    if (searchUnit) {
      dispatch(
        searchUnits({
          unit_number: searchUnit,
        }),
      );
    }
  }, [dispatch, searchUnit]);

  useEffect(() => {
    if (
      prevtaskDetailsloading === true &&
      taskDetailsloading === false &&
      taskDetailsData?.length > 0
    ) {
      navigate('TaskDetails', {
        taskDetail: taskDetailsData,
      });
    } else if (
      (prevtaskDetailsloading === true &&
        taskDetailsloading === false &&
        taskDetailsData?.length === 0) ||
      taskDetailsError
    ) {
      Alert.alert('Something went wrong!');
    }
  }, [
    taskDetailsData,
    taskDetailsloading,
    taskDetailsError,
    prevtaskDetailsloading,
    navigate,
  ]);

  const containerProps = {
    navbar: {
      title: 'Search',
    },
    onModalPress: () => {
      // onRefresh();
    },
    // onRefresh : () =>{
    //  refreshData()
    // }
  };

  const refreshData = () =>{
    if (searchUnit) {
      dispatch(
        searchUnits({
          unit_number: searchUnit,
        }),
      );
    }
  }

  const taskClicked = (_item) => {
    const data = {
      id_snag_property: _item?.id_snag_property,
    };
    dispatch(getTaskDetails(data));
  };

  const taskRender = ({ item, index }) => {
    return (
      <View style={{ paddingBottom: 20 }}>
        <TaskCard
          {...item}
          index={index}
          onPress={() => taskClicked(item)}
          cardStyle={[styles.cardStyle, { padding: theme.spacing.sm }]}
        />
      </View>
    );
  };

  return (
    <CommonContainer {...containerProps}>
      <View style={styles.container}>
        <View style={styles.searchSection}>
          <TextInput
            style={styles.input}
            placeholder="Search Unit"
            onChangeText={(txt) => setSearchUnit(txt)}
            underlineColorAndroid="transparent"
          />
          <View
            style={[
              styles.searchIconSty,
              { backgroundColor: theme.color.primary },
            ]}
          >
            <SearchIcon />
          </View>
        </View>
        {searchData?.loading && (
          <View style={styles.errView}>
            <ActivityIndicator />
            <Text>Loading...</Text>
          </View>
        )}
        <View style={{ paddingTop: 20 }}>
          <TaskList
            data={searchData?.data || []}
            renderItem={taskRender}
            // loading={searchData?.loading}
            showNoDataMessage={false}
            isSearch
          />
        </View>
        {(searchData?.data?.length < 0 || searchData?.error.status) &&
          !searchData?.loading && (
            <View style={styles.errView}>
              <Text>No data Found</Text>
            </View>
          )}
      </View>
    </CommonContainer>
  );
};

export default Search;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 10,
    paddingLeft: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    ...getAppFont('Regular'),
  },
  searchIconSty: {
    padding: 10,
    marginHorizontal: 4,
    marginVertical: 2,
    borderRadius: 12,
  },
  errView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
   height: 200,
  },
});
