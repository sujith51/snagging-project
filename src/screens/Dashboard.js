/* eslint-disable no-nested-ternary */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-shadow */
/* eslint-disable camelcase */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable import/no-unresolved */
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ActivityIndicator, Alert,Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { SearchIcon, NotificationIcon } from '_svg';

import {
  CommonContainer,
  DropDown,
  TaskCard,
  TaskList,
  Text,
  BarGraph,
  Spinner,
  SelectCommunity,
} from '_components';
import { useRoute, useNavigation } from '@react-navigation/native';
import {
  getBarGraphData,
  getDashboardListing,
  getTaskDetails,
  getCommunityList,
} from '_actions';
import { usePrevious } from '_hooks';
import MoveinInspection from './MoveinInspection';
import {
  useTheme,
  getAppFont,
  getColor,
  theme as themeIn,
  returnWithoutStatic,
} from '../utils';
import { getCircularGraphData } from '../redux/actions/dashboardAction';

const DropDownValues = [
  { value: 'Weekly', label: 'Weekly' },
  { value: 'Monthly', label: 'Monthly' },
  { value: 'Yearly', label: 'Yearly' },
];

const statusValues = [
  { value: 0, label: 'Pending' },
  { value: 2, label: 'Completed' },
  // { value: 3, label: 'Review' },
];
const all_inpection = 'All Inspection';
const DropDownInspection = [
  { value: 'All', label: all_inpection },
  { value: 'move_in_inspection', label: 'Move-in Inspection' },
  { value: 'move_out_inspection', label: 'Move-out Inspection' },
  // { value: 'makeready_inspection', label: 'Make Ready Inspection' },
  { value: 'vacant', label: 'Vacant Inspection' },
];

const tabStatus = [
  {
    title: 'Completed',
    id: 1,
    id_tab: 'completed',
    color: themeIn.color.primary,
    textColor: themeIn.color.primaryWhite,
  },
  {
    title: 'In Progress',
    id: 2,
    id_tab: 'in_progress',
    color: themeIn.color.secondary,
    textColor: themeIn.color.primaryWhite,
  },
  {
    title: 'Upcoming',
    id: 3,
    id_tab: 'upcoming',
    color: themeIn.color.lime,
    textColor: themeIn.color.primary,
  },
];

const Dashboard = () => {
  // :States
  const theme = useTheme();
  const dispatch = useDispatch();
  const [duration, setDuration] = useState('Weekly');
  const [status, setStatus] = useState(0);
  const [inspectionType, setInspectionType] = useState('All');
  const [selected, setSelected] = useState(3);
  const [selectedTab, setSelectedTab] = useState('upcoming');
  const [tabTitle, setTabTitle] = useState('Snagging Inspection');
  const [communities, setCommunities] = useState([]);
  const [units, setUnits] = useState([]);
  // used to send as props to selectcommunity to load or not
  const [loadAllData, setLoadAllData] = useState(true);
  const [reloadBar, setReloadBar] = useState(true);
  // used to load if we come to dashboard or come from tasks
  const [loadOnlyFirstTime, setLoadOnlyFirstTime] = useState(true);
  // const [fromTaskList, setFromTaskList] = useState(false);
  const [statusTitle, setStatusTitle] = useState(statusValues[0].label);
  // new api changes
  const [requestType, setRequestType] = useState('upcoming');

  const { navigate } = useNavigation();

  const [graphValue, setGraphValue] = useState({
    color: theme.color.lime,
    textColor: theme.color.primary,
  });

  const {
    dashboardData: {
      data: dashboardData,
      count: dataCount,
      loading: dashboardLoading,
    },
    barGraphData: { data: barData, loading: barLoading },
    circularGraphData: { data: circularData, loading: circiularLoading },
  } = useSelector((state) => state.dashboard);
  const {
    saveDamage: {
      data: saveDamageData,
      loading: saveDamageloading,
      error: { status: saveDamageError },
    },
    taskDetails: {
      data: taskDetailsData,
      loading: taskDetailsloading,
      error: { status: taskDetailsError },
    },
  } = useSelector((state) => state.taskdetails);

  const {
    data,
    selectCommunity = [],
    selectUnit,
    communityList,
  } = useSelector((state) => state.user);

  const prevtaskDetailsloading = usePrevious(taskDetailsloading);

  const finalBarGraph =
    barData?.length > 0
      ? barData.map((item) => ({
          ...item,
        
          value: parseInt(item.value),
          svg: { fill: getColor(item.id) },
        }))
      : [];
  // :Effects.

  useEffect(() => {
    if (data?.id_user && data?.id_user_type) {
      dispatch(
        getCommunityList({
          id_user: data?.id_user,
          id_user_type: data?.id_user_type,
        }),
      );
    }
  }, [data?.id_user]);

  useEffect(() => {
    const commun = [];
    if (communityList?.data) {
      Object.keys(communityList?.data).forEach((key) =>
        commun.push({
          value: key,
          label: communityList?.data[key],
        }),
      );
      setCommunities(commun);
    }
  }, [communityList?.data]);

  useEffect(() => {
    if (saveDamageData) {
      callDashboardListing()
      // setFromTaskList(true);
     
    }
  }, [saveDamageData]);

  useEffect(() => {
    const allCommunity =
      communityList?.data && Object.keys(communityList?.data);
    if (
      allCommunity &&
      selectCommunity?.length === 0 &&
      (loadOnlyFirstTime)
    ) {
      // load in selectcommunity
      setLoadAllData(true);
      setLoadOnlyFirstTime(false);
      // setFromTaskList(false);
      callDashboardListing()
    }
    if (selectCommunity?.length > 0) {
      setLoadAllData(false);
      callDashboardListing()
      getCircularGraph()
    }
   
  }, [
    inspectionType,
    selectCommunity,
    selectUnit,
    selectedTab,
    communityList,
    status,

  ]);

  useEffect(() => {
    if (reloadBar) {
      if (inspectionType === 'All') {
        dispatch(getBarGraphData({ duration, id_user: data?.id_user }));
        callDashboardListing()
        getCircularGraph()
        setReloadBar(false);
      } else {
        setSelected(1);
        setSelectedTab('completed');
        getCircularGraph()
        setReloadBar(false);
      }
    }
  }, [duration, reloadBar]);

  useEffect(() => {
    if (
      prevtaskDetailsloading === true &&
      taskDetailsloading === false &&
      taskDetailsData?.length > 0
    ) {
      navigate('TaskDetails', {
        taskDetail: taskDetailsData,
        type:'Dashboard'
      });
    } else if (
      (prevtaskDetailsloading === true &&
        taskDetailsloading === false &&
        taskDetailsData?.length === 0) ||
      taskDetailsError
    ) {
      Alert.alert('Something went wrong!');
    }
  }, [taskDetailsData, taskDetailsloading, taskDetailsError]);

  const handleDropDown = (data) => {
    if (data?.value) {
      setDuration(data.value);
      setReloadBar(true);
    }
  };

  const handleDropDownStatus = (data) => {
    // if (data?.value) {
    setStatus(data.value);
    setStatusTitle(data.label);
    if (data.value === 0) {
      setRequestType('upcoming');
    } else if (data.value === 2) {
      setRequestType('completed');
    }

    // }
  };

  const handleDropDownInspections = (data) => {
    if (data?.value) {
      setInspectionType(data.value);
      setReloadBar(data.value === 'All');
    
    }
  };

  const taskClicked = (_item) => {
    const data = {
      id_snag_property: _item?.id_snag_property,
    };
    dispatch(getTaskDetails(data));
  };
  const callDashboardListing=()=>{
    dispatch(
      getDashboardListing({
        id_user: data?.id_user,
        duration,
        page: 1,
        id_community: returnWithoutStatic(selectCommunity),      
        requestType,
        actual_role:data?.actual_role    
      }),
    )
  }
  const getCircularGraph=()=>{
   
  }
  const taskRender = ({ item, index }) => {
    return (
      <View>
        <TaskCard
          {...item}
          type='Dashboard'
          index={index}
          onPress={() => taskClicked(item)}
          cardStyle={[styles.cardStyle, { padding: theme.spacing.sm }]}
        />
      </View>
    );
  };


  // Refresh Added+++
  const refreshClicked = () => {
    if (data?.id_user && data?.id_user_type) {
      dispatch(
        getCommunityList({
          id_user: data?.id_user,
          id_user_type: data?.id_user_type,
        }),
      );

      dispatch(getBarGraphData({ duration, id_user: data?.id_user }));
    }

    if (selectCommunity?.length > 0) {
      if (inspectionType === 'All') {
      
        callDashboardListing()
        
      } else {
        callDashboardListing()
      }
    }
    if (inspectionType !== 'All') {
    
      getCircularGraph()
    }
  };

  const containerProps = {
    navbar: {
      type: 'outer',
      title: 'Dashboard',
      actions: [
        // {
        //   icon: <SearchIcon />,
        //   onPress: () => navigate('Search'),
        // },
        {
          icon: <NotificationIcon />,
          onPress: () => navigate('Notifications'),
        },
      ],
    },
    style: { backgroundColor: theme.color.secondaryLight },
    scrollEnabled: true,
    // style: { flex: 1 },
    onModalPress: () => {
      // onRefresh();
    },
    onRefresh: () => {
      // alert("refresh called")
      refreshClicked();
    },
  };

  const onSelectPress = (item) => {
    setSelected(item.id);
    setSelectedTab(item.id_tab);
    // setTabTitle(item.title);
    setGraphValue({ color: item.color, textColor: item.textColor });
  };

  return (
    <CommonContainer {...containerProps}>
      <View
        style={[
          styles.container,
          { backgroundColor: theme.color.secondaryLight },
        ]}
      >
        <View
         
        >
          {communities?.length > 0 && (
            <SelectCommunity
              units={units}
              communities={communities}
              loadInit={loadAllData}
            />
          )}
          <View style={styles.dropDownCont}>
         
          <Text
            style={[
              styles.label,
              { color: theme.color.primary, ...getAppFont('Bold') },
            ]}
          >
           Snagging Inspection
          </Text>
          <DropDown
              value={statusTitle}
              options={statusValues}
              placeHolderStyle={{
                color: theme.color.primary,
                ...getAppFont('Bold'),
              }}
              onClose={handleDropDownStatus}
            />
          {/* <View style={{}}>
            <MoreMenu
              options={MoreMenuOptions}
              // isOpen={moreMenu}
              onClose={(data) => closeMoreMenu(data)}
            />
          </View> */}
        </View>
          
      </View>

        <TaskList
          data={dashboardData || []}
          renderItem={taskRender}
          // listHeaderComponent={listHeader}
          loading={dashboardLoading}
        />
        <Spinner
          visible={taskDetailsloading || dashboardLoading}
          size="large"
          animationType="none"
        />
      </View>
      
    </CommonContainer>
  );
};

export default Dashboard;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingHorizontal:10
  },
  listHeadCont: {
    paddingLeft: 20,
    paddingBottom: 20,
  },
  label: {
    textAlign: 'center',
    paddingTop: 5,
    fontSize: Platform.isPad ? 20 : 14,
  },
  dropDownCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20,
    paddingLeft: 22,
    paddingBottom: 20,
  },
  cardStyle: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    borderRadius: 30,
  },
});
