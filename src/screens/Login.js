/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-shadow */
import React, { useState, useRef, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  BackHandler,
  Alert,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation, useNavigationState } from '@react-navigation/native';
import { InputText, Spinner } from '_components';
import { getAppFont } from '../utils/theme';
import { useTheme } from '../utils';
import {
  User,
  Password,
  RightArrow,
  MakeReadyLogo,
} from '../assets/svg_components';
import { login, updateDeviceToken, saveUserPushToken } from '../redux/actions';
import registerForPushNotificationsAsync from '../push_notification/index';

const Login = () => {
  const theme = useTheme();
  const passwordRef = useRef();
  const dispatch = useDispatch();
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [validationErr, setValidationErr] = useState(null);
  const [expoPushToken, setExpoPushToken] = useState(null);
  const state = useNavigationState((state) => state);
  const routeName = state.routeNames[state.index];

  const { navigate } = useNavigation();
  const user = useSelector((state) => state.user);
  const loginLoading = user?.loading;
  useEffect(() => {
    if (__DEV__) {
      // setUsername('snag_qa_admin@realcube.co');
      setUsername('qa_team1@realcube.co');
      // setUsername('fmengeniour@realcube.co');
      // setUsername('handover_member@realcube.co');
      // setUsername('a.ahmed@provis.ae');
      // setUsername('mrt3@khidmah.com');
      // setUsername('MRT3@khidmah.com');
      // setUsername('benoit.hesnard123@gmail.com');
      setPassword('owner123');
      // setPassword('EX@logi(123');
    }
    registerForPushNotificationsAsync().then((token) => {
      setExpoPushToken(token);
    });
  }, []);

  useEffect(() => {
    if (user.data !== null) {
      navigate('Main');
      setUsername(null);
      setPassword(null);
    } else if (user.error.status) {
      setValidationErr('Invalid credentials');
    }
  }, [user.data, navigate, user, user.error.status]);

  useEffect(() => {
    if (user.data !== null && expoPushToken) {
      const data = {
        id_user: user.data.id_user,
        device_token: expoPushToken,
        device_type: Platform.OS,
      };
      dispatch(updateDeviceToken(data));
      dispatch(saveUserPushToken(expoPushToken));
    }
  }, [dispatch, expoPushToken, user.data]);

  useEffect(() => {
    if (username && password) {
      setValidationErr(null);
    }
  }, [username, password]);

  // useEffect(() => {
  //   const backAction = () => {
  //     if (routeName === 'Intro' || routeName === 'Login') {
  //       Alert.alert('Hold on!', 'Are you sure you want to go back?', [
  //         {
  //           text: 'Cancel',
  //           onPress: () => null,
  //           style: 'cancel',
  //         },
  //         { text: 'YES', onPress: () => BackHandler.exitApp() },
  //       ]);
  //       return true;
  //     }
  //   };

  //   const backHandler = BackHandler.addEventListener(
  //     'hardwareBackPress',
  //     backAction,
  //   );

  //   return () => backHandler.remove();
  // }, [routeName]);

  useEffect(() => {
    const backAction = () => {
      backPressed();
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
    // return () => {
    //   backHandler.removeEventListener('hardwareBackPress', backAction);
    // };
  }, []);

  const backPressed = () => {
    Alert.alert('Hold on!', 'Are you sure, want to close the App', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {
        text: 'YES',
        onPress: () => {
          BackHandler.exitApp();
        },
      },
    ]);
  };

  const handleLogin = () => {
    if (username && password) {
     
      dispatch(login({ username, password }));
    } else {
      setValidationErr('Please enter the credentials');
    }
  };


  return (
    <View style={styles.container}>
      <View style={{ paddingTop: Platform.isPad ? 100 : 0 }}>
        <View style={styles.loginContainer}>
          <Text style={[styles.loginTxt, { color: theme.color.primary }]}>
            Login
          </Text>
        </View>
        <View style={styles.iconContainer}>
          <View>
            {/* <ProvisPrimaryIcon color={theme.color.primary} /> */}
            <MakeReadyLogo />
          </View>
          {/* <View style={styles.iconSubCont}>
          <ProvisConnectIcon color={theme.color.primary} />
        </View> */}
        </View>
        {validationErr && (
          <View style={styles.loginContainer}>
            <Text style={[styles.errorText, { color: theme.color.red }]}>
              {validationErr}
            </Text>
          </View>
        )}
        <View style={styles.inputTextpadding}>
          <InputText
            icon={<User color={theme.color.secondaryDark} />}
            placeholder="USERNAME"
            onChangeText={setUsername}
            onSubmitEditing={() => passwordRef.current.focus()}
          />
          <InputText
            icon={<Password color={theme.color.secondaryDark} />}
            placeholder="PASSWORD"
            secureTextEntry
            innerRef={passwordRef}
            onChangeText={setPassword}
            blurOnSubmit={false}
            onSubmitEditing={handleLogin}
          />
        </View>
        {/* <View style={styles.forgotPassword}>
        <Text
          style={{ color: theme.color.primary, ...getAppFont('Medium') }}
          onPress={handleForgotPassword}
        >
          Forgot Password?
        </Text>
      </View> */}
        <TouchableOpacity onPress={handleLogin}>
          <View style={styles.row}>
            <Text style={[styles.paddingleft, { color: theme.color.primary }]}>
              LOGIN
            </Text>
            <View
              style={[styles.circle, { backgroundColor: theme.color.primary }]}
            >
              <RightArrow color={theme.color.primaryWhite} />
            </View>
          </View>
        </TouchableOpacity>
        <Spinner visible={loginLoading} size="large" animationType="none" />
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  errorText: {
    fontSize: 12,
    // color: 'red',
    position: 'absolute',
    ...getAppFont('Regular'),
  },
  loginContainer: {
    alignItems: 'center',
  },
  loginTxt: {
    marginTop: 120,
    fontSize: Platform.isPad ? 30 : 26,
    ...getAppFont('Bold'),
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    marginBottom: 30,
  },
  iconSubCont: {
    paddingLeft: 13,
  },
  circle: {
    height: 50,
    width: 50,
    borderRadius: 50,

    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingRight: 20,
    paddingTop: 30,
  },
  paddingleft: {
    paddingRight: 10,
    ...getAppFont('Bold'),
    fontSize: Platform.isPad ? 20 : 16,
  },
  // forgotPassword: {
  //   flexDirection: 'row',
  //   justifyContent: 'flex-end',
  //   paddingRight: 30,
  //   marginTop: -25,
  // },
  inputTextpadding: {
    padding: 30,
  },
});
