/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import { View, Alert, StyleSheet, Platform } from 'react-native';
import {
  CommonContainer,
  CalendarComponent,
  DropDown,
  TaskCard,
  TaskList,
} from '_components';
import { SearchIcon, NotificationIcon, LeftIcon, RightIcon } from '_svg';

import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { usePrevious } from '_hooks';
import { useNavigation } from '@react-navigation/native';
import { getCalendarData, getTaskDetails } from '../redux/actions';
import { getAppFont, returnWithoutStatic, useTheme } from '../utils';

const DropDownValues = [
  // { value: 'All', label: 'All' },
  { value: 'move_in_inspection', label: 'Move In' },
  { value: 'move_out_inspection', label: 'Move Out' },
  { value: 'makeready_inspection', label: 'Make Ready' },
  { value: 'vacant', label: 'Vacant' },
];

const Calendar = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { navigate } = useNavigation();

  // :States

  const [clickedDates, setClickedDates] = useState([]);
  const [copyDates, setCopyDates] = useState([]);
  const [markingDates, setMarkingDates] = useState({});
  const [inspectionType, setInspectionType] = useState('move_in_inspection');
  const [monthDate, setMonthDate] = useState({
    startDate: moment().startOf('month').format('YYYY-MM-DD'),
    endDate: moment().endOf('month').format('YYYY-MM-DD'),
  });

  const { data, selectCommunity, selectUnit } = useSelector(
    (state) => state?.user,
  );
  const { data: userData } = useSelector((state) => state.user);
  const {
    calendarData: { data: calendarData, loading: calendarLoading },
  } = useSelector((state) => state.calendar);

  const {
    taskDetails: {
      data: taskDetailsData,
      loading: taskDetailsloading,
      error: { status: taskDetailsError },
    },
  } = useSelector((state) => state.taskdetails);

  const prevtaskDetailsloading = usePrevious(taskDetailsloading);
  const prevcalendarLoading = usePrevious(calendarLoading);
  useEffect(() => {
    if (calendarData?.length > 0) {
      const finalObj = resetColor(calendarData);
      setMarkingDates(finalObj);
      setClickedDates(calendarData);
      setCopyDates(calendarData);
    } else {
      setMarkingDates({});
      setClickedDates([]);
      setCopyDates([]);
    }
  }, [calendarData]); // Don't add maked styles It will run into Infinite Loop
  const makedStyle = {
    customStyles: {
      container: {
        width: 36,
        height: 36,
        backgroundColor: theme.color.primaryWhite,
        borderRadius: 0,
      },
      text: {
        color: theme.color.primary,
      },
    },
  };
  const selectedDayStyle = {
    customStyles: {
      container: {
        width: 36,
        height: 36,
        backgroundColor: theme.color.primaryLight,
        borderRadius: 0,
      },
      text: {
        color: theme.color.primary,
      },
    },
  };
  const containerProps = {
    navbar: {
      title: 'Calendar',
      actions: [
        // {
        //   icon: <SearchIcon />,
        //   // onPress: () => navigate('Search'),
        // },
        {
          icon: <NotificationIcon />,
          onPress: () => navigate('Notifications'),
        },
      ],
    },
    scrollEnabled: true,
    style: { flex: 1 },
    onModalPress: () => {
      // onRefresh();
    },
    onRefresh: () => {
      refreshData();
    },
  };

  const resetColor = (data) => {
    const finalObj = {};
    for (let i = 0; i < data.length; i++) {
      finalObj[data[i].booking_date] = makedStyle;
    }
    return finalObj;
  };
  const dayClicked = (date) => {
    // console.log('clicked++', date, copyDates);
    const day = date.dateString;
    // let finaArr = []
    const finalObj = resetColor(calendarData);
    const copy = { ...finalObj };
    // setMarkingDates(finalObj);
    if (date?.dateString) {
      const exist = copyDates.filter(
        (i) => i.booking_date === date?.dateString,
      );

      if (exist?.length > 0) {
        copy[date?.dateString] = selectedDayStyle;
        const selectedDate = copyDates.filter(
          (i) => i.booking_date == date?.dateString,
        );
        // console.log('Full Date clicked++', copy);
        setMarkingDates(copy);
        setClickedDates(selectedDate);
      } else {
        setClickedDates([]);
        setMarkingDates(copy);
      }
    }
  };

  useEffect(() => {
    // Need to make the filter
    if (selectCommunity?.length > 0) {
      dispatch(
        getCalendarData({
          id_community: returnWithoutStatic(selectCommunity),
          service_name: inspectionType,
          from_date: monthDate.startDate,
          to_date: monthDate.endDate,
          inspector_id: userData?.id_user,
        }),
      );
    }
  }, [dispatch, inspectionType, monthDate, selectCommunity]);

  useEffect(() => {
    if (
      prevtaskDetailsloading === true &&
      taskDetailsloading === false &&
      taskDetailsData?.length > 0
    ) {
      navigate('TaskDetails', {
        taskDetail: taskDetailsData,
      });
    } else if (
      (prevtaskDetailsloading === true &&
        taskDetailsloading === false &&
        taskDetailsData?.length === 0) ||
      taskDetailsError
    ) {
      Alert.alert('Something went wrong!');
    }
  }, [
    taskDetailsData,
    taskDetailsloading,
    taskDetailsError,
    prevtaskDetailsloading,
    navigate,
  ]);

  const handleDropDown = (data) => {
    // console.log('data week', data);
    if (data?.value) {
      setInspectionType(data.value);
    }
  };
  const taskClicked = (_item) => {
    const data = {
      id_snag_property: _item?.id_snag_property,
    };
    dispatch(getTaskDetails(data));
  };

  const monthChangeClicked = (month) => {
    // console.log('month cahnge+++ ', month);
    if (month) {
      const startDate = moment(month.dateString)
        .startOf('month')
        .format('YYYY-MM-DD');
      const endDate = moment(month.dateString)
        .endOf('month')
        .format('YYYY-MM-DD');
      setMonthDate({ startDate, endDate });
    }
  };

  const taskRender = ({ item, index }) => {
    return (
      <View>
        <TaskCard
          {...item}
          index={index}
          calendar
          onPress={() => taskClicked(item)}
          // cardStyle={{
          //   marginLeft: 20,
          //   marginRight: 19,
          //   marginBottom: 18,
          //   padding: 3,
          // }}
          cardStyle={[styles.cardStyle, { padding: theme.spacing.sm }]}
        />
      </View>
    );
  };

  // console.log("Full Date clicked++", markingDates)

  const refreshData = () => {
    if (selectCommunity?.length > 0) {
      dispatch(
        getCalendarData({
          id_community: returnWithoutStatic(selectCommunity),
          service_name: inspectionType,
          from_date: monthDate.startDate,
          to_date: monthDate.endDate,
          inspector_id: userData?.id_user,
        }),
      );
    }
  };
  return (
    <CommonContainer {...containerProps} safeArea>
      <View style={{ flex: 1, backgroundColor: theme.color.primary }}>
        <View
          style={{
            height: 50,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            marginRight: 15,
          }}
        >
          <DropDown
            value={DropDownValues[0].label}
            options={DropDownValues}
            placeHolderStyle={{
              color: theme.color.primaryWhite,
              ...getAppFont('Regular'),
              fontSize: Platform.isPad ? 16 : 13,
            }}
            iconColor={theme.color.primaryWhite}
            labelContStyle={{ padding: 5 }}
            labelStyle={{ fontSize: 10 }}
            contStyle={{
              borderWidth: 1,
              borderColor: theme.color.primaryWhite,
              width: Platform.isPad ? 150 : 120,
              height: Platform.isPad ? 50 : 40,
              justifyContent: 'space-between',
            }}
            onClose={handleDropDown}
            drDownMdlwidth={false}
          />
        </View>
        <View style={styles.calendarSubCont}>
          <CalendarComponent
            dayClicked={(date) => dayClicked(date)}
            markingType="custom"
            markingDates={markingDates}
            theme={{
              calendarBackground: theme.color.primary,
              // textDayFontFamily: 'Montserrat-Regular',
              textDayFontSize: 24,
              textDayHeaderFontSize: 21,
              dayTextColor: theme.color.primaryWhite,
              // textMonthFontFamily: 'Montserrat-Regular',
              textMonthFontSize: 25,
              monthTextColor: theme.color.primaryWhite,
              todayTextColor: theme.color.primaryWhite,
            }}
            renderArrow={(direction) =>
              direction === 'left' ? <LeftIcon /> : <RightIcon />
            }
            disableAllTouchEventsForDisabledDays
            monthChanged={(month) => monthChangeClicked(month)}
          />
        </View>
      </View>
      <View style={{ flex: 1, paddingTop: 20 }}>
        <TaskList
          // data={calendarData || []}
          data={clickedDates}
          renderItem={taskRender}
          loading={calendarLoading}
        />
      </View>
    </CommonContainer>
  );
};

export default Calendar;

const styles = StyleSheet.create({
  calendarSubCont: {
    marginLeft: 25,
    marginRight: 15,
    paddingBottom: 22,
  },
  cardStyle: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 30,
  },
});
