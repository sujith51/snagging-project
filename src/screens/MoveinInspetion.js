import React,{useState, useEffect} from 'react';
import { View, Text,StyleSheet } from 'react-native';
import TaskStatus from '../components/TaskStatus';
import CircularGraph from '../components/ui-elements/CircularGraph';
import { useTheme } from '../utils';

const MoveinInspetion = () =>{
    const theme = useTheme();
    const [selected, setSelected] = useState(1);
    const [graphValue, setGraphValue] = useState({color:theme.color.primary,textColor:'#fff'});

    let inspStatus = [
        {title:'Completed',    id:1, color:theme.color.primary,  textColor:theme.color.white},
        {title:'In Progress',  id:2, color:theme.color.secondary,textColor: theme.color.white},
        {title:'Upcoming',     id:3, color:theme.color.lime,     textColor: theme.color.primary},           
    ];
    onSelectPress = (item)=>{
        setSelected(item.id);
        setGraphValue({color:item.color,textColor:item.textColor});
    };

    return (
        <View style={styles.container}>
            <View style={styles.tabStatus}>
            { inspStatus.map((item, index)=>{
                return(
                    <TaskStatus
                        id={item}
                        handleOnSelectPress={onSelectPress}
                        inspStatusColor={item.color}
                        inspTextColor={item.textColor}
                        selectedId={selected === item.id}
                        index={index}
                        inspTitle={item.title}

                    />
                )})}
            </View>
            <View style={styles.chartView}>
                <CircularGraph 
                    selectedId ={selected}
                    graphColor={graphValue}
                    totalFill={null}
                    completedData={null}
                />
           </View>
        </View>
    )
}

export default MoveinInspetion;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
    },
	tabStatus: {
		height: 50,
        flexDirection: 'row',
    },
    chartView:{
		flex:1,
		alignItems:"center",
        justifyContent:"center",
        paddingBottom:200   
	},
})