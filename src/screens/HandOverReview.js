/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable import/no-unresolved */
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {useRoute, useNavigation} from '@react-navigation/native';
import GetLocation from 'react-native-get-location';
import {
  CommonContainer,
  Accordian,
  Text,
  Button,
  SuccessModal,
  ConfirmModal,
  Spinner,
  ValidationModal,
  SignModal,
  QuantityModal,
  TitleModal,
  Loader,
} from '_components';
import {usePrevious} from '_hooks';
import {useDispatch, useSelector} from 'react-redux';
import {useTheme, getAppFont, config} from '../utils';
// import Geolocation from '@react-native-community/geolocation'
import {
  savechklistquesctions,
  saveDamageInspection,saveOwnerWorkStatus,
  deletesubmittedIns,
  saveLocationProperty,
} from '../redux/actions';

import {Ticked} from '../assets/svg_components';
// let arrData = [];
const owner = 'owner';
const tenant = 'tenant';
const additionalCommentsId = '205';
let disabled = true;
const HandOverReview = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const {navigate} = useNavigation();
  const {params} = useRoute();
  const {taskDetail, listDamage, type = 'others'} = params;
  const flatListScrollRef = useRef();
  const loaderRef = useRef(null);
  const idInsBooking = listDamage.inspection_data[0].id_snag_property;
  const disableTenant =
    listDamage.inspection_data[0].id_service_category === '42';
  // listDamage.inspection_data[0].id_service_category === '29';
  // states
  const [modal, setModal] = useState(false);
  const [confirm, setConfirm] = useState(false);
  const [validErr, setValidErr] = useState(false);
  const [quantity, setQuantity] = useState(false);
  const [quantityObj, setQuantityObj] = useState({});
  const [validErrData, setValidErrData] = useState(null);
  const [validErrDataforField, setvalidErrDataforField] = useState(false);
  const [showErrBor, setshowErrBor] = useState(false);
  const [saveLocation, setSaveLocation] = useState({
    latitude: null,
    longitude: null,
  });
  const [signModal, setSignModal] = useState(false);
  const [signModalInspector, setSignModalInspector] = useState(false);
  const [continueInsp, setContinueInsp] = useState(false);
  const [ownerSign, setOwnerSign] = useState(null);
  const [inspectorSign, setInspectorSign] = useState(null);
  const [scrollToCategory, setScrollToCategory] = useState(null);
  const [scrollIndex, setScrollIndex] = useState([]);
  const [refName, setRefName] = useState([]);
  const [position, setPosition] = useState(null);
  const [parentSelected, setParentSelected] = useState(null);
  const [categorySelected, setCategorySelected] = useState(null);
  const [listCount, setListCount] = useState(null);
  const [changeState, setChangeState] = useState(false);
  const [changeIndex, setChangeIndex] = useState(null);
  const [showLoader, setShowLoader] = useState(false);
  const [show, setShow] = useState(false);
  const [openList, setOpenList] = useState(false);
  const [additionalValueKey, setAdditionalValueKey] = useState(null);
  const [titleModalText, setTitleModalText] = useState(null);
  const [titleModal, setTitleModal] = useState(false);
  const [materialArray, setMaterialArray] = useState([]);
  // for ios modal issue
  const [iosModalOpen, setIosModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  const comments = {
    id_template_category: '44',
    category_question: 'Comments',
    sub_categery: 'Comments',
    is_multiple: '1',
  };
  const {
    rateCardList: {
      // data: materialRateCardData,
      loading: materialRateCardLoading,
    },
    chklistQuesData,
    saveWorkStatus: {
      data: saveDamageData,
      loading: saveDamageloading,
      error: {status: saveDamageError},
    },
    positionData,
  } = useSelector(state => state.taskdetails);
  const checjlistObj = chklistQuesData[idInsBooking];
  const {data: userData} = useSelector(state => state.user);
  const prevsaveDamageloading = usePrevious(saveDamageloading);
  // useEffect(() => {
  //   if (flatListScrollRef?.current && position) {
  //     flatListScrollRef.current.scrollToOffset({
  //       animated: true,
  //       offset: position,
  //     });
  //   }
  // }, [position]);

  /* useEffect(() => {
    // had to change this as additional comments were not coming consistently
    arrData = listDamage?.list_damage?.category;

    const b = {
      id_template_category: 'AdditionalComments',
      category_question: 'Additional Comments',
      sub_categery: 'Additional Comments',
      is_multiple: '1',
      questions: [],
    };
    arrData.push(b);
  }, []);
 */
  console.log(listDamage.list_damage.category);
  useEffect(() => {
    if (!chklistQuesData[idInsBooking]) {
      dispatch(savechklistquesctions(idInsBooking, {}));
    }
  }, []);

  useEffect(() => {
    if (
      chklistQuesData[idInsBooking] &&
      Object.keys(chklistQuesData[idInsBooking]?.answer?.sub_categery).length >
        0
    ) {
      Object.keys(chklistQuesData[idInsBooking]?.answer?.sub_categery).map(
        list => {
          ifSubCategoryExists(
            list,
            chklistQuesData[idInsBooking]?.answer?.sub_categery[list],
          );
        },
      );
    } else {
      let copy = [...listDamage.list_damage.category, comments];

      listDamage.list_damage.category.map(list => {
        return list.sub_categery && saveSubCategory(list.sub_categery);
      });
    }
  }, []);

  useEffect(() => {
    // console.log('disableTenant', disableTenant);
    if (disableTenant) {
      setConfirm(true);
    }
  }, []);
  useEffect(() => {
    if (iosModalOpen) {
      setLoading(true);
      const timer = setTimeout(() => {
        setQuantity(true);
        setIosModalOpen(true);
        setLoading(false);
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [iosModalOpen]);

  const listModal = (value, ques, nof) => {
    setShow(false);
    setOpenList(true);
    // handleAnswersNewMaterial(value, ques, nof);
    // handleAnswers(ques, 'newMaterial', value, nof);
  };
  const ifSubCategoryExists = (cate, data) => {
    // console.log('cate',cate,data)
    if (cate && data) {
      const saveRes = {
        details: 'sub_categery',
        numberOf: cate,
        quesction: data,
      };
      // console.log('here', cate, saveRes);
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    }
  };

  const saveSubCategory = cate => {
    if (cate) {
      const saveRes = {
        details: 'sub_categery',
        numberOf: cate,
        quesction: {
          data: cate == 'Additional Comments' ? [] : [1],
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    }
  };

  useEffect(() => {
    if (
      saveDamageData &&
      prevsaveDamageloading === true &&
      saveDamageloading === false
    ) {
      setModal(true);

      disabled = true;
      // navigate('Dashboard');
    }
  }, [
    // chklistQuesData,
    // dispatch,
    // idInsBooking,
    prevsaveDamageloading,
    saveDamageData,
    // saveDamageError,
    saveDamageloading,
  ]);

  useEffect(() => {
    // console.log('')
    handleLocation();
  }, []);

  useEffect(() => {
    if (categorySelected && showLoader) {
      loaderRef.current = setTimeout(() => {
        setShowLoader(false);
        flatListScrollRef.current.scrollToOffset({
          animated: true,
          offset: position,
        });
      }, 2000);
      return () => {
        clearTimeout(loaderRef.current);
      };
    }
  }, [categorySelected, position, showLoader]);
  const scrollToParent = (index, data) => {
    const yValue = positionData[refName[index].id][refName[index].repeat].y;
    setShowLoader(false);
    setValidErr(false);
    setPosition(yValue);
    setChangeState(prev => !prev);
    setChangeIndex(refName[index].index);
    setPosition(yValue);
    setParentSelected(refName[index].parent);
    setCategorySelected(refName[index].category);
    setListCount(refName[index].repeat);
    setShowLoader(true);
    // console.log('scrollToParent',
    //     index,
    //     yValue,
    //      positionData[refName[index].id],
    //     // refName,
    //     //  positionData,
    //     //   refName[index].index
    //       );

    flatListScrollRef.current.scrollToIndex({
      animated: false,
      index: refName[index].index,
    });
  };
  const handleLocation = () => {
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(location => {
        setSaveLocation({
          latitude: location.latitude,
          longitude: location.longitude,
        });
      })
      .catch(error => {
        const {code, message} = error;
        if ('UNAVAILABLE') {
          alert('Please turn on location to save the property location');
        }
      });
  };

  const scrollToIndex = () => {
    flatListScrollRef.current.scrollToIndex({
      animated: true,
      index: scrollToCategory,
    });
  };

  const scrollToIndexFailed = error => {
    const offset = error.averageItemLength * error.index;
    flatListScrollRef.current.scrollToffset({offset});
    setTimeout(
      () =>
        flatListScrollRef.current.scrollToIndex({
          animated: true,
          index: error.index,
        }),
      100,
    );
  };

  const navToQuotation = () => {
    setModal(!modal);
    // if (checjlistObj?.answer?.newMaterial?.newMaterialData.data.length > 0) {
    //   const updatedData = chklistQuesData;
    //   delete updatedData[idInsBooking];
    //   dispatch(deletesubmittedIns(updatedData));

    //   navigate('Quotations');
    // } else {
    //   const updatedData = chklistQuesData;
    //   delete updatedData[idInsBooking];
    //   dispatch(deletesubmittedIns(updatedData));

    //   // onSubmit();
    //   navigate('Dashboard', { loaded: true });
    // }
    const updatedData = chklistQuesData;
    delete updatedData[idInsBooking];
    dispatch(deletesubmittedIns(updatedData));

    // onSubmit();
    navigate('Dashboard', {loaded: true});
  };

  const validErrModal = () => {
    setValidErr(false);
    scrollToIndex();
  };

  function groupBy(objectArray, property) {
    return objectArray.reduce((acc, obj) => {
      const key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      // Add object to list for given key's value
      acc[key].push(obj);
      return acc;
    }, {});
  }
  function groupBySplit(objectArray, property1, property2) {
    return objectArray.reduce((acc, obj) => {
      const combined = `${obj[property1]} (${obj[property2]})`;
      if (!acc[combined]) {
        // acc[key] = [];
        acc[combined] = [];
      }
      // Add object to list for given key's value
      // acc[key].push(obj);
      acc[combined].push(obj);
      return acc;
    }, {});
  }
  const getVendorDetails = data => {
    if (!data) return [];
    const vendor = data.reduce((acc, curr) => {
      return [...acc, ...curr?.newMaterialVendors];
    }, []);
    const label = vendor?.length > 0 ? vendor?.map(val => val.label) : [];
    const unique =
      label?.length > 0
        ? label?.reduce((acc, curr) => {
            if (!acc.includes(curr)) {
              acc.push(curr);
            }
            return acc;
          }, [])
        : [];
    return unique;
  };
  const futureChanges = data => {
    if (!data) return [];
    const final = [];
    data.map(val => {
      for (let i = 0; i < val?.newMaterial?.length; i++) {
        for (let j = 0; j < val?.newMaterialVendors?.length; j++) {
          final.push({...val?.newMaterial[i], ...val?.newMaterialVendors[j]});
        }
      }
    });
    const arr = final.reduce((accum, obj) => {
      const key = obj.label;
      if (!accum[key]) {
        accum[key] = [];
      }
      accum[key].push(obj);
      return accum;
    }, {});

    const allDetails = Object.keys(arr).map(val =>
      arr[val].reduce((acc, obj) => {
        if (!acc.quantity) {
          acc.quantity = 0;
          acc.qtyOwner = 0;
          acc.qtyTenant = 0;
          acc.title = [];
          acc.vender = null;
        }
        const total = parseInt(obj.quantity || 0);
        const onwer = parseInt(obj.qtyOwner || 0);
        const tenantCount = parseInt(obj.qtyTenant || 0);
        acc.quantity += total;
        acc.qtyOwner += onwer;
        acc.qtyTenant += tenantCount;
        acc.title.push({item: obj.item, specification: obj.specification});
        acc.vender = val;
        return acc;
      }, {}),
    );
    return allDetails;
  };

  const getMaterailsDetails = data => {
    if (!data) return [];
    return data.reduce((acc, curr) => {
      return [...acc, ...curr.newMaterial];
    }, []);

    // console.log('dataArr', dataArr, getAllCounts(dataArr));
  };

  const getAllCounts = data => {
    if (!data)
      return {
        quantity: 0,
        qtyOwner: 0,
        qtyTenant: 0,
      };
    return data.reduce((acc, curr) => {
      if (!acc.quantity) {
        acc.quantity = 0;
        acc.qtyOwner = 0;
        acc.qtyTenant = 0;
      }
      const total = parseInt(curr.quantity || 0);
      const onwer = parseInt(curr.qtyOwner || 0);
      const tenantCount = parseInt(curr.qtyTenant || 0);
      acc.quantity += total;
      acc.qtyOwner += onwer;
      acc.qtyTenant += tenantCount;
      return acc;
    }, {});
  };
  const quantityReduce = (payedBy, type) => {
    if (!payedBy[type]) {
      return {
        qty: 0,
        price: 0,
      };
    }
    return payedBy[type].reduce((acc, obj) => {
      if (!acc.qty) {
        acc.qty = 0;
      }
      if (!acc.price) {
        acc.price = 0;
      }
      const quantiy = parseFloat(obj.qty);
      const cost = parseInt(obj.price);
      acc.qty += quantiy;
      acc.price += cost * quantiy;

      return acc;
    }, {});
  };
  const callValidation = () => {
    const filteredArr = [];
    const materialArr = [];
    console.log({aa: listDamage.list_damage});
    listDamage.list_damage.category.map((damages, index) =>
      damages.questions.map(question => {
        checjlistObj?.answer?.sub_categery[damages.sub_categery].data.map(
          sub => {
            if (
              question.id_template_mapping in checjlistObj?.answer &&
              sub in checjlistObj?.answer[question.id_template_mapping]
            ) {
              if (
                checjlistObj?.answer[question.id_template_mapping][sub]
                  .damage === 0
              ) {
                // filteredArr.push({
                //   payBy:
                //     checjlistObj?.answer[question.id_template_mapping][sub]
                //       .payBy == 3
                //       ? owner
                //       : checjlistObj?.answer[question.id_template_mapping][sub]
                //           .payBy == 4
                //       ? tenant
                //       : 'owner_tenant',
                //   materialList:
                //     checjlistObj?.answer[question.id_template_mapping][sub]
                //       .materialList,
                // });
              }
            }
          },
        );
      }),
    );
    // listDamage.list_damage.category.map((damages, index) =>
    //   damages.questions.map((question) => {
    //     checjlistObj?.answer?.sub_categery[damages.sub_categery].data.map(
    //       (sub) => {
    //         if (
    //           question.id_template_mapping in checjlistObj?.answer &&
    //           sub in checjlistObj?.answer[question.id_template_mapping]
    //         ) {
    //           if (
    //             checjlistObj?.answer[question.id_template_mapping][sub]
    //               .damage === 0 &&
    //             checjlistObj?.answer[question.id_template_mapping][sub]
    //               ?.newMaterial != null &&
    //             checjlistObj?.answer[question.id_template_mapping][sub]
    //               ?.newMaterial?.length > 0
    //           ) {
    //             materialArr.push({
    //               newMaterial:
    //                 checjlistObj?.answer[question.id_template_mapping][sub]
    //                   .newMaterial,
    //               newMaterialVendors: checjlistObj?.answer[
    //                 question.id_template_mapping
    //               ][sub]?.newMaterialVendors
    //                 ? checjlistObj?.answer[question.id_template_mapping][
    //                     sub
    //                   ]?.newMaterialVendors.filter((val) => val.checked)
    //                 : null,
    //             });
    //           }
    //         }
    //       },
    //     );
    //   }),
    // );
    const getAllData = futureChanges(materialArr);
    const materialData = getMaterailsDetails(materialArr);
    const vendorArr = getVendorDetails(materialArr);
    const countObj = getAllCounts(materialData);

    // const finalData = {
    //   materialData,
    //   vendorArr,
    //   countObj,
    // };

    setMaterialArray(getAllData);

    const additionalData = checjlistObj?.answer?.AdditionalComments;

    const AdditionalQty =
      additionalData &&
      Object.keys(additionalData)?.map((a, i) => {
        return {materialList: additionalData[a]?.materialList};
      });

    // console.log("logs .... ", AdditionalQty)

    const payedBy = filteredArr.reduce((acc, obj) => {
      const key = obj.payBy;

      if (!acc[key]) {
        acc[key] = [];
      }
      obj.materialList !== null && acc[key].push(...obj.materialList);
      return acc;
    }, {});

    const AdditionalValue =
      AdditionalQty &&
      AdditionalQty?.reduce((acc, obj) => {
        const key = 'Additional';

        if (!acc[key]) {
          acc[key] = [];
        }
        obj.materialList !== null && acc[key].push(...obj.materialList);
        return acc;
      }, {});

    // console.log("payedBy", payedBy, AdditionalValue)

    const {qty: ownerQty = 0, price: ownerPrice = 0} = quantityReduce(
      payedBy,
      owner,
    );

    const {qty: tenantQty = 0, price: tenantPrice = 0} = quantityReduce(
      payedBy,
      tenant,
    );

    const {
      qty: ownerTenantQty = 0,
      price: ownerTenantPrice = 0,
      qtyOwner: qtyOwnerValue = 0,
      qtyTenant: qtyTenantValue = 0,
      priceOwner = 0,
      priceTenant = 0,
    } = quantityReduceOwnerTenant(
      payedBy,
      'owner_tenant',
      priceOwner,
      priceTenant,
    );

    // if()
    // const { qty: additionalQty, price: additionalPrice } = quantityReduce(
    //   AdditionalValue,
    //   'Additional',
    // );

    const {qty: additionalQty = 0, price: additionalPrice = 0} =
      quantityReduceOwnerTenant(
        AdditionalValue !== undefined && AdditionalValue,
        'Additional',
      );

    setQuantityObj({
      ownerQty,
      ownerPrice,
      tenantQty,
      tenantPrice,
      totalQtr: ownerQty + tenantQty + ownerTenantQty + additionalQty,
      totalPrice: ownerPrice + tenantPrice + ownerTenantPrice + additionalPrice,
      // ownerTenantQty,
      // ownerTenantPrice,
      qtyOwnerValue,
      qtyTenantValue,
      priceOwner,
      priceTenant,
      payedBy,
      additionalData,
      // additionalQty,
      // additionalPrice
    });
  };
  const quantityReduceOwnerTenant = (payedBy, type) => {
    // console.log('quantityReduce', payedBy, type);

    if (payedBy && !payedBy[type]) {
      return {
        qty: 0,
        price: 0,
        qtyOwner: 0,
        qtyTenant: 0,
        priceOwner: 0,
        priceTenant: 0,
        additionalQty: 0,
        additionalPrice: 0,
      };
    }
    return (
      payedBy &&
      payedBy[type].reduce((acc, obj) => {
        if (!acc.qty) {
          acc.qty = 0;
        }
        if (!acc.price) {
          acc.price = 0;
        }

        if (!acc.qtyOwner) {
          acc.qtyOwner = 0;
        }
        if (!acc.qtyTenant) {
          acc.qtyTenant = 0;
        }
        if (!acc.priceOwner) {
          acc.priceOwner = 0;
        }
        if (!acc.priceTenant) {
          acc.priceTenant = 0;
        }
        if (!acc.additionalQty) {
          acc.additionalQty = 0;
        }
        if (!acc.additionalPrice) {
          acc.additionalPrice = 0;
        }

        const quantiy = parseFloat(obj?.qty || 0);
        const cost = parseInt(obj?.price || 1);
        acc.qty += quantiy;
        acc.price += cost * quantiy;

        if (type === 'owner_tenant') {
          const qtyOwnerValue = parseInt(obj?.qtyOwner || 0);
          const qtyTenantValue = parseInt(obj?.qtyTenant || 0);
          acc.qtyOwner += qtyOwnerValue;
          acc.qtyTenant += qtyTenantValue;
          acc.priceOwner += cost * qtyOwnerValue;
          acc.priceTenant += cost * qtyTenantValue;
        }
        if (type === 'Additional') {
          const qtyAdditionalValue = parseInt(obj?.qty || 0);
          acc.additionalQty += qtyAdditionalValue;
          acc.additionalPrice += cost * qtyAdditionalValue;
        }

        return acc;
      }, {})
    );
  };

  const handleValidateSubmit = () => {
    if(!ownerSign){
      alert('Owner Signature is Missing');
      return;
    }
    if(!inspectorSign){
      alert('Handover Team Signature is Missing');
      return;
    }
    const local = [];
    const fileldValid = [];
    setScrollIndex([]);
    setRefName([]);

    // const data = checjlistObj?.answer

    // Object.keys(data?.AdditionalComments).map((a) => {
    //   // return (
    //   setAdditionalValueKey(a)
    //   // )
    // })
    let isReqAddCommentValid = false;
    const additionalCommentId = 34;
    const additionalCommentQuestion = 205;
    listDamage.list_damage.category
      .filter(val => val.id_template_category == additionalCommentId)
      .map((he, index) => {
        he.questions.map(da => {
          const valid = checjlistObj?.answer?.sub_categery[
            he.sub_categery
          ].data.map(sub => {
            if (
              da.id_template_mapping in checjlistObj?.answer &&
              sub in checjlistObj?.answer[da.id_template_mapping]
            ) {
              isReqAddCommentValid = true;
            }
          });
        });
      });
    const validations = listDamage.list_damage.category;

    const isValid = validations
      .map((he, index) =>
        he.questions.map(da => {
          const valu = checjlistObj?.answer?.sub_categery[
            he.sub_categery
          ].data.map(sub => {
            if (
              da.id_template_mapping in checjlistObj?.answer &&
              sub in checjlistObj?.answer[da.id_template_mapping]
            ) {
              //this is just for additional comment
              let count = 0;
              if (da.id_template_mapping == additionalCommentQuestion) {
                console.log(
                  'iner loop',
                  sub,
                  da.id_template_mapping,
                  `${he.sub_categery} ${sub}`,
                );
                if (
                  checjlistObj?.answer[da.id_template_mapping][sub].taskType ===
                    null ||
                  checjlistObj?.answer[da.id_template_mapping][sub].taskType ===
                    0
                ) {
                  fileldValid.push({
                    category: `${he.sub_categery} ${sub}`,
                    ques: da.question,
                    field: 'taskType',
                  });

                  count++;
                }
                if (
                  checjlistObj?.answer[da.id_template_mapping][sub].comment ===
                  null
                ) {
                  fileldValid.push({
                    category: `${he.sub_categery} ${sub}`,
                    ques: da.question,
                    field: 'title',
                  });

                  count++;
                }
                if (
                  checjlistObj?.answer[da.id_template_mapping][sub].image ===
                  null
                ) {
                  fileldValid.push({
                    category: `${he.sub_categery} ${sub}`,
                    ques: da.question,
                    field: 'image',
                  });
                  // setScrollIndex(prev=>[...prev,index])
                  // scrollIndex.push(index);
                  count++;
                }
              }
              // till here
              if (
                da.id_template_mapping != additionalCommentQuestion &&
                checjlistObj?.answer[da.id_template_mapping][sub].damage === 0
              ) {
                if (
                  checjlistObj?.answer[da.id_template_mapping][sub].taskType ===
                    null ||
                  checjlistObj?.answer[da.id_template_mapping][sub].taskType ===
                    0
                ) {
                  fileldValid.push({
                    category: `${he.sub_categery} ${sub}`,
                    ques: da.question,
                    field: 'taskType',
                  });
                  // setScrollIndex(prev=>[...prev,index])
                  // scrollIndex.push(index);
                  count++;
                }

                if (
                  checjlistObj?.answer[da.id_template_mapping][sub].image ===
                  null
                ) {
                  fileldValid.push({
                    category: `${he.sub_categery} ${sub}`,
                    ques: da.question,
                    field: 'image',
                  });
                  // setScrollIndex(prev=>[...prev,index])
                  // scrollIndex.push(index);
                  count++;
                }

                if (count > 0) {
                  setScrollIndex(prev => [...prev, index]);
                  setRefName(prev => [
                    ...prev,
                    {
                      id: da.id_template_mapping,
                      parent: he.id_template_category,
                      category: he.sub_categery,
                      index,
                      repeat: sub,
                    },
                  ]);
                }
                return fileldValid;
              }
              return true;
            }
            // scrollIndex.push(index);
            local.push({
              category: `${he.sub_categery} ${sub}`,
              ques: da.question,
            });
            return `${he.sub_categery} ${sub}`;
          });
          return valu;
        }),
      )
      .flat()
      .flat();

    const queErr = isValid.filter(f => f !== true);
    const groupedcategory = groupBy(local, 'category');
    // const groupedField = groupBy(fileldValid, 'ques');
    const groupedField = groupBySplit(fileldValid, 'ques', 'category');
    if (scrollIndex.length > 0) {
      setScrollToCategory(scrollIndex[0]);
    }
    // if (local.length > 0) {
    //   setValidErrData(groupedcategory);
    //   setValidErr(true);
    //   setvalidErrDataforField(false);
    //   setshowErrBor(true);
    // } else
    if (fileldValid.length > 0) {
      setValidErrData(groupedField);
      setValidErr(true);
      setvalidErrDataforField(true);
      setshowErrBor(true);
    } else if (local.length > 0 && !continueInsp) {
      setValidErrData(groupedcategory);
      setValidErr(true);
      setvalidErrDataforField(false);
      setshowErrBor(true);
    } else {
      setshowErrBor(false);
      if (disabled) {
        console.log({disabled});
        disabled = false;
        setQuantity(false);
        const data = {
          damage_inspection: chklistQuesData[idInsBooking],
          id_user: userData.id_user,
          id_property: taskDetail[0]?.id_unit,
          ownerSignature:ownerSign,
          handoverSignature:inspectorSign,
        
          id_service_category:
            listDamage.inspection_data[0].id_service_category,
          id_snag_property: idInsBooking,
          // inspection_remarks:
          //   type == 'Dashboard' ? 'Snagging Inspection Finished' : '',
          // // id_service_category:42,
        };
        dispatch(saveOwnerWorkStatus(data));
        if (saveLocation?.latitude && saveLocation?.longitude) {
          dispatch(
            saveLocationProperty({
              ...saveLocation,
              id_snag_property: taskDetail[0]?.id_snag_property,
            }),
          );
        }
      }
    }
  };

  const onContinue = () => {
    // if (disabled) {
    //   console.log({disabled});
    //   disabled = false;
    //   setQuantity(false);
    //   const data = {
    //     damage_inspection: chklistQuesData[idInsBooking],
    //     id_user: userData.id_user,
    //     id_property: taskDetail[0]?.id_unit,
    //     tenantSign,
    //     inspectorSign,
    //     id_service_category: listDamage.inspection_data[0].id_service_category,
    //     id_snag_property: idInsBooking,
    //     inspection_remarks:
    //       type == 'Dashboard' ? 'Snagging Inspection Finished' : '',
    //     // id_service_category:42,
    //   };
    //   dispatch(saveOwnerWorkStatus(data));
    //   if (saveLocation?.latitude && saveLocation?.longitude) {
    //     dispatch(
    //       saveLocationProperty({
    //         ...saveLocation,
    //         id_snag_property: taskDetail[0]?.id_unit,
    //       }),
    //     );
    //   }
    // }
  };

  const openTitleModal = value => {
    setTitleModalText(value);
    setTitleModal(true);
  };
  const closeTitleModal = () => {
    setTitleModalText(null);
    setTitleModal(false);
  };

  const continueWithoutAns = () => {
    callValidation();
    // setQuantity(true);
    setIosModalOpen(true);
    setContinueInsp(true);
    setValidErr(false);
    disabled = true;
  };

  const listFooter = () => {
    return (
      <>
       <TouchableWithoutFeedback onPress={() => setSignModal(true)}>
              <View>
                <View
                  style={[
                    styles.SignSty,
                    {
                      backgroundColor: theme.color.secondaryLight2,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    },
                  ]}>
                  <Text>Owner Signature</Text>
                  {ownerSign && (
                    <View
                      style={[
                        styles.circle,
                        {
                          backgroundColor: theme.color.green,
                          // justifyContent: 'flex-end',
                        },
                      ]}>
                      <Ticked height={10} color='red'/>
                    </View>
                  )}
                </View>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => setSignModalInspector(true)}>
              <View
                style={[
                  styles.SignSty,
                  {
                    backgroundColor: theme.color.secondaryLight2,
                    flexDirection: 'row',
                  },
                ]}>
                <Text>Handover Team Signature*</Text>
                {inspectorSign && (
                  <View
                    style={[
                      styles.circle,
                      {backgroundColor: theme.color.green},
                    ]}>
                    <Ticked height={10} />
                  </View>
                )}
              </View>
            </TouchableWithoutFeedback>
        <View style={styles.finishStyle}>
          <Button
            onPress={() => handleValidateSubmit()}
            title="FINISH"
            style={styles.button1}
            type="primary"
            titleStyle={{...getAppFont('Bold')}}
          />
        </View>
      </>
    );
  };
  const containerProps = {
    navbar: {
      title: 'Hand Over Review',
    },
    scrollEnabled: false,
  };
  // console.log('listDamage', checjlistObj?.answer)
  return (
    <CommonContainer {...containerProps}>
      <View style={styles.container}>
        <View
          style={[
            styles.row,
            styles.spacing,
            {
              // paddingBottom: 11,
              borderBottomColor: theme.color.secondaryLight2,
              marginBottom: theme.spacing.m,
              // paddingTop: 11,
            },
          ]}>
          <View style={styles.left}>
            <Text variant="bodyDark">{taskDetail[0]?.unit_name}</Text>
          </View>
          <View style={styles.right}>
            <Text variant="bodyDark" style={styles.rightText}>
              {taskDetail[0]?.reference_number}
            </Text>
          </View>
        </View>
        {loading && <Loader open={loading} />}
        <View style={{flex: 1}}>
          <FlatList
            data={listDamage.list_damage.category}
            ref={flatListScrollRef}
            onScrollToIndexFailed={scrollToIndexFailed}
            ListFooterComponent={listFooter()}
            showsVerticalScrollIndicator
            persistentScrollbar
            renderItem={({item}) => {
              return (
                <Accordian
                  title={item.category_question}
                  question={item.questions}
                  key={item.id_template_category}
                  listDamage={listDamage}
                  taskDetail={taskDetail}
                  is_multiple={item.is_multiple}
                  sub_categery={item.sub_categery}
                  idInsBooking={idInsBooking}
                  showBorder={showErrBor}
                  parentId={item?.id_template_category}
                  parentSelected={parentSelected}
                  categorySelected={categorySelected}
                  listCount={listCount}
                  changeState={changeState}
                  disableTenant={disableTenant}
                  isAdditionalComment={
                    item?.id_template_mapping === additionalCommentsId
                  }
                  additionalCommentsId={additionalCommentsId}
                />
              );
            }}
          />
        </View>

        <ConfirmModal
          open={confirm}
          close={() => setConfirm(false)}
          title="As this is Vacant unit, all items will be charged to owner"
          onSuccess={() => setConfirm(false)}
          onFailure={() => setConfirm(false)}
          showContinue={false}
          showFailure={false}
          sucessText="OK"
        />
        {/* <SuccessModal
          open={confirm}
          close={() => setConfirm(false)}
          title={
            'all items will be charged to owner'
          }
        /> */}
       
        <SuccessModal
          open={modal}
          close={() => navToQuotation()}
          title={
            checjlistObj?.answer?.newMaterial?.newMaterialData.data.length > 0
              ? 'Inspection Complete and Quotation Request Sent Successfuly'
              : 'Inspection Report Sent Successfully'
          }
        />
          <SignModal
          open={signModal}
          close={() => setSignModal(!signModal)}
          saveSign={sgn => setOwnerSign(sgn)}
        />
        <SignModal
          open={signModalInspector}
          close={() => setSignModalInspector(!signModalInspector)}
          saveSign={sgn => setInspectorSign(sgn)}
        />
        <ValidationModal
          open={validErr}
          close={() => validErrModal()}
          continueWithoutAns={() => continueWithoutAns()}
          title="Please answer all the questions and fields which are marked with * symbol"
          data={validErrData}
          validErrDataforField={validErrDataforField}
          scrollToParent={scrollToParent}
        />
        <QuantityModal
          open={quantity}
          materialArray={materialArray}
          close={quantity => setQuantity(!quantity)}
          data={quantityObj}
          onContinue={onContinue}
          // disabled={disabled}
          openTitleModal={text => openTitleModal(text)}
        />
        <TitleModal
          modal={titleModal}
          text={titleModalText}
          closeModal={() => closeTitleModal()}
        />
        {/* <Spinner
          visible={saveDamageloading || materialRateCardLoading}
          size="large"
          animationType="none"
        /> */}
        <Spinner visible={showLoader} size="large" animationType="none" />
      </View>
    </CommonContainer>
  );
};

export default HandOverReview;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
  },
  finishStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    marginTop: 15,
    paddingBottom: 40,
  },

  row: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // flexWrap: 'wrap',
  },
  left: {
    flex: 0.45,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  right: {
    flex: 0.55,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  rightText: {
    textAlign: 'right',
  },
  commentbox: {
    borderWidth: 1,
    height: Platform.isPad ? 110 : 70,
    // marginLeft: 15,
    // marginRight: 15,
    marginBottom: 10,
    marginTop: 10,
    alignItems: 'flex-start',
  },
  txtFieldStyle: {
    textAlignVertical: 'top',
    alignItems: 'flex-start',
    ...getAppFont('Regular'),
    fontSize: Platform.isPad ? 18 : 13,
  },
  txtfldcontainstyle: {
    borderBottomWidth: 0,
  },
  SignSty: {
    borderRadius: Platform.isPad ? 10 : 30,
    padding: Platform.isPad ? 18 : 12,
    marginBottom: 17,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  circle: {
    width: 25,
    height: 25,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    paddingBottom: 15,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
    borderWidth: 0.3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20,
    elevation: 3,
    paddingTop: 15,
  },
  // button1: {
  //   width: Platform.isPad ? 300 : 100,
  //   // marginRight: 5,
  //   padding: Platform.isPad ? 25 : 10,
  //   borderRadius: 10,
  // },
  button1: {
    width: config.DEVICE_WINDOW_WIDTH - 200,
    marginRight: Platform.isPad ? 15 : 5,
    padding: Platform.isPad ? 20 : 10,
    borderRadius: 10,
  },
  appFontReg: {
    ...getAppFont('SemiBold'),
  },
  dropDownContainer: {
    justifyContent: 'space-between',
    left: 0,
    height: 46,
    borderRadius: 10,
  },
  roundStyl: {
    height: 30,
    width: 30,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
