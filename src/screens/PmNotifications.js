import React, { useState } from 'react';
import {
  View,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import { CommonContainer, Button, Text, InputText, } from '_components';
import { NotificationIcon, Cancel } from '_svg';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { getAppFont, useTheme } from '../utils';
import TabView from "../components/ui-elements/TabView";
const left = 'left';
const right = 'right';
const PmNotifications = ({ navigation: { goBack } }) => {
  const theme = useTheme();
    const { navigate } = useNavigation();

    const [selected, selectedTab] = useState(1);
    const tabTitles = [
        { name: 'Notification Sent', value:1, type:'notification_sent' , activeBar:theme.color.primary,},
        { name: 'Notification Received', value: 2, type:'notification_received', activeBar:theme.color.primary },
    ];

    const [commentSent, setCommentSent] = useState('Key is missing');
  const [commentRec, setCommentRec] = useState(
    'Key is ready you can go and collect at security Gate',
  );
  const containerProps = {
    navbar: {
      type: 'outer',
      title: '   ',
      actions: [
        {
          icon: <NotificationIcon />,
        },
      ],
    },
    scrollEnabled: true,
    onModalPress: () => {
      // onRefresh();
    },
  };
  const selectOne = () => {
    setIndex(left);
  };
  const selectTwo = () => {
    setIndex(right);
  };

  return (
    <CommonContainer navbar={{ title: 'PM Notifications' }}>
      <View
        style={[
          styles.container,
          { backgroundColor: theme.color.primaryWhite },
        ]}
      >
        <View style={{paddingTop:10,marginBottom:20}}>
        <TabView onTabChange={selectedTab} tabs={tabTitles} selected={selected} />
        </View>
        {selected === 1 && (
          <>
            <View style={[styles.row, styles.leftRow]}>
              <View style={styles.left}>
                <Text variant="textRegular" size={16}>
                  Community Name
                </Text>
              </View>
              <View style={styles.right}>
                <Text variant="bodyDark" size={16}>
                  Al Jaddaf
                </Text>
              </View>
            </View>
            <View style={[styles.row, styles.leftRow]}>
              <View style={styles.left}>
                <Text variant="textRegular" size={16}>
                  Unit Number
                </Text>
              </View>
              <View style={styles.right}>
                <Text variant="bodyDark" size={16}>
                  #1234
                </Text>
              </View>
            </View>
            <View>
              <View style={[styles.left, styles.leftRow]}>
                <Text variant="textRegular" size={16}>
                  Comment
                </Text>
              </View>
              <View style={styles.right}>
                <InputText
                  type="textArea"
                  textFieldContainerStyle={{
                    borderColor: theme.color.secondaryLight2,
                  }}
                  style={{fontWeight:'bold'}}
                  value={commentSent}
                  onChangeText={(text) => setCommentSent(text)}
                />
              </View>
            </View>
            <View style={{ marginHorizontal: 30 }}>
              <Button
                title="Done"
                type="primary"
                style={[styles.button1, { marginBottom: 100 }]}
                onPress={()=> navigate('Tasks')}
              />
            </View>
          </>
        )}
        {selected === 2 && (
          <>
            <View style={[styles.row, styles.leftRow]}>
              <View style={styles.left}>
                <Text variant="textMedium" size={16}>
                  Community Name
                </Text>
              </View>
              <View style={styles.right}>
                <Text variant="bodyDark" size={16}>
                  Al Jaddaf
                </Text>
              </View>
            </View>
            <View style={[styles.row, styles.leftRow]}>
              <View style={styles.left}>
                <Text variant="textMedium" size={16}>
                  Unit Number
                </Text>
              </View>
              <View style={styles.right}>
                <Text variant="bodyDark" size={16}>
                  #1234
                </Text>
              </View>
            </View>
            <View>
              <View style={[styles.left, styles.leftRow]}>
                <Text variant="textRegular" size={16}>
                  Comment
                </Text>
              </View>
              <View style={styles.right}>
                <InputText
                  type="textArea"
                  textFieldContainerStyle={{
                    borderColor: theme.color.secondaryLight2,
                  }}
                  value={commentRec}
                  onChangeText={(text) => setCommentRec(text)}
                  numberOfLines={4}
                />
              </View>
            </View>
            <View style={{ marginHorizontal: 30 }}>
              <Button
                title="Done"
                type="primary"
                style={[styles.button1, { marginBottom: 100 }]}
                onPress={()=> navigate('Tasks')}
              />
            </View>
          </>
        )}
      </View>
    </CommonContainer>
  );
};

export default PmNotifications;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  row: {
    flexDirection: 'row',
  },
  center: {
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
    marginBottom: 20,
  },
  left: {
    flex: 1,
  },
  border: {
    position: 'absolute',
    bottom: 0,

    borderBottomWidth: 2,
  },
  right: {
    flex: 1,
  },
  leftRow: {
    paddingBottom: 20,
  },
});
