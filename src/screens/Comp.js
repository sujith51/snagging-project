// @flow
import React from 'react';
import { View, Text } from 'react-native';
import { InputText } from '_components';
import { User, Password } from '_svg';
import Button from '../components/Button';
import TaskCard from '../components/TaskCard';
import { theme } from '../utils';
const DATA = [
  { name: 'Inpection Date', value: '03/09/2020' },
  { name: 'sd amount', value: 'aed 1200' },
  { name: 'Inpection status', value: 'pending' },
  { name: 'Inpection type', value: 'move in' },
];
const Comp = () => {
  return (
    <View
      style={{
        height: 200,
      }}
    >
      {/* <Button title="hi" style={{}} /> */}
      <TaskCard data={DATA} headerRight="# 123" headerLeft="Al Jaddaf" />
    </View>
  );
};

export default Comp;
