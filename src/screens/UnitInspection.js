/* eslint-disable react-native/no-inline-styles */
/* eslint-disable import/no-unresolved */
import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,ScrollView,TouchableOpacity
} from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import moment from 'moment';
import {
  CommonContainer,
  Accordian,
  Text,
  Button,
  SuccessModal,
  Spinner,
  InputText,
  DropDown,
  DateSelector,
  TimeSelector,
    CalendarModal,TimeModal
} from '_components';
import { usePrevious } from '_hooks';
import { useDispatch, useSelector } from 'react-redux';
import { useTheme, getAppFont } from '../utils';
import {
  savechklistquesctions,
  saveDamageInspection,
  deletesubmittedIns,
  saveLocationProperty,
} from '../redux/actions';
import SignInspector from '../components/SignModal';

const UnitInspection = () => {
  const [success, setSuccess] = useState(false);
  const [openDate, setOpenDate] = useState(false);
  const [openTime, setOpenTime] = useState(false);
  const [community, setCommunity] = useState(null);
  const [building, setBuilding] = useState(null);
  const [unit, setUnit] = useState(null);
  const [name, setName] = useState(null);
  const [phone, setPhone] = useState(null);
  const [description, setDescription] = useState(null);
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'));
  const [time, setTime] = useState(new Date());
  const theme = useTheme();
  const dispatch = useDispatch();
  const { navigate } = useNavigation();

  const { data, selectCommunity, selectUnit, communityList } = useSelector(
    (state) => state.user,
  );
  const onSubmit=()=>{
      setSuccess(true)
  }
  const calendarProps={
    calendar:{
        current:date,
        minDate:moment().format('YYYY-MM-DD')
    }
  }
  const changeDate=(date)=>{
      setDate(date)
  }
  const changeTime=(date)=>{
      setOpenDate(date)
  }
  return (
    <CommonContainer scrollEnabled={false}>
      <SuccessModal
          open={success}
          close={() => setSuccess(false)}
          title='Success'
      />
      <CalendarModal
          open={openDate}
          close={()=>setOpenDate(false)}
          title={'Select Date'}
          {...calendarProps}
          changeDate={changeDate}
      />
      <TimeModal
          open={openTime}
          close={()=>setOpenTime(false)}
          title={'Select Time'}
          changeDate={changeTime}
      />
      <ScrollView

        style={styles.container}
      >
        <Text style={styles.pb5} variant="taskBold">
          Select Community
        </Text>
        <DropDown
          options={Object.keys(communityList?.data).map((item) => ({
            label: communityList?.data[item],
            value: item,
          }))}
          placeHolderStyle={{
            color: theme.color.primaryWhite,
            ...getAppFont('Bold'),
          }}
          drDownMdlwidth
          contStyle={[
            styles.dropDownContain,
            { backgroundColor: theme.color.secondaryDark3 },
          ]}
          iconColor={theme.color.primaryWhite}
          onClose={(itm) => setCommunity(itm)}
        />
        <Text style={styles.ptb5} variant="taskBold">
          Select Building
        </Text>
        <DropDown
          options={Object.keys(communityList?.data).map((item) => ({
            label: communityList?.data[item],
            value: item,
          }))}
          placeHolderStyle={{
            color: theme.color.primaryWhite,
            ...getAppFont('Bold'),
          }}
          drDownMdlwidth
          contStyle={[
            styles.dropDownContain,
            { backgroundColor: theme.color.secondaryDark3 },
          ]}
          iconColor={theme.color.primaryWhite}
          onClose={(itm) => setBuilding(itm)}
        />
        <Text style={styles.ptb5} variant="taskBold">
          Select Unit
        </Text>
        <DropDown
          options={Object.keys(communityList?.data).map((item) => ({
            label: communityList?.data[item],
            value: item,
          }))}
          placeHolderStyle={{
            color: theme.color.primaryWhite,
            ...getAppFont('Bold'),
          }}
          drDownMdlwidth
          contStyle={[
            styles.dropDownContain,
            { backgroundColor: theme.color.secondaryDark3 },
          ]}
          iconColor={theme.color.primaryWhite}
          onClose={(itm) => setUnit(itm)}
        />
        <InputText
            editable={false}
          placeholder="Enter Name"
          onChangeText={(text) => setName(text)}
          value={name}
          type = 'default'
          style={[
            styles.commentbox,

          ]}
        />
        <InputText
          placeholder="Enter Phone"
          editable={false}
          onChangeText={(text) => setPhone(text)}
          value={phone}
          type = 'default'
          style={[
            styles.commentbox,
              {marginBottom:10}
          ]}
        />
        <InputText
            placeholder="Description"
            onChangeText={(text) => setDescription(text)}
            value={description}
            type = 'textArea'
            style={[
                styles.commentbox,
                {marginBottom:10}
            ]}
        />
        <Text style={styles.ptb5} variant="taskBold">
          Select Date
        </Text>
        <TouchableOpacity onPress={()=>setOpenDate(true)} style={styles.viewcontainer}>
          <Text>{date}</Text>
        </TouchableOpacity>

        {/*<DateSelector*/}
          {/*icon*/}
          {/*Date={date}*/}
          {/*placeholder="Date"*/}
          {/*dateFormat="YYYY-MM-DD"*/}
          {/*fontStyle={{ color: 'white' }}*/}
          {/*viewStyle={{ backgroundColor: theme.color.primary, borderRadius: 10 }}*/}
          {/*style={styles.dateStyle}*/}
          {/*onChangeDate={(toDate) => setDate(toDate)}*/}
          {/*minDate={moment().format('YYYY-MM-DD')}*/}
        {/*/>*/}
        <Text style={styles.ptb5} variant="taskBold">
          Select Time
        </Text>
        <TouchableOpacity onPress={()=>setOpenTime(true)} style={styles.viewcontainer}>
          <Text>time</Text>
        </TouchableOpacity>
        {/*<TimeSelector*/}
          {/*time={time}*/}
          {/*placeholder="From Time"*/}
          {/*onChangeDate={(toDate) => setTime(toDate)}*/}
        {/*/>*/}
        <View style={styles.finishStyle}>
          <Button
            onPress={() => onSubmit()}
            title="FINISH"
            style={styles.button1}
          />
        </View>
      </ScrollView>
    </CommonContainer>
  );
};

export default UnitInspection;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
  },
  dropDownContain: {
    justifyContent: 'space-between',
    left: 0,
    height: 46,
    borderRadius: 10,
  },
  pb5: {
    paddingBottom: 5,
  },
  ptb5: {
    paddingTop: 5,
    paddingBottom: 5,
  },
  commentbox: {
    // borderWidth: 1,
    // height: 60,
    marginTop: 15,
    alignItems: 'flex-start',
  },
  finishStyle: {

    marginBottom: 20,
    marginTop: 15,
  },
  button1: {
    // width: 100,
    // marginRight: 5,
    // height:200,
    padding: 10,
    borderRadius: 10,
  },
  dateStyle: { width: '100%', marginBottom: 20 },
    viewcontainer:{
      paddingHorizontal:15,
        paddingVertical:15,
        borderRadius:5,
        borderWidth:1,
        borderColor:'grey',
        justifyContent:'center',
        marginBottom:10
  }
});
