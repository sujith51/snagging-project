/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  pushNotificationData,
  notificationDetailData,
  getTaskDetails,
} from '_actions';
import { usePrevious } from '_hooks';
import { CommonContainer, Spinner, Text } from '_components';
import { NotificationIcon, Cancel, SearchIcon } from '_svg';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import {
  getAppFont,
  useTheme,
  stripHtmlTags,
  returnWithoutStatic,
} from '../utils';

const SIZE = 48;

const Notifications = ({ navigation: { goBack } }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const [getNotification, setNotification] = useState();

  const {
    pushNotification: {
      data: notificationData,
      loading: pushNotificatonDataloading,
      idIns: IdInsData,
      error: { status: pushNotificatonDataError },
    },
    // notificationDetail: { detailData, nDetailLoading, nDetailError },
    // pushNotification,
  } = useSelector((state) => state.notificationData);

  const {
    data: { id_user = null, email = null, id_role = null },
    selectCommunity,
    pushToken,
  } = useSelector((state) => state.user);
  const {
    taskDetails: {
      data: taskDetailsData,
      loading: taskDetailsloading,
      error: { status: taskDetailsError },
    },
  } = useSelector((state) => state.taskdetails);

  const prevtaskDetailsloading = usePrevious(taskDetailsloading);

  useEffect(() => {
    if (selectCommunity?.length > 0) {
      // const data = {
      //   id_community: 4,
      //   id_user,
      //   device_token: pushToken,
      // };
      // dispatch(pushNotificationData(data));
      // } else {
      const data = {
        // id_community_arr: returnWithoutStatic(selectCommunity),
        id_user,
        device_token: pushToken,
      };
      dispatch(pushNotificationData(data));
    }
  }, []);

  useEffect(() => {
    if (
      prevtaskDetailsloading === true &&
      taskDetailsloading === false &&
      taskDetailsData?.length > 0
    ) {
      navigate('TaskDetails', {
        taskDetail: taskDetailsData,
      });
    } else if (
      (prevtaskDetailsloading === true &&
        taskDetailsloading === false &&
        taskDetailsData?.length === 0) ||
      taskDetailsError
    ) {
      alert('Something went wrong!');
    }
  }, [
    taskDetailsData,
    taskDetailsloading,
    taskDetailsError,
    prevtaskDetailsloading,
    navigate,
  ]);

  useEffect(() => {
    const dataMsg = [];
    const units = [];
    if (notificationData) {
      notificationData.forEach((key) => {
        const timePM = key.created_on && key.created_on.split(' ');
        const timeAm = moment(timePM.length && timePM[1], ['HH:mm']).format(
          'hh:mm A',
        );
        dataMsg.push({
          name: key.notification,
          image: require('../assets/icons/man.png'),
          time: timeAm,
          navScreen: key.event_code,
          id_notifications: key.id_notifications,
          id_community: key.id_community,
          id_user: key.id_user,
          id_ins: key.id_ins,
        });
      });
      setNotification(dataMsg);
    }
  }, [notificationData]);

  const notificationClicked = (id_ins) => {
    if (id_ins) {
      const data = {
        id_snag_property: id_ins,
      };
      dispatch(getTaskDetails(data));
    }
  };

  const containerProps = {
    navbar: {
      type: 'outer',
      title: '   ',
      actions: [
        // {
        //   icon: <SearchIcon />,
        //   // onPress: () => navigate('Search'),
        // },
        {
          // icon: <NotificationIcon />,
          // onPress: () => navigate('Notifications'),
        },
      ],
    },
    scrollEnabled: true,
    onModalPress: () => {},
  };
  // onDetailClick = () => {}
  const NavigationCard = ({
    name,
    image,
    time,
    navScreen,
    id_notifications,
    id_community,
    id_user,
    id_ins,
  }) => {
    const onModalClick = () => {
      // let listData = {};
      // listData['id_community'] = id_community;
      // listData['id_user'] = id_user;
      // listData['id_notifications'] = id_notifications;
      switch (navScreen) {
        case 'kh_proceed_with_inspection':
          notificationClicked(id_ins);
          // navigate('TaskDetails');
          break;
        case 'inspection_notification_to_inspector':
          navigate('Dashboard');
          break;
        case 'review_repair_changes':
          navigate('Tasks');
          break;
        default:
          navigate('Dashboard');
      }
      // dispatch(notificationDetailData(listData));
    };
    return (
      <TouchableWithoutFeedback onPress={onModalClick}>
        <View
          style={[
            styles.navContainer,
            {
              borderBottomColor: theme.color.secondaryDark3,
              borderBottomWidth: 1,
            },
          ]}
        >
          <View
            style={[
              styles.imgRow,
              { borderWidth: 1, borderColor: theme.color.primaryDark },
            ]}
          >
            <Image source={image} style={styles.imageView} />
          </View>
          <View style={styles.message}>
            <Text variant="textRegular" size={14} color="primaryWhite">
              {stripHtmlTags(name)}
            </Text>
          </View>
          <View style={styles.timeView}>
            <Text variant="textRegular" size={12} color="primaryWhite">
              {time}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };
  return (
    <CommonContainer
      {...containerProps}
      style={{ backgroundColor: theme.color.primary }}
    >
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableWithoutFeedback onPress={() => goBack()}>
            <Cancel />
          </TouchableWithoutFeedback>
          <Text
            variant="header"
            size={24}
            color="primaryWhite"
            style={{ paddingLeft: 40 }}
          >
            Notifications
          </Text>
        </View>
        <FlatList
          data={getNotification}
          renderItem={({ item }) => <NavigationCard {...item} />}
          keyExtractor={(i, k) => `index${k}`}
          bounces={false}
          ListEmptyComponent={() => {
            return (
              <>
                {!pushNotificatonDataloading && (
                  <View style={styles.midView}>
                    <Text color={theme.color.primaryWhite}>
                      No notifications found
                    </Text>
                  </View>
                )}
              </>
            );
          }}
        />
      </View>
      <Spinner
        visible={pushNotificatonDataloading}
        size="large"
        animationType="none"
      />
    </CommonContainer>
  );
};

export default Notifications;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  navContainer: {
    paddingVertical: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgRow: {
    flex: 0,
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE / 2,
    overflow: 'hidden',
  },
  imageView: {
    flex: 1,
    width: null,
    height: null,
  },
  message: {
    flex: 3,
    paddingLeft: 20,
  },
  timeView: {
    flex: 1,
    alignItems: 'flex-end',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 20,
    paddingLeft: 10,
  },
  midView: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 400,
  },
});
