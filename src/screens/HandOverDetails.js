/* eslint-disable react-native/no-unused-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Linking,
  Alert,
  Platform,
  Text as RNText,
} from 'react-native';
import {useRoute, useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {
  CommonContainer,
  OwnerCard,
  MrdCard,
  Button,
  Card,
  InputText,
  TaskList,
  Spinner,
  SuccessModal,
} from '_components';
import {usePrevious} from '_hooks';
import {getOwnerListDamage, getTaskLogs, keyNotCollected} from '_actions';
import {config, getAppFont, useTheme} from '../utils';
import CheckBox from '../components/ui-elements/CheckBox';
import {getInspectionHistory} from '../redux/actions';

const HandOverDetails = () => {
  const {params} = useRoute();
  const {navigate} = useNavigation();
  const dispatch = useDispatch();
  const {taskDetail, type = 'other'} = params;

  const theme = useTheme();
  // : states
  const [collected, setCollected] = useState(false);
  const [notCollected, setNotCollected] = useState(false);
  const [success, setSuccess] = useState(false);
  const [nav, setNav] = useState(false);
  const [comment, setComment] = useState(null);
  const {
    ownerListDamage: {
      data: listDamageData,
      loading: listDamageLoading,
      error: {status: listDamageError},
    },
    inspectionHistory: {
      data: inspectionData,
      loading: inspectionloading,
      error: {status: inspectionError},
    },
    taskLogs: {
      data: taskLogData,
      loading: taskLogloading,
      error: {status: taskLogError},
    },
    keyNotCollected: {
      data: keyNotCollectedData,
      loading: keyNotCollectedloading,
      error: {status: keyNotCollectedError},
    },
  } = useSelector(state => state.taskdetails);
  console.log({listDamageData});
  const {data: userData} = useSelector(state => state.user);
  const prevlistDamageLoading = usePrevious(listDamageLoading);
  const prevInspectionLoading = usePrevious(inspectionloading);
  const prevTaskLogloading = usePrevious(taskLogloading);
  const prevkeyNotCollectedloading = usePrevious(keyNotCollectedloading);
  // :Effects
  // useEffect(() => {
  //   if (collected) {
  //     dispatch(
  //       getOwnerListDamage({ id_snag_property: taskDetail[0]?.id_snag_property }),
  //     );
  //   }

  // }, []);

  useEffect(() => {
  
    if (
      prevlistDamageLoading === true &&
      listDamageLoading === false &&
      listDamageData &&
      nav
    ) {
      setNav(false);
      navigate('HandOverReview', {
        taskDetail,
        listDamage: listDamageData,
      });
    } else if (
      (prevlistDamageLoading === true &&
        listDamageLoading === false &&
        !listDamageData?.data &&
        nav) ||
      listDamageError
    ) {
      setNav(false);
      Alert.alert('Something went wrong!');
    }
  }, [
    listDamageLoading,
    prevlistDamageLoading,
    listDamageError,
    listDamageData,
    navigate,
    taskDetail,
    nav,
  ]);

  // Navigate to Inspection History if data Exist.
  useEffect(() => {
    if (
      prevInspectionLoading === true &&
      inspectionloading === false &&
      inspectionData
    ) {
      navigate('InspectionHistory');
    } else if (prevInspectionLoading && !inspectionloading && inspectionError) {
      Alert.alert('Error', 'No previous records found.');
    }
  }, [
    inspectionloading,
    prevInspectionLoading,
    inspectionData,
    inspectionError,
    navigate,
  ]);

  useEffect(() => {
    if (prevTaskLogloading && !taskLogloading && taskLogData) {
      navigate('TaskLog');
    } else if (prevTaskLogloading && !taskLogloading && taskLogError) {
      Alert.alert('Error', 'Could not able to found Data. Try later');
    }
  }, [prevTaskLogloading, taskLogloading, taskLogData, taskLogError, navigate]);

  useEffect(() => {
    if (
      prevkeyNotCollectedloading &&
      !keyNotCollectedloading &&
      !keyNotCollectedError
    ) {
      setSuccess(Prev => !Prev);
    } else if (
      prevkeyNotCollectedloading &&
      !keyNotCollectedloading &&
      keyNotCollectedError
    ) {
      Alert.alert('Error', 'Could not able to complete action. Try later');
    }
  }, [
    prevkeyNotCollectedloading,
    keyNotCollectedloading,
    keyNotCollectedError,
  ]);

  const handleCollected = () => {
    setCollected(!collected);
    setNotCollected(false);
  };

  const navToTask = () => {
    setSuccess(Prev => !Prev);
    navigate('Tasks');
  };

  const handleNotCollected = () => {
    setNotCollected(!notCollected);
    setCollected(false);
  };

  const handleMap = (lat, lon) => {
    if (lat && lon) {
      const direction = `http://maps.google.com/maps?daddr=${lat},${lon}`;
      Linking.openURL(direction);
    } else {
      alert('Location Not Available, Try Later.');
    }
  };


  const handleInspectionHistory = item => {
    // console.log('navigate inspection history', item);
    const id = item?.id_snag_property;

    dispatch(getInspectionHistory({id_snag_property: id}));
  };

  const callNext = () => {
    console.log('hdtaskDetail',{taskDetail})
    dispatch(
      getOwnerListDamage({id_snag_property: taskDetail[0]?.id_snag_property}),
    );
    setNav(true);
  };

  const hadleKeyNotCollected = () => {
    const data = {
      id_property: '11679',
      // access_card_count: '1',
      // key_count: '1',
      // handover_to: '1',
      purpose: comment,
      created_by: userData.id_user,
    };
    dispatch(keyNotCollected(data));
  };

  // :Renders
  const taskRender = ({item}) => {
    return (
      <View>
        <OwnerCard
          {...item}
          detailsPage
          type={type}
          cardStyle={[
            styles.cardStyle,
            {padding: theme.spacing.sm, ...theme.cardShadow},
          ]}
          callNext={callNext}
          mapTxtPress={handleMap}
          inspectionHistroyPress={() => handleInspectionHistory(item)}
        />
      </View>
    );
  };

  const containerProps = {
    navbar: {
      title: 'Task Details',
    },
    style: {backgroundColor: theme.color.secondaryLight},
  };
  const collectKeyRender = () => {
    return (
      <Card
        cardStyle={[
          styles.cardStyle,
          styles.paddingtop,
          {padding: theme.spacing.sm},
        ]}>
        <Text style={styles.collectKey}> Collect key from PM/Security</Text>
        <View style={styles.row}>
          <CheckBox
            rigthText="Collected"
            onPress={handleCollected}
            checked={collected}
          />
          <CheckBox
            rigthText="Not Collected"
            onPress={handleNotCollected}
            checked={notCollected}
          />
        </View>
      </Card>
    );
  };
  console.log('taskDetail',taskDetail)
  return (
    <CommonContainer {...containerProps}>
      <View style={styles.container}>
        {/* {taskRender(taskDetail)} */}
        {taskDetail?.length >= 0 && (
          <View>
            <TaskList
              data={taskDetail?.length>=0?taskDetail?.filter((_,i)=>i==0):[]} // to remove duplicare data
              renderItem={taskRender}
            />
          </View>
        )}
        <SuccessModal
          open={success}
          close={() => navToTask()}
          title="Informed to property manager"
        />
        <Spinner
          visible={
            inspectionloading || taskLogloading || keyNotCollectedloading
          }
          size="large"
          animationType="none"
        />
        {/* <Spinner */}
        {/* visible={listDamageLoading} */}
        {/* size="large" */}
        {/* animationType="none" */}
        {/* /> */}
      </View>
    </CommonContainer>
  );
};

export default HandOverDetails;

const styles = StyleSheet.create({
  buttonTxt: {
    ...getAppFont('Bold'),
  },
  buttonTxtNormal: {
    ...getAppFont('Bold'),
  },
  buttonView: {
    alignItems: 'flex-end',
    right: 15,
  },
  buttonContainer: {
    // height: 35,
    borderRadius: 10,
    width: Platform.isPad ? 200 : 100,
    padding: Platform.isPad ? 20 : 5,
  },
  buttonContainerNormal: {
    // height: 35,
    width: 350,
    borderRadius: 10,
    padding: Platform.isPad ? 20 : 5,
  },
  container: {
    flex: 1,
    padding: 10,
    paddingTop: 40,
    marginBottom: 30,
  },
  cardStyle: {
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 35,
  },
  collectKey: {
    ...getAppFont('Bold'),
    fontSize: 18,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 40,
  },
  paddingtop: {
    marginTop: 15,
  },
  commentbox: {
    borderWidth: 1,
    height: 100,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 35,
    alignItems: 'flex-start',
  },
  txtFieldStyle: {
    textAlignVertical: 'top',
    alignItems: 'flex-start',
    ...getAppFont('Regular'),
  },
  txtfldcontainstyle: {
    borderBottomWidth: 0,
  },
  alinCenter: {alignItems: 'center', paddingTop: 40},
});
