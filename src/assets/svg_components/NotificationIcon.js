import * as React from 'react';
import Svg, { Defs, G, Path, Rect, Text, TSpan } from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: filter */

function NotificationIcon({ val = null, props }) {
  return (
    <Svg width={36} height={37} viewBox="0 0 36 37" {...props}>
      <Defs></Defs>
      <G data-name="Group 16479">
        <Path
          d="M27.5 20a2.006 2.006 0 002-2h-4a2.006 2.006 0 002 2zm6.5-6V8.5a6.435 6.435 0 00-5-6.3v-.7a1.5 1.5 0 00-3 0v.7a6.435 6.435 0 00-5 6.3V14l-2 2v1h17v-1zm-2 1h-9V8.5a4.5 4.5 0 019 0z"
          fill="#fff"
        />
        {val && (
          <G data-name="Group 16478">
            <G filter="url(#prefix__a)">
              <Rect
                data-name="indicator base"
                width={17}
                height={15}
                rx={7}
                transform="translate(9 9)"
                fill="#ff6969"
              />
            </G>
            <Text
              data-name={10}
              transform="translate(12 20)"
              fill="#fff"
              fontSize={10}
              fontWeight={700}
            >
              <TSpan x={0} y={0}>
                {val}
              </TSpan>
            </Text>
          </G>
        )}
      </G>
    </Svg>
  );
}

export default React.memo(NotificationIcon);
