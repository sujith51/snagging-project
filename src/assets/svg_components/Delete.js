import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Delete(props) {
    return (
        <Svg width={13.535} height={17.402} viewBox="0 0 13.535 17.402" {...props}>
            <Path
                data-name="Icon material-delete"
                d="M.967 15.469A1.939 1.939 0 002.9 17.4h7.734a1.939 1.939 0 001.934-1.934V3.867H.967zM13.535.969h-3.384L9.185 0H4.351l-.967.967H0V2.9h13.535z"
                fill="#65013d"
            />
        </Svg>
    )
}

export default Delete
