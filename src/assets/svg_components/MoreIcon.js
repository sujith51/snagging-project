import * as React from "react"
import Svg, { G, Circle } from "react-native-svg"

function MoreIcon(props) {
  return (
    <Svg width={7} height={27} viewBox="0 0 7 27" {...props}>
      <G data-name="Group 16505" fill="none" stroke="#65013D" strokeWidth={1.5}>
        <G data-name="Ellipse 605" transform="rotate(-90 13.5 13.5)">
          <Circle cx={3.5} cy={3.5} r={3.5} stroke="none" />
          <Circle cx={3.5} cy={3.5} r={2.75} />
        </G>
        <G data-name="Ellipse 604" transform="rotate(-90 8.5 8.5)">
          <Circle cx={3.5} cy={3.5} r={3.5} stroke="none" />
          <Circle cx={3.5} cy={3.5} r={2.75} />
        </G>
        <G data-name="Ellipse 603" transform="rotate(-90 3.5 3.5)">
          <Circle cx={3.5} cy={3.5} r={3.5} stroke="none" />
          <Circle cx={3.5} cy={3.5} r={2.75} />
        </G>
      </G>
    </Svg>
  );
}

export default React.memo(MoreIcon);
