import * as React from "react"
import Svg, { Path } from "react-native-svg"

function CalendarIcon({color="#fff", props}) {
  return (
    <Svg width={22.696} height={24.209} viewBox="0 0 22.696 24.209" {...props}>
      <Path
        data-name="Icon metro-calendar"
        d="M7.565 9.079h3.026v3.026H7.565zm4.539 0h3.025v3.026h-3.025zm4.539 0h3.026v3.026h-3.025zM3.029 18.157h3.023v3.026H3.029zm4.539 0h3.026v3.026H7.565zm4.539 0h3.022v3.026h-3.025zm-4.539-4.539h3.026v3.026H7.565zm4.539 0h3.022v3.026h-3.025zm4.539 0h3.026v3.026h-3.028zm-13.618 0h3.024v3.026H3.029zM19.67 0v1.513h-3.026V0H6.052v1.513H3.029V0H0v24.209h22.7V0h-3.03zm1.513 22.7H1.513V6.052h19.67z"
        fill={color}
      />
    </Svg>
  )
}

export default React.memo(CalendarIcon);
