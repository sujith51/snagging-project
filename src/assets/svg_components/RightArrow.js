import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function SvgComponent({ color = '#fff', ...props }) {
  return (
    <Svg width={17.744} height={17.294} viewBox="0 0 17.744 17.294" {...props}>
      <Path
        data-name="Icon awesome-arrow-right"
        d="M7.544 1.159L8.423.28a.947.947 0 011.343 0l7.7 7.695a.947.947 0 010 1.343l-7.7 7.7a.947.947 0 01-1.343 0l-.879-.879a.952.952 0 01.016-1.358l4.772-4.546H.95a.948.948 0 01-.95-.95V8.014a.948.948 0 01.95-.95h11.382L7.56 2.517a.945.945 0 01-.016-1.358z"
        fill={color}
      />
    </Svg>
  );
}

export default SvgComponent;
