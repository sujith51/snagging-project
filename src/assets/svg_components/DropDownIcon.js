import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function SvgComponent({ color = '#fff', ...props }) {
  return (
    <Svg width={10.222} height={6.169} viewBox="0 0 10.222 6.169" {...props}>
      <Path
        data-name="Path 15926"
        d="M9.303.919l-4.409 4.6L.917.919"
        fill="none"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1.3}
      />
    </Svg>
  );
}

export default SvgComponent;
