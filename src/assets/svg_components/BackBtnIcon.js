import * as React from "react"
import Svg, { Path } from "react-native-svg"

function BackBtnIcon(props) {
  return (
    <Svg width={9.809} height={17.166} viewBox="0 0 9.809 17.166" {...props}>
      <Path
        data-name="Path 11"
        d="M0 8.585a1.222 1.222 0 00.375.882l7.357 7.357a1.226 1.226 0 101.647-1.815L2.955 8.585l6.508-6.508A1.226 1.226 0 007.73.344L.373 7.701a1.222 1.222 0 00-.375.882z"
        fill="#fff"
      />
    </Svg>
  )
}

export default React.memo(BackBtnIcon);
