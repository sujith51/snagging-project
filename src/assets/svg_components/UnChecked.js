import * as React from 'react';
import Svg, { Defs, ClipPath, Path, G } from 'react-native-svg';

function SvgComponent(props) {
  return (
    <Svg width={22} height={22} viewBox="0 0 22 22" {...props}>
      <Defs>
        <ClipPath id="prefix__a">
          <Path fill="none" d="M0 0h22v22H0z" />
        </ClipPath>
      </Defs>
      <G data-name="Repeat Grid 1">
        <G
          data-name="Rectangle 9381"
          fill="#fff"
          stroke="#65003c"
          clipPath="url(#prefix__a)"
        >
          <Path stroke="none" d="M0 0h22v22H0z" />
          <Path fill="none" d="M.5.5h21v21H.5z" />
        </G>
      </G>
    </Svg>
  );
}

export default SvgComponent;
