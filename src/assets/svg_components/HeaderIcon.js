import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function HeaderIcon(props) {
  return (
    <Svg width={21} height={15.978} viewBox="0 0 21 15.978" {...props}>
      <G
        data-name="Group 16666"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth={3}
      >
        <Path data-name="Line 2" d="M1.5 1.5h18" />
        <Path data-name="Line 3" d="M19.5 7.989h-18" />
        <Path data-name="Line 567" d="M19.5 14.478h-18" />
      </G>
    </Svg>
  )
}


export default React.memo(HeaderIcon);
