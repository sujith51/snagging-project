import * as React from "react"
import Svg, { G, Circle } from "react-native-svg"

function SuccessIcon(props) {
  return (
    <Svg width={140} height={140} viewBox="0 0 140 140" {...props}>
      <G
        data-name="Ellipse 1718"
        fill="#026978"
        stroke="#026978"
        strokeWidth={5}
      >
        <Circle cx={70} cy={70} r={70} stroke="none" />
        <Circle cx={70} cy={70} r={67.5} fill="none" />
      </G>
    </Svg>
  )
}

export default SuccessIcon