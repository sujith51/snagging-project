import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Ticked(props) {
    return (
        <Svg width={56.058} height={41.363} viewBox="0 0 56.058 41.363" {...props}>
            <Path
                data-name="Path 25896"
                d="M3.536 23.132l14.7 14.7 7.349-7.349 11.634-11.635 5.741-5.74 9.568-9.568"
                fill="none"
                stroke="#fff"
                strokeLinecap="round"
                strokeWidth={5}
            />
        </Svg>
    )
}

export default Ticked
