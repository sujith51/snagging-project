import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function QuotationsIcon({color = '#fff', props}) {
  return (
    <Svg width={27.247} height={27.247} viewBox="0 0 27.247 27.247" {...props}>
      <G data-name="Group 16436">
        <G data-name="Group 16365">
          <G data-name="Group 16106">
            <G data-name="Group 16105">
              <Path
                data-name="Path 25743"
                d="M24.328 0H2.919A2.919 2.919 0 000 2.919v21.409a2.919 2.919 0 002.919 2.919h21.409a2.919 2.919 0 002.919-2.919V2.919A2.919 2.919 0 0024.328 0zm.972 24.328a.973.973 0 01-.973.973H2.919a.973.973 0 01-.973-.973V2.919a.973.973 0 01.973-.973h21.409a.973.973 0 01.973.973v21.409z"
                fill={color}
              />
            </G>
          </G>
          <G data-name="Group 16108">
            <G data-name="Group 16107">
              <Path
                data-name="Path 25744"
                d="M8.757 5.839H5.838a.973.973 0 100 1.946h2.919a.973.973 0 100-1.946z"
                fill={color}
              />
            </G>
          </G>
          <G data-name="Group 16110">
            <G data-name="Group 16109">
              <Path
                data-name="Path 25745"
                d="M21.409 5.839h-8.758a.973.973 0 000 1.946h8.758a.973.973 0 000-1.946z"
                fill={color}
              />
            </G>
          </G>
          <G data-name="Group 16112">
            <G data-name="Group 16111">
              <Path
                data-name="Path 25746"
                d="M8.757 12.65H5.838a.973.973 0 100 1.946h2.919a.973.973 0 100-1.946z"
                fill={color}
              />
            </G>
          </G>
          <G data-name="Group 16114">
            <G data-name="Group 16113">
              <Path
                data-name="Path 25747"
                d="M21.409 12.65h-8.758a.973.973 0 100 1.946h8.758a.973.973 0 100-1.946z"
                fill={color}
              />
            </G>
          </G>
          <G data-name="Group 16116">
            <G data-name="Group 16115">
              <Path
                data-name="Path 25748"
                d="M8.757 19.462H5.838a.973.973 0 100 1.946h2.919a.973.973 0 100-1.946z"
                fill={color}
              />
            </G>
          </G>
          <G data-name="Group 16118">
            <G data-name="Group 16117">
              <Path
                data-name="Path 25749"
                d="M21.409 19.462h-8.758a.973.973 0 100 1.946h8.758a.973.973 0 100-1.946z"
                fill={color}
              />
            </G>
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default React.memo(QuotationsIcon);
