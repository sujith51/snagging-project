import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function TaskIcon({color = '#fff', props}) {
  return (
    <Svg width={26.735} height={26.735} viewBox="0 0 26.735 26.735" {...props}>
      <G data-name="Group 5745">
        <G data-name="Group 5744">
          <Path
            data-name="Path 15765"
            d="M25.78 6.684h-4.774V.955A.955.955 0 0020.051 0H.955A.955.955 0 000 .955v21.96a3.819 3.819 0 003.819 3.819h19.1a3.819 3.819 0 003.819-3.819V7.638a.955.955 0 00-.958-.954zM3.819 24.825a1.91 1.91 0 01-1.91-1.91V1.91H19.1v21.005a3.776 3.776 0 00.07.686c.01.054.02.108.032.161a3.83 3.83 0 00.2.633c0 .01.01.018.013.027a3.767 3.767 0 00.2.4zm21.006-1.91a1.91 1.91 0 01-3.819 0V8.593h3.819v14.322z"
            fill={color}
          />
        </G>
      </G>
      <G data-name="Group 5747">
        <G data-name="Group 5746">
          <Path
            data-name="Path 15766"
            d="M13.368 4.774H7.639a.955.955 0 100 1.91h5.729a.955.955 0 100-1.91z"
            fill={color}
          />
        </G>
      </G>
      <G data-name="Group 5749">
        <G data-name="Group 5748">
          <Path
            data-name="Path 15767"
            d="M16.232 9.548H4.774a.955.955 0 000 1.91h11.458a.955.955 0 100-1.91z"
            fill={color}
          />
        </G>
      </G>
      <G data-name="Group 5751">
        <G data-name="Group 5750">
          <Path
            data-name="Path 15768"
            d="M16.232 13.367H4.774a.955.955 0 100 1.91h11.458a.955.955 0 100-1.91z"
            fill={color}
          />
        </G>
      </G>
      <G data-name="Group 5753">
        <G data-name="Group 5752">
          <Path
            data-name="Path 15769"
            d="M16.232 17.187H4.774a.955.955 0 000 1.91h11.458a.955.955 0 000-1.91z"
            fill={color}
          />
        </G>
      </G>
      <G data-name="Group 5755">
        <G data-name="Group 5754">
          <Path
            data-name="Path 15770"
            d="M16.232 21.006H4.774a.955.955 0 100 1.91h11.458a.955.955 0 100-1.91z"
            fill={color}
          />
        </G>
      </G>
    </Svg>
  )
}

export default React.memo(TaskIcon);
