import * as React from "react"
import Svg, { Path } from "react-native-svg"

function RightIcon(props) {
  return (
    <Svg width={11.937} height={21.046} viewBox="0 0 11.937 21.046" {...props}>
      <Path
        data-name="Path 25868"
        d="M.707.707l9.816 9.816-9.816 9.816"
        fill="none"
        stroke="#fff"
        strokeWidth={2}
      />
    </Svg>
  )
}

export default React.memo(RightIcon);
