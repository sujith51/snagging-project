import * as React from "react"
import Svg, { Defs, Pattern, Image, Path } from "react-native-svg"

function ProvisIcon(props) {
  return (
    <Svg width={87} height={39} viewBox="0 0 87 39" {...props}>
      <Defs>
        <Pattern
          id="prefix__a"
          preserveAspectRatio="none"
          width="100%"
          height="100%"
          viewBox="0 0 181 81"
        >
          <Image
            width={181}
            height={81}
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALUAAABRCAMAAACjWBcPAAAArlBMVEUAAAD///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8tivQqAAAAOXRSTlMACqxKPI5OXugG+LBVLtPkdwJEFvJotoTXvZxAE/wqJc1vD3sioVkxwt0cOex/x3PhppI1lRiYiWJXrLzDAAAIsklEQVRo3u2aaXeiMBSGgztgtSwqKLggKq7VWq33//+xacDhkoWRMmdmjufM+6kJkDyENzfJteQvSJkY5OnUtOG4Ik+miwYAHfJkMoFKJc+l5xxr0tSgeyZPJ2X8bP74r/96Go2aXxoxVQ1atZLOxVW1edeYuWCMzeX9wjZbv202FbG72m9TV+BLFaaqQ6sGhJex7GtgH49Hn15/JajaaQdat9vV6IUh29SRMNLhS4u/R137qnbjWN1lqTc6wLpBA2Kfpx4D1EtSX3pU6Ycyacn8NnXPB7gSIlJvWwCHgBAZ9R5gW5J6BFSWDLM4dQ++2AwJ9YsNsLsQnhoN8g+pa9SyKyJSe2sAmBGeGg3yL6mndKiJhPoTALRAoEaD/EPqNnKy1JYNADoRqNEg/4Ya+xqx1KndYS5Qo0GKURubP0Ct2rTGY6mRsyFQo0GKUQ9Of4B6BVSGhHoHgGg8dQe6pBB1TTsXota/RV2lFTYRqVWgGvPUaJBi1If3PF+HDKZLZEWkZjts0oqdhHoDVBOeGg1SiNrUNnnUDQbTYak7InXWaUtaoUmoL0C14qnRIIWor/3cGDJnMH1FXkRqh3cIqHkOqbHUaJCoGHVrlkvdZzEbQhHloKUShWgE2Wxss9RokLUlpx4yVRbUcqkP25/WpyU9dXLEFBHFzRo7jnxVaeQT4zUaZC4O4gEfwAgV5lHv5lHyhYOYYHlskqS4i4vdJd4/ASpTO3MjtJdQ79MIhNQYQc72hKCwuwZTV4UAC6wrO8EuulAnDmPTqjMtxlYXWLwriBJDfdhLZscHkYR6S/9syajnNIL0HTYQG8OYRWUqt7AhotpJbKtqdn9f78TdnImxgM7nW91hilTXmMm5EG8K0+tbos/Y/G+zRDZSkyP9u1lLdEioMYJMur6+76UaTO/dMZoAVqDe7uE3/FhrAGBP6/EXaegtP1Ns6y24a3d4paOhziIb5ELqAVOJ1JMk4nufjg9ZaZ0Pj+NTNFkMuWa2PoqiEJQhFhW2SlXydP/KnpmRC1/6QIPEMjwrI5WIcjEWsOF3Z5A/K/T1AA1SUDNgJ2266l7J31Bs2zYapKCCXYWg0Hn2hjyUtVjrISFK3XG35aAVjc5ZCw2SUa8zbZI8vUKbq/HseOo8lhuHQFKnL3kpRd2IgxUahN/JmCRHaqRV2QjZpy2p5KE8oHq5h7Iy0GonRRMMUsF5KpO11t4yjApdRSKlSJ8afGmTeLNaxh+4cRcNcsXFVSrPhU7Tu5M06OsvHkFjWH8npKoBuN+KOPOlaTaW9TjcO4E8goQ7gKNF8qXOWuBHp/r8vUIb6pikoLZv8a3jWVv91hh3IZVrkZwIYvVuwYN2eroNsZzrlvxxqUt9B1TR0kj3ILVSLW1W5mgVkL8kI9yaY+xtCl3ydJrQCfJ0ig3ydPoNg2xeUJNNYDCfkLnmPXStNb6o3zWI8oKyuOAcjvP6fFn2TkA13O/rw6nm6PNVmutZzuINvL3/unbqaGt3YOZghT193R8OPnTHfV2x7Sc65xgkaNzmQLXotUO8eJm57umzsvZbh8FI2mc7c+63PjWA4yxdcOqZo1NIV67dp+T9Ry6sm8kzavUAztLAAVtV15hKkRpEAf6Q6L370yQW92za5wNqSkDv69Z4atzctBr8p44AFgEaZe6DYzK7SaqJzCBy6skapj/H96xxz8qpyY2W/KWMmgxpUTPZx22AE7d9B3/OJy1fJQaRU3st0CZMGq79mJqsY7QXGfUY4rLK/hIEh7QCX05X2QanokEMOfXpfjMWBw+o8YvqMmrSBdyhYjJhK9usX7kGQ9EgUuqLz2adrB3oBagbQOVfZNQRsDtg0weQNDpPPYEWeePOJVCTU/f4XNkNjg+osRMwZdQ6QBZTOWKHwiFUC5mM4EEwiJz6E/h8ZQTeY+oJoA146j4w6d9XWrIVIs/TVViLbHiDyKmv9ww9aqyNHlOPAZvhqadMPPDsFA6Fr5Mmi16AasYZRKTGRw8BcxKfPaauQayVhFrxaTnN2TfZlLeQyn5nLBLxBhGpEecQEpRSYKxnQLWWxZAbO2gV7E9mbJxFA5zfsUJ8I4Ha04BKq18I6jG1jmgctXWkzaXQio2rHq/k0oqxSI8ziEiNfdKOFqvi1KGPYYKlHnfohRd+AgREonU6o9EiLm8QOXXgwE9FDaMYtRrFUULJUnueFW57um+7r1m7mcD/j6R02qJFPM4gcmoSriGV0zQKUG8o9PrGfC3tqidWxxCXmYwakSnC6YgWuUkMgtQoywVUx/wFtako1ni50GB3SlMG6BCXWROZabvLz5vCgjAW6acfomXIqFG3LqA+1Fzq/mIxrA9m1ZC2J1Bv7KTpMmONFtG8nwb5IHJqrJxlbOIqj2OISJ0S7jakhK/RIk00iEjNyWhHKXalDDXGQ0ySFYwhrEV0NIhILWqbcrdLUlstXAi/G6/RIgEaRKCWqXr3yaEUNW5g8b7iayNaZJkYZPQLaoXPosaySlKTRUKChii8D0GLVNAgOdTBiTBSk2i0LUsdHPmZETze8/EWUdAgOWPd5eJzEr7MstRkBLidKLi/FizSRoPkUPcdVcIwLkWNJdCwBXX94CwjWOQkGEQ8y+yJ8GONr5SnVhyg6iiPzo17PChyFrHHaBA5dRPzCngirJDy1OTs8yv7TfashWd0IbxHaBA59Qo3h3hKqZWixkHELvA4PpXkQypoA8YigAbhqfG81OQO/HXyW9TqFFf23NzTG+aeRN9wBhGpiUN9RLKpKF0tSY3nZaCKjF/k+QAcNIG4AxjlUuPatbZS6Mj/NITZXmu8A9WgYZ49LoE4asQh3l5Wayt2ewrXRm0jy6kamFMV5YgGUc7mDajq1a2HnoOWmTDPW+5Kkr+uZsSFqlr2Wlqb1qz4/PV1/15xIsxfC9qLBgkk3Y97w2lXO9QHJ9cdTMiflXEZbwQDsoirL3mkmIKQ+enhByvflMaSqQ7qAAAAAElFTkSuQmCC"
          />
        </Pattern>
      </Defs>
      <Path data-name="logo w" fill="url(#prefix__a)" d="M0 0h87v39H0z" />
    </Svg>
  )
}

export default React.memo(ProvisIcon);