import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function Cancel({color='#fff', ...props}) {
    return (
        <Svg width={17.243} height={17.243} viewBox="0 0 17.243 17.243" {...props}>
            <G
                data-name="Group 16609"
                fill="none"
                stroke={color}
                strokeLinecap="round"
                strokeWidth={3}
            >
                <Path data-name="Line 360" d="M15.121 2.121l-13 13" />
                <Path data-name="Line 361" d="M15.121 15.121l-13-13" />
            </G>
        </Svg>
    )
}

export default React.memo(Cancel)
