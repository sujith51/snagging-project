import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DownArrow(props) {
    return (
        <Svg width={15.202} height={8.308} viewBox="0 0 15.202 8.308" {...props}>
            <Path
                data-name="Path 25638"
                d="M.707.707l6.894 6.894L14.495.707"
                fill="none"
                stroke="#707070"
                strokeLinecap="round"
            />
        </Svg>
    )
}

export default DownArrow
