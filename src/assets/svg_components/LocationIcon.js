import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function SvgComponent({ color = '#65003c', ...props }) {
  return (
    <Svg width={13.394} height={19.134} viewBox="0 0 13.394 19.134" {...props}>
      <Path
        data-name="Icon material-location-on"
        d="M6.7 0A6.692 6.692 0 000 6.7c0 5.023 6.7 12.437 6.7 12.437s6.7-7.415 6.7-12.437A6.692 6.692 0 006.7 0zm0 9.089A2.392 2.392 0 119.089 6.7 2.393 2.393 0 016.7 9.089z"
        fill={color}
      />
    </Svg>
  );
}

export default SvgComponent;
