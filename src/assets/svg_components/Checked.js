import * as React from 'react';
import Svg, { Defs, G, Path, ClipPath } from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: filter */

function SvgComponent({ checked, ...props }) {
  return checked ? (
    <Svg width={40} height={40} viewBox="0 0 40 40" {...props}>
      <Defs>
        <ClipPath id="prefix__a">
          <Path fill="none" d="M0 0h22v22H0z" />
        </ClipPath>
      </Defs>
      <G data-name="Group 16280">
        <G filter="url(#prefix__a)">
          <Path data-name="Rectangle 9381" fill="#65003c" d="M9 6h22v22H9z" />
        </G>
        <Path
          data-name="Path 25722"
          d="M14.243 16.641l4.152 4.324 8.556-8.556"
          fill="none"
          stroke="#fff"
          strokeWidth={2}
        />
      </G>
      {/* <Path stroke="none" d="M0 0h22v22H0z" /> */}
      {/* <Path fill="none" d="M.5.5h21v21H.5z" /> */}
    </Svg>
  ) : (
    <Svg width={39} height={22} viewBox="0 0 22 22" {...props}>
      <Defs>
        <ClipPath id="prefix__a">
          <Path fill="none" d="M0 0h22v22H0z" />
        </ClipPath>
      </Defs>
      <G data-name="Group 16280">
        <G
          data-name="Rectangle 9381"
          fill="#fff"
          stroke="#65003c"
          clipPath="url(#prefix__a)"
        >
          <Path stroke="none" d="M0 0h22v22H0z" />
          <Path fill="none" d="M.5.5h21v21H.5z" />
        </G>
      </G>
    </Svg>
  );
}

export default SvgComponent;
