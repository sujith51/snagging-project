import * as React from "react"
import Svg, { G, Rect } from "react-native-svg"

function DashboardIcon({color='#65013d', props}) {
  return (
    <Svg width={24} height={24} viewBox="0 0 24 24" {...props}>
      <G data-name="Group 16263" transform="translate(-52 -846)" fill={color}>
        <Rect
          data-name="Rectangle 62"
          width={10}
          height={10}
          rx={2}
          transform="translate(52 846)"
        />
        <Rect
          data-name="Rectangle 11329"
          width={10}
          height={10}
          rx={2}
          transform="translate(52 860)"
        />
        <G data-name="Group 16262">
          <Rect
            data-name="Rectangle 62"
            width={10}
            height={10}
            rx={2}
            transform="translate(66 846)"
          />
          <Rect
            data-name="Rectangle 11330"
            width={10}
            height={10}
            rx={2}
            transform="translate(66 860)"
          />
        </G>
      </G>
    </Svg>
  )
}

export default React.memo(DashboardIcon);
