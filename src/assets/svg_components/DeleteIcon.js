import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DeleteIcon(props) {
  return (
    <Svg width={25.657} height={32.988} viewBox="0 0 25.657 32.988" {...props}>
      <Path
        data-name="Icon material-delete"
        d="M1.833 29.323A3.676 3.676 0 005.5 32.988h14.659a3.676 3.676 0 003.665-3.665V7.331H1.833zm23.824-27.49h-6.414L17.41 0H8.247L6.414 1.833H0V5.5h25.657z"
        fill="#65013d"
      />
    </Svg>
  )
}

export default React.memo(DeleteIcon);
