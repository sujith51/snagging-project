import * as React from "react"
import Svg, { Path } from "react-native-svg"

function UpArrow(props) {
    return (
        <Svg width={15.202} height={8.308} viewBox="0 0 15.202 8.308" {...props}>
            <Path
                data-name="Path 25647"
                d="M.707 7.602L7.601.708l6.894 6.894"
                fill="none"
                stroke="#707070"
                strokeLinecap="round"
            />
        </Svg>
    )
}

export default UpArrow
