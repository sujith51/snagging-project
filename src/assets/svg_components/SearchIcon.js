import * as React from "react"
import Svg, { G, Circle, Path } from "react-native-svg"

function SearchIcon(props) {
  return (
    <Svg width={19.095} height={19.095} viewBox="0 0 19.095 19.095" {...props}>
      <G
        data-name="Group 16318"
        transform="translate(-353 -74)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit={10}
        strokeWidth={2}
      >
        <Circle cx={7.414} cy={7.414} r={7.414} transform="translate(354 75)" />
        <Path d="M370.68 91.68l-4.031-4.031" />
      </G>
    </Svg>
  )
}

export default React.memo(SearchIcon);
