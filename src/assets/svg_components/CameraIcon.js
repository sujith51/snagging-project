import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function CameraIcon(props) {
  return (
    <Svg width={29.309} height={24.343} viewBox="0 0 29.309 24.343" {...props}>
      <G
        data-name="Icon feather-camera"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      >
        <Path
          data-name="Path 25881"
          d="M28.309 20.861a2.483 2.483 0 01-2.483 2.483H3.483A2.483 2.483 0 011 20.861V7.206a2.483 2.483 0 012.483-2.482h4.965L10.93 1h7.448l2.483 3.724h4.965a2.483 2.483 0 012.483 2.483z"
        />
        <Path
          data-name="Path 25882"
          d="M19.619 13.413a4.965 4.965 0 11-4.965-4.965 4.965 4.965 0 014.965 4.965z"
        />
      </G>
    </Svg>
  )
}

export default React.memo(CameraIcon);
