import * as React from "react"
import Svg, { Defs, G, Rect } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: filter */

function RadioChecked({checked,...props}) {
  return checked ? (
    <Svg width={40} height={40} viewBox="0 0 40 40" {...props}>
      <Defs></Defs>
      <G filter="url(#prefix__a)">
        <G
          data-name="Rectangle 11517"
          transform="translate(9 6)"
          fill="#65003C"
          stroke="#65003c"
        >
          <Rect width={22} height={22} rx={11} stroke="none" />
          <Rect x={0.5} y={0.5} width={21} height={21} rx={10.5} />
        </G>
      </G>
    </Svg>
  ):
   (
    <Svg width={40} height={40} viewBox="0 0 40 40" {...props}>
      <Defs></Defs>
      <G filter="url(#prefix__a)">
        <G
          data-name="Rectangle 11517"
          transform="translate(9 6)"
          fill="none"
          stroke="#65003c"
        >
          <Rect width={22} height={22} rx={11} stroke="none" />
          <Rect x={0.5} y={0.5} width={21} height={21} rx={10.5} />
        </G>
      </G>
    </Svg>
  )
}

export default RadioChecked
