import React, { useEffect } from 'react';
// import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import NetInfo from '@react-native-community/netinfo';
import { useSelector, useDispatch } from 'react-redux';
import {
  Login,
  TaskDetails,
  HandOverDetails,
  CheckList,
  Profile,
  Notifications,
  PmNotifications,
  ReviewInspection,
  Search,
  UnitInspection,HandOverReview
} from '_screens';
import {
  Intro,
  TaskLog,
  InspectionHistory,
  QuotationRequested,
  SegregationReport,
} from '_components';
import { updateNetinfo } from '_actions';
import MainTab from './MainTab';
// import { useTaskSync } from '../hooks';
import Log from '../screens/Login';
import SecondaryTab from './SecondaryTab';

const Stack = createStackNavigator();
const AppStart = () => {
  const { data: userData } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  // const { taskQueue, status } = useSelector((state) => state.taskQueue);
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      dispatch(updateNetinfo(state));
    });
    // subscription removal
    return () => {
      unsubscribe();
    };
  }, []);

  // useTaskSync();

  return (
    <Stack.Navigator
      headerMode="none"
      name="AppStart"
      initialRouteName={userData ? 'Main' : 'Login'}
    >
      {/* <Stack.Screen name="MoveinInspetion" component={MoveinInspetion} /> */}
      {!userData && <Stack.Screen name="Login" component={Login} />}
      {!userData && <Stack.Screen name="Intro " component={Intro} />}
      {userData && (
        <>
        {
          userData?.actual_role==='55'?
          <Stack.Screen   name="Main" component={SecondaryTab} />:
          <Stack.Screen   name="Main" component={MainTab} />
        }
         
          {/* <Stack.Screen   name="Main" component={SecondaryTab} /> */}
          <Stack.Screen name="TaskDetails" component={TaskDetails} />
          <Stack.Screen name="HandOverDetails" component={HandOverDetails} />
          <Stack.Screen name="CheckList" component={CheckList} />
          <Stack.Screen name="TaskLog" component={TaskLog} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen
            name="InspectionHistory"
            component={InspectionHistory}
          />
          <Stack.Screen name="Notifications" component={Notifications} />
          <Stack.Screen name="HandOverReview" component={HandOverReview} />
          <Stack.Screen name="PmNotifications" component={PmNotifications} />
          <Stack.Screen name="Quotations" component={QuotationRequested} />
          <Stack.Screen
            name="SegregationReport"
            component={SegregationReport}
          />
          <Stack.Screen name="ReviewInspection" component={ReviewInspection} />
          <Stack.Screen name="Search" component={Search} />
          <Stack.Screen name="log" component={Log} />
          <Stack.Screen name="UnitInspection" component={UnitInspection} />
        </>
      )}
    </Stack.Navigator>
  );
};

export default AppStart;
