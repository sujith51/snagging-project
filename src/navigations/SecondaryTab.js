/* eslint-disable no-underscore-dangle */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HandOverDashboard, Tasks, Calendar } from '_screens';
import { CalendarIcon, DashboardIcon, QuotationsIcon, TaskIcon } from '_svg';
import { useTheme, getAppFont } from '../utils';
import { QuotationRequested } from '../components';
import { DEVICE_WINDOW_WIDTH } from '../utils/config';

const Tab = createBottomTabNavigator();

// :Render
const SecondaryTab = () => {
  const theme = useTheme();
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let return_;
          switch (route.name) {
            case 'Dashboard':
              return_ = (
                <View
                  style={[
                    styles.commTabStyle,
                    focused && {
                      backgroundColor: theme.color.secondaryLight,
                      left: 10,
                      width: DEVICE_WINDOW_WIDTH,
                    
                    },
                  ]}
                >
                  <DashboardIcon
                    color={focused ? theme.color.primary : theme.color.darkGray}
                  />
                  {focused && (
                    <Text
                      style={[
                        styles.commTabTxt,
                        focused
                          ? [styles.focusTxt, { color: theme.color.primary }]
                          : [
                              styles.focusOutTxt,
                              { color: theme.color.primaryWhite },
                            ],
                      ]}
                    >
                      Dashboard
                    </Text>
                  )}
                </View>
              );
              break;

            case 'Tasks':
              return_ = (
                <View
                  style={[
                    styles.commTabStyle,
                    focused && {
                      backgroundColor: theme.color.secondaryLight,
                      width: Platform.OS === 'ios' ? 150 : 150,
                    },
                  ]}
                >
                  <TaskIcon
                    color={focused ? theme.color.primary : theme.color.darkGray}
                  />
                  {focused && (
                    <Text
                      style={[
                        styles.commTabTxt,
                        focused
                          ? [styles.focusTxt, { color: theme.color.primary }]
                          : [
                              styles.focusOutTxt,
                              { color: theme.color.primaryWhite },
                            ],
                      ]}
                    >
                      De-Snag
                    </Text>
                  )}
                </View>
              );
              break;

            case 'Quotations':
              return_ = (
                <View
                  style={[
                    styles.commTabStyle,
                    focused && {
                      backgroundColor: theme.color.secondaryLight,
                      width: Platform.OS === 'ios' ? 150 : 150,
                    },
                  ]}
                >
                  <QuotationsIcon
                    color={focused ? theme.color.primary : theme.color.darkGray}
                  />
                  {focused && (
                    <Text
                      style={[
                        styles.commTabTxt,
                        focused
                          ? [styles.focusTxt, { color: theme.color.primary }]
                          : [
                              styles.focusOutTxt,
                              { color: theme.color.primaryWhite },
                            ],
                      ]}
                    >
                      Quotations
                    </Text>
                  )}
                </View>
              );
              break;

            case 'Calendar':
              return_ = (
                <View
                  style={[
                    styles.commTabStyle,
                    focused && {
                      backgroundColor: theme.color.secondaryLight,
                      right: 10,
                      width: Platform.OS === 'ios' ? 150 : 150,
                    },
                  ]}
                >
                  <CalendarIcon
                    // width={100}
                    // height={100}
                    color={focused ? theme.color.primary : theme.color.darkGray}
                  />
                  {focused && (
                    <Text
                      style={[
                        styles.commTabTxt,
                        focused
                          ? [styles.focusTxt, { color: theme.color.primary }]
                          : [
                              styles.focusOutTxt,
                              { color: theme.color.primaryWhite },
                            ],
                      ]}
                    >
                      Calendar
                    </Text>
                  )}
                </View>
              );
              break;
            default:
              // console.log('hello');
          }
          return return_;
        },
      })}
     
      tabBarOptions={{
        showLabel: false,
        style: {
          height: 65,
          backgroundColor: theme.color.primaryWhite,
        },
      }}
    >
      <Tab.Screen name="Dashboard"  options={{headerShown: false}}  component={HandOverDashboard} />
      {/* <Tab.Screen name="Tasks" options={{headerShown: false}} component={Tasks} /> */}
      {/* <Tab.Screen name="Quotations"  options={{headerShown: false}} component={QuotationRequested} /> */}
      {/* <Tab.Screen name="Calendar"  options={{headerShown: false}} component={Calendar} /> */}
    </Tab.Navigator>
  );
};

export default SecondaryTab;

// :Styles
const styles = StyleSheet.create({
  commTabStyle: {
    width: Platform.OS === 'ios' ? 100 : 0,
    // height: 46,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    flexDirection: 'row',
    padding: 10,
    // margin: -19,
  },
  commTabTxt: {
    // paddingTop: 7,
    fontSize: Platform.OS === 'ios' ? 15 : 11,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  focusTxt: {
    ...getAppFont('SemiBold'),
    left: 4,
  },
  focusOutTxt: {
    ...getAppFont('Regular'),
  },
});
