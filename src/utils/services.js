import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { API_KEY_UAT } from './config';
// axios.defaults.headers.common['Authorization'] = API_KEY_UAT;
const get = (url, params = {}) => {
  const aParams = Object.entries({ ...params, api_key: API_KEY_UAT });
  const uPram = aParams.map((param) => `${param[0]}=${param[1]}`);
  return axios
    .get(`${url}?${uPram.join('&')}`)
    .then((response) => response.data)
    .catch((error) => ({ error }));
};

const getWithoutKey = (url, params = {}) => {
  const aParams = Object.entries({ ...params });
  const uPram = aParams.map((param) => `${param[0]}=${param[1]}`);
  return axios
    .get(`${url}?${uPram.join('&')}`)
    .then((response) => response.data)
    .catch((error) => ({ error }));
};

const post = (
  url,
  params = {},
  // headers = { 'Content-Type': 'application/json' },
) => {
  return axios
    .post(url, { ...params, api_key: API_KEY_UAT })
    .then((response) => response.data)
    .catch((error) => ({ error }));
};

const postWithOutKey = (
  url,
  params = {},
  // headers = { 'Content-Type': 'application/json' },
) => {
  return axios
    .post(url, { ...params })
    .then((response) => response.data)
    .catch((error) => ({ error }));
};

const localGet = (key) => {
  return AsyncStorage.getItem(`pckey@${key}`).then((res) => JSON.parse(res));
};

const localSet = (key, data) => {
  return AsyncStorage.setItem(`pckey@${key}`, JSON.stringify(data));
};

const apiSync = async ({ data, id, url }) => {
  const params = { ...data.data };
  return axios
    .post(url, { ...params, api_key: API_KEY_UAT })
    .then((response) => {
      return { status: 'SUCCESS', id, response };
    })
    .catch((error) => ({ status: 'FAIL', error }));
};

export {
  get,
  post,
  localGet,
  localSet,
  apiSync,
  getWithoutKey,
  postWithOutKey,
};
