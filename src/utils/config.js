import { Dimensions, Platform } from 'react-native';

export const TESTING = 0;
export const COMPONENT_LIST = false;
export const LIVE_API_TESTING = false;

// export const SERVER_URL_PRD = 'http://pm.realcube.estate/';
// export const SERVER_URL_UAT = 'http://pm.realcube.estate/';

// export const SERVER_URL_PRD = 'http://aqaar.realcube.estate/';
// export const SERVER_URL_UAT = 'http://aqaar.realcube.estate/';

// new uat http://alpha-myportal.realcube.estate/
// url for pm http://pm.realcube.estate/
// live url http://myportal.provis.ae/
// http://snagging.realcube.estate/

export const SERVER_URL_PRD_PM = 'http://pm.realcube.estate/';
export const SERVER_URL_UAT_PM = 'http://pm.realcube.estate/';

// export const SERVER_URL_PRD_PM = 'http://provis.realcube.estate/';
// export const SERVER_URL_UAT_PM = 'http://provis.realcube.estate/';

// export const API_KEY_UAT = 'qazxcvbnm123wsdfrety';
// export const API_KEY_PRD = 'qazxcvbnm123wsdfrety';

export const {
  height: DEVICE_WINDOW_HEIGHT,
  width: DEVICE_WINDOW_WIDTH,
} = Dimensions.get('window');

export const PLATFORM_ANDROID = Platform.OS === 'android';
export const PLATFORM_IOS = Platform.OS === 'ios';
