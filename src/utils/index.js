/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable import/prefer-default-export */
import moment from 'moment';
import * as services from './services';
import { endpoints } from './endpoints';
import * as config from './config';
import {
  ThemeProvider,
  withTheme,
  useTheme,
  getAppFont,
  textVariant,
  colorType,
  getColor,
  buttonType,
  inputType,
  monthDayYear,
  numberFormat,
  theme,
  getTime
} from './theme';

// const getFullUrl = (endpoint, api_prefix = 'api') => {
//   if (__DEV__ && config.LIVE_API_TESTING === false) {
//     return `${config.SERVER_URL_UAT}${api_prefix}${endpoint}`;
//   }
//   return `${config.SERVER_URL_PRD}${api_prefix}${endpoint}`;
// };

const getFullUrlPM = (endpoint, api_prefix = 'api') => {
  if (__DEV__ && config.LIVE_API_TESTING === false) {
    return `${config.SERVER_URL_UAT_PM}${api_prefix}${endpoint}`;
  }
  return `${config.SERVER_URL_PRD_PM}${api_prefix}${endpoint}`;
};

const returnWithoutStatic = (data) => {
  if (!data) return [];
  return data?.filter((val) => val?.value != 'All')?.map((val) => val.value);
};
const showFixedLength = (data, length = 17) => {
  if (!data) return null;
  return data.length > length ? `${data.slice(0, length)}..` : data;
};

const getFormattedDate = (date = undefined) =>
  moment(date).format('YYYY-MM-DD HH:mm:ss');

const getTasksDate = (date = undefined) => {
  return `${moment(date).format('MMMM')} ${moment(date).format('DD')} ${moment(
    date,
  ).format('YYYY')} ${moment(date).format('HH:mm')}`;
};
export {
  config,
  endpoints,

  services,
  getFormattedDate,
  getTasksDate,
  ThemeProvider,
  withTheme,
  useTheme,
  getAppFont,
  textVariant,
  colorType,
  getColor,
  buttonType,
  inputType,
  monthDayYear,
  numberFormat,
  getFullUrlPM,
  showFixedLength,
  returnWithoutStatic,
  getTime,
  theme,
};

export const stripHtmlTags = (str) => {
  if (str) {
    let out = str.replace(/(<([^>]+)>)/gi, '');
    out = out.replace(/&nbsp;/g, ' ');
    return out;
  }
  return null;
};
