/* eslint-disable import/prefer-default-export */
const endpoints = {
  login: '/Login/checklogin',
  getuserunits: '/Login/getuserunits',
  quotationList: 'itemRequested',
  getCommunities: '/Login/setsessionaccesscommunity',
  // calender
  unitInspectionsCalendar: '/Snagging/unitInspectionsCalendar',
  // dashboard
  snagInspectionUnitList: '/Snagging/snagInspectionUnitList',
  ownerInspectionUnitList: '/Snagging/ownerInspectionUnitList',
  unitsListForInspectionMRD: '/Snagging/desnagInspectionUnitList',
  unitsInspectionGraph: '/makeready/unitsInspectionGraph',
  inspectionStatusGraph: '/makeready/inspectionStatusGraph',
  // notificaions
  pushNotification: '/Snagging/pushNotification',
  notification: '/Snagging/Notification',
  // quotation
  itemRequested: '/makeready/itemRequested',
  generateSegregationReport: '/makeready/generateSegregationReport',
  submitSegregationReport: '/makeready/submitSegregationReport',
  // task
  listDamages: '/Snagging/listDamages',
  ownerWorkStatusDetails: '/Snagging/ownerWorkStatusDetails',
  listRateCard: '/makeready/listRateCard',
  unitInspectionHistory: '/Snagging/unitInspectionHistory',
  taskLogs: '/makeready/taskLogs',
  saveDamageInspection: '/Snagging/saveSnagDamageInspection',
  keyNotCollected: '/makeready/keyNotCollected',
  unitsInspectionDetails: '/Snagging/unitsInspectionDetails',
  taskDetailsMrd: '/Snagging/desnageunitsInspectionDetails',
  ownerInspectionDetails: '/Snagging/ownerInspectionDetails',
  itemWorkStatusDetails: '/makeready/itemWorkStatusDetails',
  itemWork: '/Snagging/itemWorkStatusDetails',
  saveWorkStatusDetails: '/Snagging/savedeSnaggingWorkStatus',
  saveOwnerWorkStatus: '/Snagging/saveOwnerWorkStatus',
  // serach
  getinspectionList: '/Snagging/getinspectionList',
  // updatedevice token
  updateDeviceToken: '/mobilenotification/updatedevicetoken',
  listContractorRateCard: '/makeready/listContractorRateCard',
  // location
  addPropertyGeocodes: '/Snagging/addPropertyGeocodes',
  // image convert from base64 to cloudinary
  uploadBase64Image: '/Snagging/uploadBase64Image',
  // review vendor list
  getreviewVendor: '/Snagging/getreviewVendor',
  getMaterial: '/makeready/getVendorRequestedItem',
};

export { endpoints };
