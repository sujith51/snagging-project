import { createTheming } from '@callstack/react-theme-provider';
import { Platform } from 'react-native';
import moment from 'moment';

const color = {
  primary: '#65013D',
  primaryBlack: '#000000',
  primaryWhite: '#FFFFFF',
  primaryLight: '#D9D8D8',
  primaryDark: '#707070',
  secondaryLight: '#E9E9E9',
  secondary: '#026978',
  secondaryLight2: '#E1E1E1',
  secondaryLight3: '#B19DC9',
  secondaryDark: '#515C6F',
  secondaryDark2: '#DDDDDD',
  secondaryDark3: '#727C8E',
  lime: '#E7EF00',
  darkGray: '#B4B4B4',
  lightGray: '#E9E9E9',
  red: '#FE0000',
  primaryBouquet: '#B07EAD',
  pink: '#FD6768',
  darkGray2: '#4E4E4E',
  orange: '#FFA500',
  green: '#008000',
  secondaryGreenDark: '#317E78',
  secondaryGreen: '#E0F8F8',
  secondaryRed: '#FF4651',
  secondaryRedLight: '#FFE1DE',
};

export type textVariant =
  | 'bodyLight'
  | 'bodyDark'
  | 'header'
  | 'title'
  | 'bodyBold'
  | 'subTitle'
  | 'taskBold'
  | 'calDay'
  | 'textMedium'
  | 'textRegular'
  | 'calMonNum';
export type colorType =
  | 'primary'
  | 'primaryBlack'
  | 'primaryWhite'
  | '| primaryLight'
  | 'primaryDark'
  | 'secondaryLight'
  | 'secondary'
  | 'secondaryLight2'
  | 'secondaryLight3'
  | 'secondaryDark3'
  | 'secondaryDark'
  | 'secondaryDark2';

export type buttonType = 'primary' | 'default' | 'blue';
export type inputType = 'default' | 'bottom' | 'textArea';
export const theme = {
  color,
  borderRadius: {
    xs: 2,
    x: 5,
    m: 10,
    l: 15,
    xl: 20,
  },
  spacing: {
    xs: 4,
    s: 8,
    sm: 10,
    m: 16,
    l: 24,
    xl: 40,
  },
  textVariants: {
    header: {
      fontSize: Platform.isPad ? 24 : 20,
      fontFamily: 'Montserrat-Bold',
      lineHeight: 28,
      color: '#20283a',
    },
    bodyLight: {
      fontSize: Platform.isPad ? 20 : 16,
      fontFamily: 'Montserrat-Light',
      lineHeight: 19,
      color: '#000000',
    },
    bodyDark: {
      fontSize: Platform.isPad ? 20 : 16,
      fontFamily: 'Montserrat-Bold',
      lineHeight: 19,
      color: '#000000',
    },
    bodyBold: {
      fontSize: Platform.isPad ? 20 : 16,
      fontFamily: 'Montserrat-Bold',
      lineHeight: 19,
      color: '#000000',
    },
    textMedium: {
      fontSize: Platform.isPad ? 20 : 16,
      fontFamily: 'Montserrat-Medium',
      lineHeight: 19,
      color: '#000000',
    },
    textRegular: {
      fontSize: Platform.isPad ? 18 : 16,
      fontFamily: 'Montserrat-Regular',
      lineHeight: 19,
      color: '#000000',
    },
    taskBold: {
      fontSize: Platform.isPad ? 22 : 19,
      fontFamily: 'Montserrat-Bold',
      lineHeight: 23,
      color: '#000000',
    },
    title: {
      fontSize: Platform.isPad ? 24 : 20,
      fontFamily: 'Montserrat-Regular',
    },
    subTitle: {
      fontSize: Platform.isPad ? 18 : 14,
    },
    subHeading: {
      fontSize: Platform.isPad ? 22 : 18,
      fontFamily: 'Montserrat-Bold',
    },
    taskHeading: {
      fontSize: Platform.isPad ? 24 : 20,
      fontFamily: 'Montserrat-Bold',
    },
    calDay: {
      fontSize: Platform.isPad ? 28 : 24,
      fontFamily: 'Montserrat-Regular',
    },
    calMonNum: {
      fontSize: Platform.isPad ? 40 : 36,
      fontFamily: 'Montserrat-Bold',
    },
  },
  cardShadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 4,
  },
};

export const getAppFont = (type = 'Regular') => {
  return { fontFamily: `Montserrat-${type}` };
};
export const monthDayYear = (date) => {
  return date ? `${moment(date).format('MMMM DD YYYY ')}` : null;
  // January 21, 2020
};
export const getTime = (date) => {
  return date ? `${moment(date).format('hh:mm a')}` : null;
  // January 21, 2020
};
export const numberFormat = (value, length = 2) => {
  if (value === undefined || value === null) {
    return 0;
  }
  return parseFloat(value)
    .toFixed(length)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};
export const getColor = (type = 'move_in') => {
  let color = theme.color.primary;
  switch (type) {
    case 'move_in_inspection':
      color = theme.color.primary;
      break;

    case 'move_out_inspection':
      color = theme.color.secondary;
      break;

    case 'makeready_inspection':
      color = theme.color.primaryLight;
      break;

    case 'vacant_inspection':
      color = theme.color.primaryDark;
      break;

    default:
      color = theme.color.primary;
  }
  return color;
};

const { ThemeProvider, withTheme, useTheme } = createTheming(theme);
export { ThemeProvider, withTheme, useTheme };
