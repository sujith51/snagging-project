/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable import/no-unresolved */
import React, {useState, useRef, useEffect} from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  savechklistquesctions,
  saveYValues,
  saveMaterialDetails,
  getMaterialsList,
} from '_actions';
import {
  Loader,
  Button,
  InputText,
  Text,
  DropDown,
  DropDownCheckQty,
  ImageUpload,
  RequestModal,
  RequestListModal,
  AddedMaterialModal,
  ConfirmModal,
} from '_components';
import {useTheme, getAppFont} from '../utils';
import Panel from './SummaryPanel';

const AccordianQuestion = (props: Props) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const {
    question = [],
    listDamage,
    taskDetail,
    count,
    sub_categery,
    showBorder,
    is_multiple,
    categorySelected,
    parentId,
    parentSelected,
    listCount,
    toggle,
    disableTenant,
    isAdditionalComment,
    additionalCommentsId,
  } = props;
  const obj = {};
  console.log({isAdditionalComment});
  const [loading, setLoading] = useState(false);
  const [openList, setOpenList] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [commentText, setCommentText] = useState(null);
  const [quentity, setQuentity] = useState(null);
  const [parentIdValue, setParentIdValue] = useState(null);
  const [selectedObject, setSelectedObject] = useState({});
  const [selectedNof, setSelectedNof] = useState(null);
  const [selectedData, setSelectedData] = useState(null);
  const [saveSelected, setSaveSelected] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [openMaterial, setOpenMaterial] = useState(false);
  const [materials, setMaterials] = useState({});
  const [callMaterial, setCallMaterial] = useState(false);
  const [justSaveData, setJustSaveData] = useState(true);
  const [confirm, setConfirm] = useState(false);
  const [firstVendor, setFirstVendor] = useState({});

  const [iosToggleRequest, setIosOpenMaterial] = useState(false);
  const [iosShowModal, setIosShowModal] = useState(false);
  const [iosOpenList, setIosOpenList] = useState(false);

  const idInsBooking = listDamage.inspection_data[0].id_snag_property;
  const {
    chklistQuesData,
    positionData,
    newMaterialArr = {},
    materialLists: {data: materialListsData},
  } = useSelector(state => state.taskdetails);
  const checjlistObj = chklistQuesData[idInsBooking];
  // const myRefs=useRef([])
  const myRefs = useRef(
    question.map(val => `accordion-${val.id_template_mapping}`),
  );

  useEffect(() => {
    if (saveSelected) {
      if (
        checjlistObj &&
        checjlistObj?.answer &&
        selectedData?.id_template_mapping in checjlistObj?.answer &&
        selectedNof in checjlistObj?.answer[selectedData?.id_template_mapping]
      )
        setSelectedObject(
          checjlistObj?.answer[selectedData?.id_template_mapping][selectedNof],
        );
      if (justSaveData) {
        setSaveSelected(false);
        setShowModal(true);
        setJustSaveData(true);
      }
    }
  }, [checjlistObj, saveSelected, selectedData, selectedNof, justSaveData]);
  useEffect(() => {
    if (callMaterial) {
      const data = {};
      data.is_app = 'Y';
      dispatch(getMaterialsList(data));
      setCallMaterial(false);
    }
  }, [callMaterial, dispatch, setCallMaterial]);
  const sectionListRef = useRef();
  const [dataSourceCords, setDataSourceCords] = useState([]);
  const preferedVendor =
    checjlistObj?.answer?.selectedVendor?.preferedVendor?.data;

  // const isVendorEmpty = checjlistObj?.answer?.VendorFirst?.Vendor?.data;

  // useEffect(() => {
  //   if (!isVendorEmpty) {
  //     isFirstTime(0);
  //   }
  //   if (isVendorEmpty === 1) {
  //     setLoading(true);
  //     const timer = setTimeout(
  //       () => (isVendorEmpty ? setConfirm(true) : null),
  //       1000,
  //     );
  //     const timer2 = setTimeout(() => setLoading(false), 1000);
  //     return () => {
  //       clearTimeout(timer);
  //       clearTimeout(timer2);
  //     };

  //     // setTimeout(() => {
  //     //   setConfirm(true);
  //     // }, [1000]);
  //   }
  // }, [isVendorEmpty]);

  const primaryVendorQuotatio = listDamage?.contractor_list?.map(item => {
    return {
      value: item.id,
      label: item.name,
      checked: true,
    };
  });

  const primaryVendor = listDamage?.contractor_list?.map(item => {
    return {value: item.id, label: item.name};
  });

  const listCategory = listDamage?.list_categery?.map(item => {
    return {value: item.category, label: item.category};
  });

  // this method is required as without this the data isn't passed correctly
  const toggleRequestModel = (data, nof) => {
    setSelectedNof(nof);
    setSelectedData(data);
    setSaveSelected(true);
    setCallMaterial(true);
    setJustSaveData(true);
    setShowModal(false);
    setMaterials({});
  };
  useEffect(() => {
    if (sub_categery == categorySelected) {
      setIsActive(null);
      count.forEach(val => {
        if (val === listCount) {
          setIsActive(`${categorySelected} ${is_multiple === '1' ? val : ''}`);
        }
      });
    }
  }, [categorySelected, sub_categery, listCount, count, is_multiple]);
  // useEffect(()=>{
  //     if(toggle){
  //         setIsActive(null)
  //     }
  // },[toggle,categorySelected])

  // useEffect(() => {

  //   if (vendorValue !== null) {
  //     dispatch(
  //       getRateCardList({
  //         id_community: taskDetail[0].id_community,
  //         id_contractor: vendorValue,
  //         // category,
  //       }),
  //     );
  //   }
  // }, [vendorValue]);

  /*   const initAdd = (data, nof) => {
      const saveRes = {
        details: data,
        numberOf: nof,
        quesction: {
          damage: null,
          id_template_question_answers: null,
          question_answer: null,
          taskType: null,
          payBy: null,
          materialList: null,
          image: null,
          description: null,
          Vendor: preferedVendor || null,
          newMaterial: null,
          newMaterialVendors: primaryVendorQuotatio || null,
          comment: null,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    }; */
  const initAdditional = nof => {
    const saveRes = {
      details: additionalCommentsId,
      numberOf: nof,
      quesction: {
        damage: 0,
        comment: null,
        id_template_question_answers: null,
        question_answer: null,
        taskType: 0,
        image: null,
        description: null,
      },
    };
    dispatch(savechklistquesctions(idInsBooking, saveRes));
  };
  const changeValue = (value, data, nof) => {
    if (value.question_answer === 'No') {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: nof,
        quesction: {
          damage: 0,
          id_template_question_answers: value.id_template_question_answers,
          question_answer: value.question_answer,
          taskType: 0,
          // payBy: null,
          // materialList: null,
          image: null,
          description: null,
          // Vendor: preferedVendor || null,
          // newMaterial: null,
          // newMaterialVendors: primaryVendorQuotatio || null,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (value.question_answer === 'Yes') {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: nof,
        quesction: {
          damage: 1,
          id_template_question_answers: value.id_template_question_answers,
          question_answer: value.question_answer,
          taskType: 0,
          // payBy: null,
          // materialList: null,
          image: null,
          description: null,
          // Vendor: null,
          // newMaterial: null,
          // newMaterialVendors: null,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (value.question_answer === 'N/A') {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: nof,
        quesction: {
          damage: 2,
          id_template_question_answers: value.id_template_question_answers,
          question_answer: value.question_answer,
          taskType: 0,
          // payBy: null,
          // materialList: null,
          image: null,
          description: null,
          // Vendor: null,
          // newMaterial: null,
          // newMaterialVendors: null,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (value.question_answer === 'AdditionalComments') {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: nof,
        quesction: {
          // damage: 0,
          taskType: 0,
          // payBy: null,
          // materialList: null,
          image: null,
          comment: null,
          // Vendor: preferedVendor || null,
          // newMaterialVendors: primaryVendorQuotatio || null,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    }
  };

  const handleCheckDropDownClicked = (selected, key, data, nof) => {
    let finalData;
    const selectedOptions = selected?.filter(item => item.checked === true);
    const PrevData =
      checjlistObj.answer[data.id_template_mapping] &&
      checjlistObj.answer[data.id_template_mapping]?.materialList;
    if (PrevData) {
      const filterDup = PrevData?.filter(
        elem => !selectedOptions?.find(({id}) => elem.id === id),
      );
      finalData = [...filterDup, ...selectedOptions];
    } else {
      finalData = [...selectedOptions];
    }
    const copy = finalData?.length > 0 ? finalData : null;
    handleAnswers(data, key, copy, nof);
  };
  const handleAdditional = (data, key, value, nof) => {
    const idTemplateMapping =
      checjlistObj.answer[data] && checjlistObj.answer[data][nof];
    console.log('handleAdditional', {data, key, value, nof, idTemplateMapping});
    const saveRes = {
      details: data,
      numberOf: String(nof),
      quesction: {
        ...idTemplateMapping,
        damage: 0,
        [key]: value,
      },
    };
    dispatch(savechklistquesctions(idInsBooking, saveRes));
  };
  const handleAnswers = (data, key, value, nof, saveObj = false) => {
    const idTemplateMapping =
      checjlistObj.answer[data.id_template_mapping] &&
      checjlistObj.answer[data.id_template_mapping][nof];

    const storeData =
      checjlistObj?.answer[data.id_template_mapping] &&
      checjlistObj.answer[data.id_template_mapping][nof];
    setSaveSelected(saveObj);
    if (storeData?.payBy === 3 && value === 4) {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: 5,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (storeData?.payBy === 3 && value === 3) {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: null,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (storeData?.payBy === 5 && value === 3) {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: 4,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (storeData?.payBy === 5 && value === 4) {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: 3,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (storeData?.payBy === 4 && value === 4) {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: null,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (storeData?.payBy === 4 && value === 3) {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: 5,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else if (idTemplateMapping) {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: value,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    } else {
      const saveRes = {
        details: data.id_template_mapping,
        numberOf: String(nof),
        quesction: {
          ...idTemplateMapping,
          [key]: value,
        },
      };
      dispatch(savechklistquesctions(idInsBooking, saveRes));
    }
  };

  const onChanged = (data, key, text, nof) => {
    setCommentText(text);
    // console.log('onChanged', text)

    // handleAnswers(data, key, commentText, nof)
  };

  const handlePrimaryVendor = (value, key, data, nof) => {
    if (value) {
      const primaryVendorQuotation = listDamage?.contractor_list?.map(item => {
        return value.value === item.id
          ? {value: item.id, label: item.name, checked: true}
          : {value: item.id, label: item.name, checked: false};
      });
      handleAnswers(data, 'newMaterialVendors', primaryVendorQuotation, nof);
      // setVendorValue(value?.value)
    }

    let counter = checjlistObj?.answer?.VendorFirst?.Vendor?.data;
    // console.log('counter insdie', counter);
    if (value && counter < 3) {
      counter++;
      setFirstVendor(value);
      isFirstTime(counter);
    }
    // if (value && !preferedVendor) {
    // handleVendorforAllQues(value);
    // }

    if (value) {
      handleAnswers(data, key, value, nof);
      // setVendorValue(value?.value)
    }
  };

  const toggleCheck = (index, data, nof) => {
    const copy =
      checjlistObj?.answer[data.id_template_mapping][nof].newMaterial;
    copy[index].checked = !copy[index].checked;
    handleAnswers(data, 'newMaterial', copy, nof);
  };
  const deleteResult = (index, data, nof) => {
    const copy =
      checjlistObj?.answer[data.id_template_mapping][nof].newMaterial;
    const item = copy.filter((_, i) => i !== index);
    // console.log({ index, data, nof, copy, item });
    handleAnswers(data, 'newMaterial', item, nof);
    setSaveSelected(true);
    setJustSaveData(false);
  };

  const listModal = (value, ques, nof) => {
    setShowModal(false);
    setIosOpenList(true);
    setOpenMaterial(false);
    // handleAnswersNewMaterial(value, ques, nof);
    // handleAnswers(ques, 'newMaterial', value, nof);
  };

  useEffect(() => {
    if (iosOpenList) {
      setLoading(true);
      const timer = setTimeout(() => {
        setIosOpenList(false);
        setOpenList(true);
        setLoading(false);
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [iosOpenList]);

  const openAddMaterial = () => {
    setShowModal(false);
    setOpenList(false);
    setIosOpenMaterial(true);
  };

  useEffect(() => {
    if (iosToggleRequest) {
      setLoading(true);
      const timer = setTimeout(() => {
        setIosOpenMaterial(false);
        setOpenMaterial(true);
        setLoading(false);
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [iosToggleRequest]);

  const closeAddMaterial = () => {
    setOpenMaterial(false);
    setIosShowModal(true);
    setOpenList(false);
  };

  useEffect(() => {
    if (iosShowModal) {
      setLoading(true);
      const timer = setTimeout(() => {
        setIosShowModal(false);
        setShowModal(true);
        setLoading(false);
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [iosShowModal]);

  const saveMaterial = obj => {
    setMaterials(obj);
  };

  const handleVendor = (index, data, nof) => {
    const copy =
      checjlistObj?.answer[data.id_template_mapping][nof].newMaterialVendors;
    copy[index].checked = !copy[index].checked;
    handleAnswers(data, 'newMaterialVendors', copy, nof);
  };
  const saveMaterialTrue = data => {
    dispatch(saveMaterialDetails(idInsBooking, data));
  };
  const handleVendorforAllQues = ven => {
    const saveRes = {
      details: 'selectedVendor',
      numberOf: 'preferedVendor',
      quesction: {
        data: ven,
      },
    };
    dispatch(savechklistquesctions(idInsBooking, saveRes));
  };
  const isFirstTime = ven => {
    const saveRes = {
      details: 'VendorFirst',
      numberOf: 'Vendor',
      quesction: {
        data: ven,
      },
    };
    dispatch(savechklistquesctions(idInsBooking, saveRes));
  };
  const handleSecScroll = () => {
    if (sectionListRef.current) {
      sectionListRef.current.scrollToLocation({
        animated: true,
        sectionIndex: 0,
        itemIndex: 1,
        // viewPosition: 0,
      });
    }
  };

  const scrollToIndexFailed = error => {
    const offset = error.averageItemLength * error.index;
    sectionListRef.current.scrollToffset({offset});
    setTimeout(
      () =>
        sectionListRef.current.scrollToLocation({
          animated: true,
          // index: error.index,
          sectionIndex: error.index,
          itemIndex: 1,
        }),
      100,
    );
  };

  const RenderUi = nof => {
    // const additionalData = { id_template_mapping: 'AdditionalComments' };

    // const storeData =
    //   checjlistObj?.answer[additionalData.id_template_mapping] &&
    //   checjlistObj.answer[additionalData.id_template_mapping][nof];
    return (
      <>
        <View style={styles.container}>
          {question?.map((data, key) => {
            const isColor =
              checjlistObj?.answer[data.id_template_mapping] &&
              checjlistObj?.answer[data.id_template_mapping][nof];
            return (
              <View
                key={`RenderUi-${data.id_template_mapping}`}
                // ref={`accordion-${data.id_template_mapping}`}
                ref={data.innerRef}
                onLayout={event => {
                  const {layout} = event.nativeEvent;
                  count.map(val => {
                    obj[data.id_template_mapping] = {
                      ...obj[data.id_template_mapping],
                      [val]: {
                        y: layout.y,
                      },
                    };
                  });
                  // obj[data.id_template_mapping].push({y:layout.height})
                  if (key == question.length - 1) {
                    dispatch(saveYValues(obj));
                  }
                }}
                style={[
                  styles.wrapper,
                  {
                    borderWidth: 1,
                    borderColor:
                      (isColor?.damage === 1 && 'green') ||
                      (isColor?.damage === 2 && 'green') ||
                      (isColor?.damage === 0 &&
                      isColor?.taskType &&
                      isColor?.image
                        ? 'green'
                        : showBorder && 'red') ||
                      theme.color.secondaryLight2,
                  },
                ]}>
                {data?.question ? (
                  <Text variant="taskBold" size={Platform.isPad ? 21 : 19}>
                    {data.question}*
                  </Text>
                ) : (
                  <View />
                )}

                <View style={styles.row}>
                  {data.question_answers.map(quest => {
                    return (
                      <Button
                        key={`key-${quest.id_template_question_answers}`}
                        title={quest.question_answer}
                        type={
                          checjlistObj?.answer[data.id_template_mapping] &&
                          checjlistObj.answer[data.id_template_mapping][nof] &&
                          checjlistObj.answer[data.id_template_mapping][nof]
                            .id_template_question_answers ===
                            quest.id_template_question_answers
                            ? checjlistObj.answer[data.id_template_mapping][nof]
                                ?.question_answer
                            : 'default'
                        }
                        style={styles.button1}
                        titleStyle={styles.appFontReg}
                        onPress={() => changeValue(quest, data, nof)}
                      />
                    );
                  })}
                </View>

                {checjlistObj?.answer[data.id_template_mapping] &&
                  checjlistObj?.answer[data.id_template_mapping][nof] &&
                  checjlistObj.answer[data.id_template_mapping][nof]?.damage ===
                    0 && (
                    <View>
                      {data?.flag == 1 && (
                        <InputText
                          placeholder="Enter Title"
                          onChangeText={text =>
                            handleAnswers(data, 'comment', text, nof)
                          }
                          value={
                            checjlistObj?.answer[data.id_template_mapping] &&
                            checjlistObj?.answer[data.id_template_mapping][
                              nof
                            ] &&
                            checjlistObj.answer[data.id_template_mapping][nof]
                              ?.comment
                          }
                          style={[
                            styles.commentbox,
                            {
                              borderColor: theme.color.secondaryLight2,
                              padding: theme.spacing.sm,
                              borderRadius: theme.borderRadius.x,
                              backgroundColor: theme.color.primaryWhite,
                              marginBottom: 0,
                            },
                          ]}
                          textFieldStyle={styles.txtFieldStyle}
                          blurOnSubmit={false}
                          multiline
                          numberOfLines={2}
                          textFieldContainerStyle={styles.txtfldcontainstyle}
                        />
                      )}
                      <View style={{paddingTop: 10}}>
                        <Text variant="taskBold">Issue type*</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          paddingTop: 10,
                          paddingBottom: 10,
                        }}>
                        <Button
                          title="Major"
                          type={
                            checjlistObj?.answer[data.id_template_mapping] &&
                            checjlistObj?.answer[data.id_template_mapping][
                              nof
                            ] &&
                            checjlistObj.answer[data.id_template_mapping][nof]
                              .taskType === 1
                              ? 'No'
                              : 'default'
                          }
                          style={styles.button1}
                          titleStyle={styles.appFontReg}
                          onPress={() =>
                            handleAnswers(data, 'taskType', 1, nof)
                          }
                        />
                        <Button
                          title="Minor"
                          type={
                            checjlistObj?.answer[data.id_template_mapping] &&
                            checjlistObj?.answer[data.id_template_mapping][
                              nof
                            ] &&
                            checjlistObj.answer[data.id_template_mapping][nof]
                              .taskType === 2
                              ? 'Yes'
                              : 'default'
                          }
                          style={styles.button1}
                          titleStyle={styles.appFontReg}
                          onPress={() =>
                            handleAnswers(data, 'taskType', 2, nof)
                          }
                        />
                      </View>

                      {loading && <Loader open={loading} />}
                    </View>
                  )}

                {((checjlistObj?.answer[data.id_template_mapping] &&
                  checjlistObj.answer[data.id_template_mapping][nof] &&
                  checjlistObj.answer[data.id_template_mapping][nof]?.damage ==
                    0) ||
                  (checjlistObj?.answer[data.id_template_mapping] &&
                    checjlistObj.answer[data.id_template_mapping][nof] &&
                    checjlistObj.answer[data.id_template_mapping][nof]
                      ?.damage == 1)) && (
                  <View>
                    <ImageUpload
                      onImagesUpdate={img => {
                        img != null && handleAnswers(data, 'image', img, nof);
                      }}
                      preImages={
                        checjlistObj?.answer[data.id_template_mapping] &&
                        checjlistObj?.answer[data.id_template_mapping][nof] &&
                        checjlistObj.answer[data.id_template_mapping][nof]
                          ?.image
                      }
                      // style={{ paddingLeft: 6, paddingRight: 6 }}
                      renderImage={
                        checjlistObj?.answer[data.id_template_mapping] &&
                        checjlistObj?.answer[data.id_template_mapping][nof] &&
                        checjlistObj.answer[data.id_template_mapping][nof]
                          ?.image
                      }
                      multiple
                    />
                    <View>
                      <InputText
                        placeholder="Comment"
                        onChangeText={text =>
                          handleAnswers(data, 'description', text, nof)
                        }
                        value={
                          checjlistObj?.answer[data.id_template_mapping] &&
                          checjlistObj?.answer[data.id_template_mapping][nof] &&
                          checjlistObj.answer[data.id_template_mapping][nof]
                            .description
                        }
                        style={[
                          styles.commentbox,
                          {
                            borderColor: theme.color.secondaryLight2,
                            padding: theme.spacing.sm,
                            borderRadius: theme.borderRadius.x,
                            backgroundColor: theme.color.primaryWhite,
                            marginBottom: 0,
                          },
                        ]}
                        textFieldStyle={styles.txtFieldStyle}
                        blurOnSubmit={false}
                        multiline
                        numberOfLines={2}
                        textFieldContainerStyle={styles.txtfldcontainstyle}
                      />
                    </View>
                  </View>
                )}
              </View>
            );
          })}
        </View>
      </>
    );
  };
  const RenderAdditionalUI = nof => {
    let addData =
      checjlistObj?.answer[additionalCommentsId] &&
      checjlistObj?.answer[additionalCommentsId][nof];
    console.log('add', addData);
    return (
      <>
        <View style={styles.container}>
          <View
            style={[
              styles.wrapper,
              {
                borderWidth: 1,
                borderColor: theme.color.secondaryLight2,
              },
            ]}>
            <InputText
              placeholder="Enter Title"
              onChangeText={text =>
                handleAdditional(additionalCommentsId, 'comment', text, nof)
              }
              value={addData && addData?.comment}
              style={[
                styles.commentbox,
                {
                  borderColor: theme.color.secondaryLight2,
                  padding: theme.spacing.sm,
                  borderRadius: theme.borderRadius.x,
                  backgroundColor: theme.color.primaryWhite,
                  marginBottom: 0,
                },
              ]}
              textFieldStyle={styles.txtFieldStyle}
              blurOnSubmit={false}
              multiline
              numberOfLines={2}
              textFieldContainerStyle={styles.txtfldcontainstyle}
            />
            <View style={{paddingTop: 10}}>
              <Text variant="taskBold">Issue type*</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
              }}>
              <Button
                title="Major"
                type={addData && addData?.taskType === 1 ? 'No' : 'default'}
                style={styles.button1}
                titleStyle={styles.appFontReg}
                onPress={() =>
                  handleAdditional(additionalCommentsId, 'taskType', 1, nof)
                }
              />
              <Button
                title="Minor"
                type={addData && addData?.taskType === 2 ? 'Yes' : 'default'}
                style={styles.button1}
                titleStyle={styles.appFontReg}
                onPress={() =>
                  handleAdditional(additionalCommentsId, 'taskType', 2, nof)
                }
              />
            </View>
            <ImageUpload
              onImagesUpdate={img => {
                img != null &&
                  handleAdditional(additionalCommentsId, 'image', img, nof);
              }}
              preImages={addData && addData?.image}
              // style={{ paddingLeft: 6, paddingRight: 6 }}
              renderImage={addData && addData?.image}
              multiple
            />
            <View>
              <InputText
                placeholder="Comment"
                onChangeText={text =>
                  handleAdditional(
                    additionalCommentsId,
                    'description',
                    text,
                    nof,
                  )
                }
                value={addData && addData?.description}
                style={[
                  styles.commentbox,
                  {
                    borderColor: theme.color.secondaryLight2,
                    padding: theme.spacing.sm,
                    borderRadius: theme.borderRadius.x,
                    backgroundColor: theme.color.primaryWhite,
                    marginBottom: 0,
                  },
                ]}
                textFieldStyle={styles.txtFieldStyle}
                blurOnSubmit={false}
                multiline
                numberOfLines={2}
                textFieldContainerStyle={styles.txtfldcontainstyle}
              />
            </View>
          </View>
        </View>
      </>
    );
  };
  const setIsActiveFunction = (key, nof) => {
    console.log('key', {key, nof, isAdditionalComment});
    setIsActive(key);
    if (isAdditionalComment) {
      let addData =
        checjlistObj?.answer[additionalCommentsId] &&
        checjlistObj?.answer[additionalCommentsId][nof];

      if (!addData) {
        initAdditional(nof);
      }
    }
    // const additionalData = { id_template_mapping: 'AdditionalComments' };
    // const quest = {
    //   question_answer: 'AdditionalComments',
    // };
    // handleAnswers(additionalData, 'newMaterialVendors', primaryVendorQuotatio, nof);

    // if (parentId === 'AdditionalComments') {
    //   changeValue(quest, additionalData, nof);
    // }
  };

  const handleRender = () => {
    return (
      <>
        {count.map(a => {
          return (
            <>
              <Panel
                key={`key-${a}`}
                title={`${sub_categery} ${is_multiple === '1' ? a : ''}`}
                onToggle={key => setIsActiveFunction(key, a)}
                isActive={
                  isActive === `${sub_categery} ${is_multiple === '1' ? a : ''}`
                }>
                {isAdditionalComment
                  ? RenderAdditionalUI(String(a))
                  : RenderUi(String(a))}
              </Panel>
            </>
          );
        })}
        {/* {count.map((a) => {
          return (
            <>
              <TouchableWithoutFeedback
                onPress={() => setToggle(!toggle)}
                key={a}
              >
                <View
                  style={{
                    borderBottomWidth: 2,
                    borderBottomColor: theme.color.secondaryGreenDark,
                    padding: 5,
                  }}
                >
                  <Text variant="taskBold" size={19}>
                    {sub_categery} {is_multiple === '1' ? a : null}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
              {RenderUi(String(a))}
            </>
          );
        })} */}
      </>
    );
  };
  const onSuccess = () => {
    // console.log('vendor', isVendorEmpty, firstVendor);
    if (firstVendor) {
      handleVendorforAllQues(firstVendor);
      setConfirm(prev => !prev);
      isFirstTime(2);
    }
  };
  const onFailure = () => {
    // console.log('vendor', isVendorEmpty, firstVendor);
    isFirstTime(0);
    setConfirm(prev => !prev);
  };
  const handleSectionList = item => {
    return (
      <Panel
        key={`sub_categery-${item}`}
        title={`${sub_categery} ${is_multiple === '1' ? item : ''}`}
        onToggle={key => setIsActive(key)}
        isActive={
          isActive === `${sub_categery} ${is_multiple === '1' ? item : ''}`
        }>
        {RenderUi(String(item))}
      </Panel>
    );
  };

  return (
    <View>
      {confirm && (
        <ConfirmModal
          open={confirm}
          close={onFailure}
          title={`${firstVendor?.label} will be selected as the default Vendor`}
          onSuccess={onSuccess}
          onFailure={onFailure}
        />
      )}

      {handleRender()}

      {/* <SectionList
        sections={[{ data: count }]}
        ref={sectionListRef}
        keyExtractor={(item, index) => item + index}
        onScrollToIndexFailed={scrollToIndexFailed}
        renderItem={({ item }) => handleSectionList(item)}
      /> */}
    </View>
  );
};

export default AccordianQuestion;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    paddingTop: 10,
  },
  // eslint-disable-next-line react-native/no-color-literals
  wrapper: {
    paddingBottom: 15,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
    borderWidth: 0.3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20,
    elevation: 3,
    paddingTop: 15,
  },
  row: {
    flexDirection: 'row',
    paddingTop: 20,
    justifyContent: 'space-between',
  },
  button1: {
    width: Platform.isPad ? 300 : 100,
    marginRight: Platform.isPad ? 15 : 5,
    padding: Platform.isPad ? 20 : 10,
    borderRadius: 10,
  },
  commentbox: {
    borderWidth: 1,
    height: Platform.isPad ? 90 : 70,
    // marginLeft: 15,
    // marginRight: 15,
    // marginBottom: 30,
    marginTop: 20,
    alignItems: 'flex-start',
  },
  card: {
    borderWidth: 1,
    paddingHorizontal: 15,
    marginBottom: 10,
    paddingVertical: 10,
    // flexWrap: 'wrap',
  },
  txtFieldStyle: {
    textAlignVertical: 'top',
    alignItems: 'flex-start',
    ...getAppFont('Regular'),
  },
  txtfldcontainstyle: {
    borderBottomWidth: 0,
  },
  selectvendorStyle: {
    paddingTop: 15,
    paddingBottom: 5,
  },
  dropDownContainer: {
    justifyContent: 'space-between',
    left: 0,
    height: 46,
    borderRadius: 10,
  },
  appFontReg: {
    ...getAppFont('SemiBold'),
  },
  rownew: {
    flexDirection: 'row',

    marginBottom: 10,
    // flex: 1,
    justifyContent: 'space-between',
    // height: 30,
    // backgroundColor: 'red'
  },
});
