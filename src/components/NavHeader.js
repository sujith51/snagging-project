/* eslint-disable no-nested-ternary */
/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Platform } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { HeaderIcon, BackBtnIcon } from '_svg';
import { getAppFont, useTheme } from '../utils';

const NavHeader = ({
  type = 'inner',
  backEn = true,
  title = null,
  onBackPress = null,
  actions = null,
  onModalPress = () => {},
}) => {
  const [titleLine, settitleLine] = useState(1);
  const navigation = useNavigation();
  const { name } = useRoute();
  const theme = useTheme();

  const newTitle =
    title && title.includes('_') ? title.replace(/_|(^,\s)/g, ' ') : title;
  const headerOptions =
    actions &&
    actions.map((item, index) => (
      <TouchableOpacity key={`nav-head-action-${index}`} onPress={item.onPress}>
        <View
          style={{
            paddingHorizontal: 5,
            paddingVertical: 10,
            flex: 0,
            marginBottom: -10,
          }}
        >
          {item.icon}
        </View>
      </TouchableOpacity>
    ));
  const headerLeft =
    type === 'inner' ? (
      backEn && (
        <TouchableOpacity
          onPress={
            onBackPress ||
            (navigation.canGoBack ? () => navigation.goBack() : null)
          }
        >
          <View
            style={{
              paddingHorizontal: 20,
              paddingRight: 30,
              paddingVertical: 15,
            }}
          >
            <BackBtnIcon />
          </View>
        </TouchableOpacity>
      )
    ) : type === 'modal' ? (
      <TouchableOpacity onPress={() => onModalPress()}>
        <View
          style={{
            paddingHorizontal: 20,
            paddingRight: 20,
            paddingVertical: 15,
          }}
        >
          {/* <Image source={require('../../../assets/icons/close.png')} /> */}
        </View>
      </TouchableOpacity>
    ) : (
      <TouchableOpacity
        onPress={() => {
          // Enable once the Profile Screen Ready -
          if (name === 'Profile') {
            navigation.goBack();
          } else {
            navigation.navigate('Profile');
          }
        }}
      >
        <View
          style={{
            paddingHorizontal: 20,
            paddingRight: 20,
            paddingVertical: 15,
          }}
        >
          {/* <Image
            style={{ width: 35, height: 35 }}
            source={require('../../../assets/icons/profile.png')}
          /> */}
          <HeaderIcon />
        </View>
      </TouchableOpacity>
    );
  return (
    <View
      style={{
        height: 100,
        justifyContent: 'flex-end',
        paddingHorizontal: 10,
        flex: 0,
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingBottom: 10,
        }}
      >
        <View style={{ flex: 0 }}>{headerLeft}</View>
        <View style={type === 'inner' ? { flex: 1 } : { flexGrow: 1 }}>
          <TouchableOpacity onPress={() => settitleLine(titleLine ? null : 1)}>
            <Text
              style={{
                // ...theme.typography.navHead,
                color: theme.color.primaryWhite,
                ...getAppFont('Bold'),
                fontSize: Platform.isPad ? 26 : 24,
                alignSelf: 'stretch',
              }}
              numberOfLines={titleLine}
              ellipsizeMode="tail"
            >
              {newTitle || name}
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 0,
            justifyContent: 'flex-end',
            flexDirection: 'row',
            top: 7,
          }}
        >
          {headerOptions && headerOptions}
        </View>
      </View>
    </View>
  );
};

export default NavHeader;
