/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, Platform } from 'react-native';
import { BarChart, Grid, XAxis } from 'react-native-svg-charts';
import { Text as TextSvg, Defs, LinearGradient, Stop } from 'react-native-svg';
import { useTheme, getAppFont } from '../utils';

const BarGraph = ({ data = [] }) => {
  const theme = useTheme();

  const CUT_OFF = 20;

  // Top Labels.
  const Labels = ({ x, y, bandwidth, data }) => {
    return data.map((item, index) => (
      <TextSvg
        key={index}
        x={x(index) + bandwidth / 2}
        y={item.value < CUT_OFF ? y(item.value) - 7 : y(item.value) + -6}
        fontSize={Platform.isPad ? 18 : 14}
        fill={theme.color.primary}
        alignmentBaseline="middle"
        textAnchor="middle"
      >
        {item.value}
      </TextSvg>
    ));
  };

  return (
    <View
      style={{
        flex: 1,
        marginTop: 40,
        marginBottom: 20,
        backgroundColor: theme.color.primaryWhite,
        borderRadius: 20,
      }}
    >
      <BarChart
        style={{
          height: 200,
          borderBottomWidth: 2,
          borderBottomColor: theme.color.primaryLight,
          marginLeft: 2,
          marginRight: 1,
        }}
        data={data}
        gridMin={0}
        yAccessor={({ item }) => item.value}
        contentInset={{ top: 20 }}
        spacingInner={0.7}
        spacingOuter={0.5}
      >
        <Labels />
      </BarChart>
      <XAxis
        style={{ marginTop: 10, marginRight: 20, marginLeft: 30 }}
        data={data}
        formatLabel={(_, index) => data[index].label}
        contentInset={{
          left: Platform.isPad ? 31 : 25,
          right: Platform.isPad ? 28 : 25,
        }}
        svg={{
          fontSize: Platform.isPad ? 16 : 12,
          ...getAppFont('Bold'),
          fill: theme.color.primaryDark,
        }}
      />
    </View>
  );
};

export default BarGraph;
