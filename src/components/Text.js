// @flow
import * as React from 'react';
import { Text as RNText, Platform } from 'react-native';
import { useTheme, textVariant, colorType } from '../utils';

type Props = {
  variant: textVariant,
  color: colorType,
  size: number | string,
};
// please pass variant, color and size matching textVariant and colorType in theme.js
const Text = (props: Props) => {
  const theme = useTheme();
  const {
    variant = 'title',
    color = 'primaryBlack',
    size = Platform.isPad ? 20 : 16,
    style,
    ...other
  } = props;
  return (
    <RNText
      style={[
        { ...theme.textVariants[variant] },
        { color: theme.color[color] || color },
        { fontSize: size },
        { ...style },
      ]}
      {...other}
    />
  );
};

export default Text;
