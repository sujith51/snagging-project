/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import { getAppFont, useTheme } from '../utils';

const SIZE = 25;

const IncrementDecrement = ({
  qtyValue,
  inc = () => {},
  dec = () => {},
  disable = false,
  inputQty,
}) => {
  const theme = useTheme();
  const opacity = disable ? 0.6 : 1;
  return (
    <View style={styles.cotainer}>
      <View style={{ padding: 0 }}>
        <TouchableWithoutFeedback onPress={() => dec()} disabled={disable}>
          <View
            style={[
              styles.plusMinusCont,
              { backgroundColor: theme.color.primary, opacity },
            ]}
          >
            <Text
              style={{
                color: theme.color.primaryWhite,
                fontSize: 25,
                bottom: 3,
              }}
            >
              -
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
      <View style={styles.TxtCont}>
        {/* <Text
          style={{
            borderWidth: 0.5,
            textAlign: 'center',
            width: SIZE,
            height: SIZE,
            justifyContent: 'center',
            ...getAppFont('Regular'),
            // fontSize: 16,
            color: theme.color.primaryBlack,
            // marginTop: 0
          }}
          // keyboardType="number-pad"
          // numericvalue
          // onChangeText={(txt) => inputQty(Number(txt))}
          // value={String(value > 0 ? value : '')}
          // maxLength={2}
        >
          {value > 0 ? value : ''} String(qtyValue)
        </Text> */}
        <TextInput
          style={styles.txtInput}
          maxLength={2}
          keyboardType="numeric"
          onChangeText={(text) => inputQty(text)}
          value={String(qtyValue)}
        />
      </View>
      <View>
        <View style={{ padding: 0 }}>
          <TouchableWithoutFeedback onPress={() => inc()} disabled={disable}>
            <View
              style={[
                styles.plusMinusCont,
                { backgroundColor: theme.color.primary, opacity },
              ]}
            >
              <Text
                style={{
                  color: theme.color.primaryWhite,
                  fontSize: 20,
                  bottom: 2,
                }}
              >
                +
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </View>
  );
};

export default IncrementDecrement;

const styles = StyleSheet.create({
  cotainer: {
    flexDirection: 'row',
    alignItems: 'center',
    // bottom: 10,
  },
  plusMinusCont: {
    width: SIZE,
    height: SIZE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  TxtCont: {
    marginLeft: 6,
    marginRight: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    width: SIZE,
    height: SIZE,
    padding: 0,
    margin: 0,
    borderWidth: 0.5,
    textAlign: 'center',
  },
});
