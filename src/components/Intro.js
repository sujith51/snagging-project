import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ProvisConnectIcon, ProvisIcon } from '_svg';
// import { Swiper } from '_components';
import { useTheme, getAppFont } from '../utils';
import Swiper from './ui-elements/Swiper';

const Intro = () => {
  // :States
  const [swipeIndex, setSwipeIndex] = useState(0);
  const theme = useTheme();
  const { navigate } = useNavigation();

  // :Effects

  // :Functions
  const skipPressed = () => {
    navigate('Login');
  };

  const swiperData = [
    {
      title: 'Reduce downtime, gain insights',
      desc:
        'Description related to the above heading need to be typed based on Provis',
    },
    {
      title: 'Streamline your workflow',
      desc:
        'Description related to the above heading need to be typed based on Provis',
    },
    {
      title: 'Stay informed and upto date',
      desc:
        'Description related to the above heading need to be typed based on Provis',
    },
  ].map((item) => (
    <View>
      <View style={styles.swiperTitCont}>
        <Text
          style={[styles.swiperTitTxt, { color: theme.color.primaryWhite }]}
        >
          {item.title}
        </Text>
      </View>

      <View style={styles.swiperDescCont}>
        <Text
          style={[styles.swiperDescTxt, { color: theme.color.primaryWhite }]}
        >
          {item.desc}
        </Text>
      </View>
    </View>
  ));

  // :Render
  return (
    <View style={[styles.container, { backgroundColor: theme.color.primary }]}>
      <ScrollView>
        <View style={styles.welcomeContainer}>
          <Text
            style={[styles.welcomeTxt, { color: theme.color.primaryWhite }]}
          >
            WELCOME TO
          </Text>
        </View>

        <View style={styles.iconContainer}>
          <View>
            <ProvisIcon />
          </View>
          <View style={styles.iconSubCont}>
            <ProvisConnectIcon />
          </View>
        </View>

        <View style={{ paddingBottom: 100 }}>
          <Swiper
            data={swiperData}
            onIndexChanged={setSwipeIndex}
            index={swipeIndex}
          />
        </View>

        <TouchableWithoutFeedback onPress={skipPressed}>
          <View style={styles.skipCont}>
            <Text style={[styles.skipTxt, { color: theme.color.primaryWhite }]}>
              {swiperData.length === swipeIndex + 1 ? 'CONTINUE' : 'SKIP'}
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    </View>
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcomeContainer: {
    alignItems: 'center',
  },
  welcomeTxt: {
    marginTop: 162,
    fontSize: 16,
    ...getAppFont('Bold'),
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 19,
  },
  iconSubCont: {
    paddingLeft: 13,
  },
  skipCont: {
    // marginBottom: 90,
    paddingTop: 20,
    paddingBottom: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  skipTxt: {
    fontSize: 17,

    textDecorationLine: 'underline',
    ...getAppFont('SemiBold'),
  },
  swiperTitCont: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 177,
  },
  swiperTitTxt: {
    width: 340,
    fontSize: 32,
    textAlign: 'center',
    ...getAppFont('Bold'),
  },
  swiperDescCont: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 38,
  },
  swiperDescTxt: {
    width: 310,
    fontSize: 14,
    textAlign: 'center',
    ...getAppFont('Regular'),
  },
});
