import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Modal,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import Text from './Text';
import { useTheme, getAppFont, numberFormat, config, theme } from '../utils';
import Button from './Button';

const QuantityModal = ({
  open,
  close,
  data,
  onContinue,
  openTitleModal,
  materialArray,
  disabled,
}) => {
  const theme = useTheme();

  const openModal = (text) => {
    openTitleModal(text);
  };

  return (
    <View>
      <View>
        <ReactNativeModal
          isVisible={open}
          onBackdropPress={close}
          onBackButtonPress={close}
          style={[
            styles.container,
            { backgroundColor: theme.color.secondaryLight },
          ]}
          animationIn="zoomIn"
          animationOut="zoomOut"
        >
          <ScrollView
            // contentContainerStyle={{
            //     justifyContent: 'center'
            // }}
            style={{
              backgroundColor: theme.color.primaryWhite,
              padding: 20,
              borderRadius: 10,
              paddingTop: 50,
              // maxHeight: 700,
              // flex: 1,
            }}
          >
            {(data?.ownerQty > 0 ||
              data?.tenantQty > 0 ||
              (data?.qtyOwnerValue > 0 && data?.qtyTenantValue > 0) ||
              data?.additionalData ||
              data?.totalQtr > 0) && (
                <Text
                  variant="taskBold"
                  color="primary"
                  size={20}
                  style={{ paddingBottom: 10 }}
                >
                  Inspection Report
                </Text>
              )}

            {data?.ownerQty > 0 && (
              <View style={styles.wrapper}>
                {/* <Text
                            variant="taskBold"
                            size={16}
                            style={styles.boldText}
                        >
                            Owner Damage Charges
                            </Text> */}
                <View style={styles.row}>
                  <Text variant="taskBold" size={16} style={styles.boldText}>
                    Owner Damage Charges
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.qtyHeader}
                    size={14}
                  >
                    Qty
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.prices}
                    size={14}
                  >
                    Price(AED)
                  </Text>
                </View>

                {data?.payedBy?.owner?.map((item, index) => {
                  return (
                    <View key={index} style={styles.row}>
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={{
                          width: 15,
                          fontSize: 12,
                          fontWeight: '500',
                          color: 'black',
                        }}
                      >
                        {index + 1})
                      </Text>
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.materialName}
                        numberOfLines={1}
                        onPress={() => openModal(item.material_name)}
                      >
                        {item.material_name}
                      </Text>
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.qtyBody}
                        size={16}
                      >
                        {numberFormat(item.qty)}
                      </Text>
                      <Text
                        variant="textRegular"
                        color="primary"
                        size={16}
                        style={styles.pricesBody}
                      >
                        {numberFormat(item.qty * item.price)}
                      </Text>
                    </View>
                  );
                })}
              </View>
            )}
            {data?.tenantQty > 0 && (
              <View style={styles.wrapper}>
                <View style={styles.row}>
                  <Text variant="taskBold" size={16} style={styles.boldText}>
                    Tenant Damage Charges
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.qtyHeader}
                    size={14}
                  >
                    Qty
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.prices}
                    size={14}
                  >
                    Price(AED)
                  </Text>
                </View>

                {data?.payedBy?.tenant?.map((item, index) => {
                  return (
                    <View key={index} style={styles.row}>
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={{
                          width: 15,
                          fontSize: 12,
                          fontWeight: '500',
                          color: 'black',
                        }}
                      >
                        {index + 1})
                      </Text>
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.materialName}
                        numberOfLines={1}
                        onPress={() => openModal(item.material_name)}
                      >
                        {item.material_name}
                      </Text>
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.qtyBody}
                      >
                        {numberFormat(item.qty)}
                      </Text>
                      <Text
                        variant="textRegular"
                        color="primary"
                        size={16}
                        style={styles.pricesBody}
                      >
                        {numberFormat(item.qty * item.price)}
                      </Text>
                    </View>
                  );
                })}
              </View>
            )}

            {data?.qtyOwnerValue > 0 && data?.qtyTenantValue > 0 && (
              <View style={styles.wrapper}>
                <View style={styles.row}>
                  <Text variant="taskBold" size={16} style={styles.boldText}>
                    Owner & Tenant Charges{' '}
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.qtyHeader}
                    size={14}
                  >
                    Qty
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.prices}
                    size={14}
                  >
                    Price(AED)
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text
                    variant="textRegular"
                    color="primary"
                    size={16}
                    style={{
                      width: 195,
                      fontSize: 12,
                      fontWeight: 'bold',
                      color: 'black',
                    }}
                  >
                    Owner Damage Charges
                  </Text>
                </View>

                {data?.payedBy?.owner_tenant
                  ?.filter((item) => item?.qtyOwner !== 0)
                  ?.map((item, index) => {
                    return (
                      <View key={index} style={styles.row}>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={{
                            width: 15,
                            fontSize: 12,
                            fontWeight: '500',
                            color: 'black',
                          }}
                        >
                          {index + 1})
                        </Text>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={styles.materialName}
                          numberOfLines={1}
                          onPress={() => openModal(item.material_name)}
                        >
                          {item.material_name}
                        </Text>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={styles.qtyBody}
                          size={16}
                        >
                          {numberFormat(item.qtyOwner)}
                        </Text>
                        <Text
                          variant="textRegular"
                          color="primary"
                          size={16}
                          style={styles.pricesBody}
                        >
                          {numberFormat(
                            (item.qtyOwner || 0) * (item.price || 0),
                          )}
                        </Text>
                      </View>
                    );
                  })}

                <View style={styles.row}>
                  <Text
                    variant="textRegular"
                    color="primary"
                    size={16}
                    style={{
                      width: 195,
                      fontSize: 12,
                      fontWeight: 'bold',
                      color: 'black',
                    }}
                  >
                    Tenant Damage Charges
                  </Text>
                </View>
                {data?.payedBy?.owner_tenant
                  ?.filter((item) => item?.qtyTenant !== 0)
                  ?.map((item, index) => {
                    return (
                      <View key={index} style={styles.row}>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={{
                            width: 15,
                            fontSize: 12,
                            fontWeight: '500',
                            color: 'black',
                          }}
                        >
                          {index + 1})
                        </Text>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={styles.materialName}
                          numberOfLines={1}
                          onPress={() => openModal(item.material_name)}
                        >
                          {item.material_name}
                        </Text>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={styles.qtyBody}
                          size={16}
                        >
                          {numberFormat(item.qtyTenant)}
                        </Text>
                        <Text
                          variant="textRegular"
                          color="primary"
                          size={16}
                          style={styles.pricesBody}
                        >
                          {numberFormat(
                            (item.qtyTenant || 0) * (item.price || 0),
                          )}
                        </Text>
                      </View>
                    );
                  })}
              </View>
            )}
            {data?.additionalData &&
              Object.keys(data?.additionalData)?.map((a) =>
                data?.additionalData[a].materialList?.map((item, index) => {
                  // setAdditionalQty(item.qty)
                  // setAdditionalPrice(item.qty * item.price)
                  return (
                    <View style={styles.wrapper}>
                      <View style={styles.row}>
                        <Text
                          variant="taskBold"
                          // size={16}
                          style={styles.boldText}
                        >
                          Additional Comments{' '}
                        </Text>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={styles.qtyHeader}
                          size={14}
                        >
                          Qty
                        </Text>
                        <Text
                          variant="taskBold"
                          color="primary"
                          style={styles.prices}
                          size={14}
                        >
                          Price(AED)
                        </Text>
                      </View>

                      {item.qtyOwner > 0 && (
                        <View>
                          <View style={styles.row}>
                            <Text
                              variant="textRegular"
                              color="primary"
                              size={16}
                              style={{
                                width: config.DEVICE_WINDOW_WIDTH / 2,
                                fontSize: 12,
                                fontWeight: 'bold',
                                color: 'black',
                              }}
                            >
                              Owner Damage Charges
                            </Text>
                          </View>
                          <View
                            key={index}
                            style={[styles.row, { marginBottom: 10 }]}
                          >
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={{
                                width: 15,
                                fontSize: 12,
                                fontWeight: '500',
                                color: 'black',
                                textAlign: 'left',
                              }}
                            >
                              {index + 1})
                            </Text>
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={styles.materialName}
                              numberOfLines={1}
                              onPress={() => openModal(item.material_name)}
                            >
                              {item.material_name}
                            </Text>
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={styles.qtyBody}
                              size={16}
                            >
                              {numberFormat(item.qtyOwner)}
                            </Text>
                            <Text
                              variant="textRegular"
                              color="primary"
                              size={16}
                              style={styles.pricesBody}
                            >
                              {numberFormat(item.qtyOwner * item.price)}
                            </Text>
                          </View>
                        </View>
                      )}
                      {item.qtyTenant > 0 && (
                        <View>
                          <View style={styles.row}>
                            <Text
                              variant="textRegular"
                              color="primary"
                              size={16}
                              style={{
                                width: config.DEVICE_WINDOW_WIDTH / 2,
                                fontSize: 12,
                                fontWeight: 'bold',
                                color: 'black',
                              }}
                            >
                              Tenant Damage Charges
                            </Text>
                          </View>
                          <View key={index} style={styles.row}>
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={{
                                width: 15,
                                fontSize: 12,
                                fontWeight: '500',
                                color: 'black',
                              }}
                            >
                              {index + 1})
                            </Text>
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={styles.materialName}
                              numberOfLines={1}
                              onPress={() => openModal(item.material_name)}
                            >
                              {item.material_name}
                            </Text>
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={styles.qtyBody}
                              size={16}
                            >
                              {numberFormat(item.qtyTenant)}
                            </Text>
                            <Text
                              variant="textRegular"
                              color="primary"
                              size={16}
                              style={styles.pricesBody}
                            >
                              {numberFormat(item.qtyTenant * item.price)}
                            </Text>
                          </View>
                        </View>
                      )}
                    </View>
                  );
                }),
              )}
            {data?.totalQtr > 0 && (
              <View style={styles.wrapper}>
                <View style={styles.row}>
                  <Text variant="taskBold" size={16} style={styles.boldText}>
                    Total Damage Charges
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.qtyHeader}
                    size={14}
                  >
                    Qty
                  </Text>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={styles.prices}
                    size={14}
                  >
                    Price(AED)
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text
                    variant="taskBold"
                    color="primary"
                    style={{
                      width: config.DEVICE_WINDOW_WIDTH / 2 - 20,
                      fontSize: 12,
                      fontWeight: '800',
                    }}
                  />

                  <Text
                    variant="textRegular"
                    color="primary"
                    size={16}
                    style={{
                      width: 40,
                      fontSize: 12,
                      fontWeight: '500',
                      color: 'black',
                      textAlign: 'center',
                    }}
                  >
                    {numberFormat(data?.totalQtr)}
                  </Text>
                  <Text
                    variant="textRegular"
                    color="primary"
                    size={16}
                    style={styles.pricesBody}
                  >
                    {numberFormat(data?.totalPrice)}
                  </Text>
                </View>
              </View>
            )}
            {/* {materialArray?.length > 0 && (
              <Text
                variant="taskBold"
                color="primary"
                size={20}
                style={{ paddingBottom: 10 }}
              >
                Material Details
              </Text>
            )} */}

            {materialArray?.length > 0 && (
              <Text
                variant="taskBold"
                color="primary"
                size={20}
                style={{ paddingBottom: 10 }}
              >
                Material Details
              </Text>
            )}

            {materialArray?.map((material, index) => {
              return (
                <View
                  style={[
                    styles.card,
                    { borderColor: theme.color.primaryBouquet },
                  ]}
                >
                  <View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                      }}
                    >
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.specifyHeader}
                      >
                        Vendor:
                      </Text>
                      <Text variant="bodyBold" style={styles.numberFont}>
                        {material?.vender ? material?.vender : '-'}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',

                      paddingBottom: 10,
                      marginBottom: 5,
                      // flex: 1,
                      justifyContent: 'space-around',
                      borderBottomColor: theme.color.primary,
                      borderBottomWidth: 1,
                      // height: 30,
                      // backgroundColor: 'red'
                    }}
                  >
                    <View style={{ width: 30 }}>
                      <Text variant="taskBold" color="primary" />
                    </View>
                    <View style={{ flex: 0.5, textAlign: 'center' }}>
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.boldText}
                      >
                        Item
                      </Text>
                    </View>

                    <View
                      style={{
                        flex: 0.5,
                        alignItems: 'flex-end',
                      }}
                    >
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={[styles.prices, { width: '50%' }]}
                      >
                        Specification
                      </Text>
                    </View>
                  </View>

                  {material?.title?.map((value, index) => {
                    return (
                      <>
                        <View
                          style={{
                            flexDirection: 'row',
                            marginBottom: 5,
                            // flex: 1,
                            justifyContent: 'space-around',
                          }}
                        >
                          <View style={{ width: 30 }}>
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={styles.numberFont}
                            >
                              {index + 1})
                            </Text>
                          </View>
                          <View style={{ flex: 1 }}>
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={styles.materialName}
                            >
                              {value?.item}
                            </Text>
                          </View>

                          <View
                            style={{
                              flex: 1,
                              alignItems: 'flex-end',
                            }}
                          >
                            <Text
                              variant="taskBold"
                              color="primary"
                              style={styles.specify}
                            >
                              {value?.specification}
                            </Text>
                          </View>
                        </View>
                      </>
                    );
                  })}

                  <View
                    style={[
                      styles.rownew,
                      {
                        borderTopColor: theme.color.primary,
                        borderTopWidth: 1,
                        paddingTop: 15,
                        // marginTop:10,
                      },
                    ]}
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}
                    >
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.specifyHeader}
                      >
                        Owner Qty:
                      </Text>
                      <Text variant="bodyBold" style={styles.numberFont}>
                        {material?.qtyOwner ? material?.qtyOwner : 0}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                      }}
                    >
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.specifyHeader}
                      >
                        Tenant Qty:
                      </Text>
                      <Text variant="bodyBold" style={styles.numberFont}>
                        {material?.qtyTenant ? material?.qtyTenant : 0}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.rownew}>
                    <View />
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                      }}
                    >
                      <Text
                        variant="taskBold"
                        color="primary"
                        style={styles.specifyHeader}
                      >
                        Total Qty:
                      </Text>
                      <Text variant="bodyBold" style={styles.numberFont}>
                        {material?.quantity ? material?.quantity : 0}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            })}

            <View style={{ marginBottom: 150 }} />
          </ScrollView>
          <View style={styles.footer}>
            <Button
              title="Continue"
              // disabled={!disabled}
              style={{ marginVertical: 20, padding: 20, borderRadius: 20 }}
              titleStyle={{ fontSize: 16, ...getAppFont('Regular') }}
              onPress={onContinue}
            />
          </View>
        </ReactNativeModal>
      </View>
    </View>
  );
};

export default QuantityModal;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // minHeight: 340,
    // padding: 10,
    // marginHorizontal: 10,
    borderRadius: 10,
    // alignItems: 'center',
    height: 56,
    // justifyContent: 'space-between',
    marginTop: 30,
    // backgroundColor: 'red',
  },
  card: {
    borderWidth: 1,
    paddingHorizontal: 15,
    marginBottom: 10,
    paddingVertical: 10,
    // flexWrap: 'wrap',
  },
  rownew: {
    flexDirection: 'row',

    marginBottom: 10,
    // flex: 1,
    justifyContent: 'space-between',
    // height: 30,
    // backgroundColor: 'red'
  },
  prices: {
    width: '28%',
    textAlign: 'right',
    fontSize: 14,
    fontWeight: '800',

  },
  footer: {
    position: 'absolute',
    left: 20,
    right: 20,
    bottom: 0,
    backgroundColor: 'white',
    paddingTop: 5,
  },
  pricesBody: {
    width: '20%',
    textAlign: 'right',
    fontSize: 12,
    fontWeight: '500',
    color: 'black',
  },
  specifyHeader: {
    textAlign: 'right',
    fontSize: 14,
    fontWeight: '800',
    textAlign: 'right',
  },
  specify: {
    textAlign: 'right',
    fontSize: 12,
    fontWeight: '500',
    color: 'black',
  },
  borderStyle: {
    borderTopColor: theme.color.primary,
    borderTopWidth: 1,
  },
  wrapper: {
    paddingBottom: 15,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
    borderWidth: 0.3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20,
    elevation: 3,
    paddingTop: 15,
  },
  viewStyle: {
    marginLeft: 10,
    marginRight: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 5,
    paddingRight: 5,
    // height: 30,
  },
  boldText: {
    fontSize: 14,
    color: theme.color.primary,
    fontWeight: 'bold',
    width: '60%',
  },
  qtyHeader: {
    width: '10%',
    fontSize: 14,
    fontWeight: '800',
    textAlign: 'center',
  },
  materialName: {
    width: '55%',
    fontSize: 12,
    fontWeight: '500',
    color: 'black',
    textAlign: 'left',
    // left: -5
  },
  numberFont: {
    // width:20,
    fontSize: 12,
    fontWeight: '500',
    color: 'black',
    textAlign: 'left',
    // left: -5
  },
  qtyBody: {
    width: '20%',
    fontSize: 12,
    fontWeight: '500',
    color: 'black',
    textAlign: 'center',

  },
});
