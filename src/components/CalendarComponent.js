/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import moment from 'moment';

LocaleConfig.locales.fr = {
  monthNames: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  monthNamesShort: [
    'Jan.',
    'Feb.',
    'Mar',
    'April',
    'May',
    'June',
    'July.',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ],
  dayNames: [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ],
  dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
  today: "Aujourd'hui",
};
LocaleConfig.defaultLocale = 'fr';

const CalendarComponent = ({
  theme,
  dayClicked = {},
  monthChanged,
  markingDates = {},
  markingType = 'custom',
  minDate = {},
  maxDate = {},
  current = {},
  renderArrow = {},
  disableAllTouchEventsForDisabledDays = false,
}) => {
  const [selectedDates, setSelectedDates] = useState({});
  // useEffect(()=>{
  //     if(current){
  //         console.log({current})
  //         let currDate = current;
  //         let selectedDates={};
  //         selectedDates[currDate] = { selected: true, selectedColor: '#0e6977' };
  //         setSelectedDates(selectedDates)
  //     }
  // },[current])

  return (
    <Calendar
      onDayPress={(day) => dayClicked(day)}
      onMonthChange={(month) => monthChanged(month)}
      theme={theme}
      markingType={markingType}
      markedDates={markingDates}
      // minDate={minDate}
      current={current}
      // renderArrow = {renderArrow}
      hideExtraDays
      enableSwipeMonths
    />
  );
};

export default CalendarComponent;
