/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, createRef, useRef, useLayoutEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  ScrollView,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { Transition, Transitioning } from 'react-native-reanimated';
import CheckBox from './ui-elements/CheckBox';
import { DropDownIcon } from '_svg';
import { useTheme } from '../utils/theme';

const DropDownVendor = ({
  value,
  options = [],
  placeholder = 'Select',
  onOpen = () => {},
  onClose = () => {},
  placeHolderStyle = null,
  contStyle = {},
  tableHeadTxtStyle = {},
  contentStyle = {},
  handleCheckDropDown = () => {},
}) => {
  const theme = useTheme();
  // :States
  const [open, setOpen] = useState(false);
  const [animatedOpen, setAnimatedOpen] = useState(false);
  const dropdownRef = useRef(createRef());
  const [dropdownDim, setDropDownDim] = useState(null);
  const [selected, setSelected] = useState([]);
  const dropdownContainerRef = useRef(null);

  // :Effects
  useLayoutEffect(() => {
    dropdownRef.current.measureInWindow((x, y, width, height) => {
      if (!dropdownDim || dropdownDim.x !== x || dropdownDim.y !== y) {
        setDropDownDim({ x, y, width, height });
      }
    });
  });

  // :Functions
  const transition = (
    <Transition.Together>
      {/* <Transition.Out type="slide-top" /> */}
      {/* <Transition.In interpolation="easeInOut" durationMs={200} /> */}
      <Transition.Change interpolation="easeInOut" durationMs={100} />
    </Transition.Together>
  );

  const setAnimatedOpenFn = (opened) => {
    if (opened) {
      setOpen(true);
      setAnimatedOpen(true);
    } else {
      setAnimatedOpen(false);
      setTimeout(() => {
        setOpen(false);
      }, 100);
    }
    dropdownContainerRef.current?.animateNextTransition();
  };

  const onLabelClick = () => {
    setAnimatedOpenFn(true);
    onOpen();
  };

  const requestClose = () => {
    setAnimatedOpenFn(false);
    onClose(selected);
  };

  // :Render
  return (
    <>
      <View>
        <TouchableWithoutFeedback onPress={() => onLabelClick()}>
          <View
            style={[styles.container, contStyle]}
            ref={(el) => (dropdownRef.current = el)}
            collapsable={false}
          >
            {selected?.icon ? (
              <View style={styles.rowView}>
                <Image
                  source={selected?.icon ?? value ?? placeholder}
                  style={styles.imageStyle}
                />
                <Text style={styles.label}>
                  {selected?.label ?? value ?? placeholder}
                </Text>
              </View>
            ) : (
              <View>
                <Text style={[styles.label, placeHolderStyle]}>
                  {placeholder}
                </Text>
              </View>
            )}
            <View style={{ left: 15 }}>
              <DropDownIcon
                color={theme.color.primaryWhite}
                width={14}
                height={7}
                top={3}
              />
            </View>
            {/* <Image
                source={<RightArrow />}
                style={[styles.dropdownIcon, { tintColor: 'orange' }]}
              /> */}
          </View>
        </TouchableWithoutFeedback>
      </View>
      <ReactNativeModal
        isVisible={open}
        onBackdropPress={requestClose}
        onBackButtonPress={requestClose}
        backdropColor="transparent"
        style={{ padding: 0, margin: 0 }}
        animationInTiming={0.1}
        animationOutTiming={0.1}
      >
        <Transitioning.View
          ref={dropdownContainerRef}
          transition={transition}
          style={[
            styles.dropdownContainer,
            {
              top: dropdownDim?.y + dropdownDim?.height,
              left: dropdownDim?.x,
              width: dropdownDim?.width,
              backgroundColor: theme.color.primaryWhite,
              ...theme.cardShadow,
            },
          ]}
        >
          <ScrollView showsVerticalScrollIndicator style={{ maxHeight: 110 }}>
            <View
              style={{
                paddingHorizontal: 10,
                paddingTop: 20,
              }}
            >
              {options.map((item, index) => {
                return (
                  <View
                    key={item.label}
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      paddingBottom: 22,
                    }}
                  >
                    <Text>{item.label}</Text>
                    <CheckBox
                      onPress={() => handleCheckDropDown(index)}
                      checked={item.checked}
                    />
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </Transitioning.View>
      </ReactNativeModal>
    </>
  );
};

export default DropDownVendor;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 7,
    paddingHorizontal: 12,
    flex: 1,
    height: 56,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 39,
    paddingLeft: 36,
  },
  label: { textAlign: 'center', marginTop: 3 },
  dropdownContainer: {
    position: 'absolute',
    top: 0,
    // left: 0,
    // backgroundColor: theme.colors.white,
    // ...theme.cardShadow,
    paddingBottom: 7,
    borderRadius: 5,
    // height: 200,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageStyle: {
    height: 30,
    width: 40,
    marginRight: 10,
  },
});
