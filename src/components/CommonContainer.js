/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-unused-styles */
import React, { useRef, useState, useEffect } from 'react';
import { StyleSheet, View, StatusBar, RefreshControl } from 'react-native';
// import PopModal from './ui-elements/PopModal';
import { useScrollToTop, useNavigation } from '@react-navigation/native';
import Animated from 'react-native-reanimated';
// import Toast, { DURATION } from 'react-native-easy-toast';
import { Updates } from 'expo';
import { useTheme } from '../utils';
import NavHeader from './NavHeader';
// import Modal from 'react-native-modal';
// import Button from '../components/ui-elements/Button'

import * as Notifications from 'expo-notifications';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: false,
  }),
});

const CommonContainer = ({
  navbar,
  children,
  style,
  onScroll,
  showsVerticalScrollIndicator = false,
  scrollEnabled = true,
  scrollToTopOnNavChange = true,
  onRefresh = () => { },
  loading = false,
  onModalPress = () => { },
}) => {
  const _scrollerRef = useRef(null);
  const toastRef = useRef(null);
  // notification
  const { navigate } = useNavigation();
  const [getMsgData, setMsgData] = useState(null);
  const notificationListener = useRef();
  const responseListener = useRef();

  useScrollToTop(scrollToTopOnNavChange && scrollEnabled && _scrollerRef);
  //   const {
  //     netInfo: { isConnected },
  //     toast,
  //   } = useSelector((state) => state.app);

  // updates
  const [updateAvailable, setUpdateAvailable] = useState(false);
  const [updateModal, setUpdateModal] = useState(false);
  const isConnected = true;
  const theme = useTheme();


  useEffect(() => {
    notificationListener.current = Notifications.addNotificationReceivedListener(
      handleNotifications,
    );
    responseListener.current = Notifications.addNotificationResponseReceivedListener(
      (response) => {
        handleNotifications(response.notification, false);
      },
    );
    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);
  const navigateTo = (data) => {
    switch (data.screen) {
      case 'mrp_key_collection':
        navigate('Quotations');
        break;
      case 'mrp_item_requested':
        navigate('TaskLog');
        break;
      case 'Quotation':
        navigate('Quotations');
        break;
      default:
        break;
    }
  }
  const handleNotifications = (notifications, showNotifications = true) => {
    let { data = null } = notifications.request.content;
    data = Platform.OS === 'ios' ? data.body : data;
    if (showNotifications === false) {
      navigateTo(data);
    }
  };

  // doupdate fn
  const doUpdate = () => {
    Updates.reloadAsync();
  };

  return (
    <View
      style={[
        { backgroundColor: theme.color.primary, flex: 1 },
        !isConnected && { backgroundColor: theme.color.primary },
      ]}
    >
      <StatusBar
        barStyle="light-content"
        translucent
        backgroundColor="transparent"
      />
      {/* {updateModalElm} */}
      {/* <Image
        source={require('../../../assets/bgPattern.png')}
        style={styles.bgPattern}
      /> */}
      <NavHeader {...navbar} onModalPress={() => onModalPress()} />
      <View style={[styles._scrollerStyle, style]}>
        {scrollEnabled ? (
          <Animated.ScrollView
            ref={_scrollerRef}
            scrollEnabled={scrollEnabled}
            onScroll={onScroll}
            scrollEventThrottle={16}
            bounces={false}
            showsVerticalScrollIndicator={showsVerticalScrollIndicator}
            contentContainerStyle={{ paddingBottom: 80 }}
            refreshControl={
              <RefreshControl onRefresh={onRefresh} refreshing={loading} />
            }
          >
            {children}
          </Animated.ScrollView>
        ) : (
          children
        )}
      </View>
      {/* 
      <PopModal />
      <Toast ref={toastRef} /> */}
    </View>
  );
};

export default CommonContainer;

const styles = StyleSheet.create({
  container: {
    // backgroundColor: theme.color.primary,
    // flex: 1,
  },
  bgPattern: { position: 'absolute', top: 15, left: 15, flex: 0 },
  _scrollerStyle: {
    // ...theme.layoutRoundEdge,
    backgroundColor: 'white',
    flexGrow: 1,
  },
});
