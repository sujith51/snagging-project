// @flow
import * as React from 'react';
import { View, StyleSheet } from 'react-native';

import { useTheme } from '../utils';

type Props = {
  children: React.Node,
};

function Card(props: Props) {
  const theme = useTheme();
  const { children, cardStyle } = props;
  return (
    <View
      style={[
        styles.container,
        cardStyle,
        {
          borderColor: theme.color.secondaryLight2,
          // padding: theme.spacing.sm,
          borderRadius: theme.borderRadius.xl,
          backgroundColor: theme.color.primaryWhite,
        },
      ]}
    >
      {children}
    </View>
  );
}

export default Card;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 1,
    // marginLeft: 35,
    // marginRight: 35,
    // marginBottom: 35,
    // elevation:10   //for ShowDow
  },
});
