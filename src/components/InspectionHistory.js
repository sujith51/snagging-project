import React, { useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { NotificationIcon } from '_svg';
import { ProvisConnectIcon, ProvisPrimaryIcon } from '../assets/svg_components';
import { getAppFont, useTheme } from '../utils';
// import { CommonContainer, InspectionComp } from '_components';
import CommonContainer from './CommonContainer';
import InspectionComp from './InspectionComp';

const InspectionHistory = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { navigate } = useNavigation();

  // : States

  const {
    inspectionHistory: {
      data: {
        audit_title = '',
        property = '',
        unit_number = '',
        prepared_by = '',
        conducted_on = '',
        failed = '',
        created = '',
        inspectionScore = 50,
        category = null,
      },
    },
  } = useSelector((state) => state.taskdetails);

  // : Effects
  const containerProps = {
    navbar: {
      title: 'Inspection History',
      actions: [
        {
          icon: <NotificationIcon />,
          onPress: () => navigate('Notifications'),
        },
      ],
    },
    scrollEnabled: true,
    style: { flex: 1 },
    onModalPress: () => {
      // onRefresh();
    },
  };

  const renderItems = () => {
    if (category) {
      var List = [];
      for (const [key, value] of Object.entries(category)) {
        List.push(<InspectionComp data={value} title={key} />);
      }
      return List;
    } else {
      return null;
    }
  };

  return (
    <CommonContainer {...containerProps} safeArea>
      <View style={styles.container}>
        <View style={styles.iconsCont}>
          <View style={styles.leftIcon}>
            <ProvisPrimaryIcon color={theme.color.primary} />
          </View>
          <View>
            <ProvisConnectIcon color={theme.color.primary} />
          </View>
        </View>

        <View style={styles.heading1}>
          <Text style={[styles.head1txt, { color: theme.color.primary }]}>
            Provis Snagging
          </Text>
        </View>

      

        <View style={styles.heading3}>
          <Text style={styles.eachValue}>Complete</Text>
        </View>

        <View
          style={{
            height: 90,
            borderWidth: 1,
            marginLeft: 9,
            marginRight: 9,
            marginTop: 22,
          }}
        >
          <View
            style={{ flexDirection: 'row', height: 54, borderBottomWidth: 1 }}
          >
            <View style={styles.eachRow}>
              <Text style={styles.eachTitle}>Inspection Score</Text>
              <Text style={styles.eachValue}>{inspectionScore}%</Text>
            </View>

            <View style={styles.eachRow}>
              <Text style={styles.eachTitle}>Failed Item</Text>
              <Text style={styles.eachValue}>{failed}</Text>
            </View>

            <View style={styles.eachRow}>
              <Text style={styles.eachTitle}>Created Actions</Text>
              <Text style={styles.eachValue}>{created}</Text>
            </View>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 8 }}>
            <Text style={[styles.eachTitle, { paddingBottom: 2 }]}>
              Audit Title
            </Text>
            <Text style={styles.eachValue}>{audit_title}</Text>
          </View>
        </View>

        <View
          style={{
            height: 146,
            borderWidth: 1,
            marginLeft: 9,
            marginRight: 9,
            marginTop: 4,
          }}
        >
          <View style={styles.eachHorRow}>
            <Text style={[styles.eachTitle, { paddingBottom: 2 }]}>
              Property
            </Text>
            <Text style={styles.eachValue}>{property}</Text>
          </View>

          <View style={styles.eachHorRow}>
            <Text style={[styles.eachTitle, { paddingBottom: 2 }]}>
              Unit Number
            </Text>
            <Text style={styles.eachValue}>{unit_number}</Text>
          </View>

          <View style={styles.eachHorRow}>
            <Text style={[styles.eachTitle, { paddingBottom: 2 }]}>
              Conducted On
            </Text>
            <Text style={styles.eachValue}>{conducted_on}</Text>
          </View>
          <View style={[styles.eachHorRow, { borderBottomWidth: 0 }]}>
            <Text style={[styles.eachTitle, { paddingBottom: 2 }]}>
              Prepared by
            </Text>
            <Text style={styles.eachValue}>{prepared_by}</Text>
          </View>
        </View>

        <View style={{ paddingTop: 15 }}>{renderItems()}</View>
        <View style={{ marginLeft: 11 }}>
          <Text style={[styles.eachValue, { paddingTop: 46 }]}>
            Additional comments not covered above
          </Text>
          <Text style={[styles.eachValue, { paddingTop: 32 }]}>
            Inspector comments and signature
          </Text>
          <Text style={[styles.eachValue, { paddingTop: 18 }]}>
            PM comments and signature
          </Text>
          <Text style={[styles.eachValue, { paddingTop: 18 }]}>
            Tenant comments and signature
          </Text>
        </View>
      </View>
    </CommonContainer>
  );
};

export default InspectionHistory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  iconsCont: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 32,
    paddingLeft: 25,
  },
  leftIcon: {
    paddingRight: 11,
    paddingTop: 4,
  },
  heading1: {
    paddingTop: 37,
    paddingLeft: 11,
  },
  heading2: {
    paddingTop: 7,
    paddingLeft: 11,
  },
  head1txt: {
    ...getAppFont('Bold'),
    fontSize: 14,
  },
  head2txt: {
    ...getAppFont('Bold'),
    fontSize: 12,
  },
  heading3: {
    paddingTop: 18,
    paddingLeft: 11,
  },
  eachRow: {
    flex: 1,
    borderRightWidth: 1,
    paddingLeft: 8,
    justifyContent: 'center',
  },
  eachTitle: {
    fontSize: 10,
    paddingBottom: 7,
    ...getAppFont('SemiBold'),
  },
  eachValue: {
    fontSize: 10,
    ...getAppFont('SemiBold'),
  },
  eachHorRow: {
    flex: 1,
    borderBottomWidth: 1,
    paddingLeft: 8,
  },
});
