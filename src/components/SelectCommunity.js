import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import DropDownMultiple from './ui-elements/DropDownMultiple';
import { saveSelectedCommunity, saveSelectedUnit } from '../redux/actions';
import { getAppFont, useTheme } from '../utils';

const SelectCommunity = ({ units, communities, loadInit = false }) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const dataArr = communities?.map((item) => ({
    label: item.label,
    value: item.value,
  }));
  const All = 'All';
  const arr = [{ label: 'All', value: All }, ...dataArr];

  useEffect(() => {
    if (loadInit) {
      changeCommunity({ label: 'All', value: All });
    }
  }, [loadInit]);

  const { selectCommunity: selectedCommu, selectUnit } = useSelector(
    (state) => state.user,
  );

  // useEffect(() => {
  //   if (selectedCommu?.value) {
  //     const filterUni = units.filter(
  //       (item) => item.id_community === selectedCommu?.value,
  //     );
  //     setFilterUnits(filterUni);
  //   }
  // }, [selectedCommu?.value, units]);

  const changeUnit = (data) => {
    dispatch(saveSelectedUnit(data));
  };
  const changeCommunity = (data) => {
    let copy = data;
    let type = 'one';
    if (data?.value == All) {
      copy = arr;
      type = All;
    }
    dispatch(saveSelectedCommunity(copy, type));
  };

  // console.log({ selectedCommu });

  return (
    <View style={styles.container}>
      <View style={{ paddingTop: 20 }}>
        <Text style={[styles.textStyle, { color: theme.color.primaryBlack }]}>
          Select Community
        </Text>
        <View style={{ paddingTop: 10 }}>
          <DropDownMultiple
            value={selectedCommu}
            options={arr}
            placeHolderStyle={{
              color: theme.color.primaryWhite,
              ...getAppFont('Bold'),
            }}
            drDownMdlwidth
            contStyle={[
              styles.dropDownContain,
              { backgroundColor: theme.color.secondaryDark3 },
            ]}
            iconColor={theme.color.primaryWhite}
            onClose={changeCommunity}
          />
        </View>
      </View>
    </View>
  );
};

export default SelectCommunity;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'column',
  },
  dropDownContain: {
    // flex: 1,
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    right: 0,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },

  textStyle: {
    ...getAppFont('Bold'),
    fontSize: Platform.isPad ? 20 : 16,
  },
});
