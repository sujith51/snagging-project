/* eslint-disable react-native/no-inline-styles */
/* eslint-disable import/no-unresolved */
// @flow
import React, { useEffect } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { savechklistquesctions } from '_actions';
import { DownArrow, UpArrow } from '_svg';
import { useTheme } from '../utils';
import Text from './Text';
import AccordianQuestion from './AccordianQuestion';

const Accordian = ({
  title,
  question,
  listDamage,
  taskDetail,
  is_multiple,
  sub_categery,
  idInsBooking,
  showBorder,
  parentId,
  parentSelected,
  categorySelected,
  listCount,
  changeState,
  disableTenant,
  isAdditionalComment,
  additionalCommentsId
}) => {
 
  const [toggle, setToggle] = React.useState(false);
  const theme = useTheme();
  const dispatch = useDispatch();
  const { chklistQuesData } = useSelector((state) => state.taskdetails);
  const checjlistObj = chklistQuesData[idInsBooking];

  useEffect(() => {
    if (parentId === parentSelected) {
      setToggle(true);
    } else {
      setToggle(false);
    }
  }, [parentSelected, parentId, changeState]);
  const initAdditional = nof => {
    const saveRes = {
      details: additionalCommentsId,
      numberOf: nof,
      quesction: {
        damage: null,
        comment: null,
        id_template_question_answers: null,
        question_answer: null,
        taskType: 0,
        image: null,
        description: null,
      },
    };
    dispatch(savechklistquesctions(idInsBooking, saveRes));
  };
  const incrementClicked = () => {
    const saveRes = {
      details: 'sub_categery',
      numberOf: sub_categery,
      quesction: {
        data: [
          ...checjlistObj.answer?.sub_categery[sub_categery]?.data,
          checjlistObj.answer?.sub_categery[sub_categery]?.data.length + 1,
        ],
      },
    };
    dispatch(savechklistquesctions(idInsBooking, saveRes));
  };

  const decrementClicked = (remove) => {


    // const rmv = checjlistObj.answer?.sub_categery[sub_categery]?.data.filter((val, index) =>

    //   index !== remove - 1
    // )
    // const rmv = checjlistObj.answer?.sub_categery[sub_categery]?.data.filter(
    //   (f) => f !== remove,
    // );
    const rmv = checjlistObj.answer?.sub_categery[sub_categery]?.data.splice(0)
    rmv.splice(remove - 1, 1);

    

    const saveRes = {
      details: 'sub_categery',
      numberOf: sub_categery,
      quesction: {
        data: rmv,
      },
    };

   
    // console.log('rmDdata',remove,rmDdata,checjlistObj?.answer[additionalCommentsId])
    dispatch(savechklistquesctions(idInsBooking, saveRes));

    if(isAdditionalComment){
      let rmDdata=checjlistObj?.answer[additionalCommentsId] && checjlistObj?.answer[additionalCommentsId][remove]
      if(rmDdata){
        initAdditional(remove)
      }
    }
  };

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={() => setToggle(!toggle)}>
        <View
          style={[
            {
              // borderBottomWidth: 1,
              borderBottomColor: theme.color.secondaryLight3,
              paddingVertical: theme.spacing.m,
              backgroundColor: theme.color.secondaryDark2,
              paddingHorizontal: 10,
              borderRadius: 10,
              marginBottom: 10,
            },
            styles.row,
          ]}
        >
          <Text variant="taskBold" size={19}>
            {title}
          </Text>
          {is_multiple === '1' &&
            checjlistObj?.answer?.sub_categery[sub_categery] && (
              <TouchableWithoutFeedback
                onPress={() =>
                  decrementClicked(
                    checjlistObj?.answer?.sub_categery[sub_categery]?.data
                      .length === 1
                      ? null
                      : checjlistObj?.answer?.sub_categery[sub_categery]?.data
                        .length,
                  )
                }
              >
                <View
                  style={[
                    styles.roundStyl,
                    { backgroundColor: theme.color.primaryWhite },
                  ]}
                >
                  <Text>-</Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          {is_multiple === '1' &&
            checjlistObj?.answer?.sub_categery[sub_categery] && (
              <Text>
                {checjlistObj?.answer?.sub_categery[sub_categery].data.length}
              </Text>
            )}
          {is_multiple === '1' && (
            <TouchableWithoutFeedback onPress={() => incrementClicked()}>
              <View
                style={[
                  styles.roundStyl,
                  { backgroundColor: theme.color.primaryWhite },
                ]}
              >
                <Text>+</Text>
              </View>
            </TouchableWithoutFeedback>
          )}
          {toggle ? <UpArrow /> : <DownArrow />}
        </View>
      </TouchableWithoutFeedback>
      {toggle && checjlistObj?.answer?.sub_categery[sub_categery] && (
        <View style={{ flex: 1 }}>
          <AccordianQuestion
            question={question}
            listDamage={listDamage}
            taskDetail={taskDetail}
            count={checjlistObj?.answer?.sub_categery[sub_categery]?.data}
            sub_categery={sub_categery}
            showBorder={showBorder}
            is_multiple={is_multiple}
            categorySelected={categorySelected}
            parentId={parentId}
            parentSelected={parentSelected}
            listCount={listCount}
            disableTenant={disableTenant}
            toggle={toggle}
            isAdditionalComment={isAdditionalComment}
            additionalCommentsId={additionalCommentsId}
          />
        </View>
      )}

    </View>
  );
};

export default Accordian;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  roundStyl: {
    height: 30,
    width: 30,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
