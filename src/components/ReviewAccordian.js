/* eslint-disable react-native/no-inline-styles */
/* eslint-disable import/no-unresolved */
// @flow
import React, {useEffect} from 'react';
import {View, TouchableWithoutFeedback, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {savechklistquesctions, saveExtraReviewData,deleteExtraReviewData} from '_actions';
import {DownArrow, UpArrow} from '_svg';
import {useTheme} from '../utils';
import Text from './Text';
import AccordianQuestion from './AccordianQuestion';
import ReviewAccordianQuestion from './ReviewAccordianQuestion';

const ReviewAccordian = ({count, idIns,additionalCommentsId=205}) => {
  // console.log('dddfdfd',idIns,additionalCommentsId)
  const [toggle, setToggle] = React.useState(true);
  const theme = useTheme();
  const dispatch = useDispatch();
  const {saveReviewExtraDatas} = useSelector(state => state.taskdetails);
  const checjlistObj = saveReviewExtraDatas[idIns];
  const deleteAdditional = nof => {
   
    // let copy=checjlistObj?.answer;
    // let questions=copy[additionalCommentsId]
   
    // if(questions[nof]){
    //   delete questions[nof]
    // }
    // console.log('delete',copy)
    // const saveRes = {
    //   details: additionalCommentsId,
    //   numberOf: nof,
    //   quesction: {
    //     damage: null,
    //     comment: null,
    //     id_template_question_answers: null,
    //     question_answer: null,
    //     taskType: 0,
    //     image: null,
    //     description: null,
    //     // isEmpty:true
    //   },
    // };
    dispatch(deleteExtraReviewData(idIns,additionalCommentsId, nof));
  };
  const initAdditional = nof => {
    const saveRes = {
      details: additionalCommentsId,
      numberOf: nof,
      quesction: {
        damage: 0,
        comment: null,
        id_template_question_answers: null,
        question_answer: null,
        taskType: 0,
        image: null,
        description: null,
      },
    };
    dispatch(saveExtraReviewData(idIns, saveRes));
  };
  
  const incrementClicked = () => {
    const saveRes = {
      details: 'sub_categery',
      numberOf: 'additional',
      quesction: {
        data: [
          ...checjlistObj?.answer?.sub_categery?.additional?.data,
          checjlistObj?.answer?.sub_categery?.additional?.data?.length + 1,
        ],
      },
    };
    // console.log('incrementClicked',idIns, saveRes)
    dispatch(saveExtraReviewData(idIns, saveRes));
    initAdditional(checjlistObj?.answer?.sub_categery?.additional?.data?.length+1 );
  };

  const decrementClicked = remove => {
    const rmv = checjlistObj?.answer?.sub_categery?.additional?.data.splice(0);
    rmv.splice(remove - 1, 1);
    // console.log("decrementClicked", rmv,)
    const saveRes = {
      details: 'sub_categery',
      numberOf: 'additional',
      quesction: {
        data: rmv,
      },
    };
    dispatch(saveExtraReviewData(idIns, saveRes));
    let rmDdata=checjlistObj?.answer[additionalCommentsId] && checjlistObj?.answer[additionalCommentsId][remove]
    // console.log('re',remove,rmDdata,checjlistObj,checjlistObj?.answer[additionalCommentsId])
      if(rmDdata){
        deleteAdditional(remove)
      }
  };

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={() => setToggle(!toggle)}>
        <View
          style={[
            {
              // borderBottomWidth: 1,
              borderBottomColor: theme.color.secondaryLight3,
              paddingVertical: theme.spacing.m,
              backgroundColor: theme.color.secondaryDark2,
              paddingHorizontal: 10,
              borderRadius: 10,
              marginBottom: 10,
            },
            styles.row,
          ]}>
          <Text variant="taskBold" size={19}>
            Additional Comments
          </Text>

          <TouchableWithoutFeedback
            onPress={() =>
              decrementClicked(
                checjlistObj?.answer?.sub_categery?.additional?.data.length
              )
            }>
            <View
              style={[
                styles.roundStyl,
                {backgroundColor: theme.color.primaryWhite},
              ]}>
              <Text>-</Text>
            </View>
          </TouchableWithoutFeedback>
          <Text>
            {checjlistObj?.answer?.sub_categery?.additional?.data?.length}
          </Text>
          <TouchableWithoutFeedback onPress={() => incrementClicked()}>
            <View
              style={[
                styles.roundStyl,
                {backgroundColor: theme.color.primaryWhite},
              ]}>
              <Text>+</Text>
            </View>
          </TouchableWithoutFeedback>
          {toggle ? <UpArrow /> : <DownArrow />}
        </View>
      </TouchableWithoutFeedback>
      {toggle && (
        <View style={{flex: 1}}>
          <ReviewAccordianQuestion idIns={idIns} count={count} />
        </View>
      )}
    </View>
  );
};

export default ReviewAccordian;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  roundStyl: {
    height: 30,
    width: 30,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
