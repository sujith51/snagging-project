/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';

import { Delete } from '_svg';
import { useTheme } from '../utils';
import { getAppFont, monthDayYear } from '../utils/theme';
// import {
//   Text,
//   Button,
//   CheckBox,
//   DropDownVendor,
//   CalendarModal,
// } from '_components';
import Date from '../assets/svg_components/Date';
import Text from './Text';
import Button from './Button';
import CheckBox from './ui-elements/CheckBox';
import DropDownVendor from './DropDownVendor';
import CalendarModal from './CalendarModal';

const RequestListModal = ({
  open,
  close,
  data,
  avlData,
  toggleCheck,
  deleteResult,
  openSuccess,
  vendorData,
  handleVendor = () => {},
  expectedDate,
}) => {
  const theme = useTheme();
  const [calendar, setCalendar] = useState(false);
  const [calendarDate, setCalendarDate] = useState(null);
  const [isVendor, setIsVendor] = useState(false);
  // const materialSelected = avlData?.map((da) => da);

  useEffect(() => {
    if (calendarDate) {
      expectedDate(calendarDate);
    }
  }, [calendarDate]);

  useEffect(() => {
    const timer = setTimeout(
      () => (isVendor ? setIsVendor(false) : null),
      2000,
    );
    return () => clearTimeout(timer);
  }, [isVendor]);

  const handleCalender = (date) => {
    setCalendar((prev) => !prev);
    setCalendarDate(date);
  };

  const handleVendorValid = () => {
    const isVendorSlected = vendorData.some((a) => a.checked);
  
    if (isVendorSlected) {
      setIsVendor(false);
      openSuccess();
    } else {
      setIsVendor(true);
    }
  };

  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
    >
      <ScrollView style={styles.scroolStyle}>
        <CalendarModal open={calendar} close={(date) => handleCalender(date)} />
        <Text variant="bodyBold" size={21} style={styles.headerStyle}>
          Requested Material List
        </Text>
        <View>
          <Text color="primary" variant="bodyBold" style={{ paddingTop: 10 }}>
            Material not in the rate card
          </Text>
          {data.length === 0 && (
            <Text size={12} style={{ paddingTop: 10 }}>
              Newly added materials will be shown here
            </Text>
          )}
          {data?.map((value, index) => {
            return (
              <View key={`required-${value.item}`}>
                <View style={styles.header}>
                  <View style={{ flexDirection: 'row' }}>
                    {/* <CheckBox
                      onPress={() => toggleCheck(index)}
                      checked={value.checked}
                    /> */}
                    <View style={{ marginLeft: 5 }}>
                      <Text variant="bodyBold" size={15} style={styles.mt12}>
                        Material {index + 1}
                      </Text>
                    </View>
                  </View>
                  <TouchableWithoutFeedback onPress={() => deleteResult(index)}>
                    <Delete />
                  </TouchableWithoutFeedback>
                </View>
                <View
                  style={[
                    styles.card,
                    { borderColor: theme.color.primaryBouquet },
                  ]}
                >
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Item Name
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.item}
                    </Text>
                  </View>
                  {/* <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Make
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.make}
                    </Text>
                  </View> */}
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Specification
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.specification}
                    </Text>
                  </View>
                  {value.qtyOwner > 0 && (
                    <View style={styles.row}>
                      <Text variant="bodyLight" size={15}>
                        Owner Qty
                      </Text>
                      <Text variant="bodyBold" size={15}>
                        {value.qtyOwner}
                      </Text>
                    </View>
                  )}

                  {value.qtyTenant > 0 && (
                    <View style={styles.row}>
                      <Text variant="bodyLight" size={15}>
                        Tenant Qty
                      </Text>
                      <Text variant="bodyBold" size={15}>
                        {value.qtyTenant}
                      </Text>
                    </View>
                  )}
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Total Qty
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.quantity}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Description
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.description}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Rate Card
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      Unavailable
                    </Text>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
        <View style={{ marginTop: 20, marginBottom: 10 }}>
          <Text variant="bodyBold" size={21} style={styles.headerStyle}>
            Select Vendor
          </Text>
        </View>
        <DropDownVendor
          value=""
          options={vendorData}
          placeHolderStyle={{
            color: theme.color.primaryWhite,
            // fontSize: 16,
            right: 20,
            ...getAppFont('Regular'),
          }}
          placeholder="Select"
          dropDownStyle={{}}
          // onClose={handleCheckDropDown}
          contStyle={{
            backgroundColor: theme.color.primary,
            paddingLeft: 39,
            paddingRight: 36,
            borderRadius: 10,
            height: 46,
          }}
          tableHeadTxtStyle={{
            ...getAppFont('Bold'),
            fontSize: 16,
            color: theme.color.primary,
          }}
          contentStyle={{
            fontSize: 16,
            ...getAppFont('Regular'),
          }}
          handleCheckDropDown={(selected) => handleVendor(selected)}
        />
        {/* <View style={{ marginTop: 20, marginBottom: 10 }}>
          <Text variant="bodyBold" size={21} style={styles.headerStyle}>
            Expected Delivery Date
          </Text>
        </View> */}
        {/* <TouchableWithoutFeedback onPress={() => setCalendar((prev) => !prev)}>
          <View
            style={{
              borderWidth: 1,
              borderColor: theme.color.primary,
              height: 50,
              position: 'relative',
            }}
          >
            <View
              style={{
                // position: 'absolute',
                // right: 10,
                paddingHorizontal: 10,
                top: 15,
                bottom: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              {calendarDate && (
                <Text>{monthDayYear(calendarDate.dateString)}</Text>
              )}
              <View />
              <Date />
            </View>
          </View>
        </TouchableWithoutFeedback> */}
        {isVendor && (
          <Text
            size={12}
            style={{ paddingTop: 10, textAlign: 'center' }}
            color={theme.color.red}
          >
            Please select atleast one vendor
          </Text>
        )}
        <View style={{ paddingBottom: 50 }}>
          <Button
            title="Continue"
            type="primary"
            titleStyle={{ ...getAppFont('Regular') }}
            style={styles.button1}
            onPress={handleVendorValid}
          />
        </View>
      </ScrollView>
    </ReactNativeModal>
  );
};

export default RequestListModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
    borderRadius: 20,
  },
  card: {
    borderWidth: 1,
    paddingHorizontal: 15,
    marginBottom: 10,
    paddingVertical: 10,
    // flexWrap: 'wrap',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    // backgroundColor:'red'
    flexWrap: 'wrap',
  },
  header: {
    flexDirection: 'row',
    paddingVertical: 10,
    justifyContent: 'space-between',
    paddingTop: 20,
  },
  button1: {
    marginTop: 20,
    borderRadius: 10,
  },
  scroolStyle: {
    flex: 1,
    marginTop: 50,
    paddingHorizontal: 20,
    // marginBottom: 70,
  },
  headerStyle: {
    paddingTop: 5,
  },
});

// avalible material to show in the list code
// <View>
//   {/* <Text color="primary" variant="bodyBold" style={{ paddingTop: 10 }}>
//     Available materials
//   </Text> */}
//   {materialSelected?.length === 0 && (
//     <Text size={12} style={{ paddingTop: 10 }}>
//       Selected materials from the rate card will be shown here
//     </Text>
//   )}
//   {materialSelected?.map((item, index) => {
//     return item.map((value) => {
//       return (
//         <>
//           {value.checked && (
//             <View key={`required-${value.item}`}>
//               <View style={styles.header}>
//                 <View style={{ flexDirection: 'row' }}>
//                   {/* <CheckBox
//                     onPress={() => toggleCheck(index)}
//                     checked={value.checked}
//                   /> */}
//                   <View style={{ marginLeft: 5 }}>
//                     <Text
//                       variant="bodyBold"
//                       size={15}
//                       style={styles.mt12}
//                     >
//                       Material {index + 1}
//                     </Text>
//                   </View>
//                 </View>
//                 {/* <TouchableWithoutFeedback
//                   onPress={() => deleteResult(index)}
//                 >
//                   <Delete />
//                 </TouchableWithoutFeedback> */}
//               </View>
//               <View
//                 style={[
//                   styles.card,
//                   { borderColor: theme.color.primaryBouquet },
//                 ]}
//               >
//                 <View style={styles.row}>
//                   <View style={{ flex: 1 }}>
//                     <Text variant="bodyLight" size={15}>
//                       Item Name
//                     </Text>
//                   </View>

//                   <View style={{ flex: 1 }}>
//                     <Text variant="bodyBold" size={15}>
//                       {value.material_name}
//                     </Text>
//                   </View>
//                 </View>
//                 {/* <View style={styles.row}>
//             <Text variant="bodyLight" size={15}>
//               Make
//             </Text>
//             <Text variant="bodyBold" size={15}>
//               {value.make}
//             </Text>
//           </View> */}
//                 {/* <View style={styles.row}>
//             <Text variant="bodyLight" size={15}>
//               Specification
//             </Text>
//             <Text variant="bodyBold" size={15}>
//               {value.specification}
//             </Text>
//           </View> */}
//                 <View style={styles.row}>
//                   <Text variant="bodyLight" size={15}>
//                     Quantity
//                   </Text>
//                   <Text variant="bodyBold" size={15}>
//                     {value.qty}
//                   </Text>
//                 </View>
//                 <View style={styles.row}>
//                   <Text variant="bodyLight" size={15}>
//                     Rate Card
//                   </Text>
//                   <Text variant="bodyBold" size={15}>
//                     Available
//                   </Text>
//                 </View>
//               </View>
//             </View>
//           )}
//         </>
//       );
//     });
//   })}
// </View>;
