/* eslint-disable react-native/no-inline-styles */
// @flow
import * as React from 'react';
import { View, TextInput, StyleSheet, Platform } from 'react-native';

import { useTheme, inputType } from '../utils';

type Props = {
  type: inputType,
};

function InputText(props: Props) {
  const theme = useTheme();
  const {
    rightAddon,
    innerRef,
    style,
    icon,
    type = 'bottom',
    textFieldStyle,
    textFieldContainerStyle,
    ...otherInputsProps
  } = props;
  return (
    <View
      style={[
        styles.container,
        type === 'default' && { height: 45 },
        type === 'textArea' && { height: 70 },
        style,
      ]}
    >
      <View
        style={[
          styles.textFieldContainer,
          type === 'bottom' && {
            borderBottomColor: theme.color.secondaryDark2,
            paddingHorizontal: theme.spacing.xs,
            borderBottomWidth: 1,
          },
          type === 'default' && {
            borderColor: theme.color.primaryBouquet,
            borderWidth: 1,
          },
          type === 'textArea' && {
            borderColor: theme.color.primaryBouquet,
            borderWidth: 1,
          },
          textFieldContainerStyle,
        ]}
      >
        <TextInput
          placeholderTextColor={theme.color.secondaryDark}
          ref={innerRef}
          {...otherInputsProps}
          style={[styles.textWrap, textFieldStyle]}
        />
        <View style={styles.rightIcon}>{icon}</View>
      </View>
    </View>
  );
}

export default InputText;

const styles = StyleSheet.create({
  container: {
    height: 50,
    // backgroundColor: theme.color.primaryWhite,
    marginBottom: 10,
  },
  textFieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  textWrap: {
    flex: 1,
    fontSize: Platform.isPad ? 18 : 13,
    // fontFamily: 'Montserrat',
    fontWeight: '400',
  },
  rightIcon: {
    flex: 0,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
});
