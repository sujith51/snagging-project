import React, { useState } from 'react';
import { View, StyleSheet, Dimensions, Linking, Modal, TouchableWithoutFeedback } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import Text from './Text';
import { Ticked } from '_svg';
import { useTheme, config } from '../utils';

const { width } = Dimensions.get('window');
const SIZE = 140;
const SuccessModal = ({ open, close, title, link, props }) => {
  const theme = useTheme();
  const viewReport = () => {
    if (link !== '') {
      Linking.openURL(link);
    }
  };
  return (
    // <ReactNativeModal
    //   isVisible={open}
    //   onBackdropPress={close}
    //   onBackButtonPress={close}
    //   style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
    // >
    <Modal
      // deviceHeight={DEVICE_WINDOW_HEIGHT}
      // deviceWidth={DEVICE_WINDOW_WIDTH}
      // animationInTiming={200}
      // backdropTransitionInTiming={100}
      // animationIn="slideInUp"
      // animationOutTiming={200}
      // backdropTransitionOutTiming={100}
      // animationOut="slideOutDown"
      // useNativeDriver={true}
      // isVisible={props.isVisible}
      // onRequestClose={props.closeModal}
      // onBackdropPress={props.closeModal}
      // style={style.modalView}
      visible={open}
      animationType="slide"
      transparent={true}
      onRequestClose={close}
    >
      <TouchableWithoutFeedback
        activeOpacity={1}
        onPressOut={close}
      >
        <View style={{
          flex: 1,
          width: config.DEVICE_WINDOW_WIDTH,
          height: config.DEVICE_WINDOW_HEIGHT + 50,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'rgba(0,0,0,0.6)',
        }}>
          <TouchableWithoutFeedback>
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
              padding: 20,
              borderRadius: 25,
              margin: 40,
            }}>
              <View
                style={[styles.circle, { backgroundColor: theme.color.secondary }]}
              >
                <Ticked />
              </View>
              <View style={styles.viewStyle}>
                <Text
                  variant="bodyDark"
                  color="secondary"
                  size={16}
                  style={{ textAlign: 'center' }}
                >
                  {title}
                </Text>
              </View>
              {link && (
                <View style={styles.viewStyle}>
                  <Text
                    variant="bodyDark"
                    color={theme.color.primaryBouquet}
                    size={16}
                    style={{ textAlign: 'center', textDecorationLine: 'underline' }}
                    onPress={() => viewReport()}
                  >
                    View report
            </Text>
                </View>
              )}
            </View>
          </TouchableWithoutFeedback>

        </View>
      </TouchableWithoutFeedback>
    </Modal>
    // </ReactNativeModal>
  );
};

export default SuccessModal;

const styles = StyleSheet.create({
  container: {
    flex: 0,
    minHeight: 340,
    padding: 10,
    marginHorizontal: 25,
    borderRadius: 10,
  },
  circle: {
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE / 2,

    justifyContent: 'center',
    alignItems: 'center',
  },
  viewStyle: {
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
  },
});
