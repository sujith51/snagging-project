/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import moment from 'moment';
import CalendarComponent from './CalendarComponent';
import { useTheme } from '../utils';
import {
    Modal
} from '_components';
const CalendarModal = ({ open, close,calendar ,title,current,changeDate}) => {
  const theme = useTheme();
  const dayClicked = (date) => {
    const newDate=date.dateString;
      changeDate(newDate);
      close();
  };
  return (
    <Modal
        open={open}
        close={close}
        title={title}
        body={ <CalendarComponent
            dayClicked={dayClicked}
            {...calendar}
        />}
      />
  );
};

export default CalendarModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
  },
});
