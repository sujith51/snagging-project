import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { getAppFont } from '../utils';

const InspectionComp = ({data = [], title=''}) =>{   
    return (
        <View>
            <View>
                <Text style={[styles.eachQuestion, {paddingLeft : 13}]}>
                    {title}
                </Text>
            </View>

            <View style={styles.questionCont}>
                {
                    data.map((item, key) => {
                        return (
                            <View style={[{ height: 24, flexDirection: 'row'}, key !== data.length - 1 && { borderBottomWidth: 1 }]}>
                                <View style={styles.eachQuestView}>
                                    <Text numberOfLines={1} style={styles.eachQuestion}>{item.question}</Text>
                                </View>
                                <View style={styles.eachAnsView}>
                                    <Text style={styles.eachAns}>{item.is_damaged === '0' ? 'No' : 'Yes'}</Text>
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        </View>
    )
}

export default InspectionComp;

const styles = StyleSheet.create({
    questionCont : { 
        flex: 1, 
        borderWidth: 1, 
        marginLeft: 10, 
        marginRight: 10, 
        marginTop: 10, 
        marginBottom:15 
    },
    eachQuestView : { 
        flex: 3, 
        borderRightWidth: 1, 
        paddingLeft: 9 ,
        justifyContent: 'center'
    },
    eachAnsView : { 
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    eachQuestion :{
        ...getAppFont('SemiBold'),
        fontSize :10,
    },
    eachAns : {
        ...getAppFont('SemiBold'),
        fontSize :10
    }
})