/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import Text from './Text';
import { useTheme, getAppFont } from '../utils';
import Button from './Button';

const ValidationModal = ({
  open,
  close,
  title,
  data,
  validErrDataforField,
  continueWithoutAns,
  scrollToParent,
}) => {
  const theme = useTheme();
  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
      animationIn="zoomIn"
      animationOut="zoomOut"
    >
      <ScrollView
        // contentContainerStyle={{
        //     justifyContent: 'center'
        // }}
        style={{
          backgroundColor: theme.color.primaryWhite,
          padding: 20,
          borderRadius: 10,
          // maxHeight: 600,
          paddingTop: 50,
        }}
      >
        <View style={styles.viewStyle}>
          <Text
            variant="bodyDark"
            color={theme.color.red}
            size={16}
            style={{ textAlign: 'center', paddingBottom: 20 }}
          >
            {title}
          </Text>
        </View>
        {!validErrDataforField && (
          <View>
            {data &&
              Object.keys(data).map((itm) => {
                return (
                  <>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        padding: 10,
                      }}
                      key={itm}
                    >
                      <View
                        style={{
                          height: 10,
                          width: 10,
                          borderRadius: 5,
                          backgroundColor: theme.color.primaryBlack,
                        }}
                      />
                      <Text
                        variant="bodyBold"
                        color="secondary"
                        size={16}
                        style={{ bottom: 5, paddingLeft: 8 }}
                      >
                        {itm}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingBottom: 7,
                      }}
                    >
                      <Text>&#10140;</Text>
                      <Text
                        variant="textRegular"
                        color="primary"
                        size={16}
                        style={{ paddingLeft: 8 }}
                      >
                        Please answer all the required fields in the above
                        section
                      </Text>
                    </View>
                    {/* {data[itm]
                        .map((q) => {
                          return (
                            <>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  paddingBottom: 7,
                                }}
                              >
                                <Text>&#10140;</Text>
                                <Text
                                  variant="bodyBold"
                                  color="primary"
                                  size={16}
                                  style={{ paddingLeft: 8 }}
                                >
                                  {q.ques}
                                </Text>
                              </View>
                            </>
                          );
                        })
                        .flat()} */}
                  </>
                );
              })}
          </View>
        )}
        {validErrDataforField && (
          <View>
            {data &&
              Object.keys(data).map((itm, index) => {
                return (
                  <>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        padding: 10,
                      }}
                      key={itm}
                    >
                      <View
                        style={{
                          height: 10,
                          width: 10,
                          borderRadius: 5,
                          backgroundColor: theme.color.primaryBlack,
                        }}
                      />
                      <TouchableOpacity
                        onPress={() => scrollToParent(index, data)}
                      >
                        <Text
                          variant="bodyBold"
                          color="secondary"
                          size={16}
                          style={{ bottom: 5, paddingLeft: 8 }}
                        >
                          {/* {itm}({data[itm][0].category}) */}
                          {itm}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    {/* <View
                        style={{
                          flexDirection: 'row',
                          paddingBottom: 7,
                        }}
                      >
                        <Text>&#10141;</Text>
                        <Text
                          variant="textRegular"
                          color="primary"
                          size={16}
                          style={{ paddingLeft: 8 }}
                        >
                          Please answer all the required fields in the above
                          section
                        </Text>
                      </View> */}
                    {data[itm]
                      .map((q) => {
                        return (
                          <>
                            <View
                              style={{
                                flexDirection: 'row',
                                paddingBottom: 7,
                              }}
                            >
                              <Text>&#10141;</Text>
                              <Text
                                variant="textRegular"
                                color="primary"
                                size={16}
                                style={{ paddingLeft: 8 }}
                              >
                                {q.field === 'title' ? 'Title' : null}
                                {q.field === 'image' ? 'Image' : null}
                                {q.field === 'payBy' ? 'Paid By' : null}
                                {q.field === 'taskType' ? 'Issue type' : null}
                                {q.field === 'chooseMarerial'
                                  ? 'Please select material from rate card or add new material'
                                  : null}
                              </Text>
                            </View>
                          </>
                        );
                      })
                      .flat()}
                  </>
                );
              })}
          </View>
        )}

        <View style={{ marginBottom: 250 }} />
      </ScrollView>
      <View style={styles.footer}>
        <Button
          title="Close"
          style={{ marginTop: 20, padding: 20, borderRadius: 20 }}
          titleStyle={{ fontSize: 16, ...getAppFont('Regular') }}
          onPress={close}
        />
        {/* {!validErrDataforField && (
          <Button
            title="Continue without answer"
            style={{ marginTop: 20, padding: 20, borderRadius: 20 }}
            titleStyle={{ fontSize: 16, ...getAppFont('Regular') }}
            onPress={continueWithoutAns}
          />
        )} */}
      </View>
    </ReactNativeModal>
  );
};

export default ValidationModal;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // minHeight: 340,
    // padding: 10,
    // marginHorizontal: 10,
    borderRadius: 10,
    // alignItems: 'center',
    height: 56,
    // justifyContent: 'space-between',
    marginTop: 30,
    // backgroundColor: 'red',
  },
  viewStyle: {
    marginLeft: 10,
    marginRight: 10,
  },
  footer: {
    position: 'absolute',
    left: 20,
    right: 20,
    bottom: 0,
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 20,
  },
});
