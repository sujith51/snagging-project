import React from 'react';
import { View, Text, Modal, TouchableWithoutFeedback } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { useTheme, getAppFont, numberFormat, config, theme } from '../utils';


const TitleModal = ({ text, closeModal, modal }) => {
    return (
        <View>
            <Modal
                visible={modal}
                animationType="slide"
                transparent={true}
                onRequestClose={closeModal}
            >
                <TouchableWithoutFeedback
                    activeOpacity={1}
                    onPressOut={closeModal}
                >
                    <View style={{
                        flex: 1,
                        width: config.DEVICE_WINDOW_WIDTH,
                        height: config.DEVICE_WINDOW_HEIGHT + 50,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'rgba(0,0,0,0.6)',
                    }}>
                        <TouchableWithoutFeedback>
                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: 'white',
                                padding: 20,
                                borderRadius: 5,
                                margin: 40,
                            }}>
                                <View >
                                    <Text
                                        variant="taskBold"
                                        color="primary"
                                        style={{
                                            // width: 15,
                                            fontSize: 14,
                                            fontWeight: '500',
                                            color: 'black',
                                        }}
                                    >
                                        {text}
                                    </Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </View>
    )
}

export default TitleModal