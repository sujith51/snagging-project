/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import { useTheme } from '../utils';
import Text from './Text';

const TaskStatus = (props) => {
  const theme = useTheme();
  const {
    handleOnSelectPress,
    inspStatusColor,
    inspTextColor,
    selectedId,
    index,
    id,
    inspTitle,
  } = props;
  return (
    <View key={`tabButton${index}`} style={styles.mainItem}>
      <TouchableOpacity
        style={
          selectedId
            ? [
                index == 2 ? null : styles.verticleLine,
                styles.buttonStyle,
                {
                  borderColor: theme.color.darkGray,
                  backgroundColor: inspStatusColor,
                },
              ]
            : [
                styles.buttonStyle,
                index == 2 ? null : styles.verticleLine,
                {
                  borderColor: theme.color.darkGray,
                  backgroundColor: theme.color.lightGray,
                },
              ]
        }
        onPress={() => handleOnSelectPress(id)}
      >
        <Text
          style={
            selectedId
              ? {
                  fontWeight: 'bold',
                  fontSize: Platform.isPad ? 18 : 15,
                  color: inspTextColor,
                }
              : {
                  fontWeight: 'bold',
                  fontSize: Platform.isPad ? 18 : 15,
                  color: theme.color.primary,
                }
          }
          variant="subTitle"
        >
          {inspTitle}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TaskStatus;

const styles = StyleSheet.create({
  mainItem: {
    alignItems: 'center',
    paddingTop: 20,
    flexDirection: 'row',
    flex: 1,
  },
  categoryText: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  buttonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'transparent',
    paddingVertical: 7,
    // paddingHorizontal: 25,
    flex: 1,
  },
  verticleLine: {
    borderRightWidth: 1,
  },
});
