import React, { useState } from 'react';
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  TextInput,
} from 'react-native';

import { useTheme } from '../utils';

import Text from './Text';

const SIZE = 25;
const Box = ({ title, onPress, disabled }) => {
  const theme = useTheme();
  return (
    <TouchableWithoutFeedback onPress={onPress} disabled={disabled}>
      <View
        style={[
          styles.box,
          { backgroundColor: theme.color.primary },
          disabled && { opacity: 0.4 },
        ]}
      >
        <Text variant="bodyBold" color="primaryWhite" size={18}>
          {title}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

const CounterWrapper = (props: Props) => {
  const { width, quantity = 0, onIncrement, onDecrement, inputQty } = props;
  const theme = useTheme();
  const [value, setValue] = useState(0);
  return (
    <View style={[styles.container, { width }]}>
      <Box title="-" disabled={quantity === 0} onPress={onDecrement} />
      <TextInput
        value={quantity.toString()}
        maxLength={2}
        keyboardType="numeric"
        style={[styles.input, { borderColor: theme.color.primaryBouquet }]}
        onChangeText={(t) => inputQty(t)}
      />
      <Box title="+" onPress={onIncrement} />
    </View>
  );
};

export default CounterWrapper;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
  },
  input: {
    height: SIZE,
    width: SIZE,
    borderWidth: 1,
    padding: 0,
    textAlign: 'center',
    // fontSize: 12,
    marginHorizontal: 5,
    // justifyContent: 'center',
    // alignItems: 'center',
    // paddingLeft: SIZE / 2,
  },
  box: {
    justifyContent: 'center',
    alignItems: 'center',
    height: SIZE,
    width: SIZE,
  },
});
