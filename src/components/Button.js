/* eslint-disable react-native/no-inline-styles */
// @flow
import * as React from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  StyleSheet,
  Platform,
} from 'react-native';
import { useTheme, buttonType } from '../utils';

type Props = {
  title: string,
  color: string,
  disabled: boolean,
  style: StyleSheet.Styles,
  titleStyle: StyleSheet.Styles,
  type: buttonType,
  onPress: () => void,
  icon: null,
};

const Button = (props: Props) => {
  const theme = useTheme();
  // console.log('thme', theme);
  const {
    title,
    color = theme.color.primary,
    disabled = false,
    style,
    titleStyle,
    onPress,
    icon,
    type = 'primary',
  } = props;

  return (
  
    <TouchableWithoutFeedback disabled={disabled} onPress={onPress}>
      <View
        style={[
          { padding: theme.spacing.m },
          styles.container,
          style,
          { backgroundColor: color },
         
          icon && { flexDirection: 'row', justifyContent: 'space-between' },
          type === 'primary' && { backgroundColor: theme.color.primary },
          type === 'default' && {
            backgroundColor: theme.color.primaryWhite,
            borderColor: theme.color.primary,
            borderWidth: 1,
          },
          type === 'blue' && { backgroundColor: theme.color.secondary },
          type === 'Yes' && { backgroundColor: theme.color.secondaryGreen },
          type === 'No' && { backgroundColor: theme.color.secondaryRedLight },
          type === 'N/A' && { backgroundColor: theme.color.secondaryGreen },
          disabled && { opacity: 0.6,backgroundColor:'rgba(0,0,0,.2)' },
        ]}
      >
        <Text
          style={[
            type === 'primary' && { color:theme.color.primaryWhite },
            type === 'default' && { color: theme.color.primary },
            type === 'blue' && { color: theme.color.primaryWhite },
            type === 'Yes' && { color: theme.color.secondaryGreenDark },
            type === 'N/A' && { color: theme.color.secondaryGreenDark },
            type === 'No' && { color: theme.color.secondaryRed },
            Platform.isPad ? { fontSize: 20 } : { fontSize: 14 },
            titleStyle,
          ]}
        >
          {title}
        </Text>
        {icon && <View>{icon}</View>}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
