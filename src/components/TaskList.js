/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  Platform,
} from 'react-native';
import { useSelector } from 'react-redux';
import { getAppFont } from '../utils';

const TaskList = ({
  data,
  renderItem,
  listHeaderComponent,
  loading = false,
  onEndReached,
  isSearch = false,
  showNoDataMessage = true,
}) => {
  const { selectCommunity } = useSelector((state) => state.user);
  const keyExtractor = (item, index) =>
    `Task-List-${item.id_snag_property}-${index}`;

  const listEmpty = () => {
    return (
      <>
        {selectCommunity?.length === 0 && !isSearch && (
          <View style={styles.errMsg}>
            <Text style={styles.errTxt}>Please select Community</Text>
          </View>
        )}
        {selectCommunity?.length > 0 && showNoDataMessage && (
          <View style={styles.listEmptyCont}>
            <Text style={styles.errTxt}>No Data Found.</Text>
          </View>
        )}
      </>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={selectCommunity?.length === 0 ? [] : data}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ListHeaderComponent={listHeaderComponent}
        bounces={false}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.8}
        ListFooterComponent={loading && <ActivityIndicator color="#999999" />}
        ListEmptyComponent={!loading && listEmpty}
      />
    </View>
  );
};

export default TaskList;

const styles = StyleSheet.create({
  listEmptyCont: {
    flex: 1,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errMsg: {
    flex: 1,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errTxt: {
    ...getAppFont('Bold'),
    fontSize: Platform.isPad ? 18 : 14,
  },
});
