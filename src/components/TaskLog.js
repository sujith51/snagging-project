/* eslint-disable react-native/no-unused-styles */
/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
// import { CommonContainer, Text, Card } from '_components';

import { getAppFont, getFormattedDate, useTheme, getTasksDate } from '../utils';
import { monthDayYear } from '../utils/theme';
import Card from './Card';
import CommonContainer from './CommonContainer';
import Text from './Text';

type Props = {
  data: Array,
  headerRight: string | number,
  headerLeft: string | number,
};

const TaskLog = ({
  moveOutOn,
  keyCollectedOn,
  notifiactiontoPM,
  pMInitiated,
  inspectionOn,
  qRequestedOn,
  qReceivedOn,
  sApproval,
  onPress = () => {},
}) => {
  const theme = useTheme();
  const {
    taskLogs: {
      data,
      // : {
      //   unit_name = null,
      //   id_unit = '',
      //   move_out_date = '',
      //   unit_number = '',
      //   move_out_time = '',
      //   key_collected_on = '',
      //   key_collected_time = '',
      //   pm_notification_on = '',
      //   pm_notification_time = '',
      //   pm_initiated_inspection_on = '',
      //   pm_initiated_inspection_time = '',
      //   inspection_start_on = '',
      //   inspection_start_time = '',
      //   quotation_requested_on = '',
      //   quotation_requested_time = '',
      //   quotation_received_on = '',
      //   quotation_received_time = '',
      //   segregation_approval_on = '',
      //   segregation_approval_time = '',
      // },
    },
  } = useSelector((state) => state.taskdetails);

  const reassignLogs = [
    {
      'Reallocation of Inspection':
        'Inspector 1 was assigned by Mahendran on July 26, 2021',
    },
    {
      'Reallocation of Inspection':
        'Inspector 1 was assigned by Mahendran on July 26, 2021',
    },
  ];
  return (
    <CommonContainer navbar={{ title: 'Task Logs' }}>
      <Card cardStyle={[styles.cardStyle, styles.paddingtop]}>
        {data?.logs?.length === 0 && <Text>Task Logs Not Available</Text>}
        <View>
          <View>
            <View
              style={[
                styles.rowView,
                styles.spacing,
                {
                  paddingBottom: 14,
                  borderBottomColor: theme.color.secondaryLight2,
                  marginBottom: theme.spacing.m,
                  paddingTop: 15,
                  paddingLeft: 0,
                },
              ]}
            >
              <Text variant="bodyBold">{data?.community_name}</Text>
              <Text variant="bodyBold">#{data?.unit_name}</Text>
            </View>
          </View>
          {Object.keys(data?.logs).map((a) => {
            // console.log('logs', data?.logs[a]);
            if (a === 'inspection_logs') {
              return Object.keys(data?.logs[a])?.map((val) => {
                return (
                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        paddingBottom: theme.spacing.sm,
                      }}
                      variant="taskBold"
                    >
                      {val === 'move_out_logs'
                        ? 'Move Out Logs'
                        : val == 'vacant_logs'
                        ? 'Vacant Logs'
                        : 'Makeready Logs'}
                    </Text>
                    <>
                      {data?.logs[a][val]?.map((inner) => {
                        return (
                          <View
                            style={[
                              styles.rowView,
                              { paddingBottom: theme.spacing.sm },
                            ]}
                          >
                            <View style={styles.left}>
                              <Text
                                size={Platform.isPad ? 19 : 16}
                                variant="textMedium"
                              >
                                {inner?.is_first === '1'
                                  ? 'Auto allocation of inspection'
                                  : 'Reallocation of inspection'}
                              </Text>
                            </View>
                            <View style={styles.right}>
                              <Text
                                size={Platform.isPad ? 19 : 16}
                                variant="bodyBold"
                                style={styles.rightText}
                              >
                                {inner.assigned_user}, on{' '}
                                {getTasksDate(inner?.created_on)}
                              </Text>
                            </View>
                          </View>
                        );
                      })}
                    </>
                  </View>
                );
              });
            }
            return (
              <View>
                <View
                  style={[styles.rowView, { paddingBottom: theme.spacing.sm }]}
                >
                  <View style={styles.left}>
                    <Text size={Platform.isPad ? 19 : 16} variant="textMedium">
                      {a}
                    </Text>
                  </View>
                  <View style={styles.right}>
                    <Text
                      size={Platform.isPad ? 19 : 16}
                      variant="bodyBold"
                      style={styles.rightText}
                    >
                      {data?.logs[a]}
                    </Text>
                  </View>
                </View>
              </View>
            );
          })}

          {data?.reassignLogs &&
            data?.reassignLogs.map((a) => {
              return (
                <View>
                  <View
                    style={[
                      styles.rowView,
                      { paddingBottom: theme.spacing.sm },
                    ]}
                  >
                    <View style={styles.left}>
                      <Text
                        size={Platform.isPad ? 19 : 16}
                        variant="textMedium"
                      >
                        {Object.keys(a)[0]}
                      </Text>
                    </View>
                    <View style={styles.right}>
                      <Text
                        size={Platform.isPad ? 19 : 16}
                        variant="bodyBold"
                        style={styles.rightText}
                      >
                        {Object.values(a)[0]}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            })}
        </View>
      </Card>
    </CommonContainer>
  );
};

export default TaskLog;

const styles = StyleSheet.create({
  rowView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    // borderBottomWidth: 0.2,
  },
  spacing: {
    borderBottomWidth: 1,
  },
  cardStyle: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 350,
    padding: 10,
  },
  paddingtop: {
    marginTop: 29,
  },
  buttonView: {
    left: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  buttonTxt: {
    fontSize: 10,
  },
  left: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  right: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: 10,
  },
  // rightText: {
  //   textAlign: 'right',
  // },
});
