import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
// import {
//   // CommonContainer,
//   Text,
//   Button,
//   SuccessModal,
//   Spinner
// } from '_components';
import { useNavigation,useRoute } from '@react-navigation/native';
import { submitSegReport,getQuotationList } from '../redux/actions';
import { useTheme, getAppFont } from '../utils';
import { usePrevious } from '_hooks';
import SegregationRectangle from './ui-elements/SegregationRectangle';
import CommonContainer from './CommonContainer';
import Text from './Text';
import Button from './Button';
import SuccessModal from './SuccessModal';
import Spinner from './ui-elements/Spinner';

const SegregationReport = () => {
  const { navigate } = useNavigation();
  const dispatch = useDispatch();
  const theme = useTheme();
  const [modal, setModal] = useState(false);
  const { params } = useRoute();
  const { selectedVendor, selectedUnit } = params;

  const {
    subitSegReport,
    subitSegReport: {
      submitSegData,
      subitSegReportLoading,
      subitSegReportError,
    },
    geneSegReport: { segData, geneSegReportLoading, geneSegReportError },
    quotationList: { listData, quotationLoading, quotationListError },
  } = useSelector((state) => state.quotation);

  useEffect(() => {
    let qListData = {};
    qListData['accessCommunityIds[0]'] = 8;
    qListData['accessCommunityIds[1]'] = 6;
    qListData['id_role'] = 16;
    qListData['status'] = 'quotation_requested';
    dispatch(getQuotationList(qListData));
  }, []);

  const {
    data: { id_user = null, email = null, id_role = null },
  } = useSelector((state) => state.user);

  const onSubmit = () => {
    let reqData = {};
    reqData['quote_code'] = selectedVendor?.quote_code;
    reqData['id_property'] = selectedVendor?.id_property;
    reqData['unit_name'] = selectedVendor?.unit_name;
    reqData['vendor_id'] = selectedVendor?.vendor_id;
    reqData['id_user'] = id_user;
    reqData['submit_status'] = 'submit_status';
    reqData['quote_status'] = 0;
    dispatch(submitSegReport(reqData));
  };
  const prevSubitSegReport = usePrevious(subitSegReport);
  useEffect(() => {
    if (prevSubitSegReport !== subitSegReport) {
      if (submitSegData && submitSegData.status === 'success') {
        setModal(!modal);
      } else {
        // setModal(!modal);
      }
    }
  }, [subitSegReport]);

  const closeModel = () => {
    setModal(!modal);
    // navigate('Dashboard');
  };

  return (
    <CommonContainer>
      <View style={styles.container}>
      { subitSegReportLoading ? 
        <Spinner
          visible={subitSegReportLoading}
          size="large"
          animationType="none"
        /> :
        // <ActivityIndicator size="large" color="#65013D"  style={{paddingTop:80}} /> :
        <>
          <SuccessModal
            open={modal}
            close={() =>closeModel()}
            title={submitSegData && submitSegData.status === 'success' ? "Report Generated Successfuly" : 'failed!'}
          />
          {  
            segData?.owner.length != 0  ? 
            <View style={{paddingTop:10}}>
              {/* <Text style={styles.textStyle}>{'Paid By owner'}</Text> */}
              <SegregationRectangle 
                selectedData={2} 
                    headerStyle={8}
                    title = {'Paid by owner'}
                // listingData={segData?.owner}  
                // listingData={segData?.owner.length && segData.owner}  
                listingData={
                  segData ? 
                  segData.owner.length >= 0?
                    segData.owner : 
                  segData.owner.length != null 
                  ? segData.owner : [] : [] }  
                quotationLoad={quotationLoading}
                headTextStyle={10}
              /> 
            </View>
            :  
            ( <View style={styles.mesContainer}>
                <Text style={styles.mesText}>{'Segregation not available for owner'}</Text>
              </View>
            )  
          }
    
          {  
             segData?.tenant.length  !== 0 ? 
            <View style={{paddingTop:35}}>
              {/* <Text style={styles.textStyle}>{'Paid By Tenant'}</Text> */}
                <SegregationRectangle 
                  selectedData={2} 
                    headerStyle={8}
                    title = {'Paid By Tenant'}
                  // listingData={segData?.tenant.length && segData.tenant} 
                  listingData={ segData ? 
                    segData.tenant.length!=0  ? 
                    segData.tenant : 
                    segData.tenant.length !=null ? 
                    segData.tenant :[]:[]}   
                  quotationLoad={quotationLoading}
                  headTextStyle={10}
                />
            </View>
           : 
           (  <View style={styles.mesContainer}>
                <Text style={styles.mesText}>{'Segregation not available for tenant'}</Text>
              </View>
            ) 
          }
          { segData?.owner.length != 0 || segData?.tenant.length  !== 0 ? 
            <View  style={{paddingTop:45}}>
              <Text style={styles.infoTxt} numberOfLines={2} >{'Submit to Technical Manager for Segregation Approval'}</Text>
              <View style={styles.buttonView}>
                <Button
                  titleStyle={styles.buttonTxt}
                  title={'SUBMIT'}
                  style={styles.buttonContainer}
                  color={theme.color.red}
                  onPress={() =>onSubmit()}
                />   
              </View>
            </View>
            : null
          }
        </>
      }
      </View>
    </CommonContainer>
  );
};

export default SegregationReport;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },
  buttonView: {
    paddingHorizontal: 60,
    paddingVertical: 10,
    paddingBottom: 50,
  },
  buttonTxt: {
    fontSize: 18,
    ...getAppFont('Bold'),
  },
  infoTxt: {
    flex: 1,
    fontSize: 14,
    paddingLeft: 20,
    paddingBottom: 20,
    flexWrap: 'wrap',
  },
  textStyle: {
    fontSize: 20,
    paddingLeft: 2,
    ...getAppFont('Bold'),
  },
  mesContainer: {
    flex: 1,
    paddingTop: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mesText: {
    textAlign: 'center',
    fontSize: 18,
    // fontFamily:'System',
    // color: '#898989',
  },
});
