/* eslint-disable react-native/no-color-literals */
/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  createRef,
  useRef,
  useLayoutEffect,
  useCallback,
  useEffect,
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Platform,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { FlatList } from 'react-native-gesture-handler';
import { Transition, Transitioning } from 'react-native-reanimated';
import InputText from './InputText';
// import { useTheme, config, showFixedLength } from '../../utils';
import { useTheme, config, showFixedLength, getAppFont } from '../utils';

const maxHeight = config.DEVICE_WINDOW_HEIGHT - 300;

const SearchDropdown = ({
  value,
  options,
  placeholder = 'Select',
  onOpen = () => {},
  onClose = () => {},
  placeHolderStyle = null,
  count = null,
  iconColor = null,
  labelContStyle = {},
  labelStyle = {},
  contStyle = {},
  drDownMdlwidth = null,
  container,
  searchItem,
  item,
  onChangeText = () => {},
  search = false,
}) => {
  console.log({ search, options });
  const theme = useTheme();
  // :States
  const [open, setOpen] = useState(false);
  const dropdownRef = useRef(createRef());
  const [dropdownDim, setDropDownDim] = useState(null);
  const [selected, setSelected] = useState(null);

  // :Effects
  useLayoutEffect(() => {
    dropdownRef.current.measureInWindow((x, y, width, height) => {
      if (!dropdownDim || dropdownDim.x !== x || dropdownDim.y !== y) {
        setDropDownDim({ x, y, width, height });
      }
    });
  });

  const parentChanges=(e)=>{
    console.log(e)
    if(e){
      setOpen(true)
      searchItem(e)
    }
    // searchItem
  }

  // :Render
  return (
    /*  */
    <>
     
        <View
          style={{position:'relative'}}
          ref={(el) => (dropdownRef.current = el)}
          // collapsable={false}
        >
          <InputText
            type="default"
            value={item}
            onChangeText={(e) => parentChanges(e)}
            // innerRef={itemRef}
            // onSubmitEditing={() => specifyRef.current.focus()}
            textFieldContainerStyle={styles.borRadius}
          />
          <ReactNativeModal
        isVisible={open}
        // onBackdropPress={requestClose}
        // onBackButtonPress={requestClose}
        backdropColor="transparent"
        style={{ padding: 0, margin: 0,position:'absolute',top:50 }}
      >
        <View
          // style={[
            // styles.dropdownContainer,
            // {
            //   top: dropdownDim?.y + dropdownDim?.height,
            //   left: dropdownDim?.x,
            //   width: drDownMdlwidth ? dropdownDim?.width : 'auto',
            //   backgroundColor: theme.color.primaryWhite,
            // },
          // ]}
        >
          <Text>hi</Text>
        </View>
      </ReactNativeModal>
        </View>
        {/* </TouchableWithoutFeedback> */}
      

      
    </>
  );
};

export default React.memo(SearchDropdown);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 7,
    paddingHorizontal: 12,
    // borderWidth: 1.5,
    borderRadius: 6,
    alignItems: 'center',
    // justifyContent: 'space-between',
    // flex: 1,
    right: 20,
  },
  label: {
    textAlign: 'center',
    marginTop: 3,
    fontSize: Platform.isPad ? 20 : 14,
  },
  dropdownTxtStyle: {
    ...getAppFont('Medium'),
    fontSize: Platform.isPad ? 20 : 14,
  },
  dropdownContainer: {
    position: 'absolute',
    top: 0,
    // left: 0,
    // backgroundColor: theme.colors.white,
    // ...theme.cardShadow,
    paddingBottom: 7,
    borderRadius: 5,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageStyle: {
    height: 30,
    width: 40,
    marginRight: 10,
  },
});
