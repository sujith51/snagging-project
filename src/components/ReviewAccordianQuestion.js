/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable import/no-unresolved */
import React, {useState, useRef, useEffect} from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  savechklistquesctions,
  saveYValues,
  saveMaterialDetails,
  getMaterialsList,saveExtraReviewData
} from '_actions';
import {
  Loader,
  Button,
  InputText,
  Text,
  DropDown,
  DropDownCheckQty,
  ImageUpload,
  RequestModal,
  RequestListModal,
  AddedMaterialModal,
  ConfirmModal,Toggle
} from '_components';
import {useTheme, getAppFont} from '../utils';
import Panel from './SummaryPanel';

const ReviewAccordianQuestion = (props: Props) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const {
    sub_categery='Additional Comment',
    is_multiple ='1',
    count,
    idIns,
    additionalCommentsId=205
  } = props;
  const obj = {};

  const [loading, setLoading] = useState(false);
  const [openList, setOpenList] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [commentText, setCommentText] = useState(null);
  const [quentity, setQuentity] = useState(null);
  const [parentIdValue, setParentIdValue] = useState(null);
  const [selectedObject, setSelectedObject] = useState({});
  const [selectedNof, setSelectedNof] = useState(null);
  const [selectedData, setSelectedData] = useState(null);
  const [saveSelected, setSaveSelected] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [openMaterial, setOpenMaterial] = useState(false);
  const [materials, setMaterials] = useState({});
  const [callMaterial, setCallMaterial] = useState(false);
  const [justSaveData, setJustSaveData] = useState(true);
  const [confirm, setConfirm] = useState(false);
  const [firstVendor, setFirstVendor] = useState({});

  const [iosToggleRequest, setIosOpenMaterial] = useState(false);
  const [iosShowModal, setIosShowModal] = useState(false);
  const [iosOpenList, setIosOpenList] = useState(false);


  const {
    chklistQuesData,saveReviewExtraDatas,
    positionData,
    newMaterialArr = {},
    materialLists: {data: materialListsData},
  } = useSelector(state => state.taskdetails);
  const checjlistObj = saveReviewExtraDatas[idIns];
  // console.log('checjlistObj',checjlistObj)
  // const myRefs=useRef([])
 
 

 
  // useEffect(() => {
  //   if (sub_categery == categorySelected) {
  //     setIsActive(null);
  //     count.forEach(val => {
  //       if (val === listCount) {
  //         setIsActive(`${categorySelected} ${is_multiple === '1' ? val : ''}`);
  //       }
  //     });
  //   }
  // }, [categorySelected, sub_categery, listCount, count, is_multiple]);
 
  const initAdditional = nof => {
    const saveRes = {
      details: additionalCommentsId,
      numberOf: nof,
      quesction: {
        damage: 0,
        comment: null,
        id_template_question_answers: null,
        question_answer: null,
        taskType: 0,
        image: null,
        description: null,
      },
    };
    dispatch(saveExtraReviewData(idIns, saveRes));
  };
  

  
  const handleAdditional = (data, key, value, nof) => {
    const idTemplateMapping =
      checjlistObj.answer[data] && checjlistObj.answer[data][nof];
    // console.log('handleAdditional', {data, key, value, nof, idTemplateMapping});
    const saveRes = {
      details: data,
      numberOf: String(nof),
      quesction: {
        ...idTemplateMapping,
        damage: 0,
        [key]: value,
      },
    };
    dispatch(saveExtraReviewData(idIns, saveRes));
  };
  

  
  const listModal = (value, ques, nof) => {
    setShowModal(false);
    setIosOpenList(true);
    setOpenMaterial(false);
    // handleAnswersNewMaterial(value, ques, nof);
    // handleAnswers(ques, 'newMaterial', value, nof);
  };

  useEffect(() => {
    if (iosOpenList) {
      setLoading(true);
      const timer = setTimeout(() => {
        setIosOpenList(false);
        setOpenList(true);
        setLoading(false);
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [iosOpenList]);

  const openAddMaterial = () => {
    setShowModal(false);
    setOpenList(false);
    setIosOpenMaterial(true);
  };

  useEffect(() => {
    if (iosToggleRequest) {
      setLoading(true);
      const timer = setTimeout(() => {
        setIosOpenMaterial(false);
        setOpenMaterial(true);
        setLoading(false);
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [iosToggleRequest]);

  const closeAddMaterial = () => {
    setOpenMaterial(false);
    setIosShowModal(true);
    setOpenList(false);
  };

  useEffect(() => {
    if (iosShowModal) {
      setLoading(true);
      const timer = setTimeout(() => {
        setIosShowModal(false);
        setShowModal(true);
        setLoading(false);
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [iosShowModal]);

 
  const handleSecScroll = () => {
    if (sectionListRef.current) {
      sectionListRef.current.scrollToLocation({
        animated: true,
        sectionIndex: 0,
        itemIndex: 1,
        // viewPosition: 0,
      });
    }
  };

  const scrollToIndexFailed = error => {
    const offset = error.averageItemLength * error.index;
    sectionListRef.current.scrollToffset({offset});
    setTimeout(
      () =>
        sectionListRef.current.scrollToLocation({
          animated: true,
          // index: error.index,
          sectionIndex: error.index,
          itemIndex: 1,
        }),
      100,
    );
  };

 
  const RenderAdditionalUI = nof => {
    let addData =
      checjlistObj?.answer[additionalCommentsId] &&
      checjlistObj?.answer[additionalCommentsId][nof];
   
    return (
      <>
        <View style={styles.container}>
          <View
            style={[
              styles.wrapper,
              {
                borderWidth: 1,
                borderColor: theme.color.secondaryLight2,
              },
            ]}>
            <InputText
              placeholder="Enter Title"
              onChangeText={text =>
                handleAdditional(additionalCommentsId, 'comment', text, nof)
              }
              value={addData && addData?.comment}
              style={[
                styles.commentbox,
                {
                  borderColor: theme.color.secondaryLight2,
                  padding: theme.spacing.sm,
                  borderRadius: theme.borderRadius.x,
                  backgroundColor: theme.color.primaryWhite,
                  marginBottom: 0,
                },
              ]}
              textFieldStyle={styles.txtFieldStyle}
              blurOnSubmit={false}
              multiline
              numberOfLines={2}
              textFieldContainerStyle={styles.txtfldcontainstyle}
            />
            <View style={{paddingTop: 10}}>
              <Text variant="taskBold">Issue type*</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
              }}>
              <Button
                title="Major"
                type={addData && addData?.taskType === 1 ? 'No' : 'default'}
                style={styles.button1}
                titleStyle={styles.appFontReg}
                onPress={() =>
                  handleAdditional(additionalCommentsId, 'taskType', 1, nof)
                }
              />
              <Button
                title="Minor"
                type={addData && addData?.taskType === 2 ? 'Yes' : 'default'}
                style={styles.button1}
                titleStyle={styles.appFontReg}
                onPress={() =>
                  handleAdditional(additionalCommentsId, 'taskType', 2, nof)
                }
              />
            </View>
            <ImageUpload
              onImagesUpdate={img => {
                img != null &&
                  handleAdditional(additionalCommentsId, 'image', img, nof);
              }}
              preImages={addData && addData?.image}
              // style={{ paddingLeft: 6, paddingRight: 6 }}
              renderImage={addData && addData?.image}
              multiple
            />
            <View>
              <InputText
                placeholder="Comment"
                onChangeText={text =>
                  handleAdditional(
                    additionalCommentsId,
                    'description',
                    text,
                    nof,
                  )
                }
                value={addData && addData?.description}
                style={[
                  styles.commentbox,
                  {
                    borderColor: theme.color.secondaryLight2,
                    padding: theme.spacing.sm,
                    borderRadius: theme.borderRadius.x,
                    backgroundColor: theme.color.primaryWhite,
                    marginBottom: 0,
                  },
                ]}
                textFieldStyle={styles.txtFieldStyle}
                blurOnSubmit={false}
                multiline
                numberOfLines={2}
                textFieldContainerStyle={styles.txtfldcontainstyle}
              />
            </View>
          </View>
          
        </View>
       
      </>
    );
  };
  const setIsActiveFunction = (key, nof) => {
    // console.log('key', {key, nof, isAdditionalComment});
    setIsActive(key);
    let addData =checjlistObj && checjlistObj[nof];

      // if (!addData) {
      //   initAdditional(nof);
      // }
   
  };

  const handleRender = () => {
    return (
      <>
        {count?.map(a => {
          return (
            <>
            <Toggle title={`${sub_categery} ${is_multiple === '1' ? a : ''}`}>
            {RenderAdditionalUI(String(a))}
            </Toggle>
              {/* <Panel
                key={`key-${a}`}
                title={`${sub_categery} ${is_multiple === '1' ? a : ''}`}
                onToggle={key => setIsActiveFunction(key, a)}
                isActive={
                  isActive === `${sub_categery} ${is_multiple === '1' ? a : ''}`
                }>
                {RenderAdditionalUI(String(a))}
              </Panel> */}
            </>
          );
        })}
        {/* {count.map((a) => {
          return (
            <>
              <TouchableWithoutFeedback
                onPress={() => setToggle(!toggle)}
                key={a}
              >
                <View
                  style={{
                    borderBottomWidth: 2,
                    borderBottomColor: theme.color.secondaryGreenDark,
                    padding: 5,
                  }}
                >
                  <Text variant="taskBold" size={19}>
                    {sub_categery} {is_multiple === '1' ? a : null}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
              {RenderUi(String(a))}
            </>
          );
        })} */}
      </>
    );
  };
  const onSuccess = () => {
    // console.log('vendor', isVendorEmpty, firstVendor);
    if (firstVendor) {
      handleVendorforAllQues(firstVendor);
      setConfirm(prev => !prev);
      isFirstTime(2);
    }
  };
  const onFailure = () => {
    // console.log('vendor', isVendorEmpty, firstVendor);
    isFirstTime(0);
    setConfirm(prev => !prev);
  };
  

  return (
    <View>
      {confirm && (
        <ConfirmModal
          open={confirm}
          close={onFailure}
          title={`${firstVendor?.label} will be selected as the default Vendor`}
          onSuccess={onSuccess}
          onFailure={onFailure}
        />
      )}

      {handleRender()}

      {/* <SectionList
        sections={[{ data: count }]}
        ref={sectionListRef}
        keyExtractor={(item, index) => item + index}
        onScrollToIndexFailed={scrollToIndexFailed}
        renderItem={({ item }) => handleSectionList(item)}
      /> */}
    </View>
  );
};

export default ReviewAccordianQuestion;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    paddingTop: 10,
  },
  // eslint-disable-next-line react-native/no-color-literals
  wrapper: {
    paddingBottom: 15,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
    borderWidth: 0.3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20,
    elevation: 3,
    paddingTop: 15,
  },
  row: {
    flexDirection: 'row',
    paddingTop: 20,
    justifyContent: 'space-between',
  },
  button1: {
    width: Platform.isPad ? 300 : 100,
    marginRight: Platform.isPad ? 15 : 5,
    padding: Platform.isPad ? 20 : 10,
    borderRadius: 10,
  },
  commentbox: {
    borderWidth: 1,
    height: Platform.isPad ? 90 : 70,
    // marginLeft: 15,
    // marginRight: 15,
    // marginBottom: 30,
    marginTop: 20,
    alignItems: 'flex-start',
  },
  card: {
    borderWidth: 1,
    paddingHorizontal: 15,
    marginBottom: 10,
    paddingVertical: 10,
    // flexWrap: 'wrap',
  },
  txtFieldStyle: {
    textAlignVertical: 'top',
    alignItems: 'flex-start',
    ...getAppFont('Regular'),
  },
  txtfldcontainstyle: {
    borderBottomWidth: 0,
  },
  selectvendorStyle: {
    paddingTop: 15,
    paddingBottom: 5,
  },
  dropDownContainer: {
    justifyContent: 'space-between',
    left: 0,
    height: 46,
    borderRadius: 10,
  },
  appFontReg: {
    ...getAppFont('SemiBold'),
  },
  rownew: {
    flexDirection: 'row',

    marginBottom: 10,
    // flex: 1,
    justifyContent: 'space-between',
    // height: 30,
    // backgroundColor: 'red'
  },
});
