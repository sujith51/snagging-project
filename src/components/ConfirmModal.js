import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Linking,
  Modal,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { Ticked } from '_svg';
import Text from './Text';
import { useTheme, config } from '../utils';
import Button from './Button';

const { width } = Dimensions.get('window');
const SIZE = 140;
const ConfirmModal = ({
  open,
  close,
  title,
  onSuccess,
  onFailure = () => {},
  showContinue = true,
  sucessText = 'Continue',
  showFailure = true,
}) => {
  const theme = useTheme();

  return (
    <Modal
      visible={open}
      animationType="slide"
      transparent
      onRequestClose={close}
    >
      <TouchableWithoutFeedback activeOpacity={1} onPressOut={close}>
        <View
          style={{
            flex: 1,
            width: config.DEVICE_WINDOW_WIDTH,
            height: config.DEVICE_WINDOW_HEIGHT + 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(0,0,0,0.6)',
          }}
        >
          <TouchableWithoutFeedback>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                padding: 20,
                borderRadius: 25,
                margin: 40,
              }}
            >
              <View style={styles.viewStyle}>
                <Text
                  variant="bodyDark"
                  color="secondary"
                  size={16}
                  style={{ textAlign: 'center' }}
                >
                  {title}
                </Text>
              </View>
              {showContinue && <Text> Do you want to continue?</Text>}

              <View style={{ flexDirection: 'row', paddingTop: 20 }}>
                <Button
                  title={sucessText}
                  type="primary"
                  style={styles.button1}
                  titleStyle={styles.appFontReg}
                  onPress={() => onSuccess()}
                />
                {showFailure && (
                  <Button
                    title="Cancel"
                    type="default"
                    style={styles.button1}
                    titleStyle={styles.appFontReg}
                    onPress={() => onFailure()}
                  />
                )}
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default ConfirmModal;

const styles = StyleSheet.create({
  container: {
    flex: 0,
    minHeight: 340,
    padding: 10,
    marginHorizontal: 25,
    borderRadius: 10,
  },
  circle: {
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE / 2,

    justifyContent: 'center',
    alignItems: 'center',
  },
  button1: {
    width: 100,
    marginRight: Platform.isPad ? 15 : 5,
    padding: Platform.isPad ? 20 : 10,
    borderRadius: 10,
  },
  viewStyle: {
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
  },
});
