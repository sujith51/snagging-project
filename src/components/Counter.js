import React, { useState } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet } from 'react-native';

import { useTheme } from '../utils';
import Button from './Button';

import Text from './Text';
import { Plus } from '_svg';

const Counter = (props: Props) => {
  const { title } = props;
  return <Button title={title} />;
};

export default Counter;

const styles = StyleSheet.create({});
