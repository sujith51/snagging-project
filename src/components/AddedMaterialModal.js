import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  View,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux';

import { getMaterialsList } from '_actions';
import InputText from './InputText';
import Text from './Text';
import Button from './Button';

import { useTheme, getAppFont, config } from '../utils';
import CheckBox from './ui-elements/CheckBox';
import { SearchIcon } from '../assets/svg_components';

const { width } = Dimensions.get('window');
const counterWidth = width * 0.5;

const AddedMaterialModal = ({ open, close, saveMaterial }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const [materials, setMaterials] = useState([]);
  const [copyMaterials, setCopyMaterials] = useState([]);
  const [savedMaterial, setSavedMaterial] = useState({});
  const [searchText, setSearchText] = useState('');
  const {
    materialLists: {
      data: materialListsData,
      loading: materialListsloading,
      error: materialListsError,
    },
  } = useSelector((state) => state.taskdetails);

  // useEffect(() => {
  //   if (open) {
  //     const data = {};
  //     data.is_app = 'Y';
  //     dispatch(getMaterialsList(data));
  //   }
  // }, [open, dispatch]);

  useEffect(() => {
    if (materialListsData?.length > 0) {
      setMaterials(materialListsData);
      setCopyMaterials(materialListsData);
    } else {
      setMaterials([]);
      setCopyMaterials([]);
    }
    setSearchText('');
  }, [materialListsData]);
  useEffect(() => {
    if (searchText != '') {
      const setData = materials.filter((material) => {
        const data = material.item_request
          ? material.item_request.toUpperCase()
          : '';
        const textData = searchText.toUpperCase();
        return data.indexOf(textData) > -1;
      });
      setMaterials(setData);
    } else {
      setMaterials(copyMaterials);
    }
  }, [searchText]);

  const checkClicked = (item) => {
    if (item?.id_item == savedMaterial?.id_item) {
      setSavedMaterial({});
    } else {
      setSavedMaterial(item);
    }
  };
  const onSubmit = () => {
    saveMaterial(savedMaterial);
    setSavedMaterial({});
    setSearchText('');
    close();
  };
  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
    >
      <View style={{ paddingHorizontal: 20, paddingTop: 30 }}>
        <Text variant="bodyBold" size={20} style={styles.headerStyle}>
          Select New Material Request
        </Text>
        <View
          style={{
            backgroundColor: theme.color.primaryWhite,
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              borderWidth: 0.5,
              borderRadius: 10,
              paddingLeft: 20,
              marginVertical: 10,
              marginBottom: 20,
            }}
          >
            <TextInput
              style={styles.input}
              placeholder="Search Materials"
              onChangeText={(txt) => setSearchText(txt)}
              underlineColorAndroid="transparent"
            />
            <View
              style={[
                styles.searchIconSty,
                { backgroundColor: theme.color.primary },
              ]}
            >
              <SearchIcon />
            </View>
          </View>
        </View>
      </View>
      <ScrollView style={styles.scroolStyle}>
        {materialListsloading && (
          <View
            style={{
              // height: 120,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <ActivityIndicator size="small" color={theme.color.primary} />
          </View>
        )}
        {materials?.length > 0 ? (
          materials?.map((item, index, arr) => {
            return (
              <View
                key={item.id}
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingBottom: arr?.length - 1 === index ? 130 : 15,
                  paddingLeft: 10,
                  alignItems: 'flex-start',
                }}
              >
                <View>
                  <CheckBox
                    checked={item?.id_item === savedMaterial?.id_item}
                    onPress={() => checkClicked(item)}
                  />
                </View>
                <View style={{ flex: 1, paddingLeft: 10 }}>
                  <Text>{item?.item_request}</Text>
                </View>
                <View style={{ borderBottomWidth: 1 }} />
              </View>
            );
          })
        ) : (
          <View style={{ height: 100 }}>
            <Text>No Data</Text>
          </View>
        )}
      </ScrollView>
      <View style={styles.footer}>
        <Button
          title="Finish"
          type="primary"
          titleStyle={{ ...getAppFont('Regular') }}
          style={styles.button1}
          onPress={onSubmit}
        />
      </View>
    </ReactNativeModal>
  );
};

export default AddedMaterialModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 20,
    marginTop: 50,
  },
  mt12: {
    marginTop: 12,
    marginBottom: 9,
  },
  headerStyle: {
    marginBottom: 10,
    paddingTop: 5,
  },
  button1: {
    marginBottom: 20,
    borderRadius: 10,
  },
  scroolStyle: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 20,
    // paddingBottom: 40,
  },
  input: {
    flex: 1,
    // paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    ...getAppFont('Regular'),
  },
  searchIconSty: {
    padding: 10,
    marginHorizontal: 4,
    marginVertical: 2,
    borderRadius: 12,
  },
  footer: {
    position: 'absolute',
    left: 20,
    right: 20,
    bottom: 0,
    backgroundColor: 'white',
    paddingTop: 5,
  },
});
