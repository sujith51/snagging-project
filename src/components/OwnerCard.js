/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable camelcase */
// @flow
import * as React from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  ImageBackground,
  Text as RNText,
  Platform,
} from 'react-native';
import moment from 'moment';
import {useNavigation} from '@react-navigation/native';
import {LocationIcon} from '_svg';
import {useTheme, getAppFont, config} from '../utils';
import Card from './Card';
import Text from './Text';
import Button from './Button';

import {monthDayYear, numberFormat, getTime} from '../utils/theme';

type Props = {
  data: Array,
  headerRight: string | number,
  headerLeft: string | number,
};

const OwnerCard = ({
  id_ins_booking,
  unit_name,
  reference_number,
  sd_amount,
  inspection_type,
  booking_date,
  quotation_status,
  start_time,
  index,
  onPress = () => {},
  detailsPage = null,
  cardStyle = null,
  precinct_name = null,
  ou_city = null,
  sd_with_whom = null,
  id_service_req = null,
  mapTxtPress = () => {},
  inspectionHistroyPress = () => {},
  callNext = () => {},
  calendar = false,
  ins_status,
  inspection_status,
  latitude,
  longitude,
  key_handover = null,
  quotation_date,
  quotation_code,
  service_type,
  type,
  id_snag_property,
  created_on,
  unit_key = null,
}) => {
  const theme = useTheme();
  const {navigate} = useNavigation();
  // const { data, headerRight, headerLeft } = props;
  return (
    <>
      <Card cardStyle={[cardStyle]}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: Platform.isPad ? 20 : 0,
            }}>
            {calendar && (
              <View
                style={{
                  width: 85,
                  backgroundColor: theme.color.primary,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text variant="calDay" color="primaryWhite">
                  {booking_date ? moment(booking_date).format('ddd') : ''}
                </Text>
                <Text variant="calMonNum" color="primaryWhite">
                  {booking_date ? moment(booking_date).format('DD') : ''}
                </Text>
              </View>
            )}

            <View style={[{flex: 1}, calendar && {paddingLeft: 19}]}>
              <View
                style={[
                  styles.row,
                  styles.spacing,
                  {
                    paddingBottom: 11,
                    borderBottomColor: theme.color.secondaryLight2,
                    marginBottom: theme.spacing.m,
                    paddingTop: 11,
                  },
                ]}>
                <View style={styles.left}>
                  <Text variant="bodyDark">{reference_number}</Text>
                </View>
                <View style={styles.right}>
                  <Text variant="bodyDark">{unit_name}</Text>
                </View>
              </View>

              <View style={[styles.row, {paddingBottom: theme.spacing.sm}]}>
                <View style={styles.left}>
                  <Text variant="bodyLight">Inspection Date</Text>
                </View>
                <View style={styles.right}>
                  <Text variant="textMedium" style={styles.rightText}>
                    {created_on
                      ? monthDayYear(created_on)
                      : 'Not Specified '}
                  </Text>
                </View>
              </View>
              <View style={[styles.row, {paddingBottom: theme.spacing.sm}]}>
                <View style={styles.left}>
                  <Text variant="bodyLight">Inspection Time</Text>
                </View>
                <View style={styles.right}>
                  <Text variant="textMedium" style={styles.rightText}>
                    {created_on ? getTime(created_on) : 'Not Specified '}
                  </Text>
                </View>
              </View>

              {/* <View style={[styles.row, { paddingBottom: theme.spacing.sm }]}>
              <View style={styles.left}>
                <Text variant="bodyLight">Time</Text>
              </View>
              <View style={styles.right}>
                <Text variant="textMedium" style={styles.rightText}>
                  {created?moment(created, ['HH:mm']).format('hh:mm A'):''}
                </Text>
              </View>
            </View> */}

              {/* <View style={[styles.row, { paddingBottom: theme.spacing.sm }]}>
              <View style={styles.left}>
                <Text variant="bodyLight">SD Amount</Text>
              </View>
              <View style={styles.right}>
                <Text variant="textMedium" style={styles.rightText}>
                  {numberFormat(sd_amount)}
                </Text>
              </View>
            </View> */}

              {/* 
            <View style={[styles.row, { paddingBottom: theme.spacing.sm }]}>
              <View style={styles.left}>
                <Text variant="bodyLight">Service Request</Text>
              </View>
              <View style={styles.right}>
                <Text variant="textMedium" style={styles.rightText}>
                  {id_service_req || 'N/A'}
                </Text>
              </View>
            </View> */}

              <View style={[styles.row, {paddingBottom: theme.spacing.sm}]}>
                <View style={styles.left}>
                  <Text variant="bodyLight">Status</Text>
                </View>
                <View style={styles.right}>
                  <Text variant="textMedium" style={styles.rightText}>
                    {service_type>='3' ? 'Completed ' : 'Pending '}
                  </Text>
                </View>
              </View>

              <View style={[styles.row, {paddingBottom: theme.spacing.m - 4}]}>
                <View style={styles.left}>
                  <Text variant="bodyLight">Type </Text>
                </View>
                <View style={styles.right}>
                  <Text variant="textMedium" style={styles.rightText} >
                    Owner Snagging Inspection
                  </Text>
                </View>
              </View>
              {calendar && !key_handover == 1 && (
                <View
                  style={[styles.row, {paddingBottom: theme.spacing.m - 4}]}>
                  <View style={styles.left}>
                    <Text variant="bodyLight">Key Handover status</Text>
                  </View>
                  <View style={styles.right}>
                    <Text variant="textMedium" style={styles.rightText}>
                      Pending
                    </Text>
                  </View>
                </View>
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
        {detailsPage && (
          <View>
            <View style={styles.locationStyle}>
              <LocationIcon />
              <Text variant="bodyDark" style={styles.locationTxt}>
                {unit_name}
              </Text>
            </View>
            <View style={styles.locationBackGround}>
              <ImageBackground
                source={require('../assets/icons/google-map.jpg')}
                style={styles.image}>
                <View style={styles.center}>
                  <Text
                    style={styles.mapTxt}
                    onPress={() => mapTxtPress(latitude, longitude)}>
                    Click here to view in Map
                  </Text>
                </View>
              </ImageBackground>
            </View>
            <View style={styles.ChkInspHistoryView}>
              <Text
                variant="bodyDark"
                style={styles.ChkInspHistory}
                onPress={inspectionHistroyPress}
                color={theme.color.secondary}>
                Check Inspection History
              </Text>
            </View>
          </View>
        )}
      </Card>

      {detailsPage && inspection_status == 3 && (
        <View style={styles.alinCenter}>
          <Button
            type="primary"
            title={'Next'}
            // style={styles.buttonContainerNormal}
            style={styles.button1}
            onPress={callNext}
          />
        </View>
      )}
    </>
  );
};

export default OwnerCard;

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // flexWrap: 'wrap',
  },
  left: {
    flex: 0.45,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  button1: {
    width: config.DEVICE_WINDOW_WIDTH - 200,
    marginRight: Platform.isPad ? 15 : 5,
    padding: Platform.isPad ? 20 : 10,
    borderRadius: 10,
  },
  right: {
    flex: 0.55,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  rightText: {
    textAlign: 'right',
  },
  spacing: {
    borderBottomWidth: 1,
  },
  locationStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  locationTxt: {
    left: 5,
    ...getAppFont('Regular'),
  },
  alinCenter: {alignItems: 'center', paddingTop: 40},
  locationBackGround: {
    alignItems: 'center',
    paddingTop: 20,
  },
  buttonTxtNormal: {
    ...getAppFont('Bold'),
  },
  ChkInspHistory: {
    textDecorationLine: 'underline',
    ...getAppFont('Regular'),
    paddingTop: 10,
  },
  buttonContainerNormal: {
    // height: 35,
    width: 350,
    borderRadius: 10,
    padding: Platform.isPad ? 20 : 5,
  },
  ChkInspHistoryView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    height: 100,
    width: 300,
    // opacity: 1,
    color: 'rgba(0,0,0,0.5)',
  },
  mapTxt: {
    justifyContent: 'center',
    color: 'white',
    textDecorationLine: 'underline',
    ...getAppFont('Bold'),
  },
  center: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
    justifyContent: 'center',
  },
  reviewText: {
    textAlign: 'right',
    // textDecorationLine: 'underline',
    // ...getAppFont('Bold'),
    // fontSize: 16,
    // borderBottomWidth: 1,
    textDecorationLine: 'underline',
    fontStyle: 'italic',
    fontSize: 18,
    // backgroundColor: 'lightgray',
    padding: 5,
    // borderRadius: 10,
  },
});
