/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';

import ReactNativeModal from 'react-native-modal';
import moment from 'moment';
import {
    View,
    StyleSheet,
    FlatList,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    Platform,ScrollView,TouchableOpacity
} from 'react-native';
import {
    Modal,DropDown,Text
} from '_components';
import { useTheme, getAppFont } from '../utils';
const hours=[0,1,2,3,4,5,6,7,8,9,10,11];
const TimeModal = ({ open, close,calendar ,title,current,changeDate}) => {
    const theme = useTheme();
    const dayClicked = (date) => {
        const newDate=date.dateString;
        changeDate(newDate);
        close();
    };
    const numbers=(max)=>{
        let arr=[]
        for(let i=0;i<=max;i++){
            arr.push(i)
        }
        return arr
    }
    return (
        <Modal
            open={open}
            close={close}
            title={title}
            body={
                <>
                <View style={{flexDirection:'row',paddingHorizontal:10}}>
                    <View style={{flex:1,marginRight:5}}>
                        <Text style={styles.pb5} variant="taskBold">
                            Select Hours
                        </Text>
                        <DropDown
                            options={numbers(11).map((item) => ({
                                label: item,
                                value: item,
                            }))}
                            placeHolderStyle={{
                                color: theme.color.primaryWhite,
                                ...getAppFont('Bold'),
                            }}
                            drDownMdlwidth
                            contStyle={[
                                styles.dropDownContain,
                                { backgroundColor: theme.color.secondaryDark3 },
                            ]}
                            iconColor={theme.color.primaryWhite}
                            // onClose={(itm) => setCommunity(itm)}
                        />
                    </View>
                    <View style={{flex:1}}>
                        <Text style={styles.pb5} variant="taskBold">
                            Select Mins
                        </Text>
                        <DropDown
                            options={numbers(59).map((item) => ({
                                label: item,
                                value: item,
                            }))}
                            placeHolderStyle={{
                                color: theme.color.primaryWhite,
                                ...getAppFont('Bold'),
                            }}
                            drDownMdlwidth
                            contStyle={[
                                styles.dropDownContain,
                                { backgroundColor: theme.color.secondaryDark3 },
                            ]}
                            iconColor={theme.color.primaryWhite}
                            // onClose={(itm) => setCommunity(itm)}
                        />
                    </View>
                </View>

                </>
            }
        />
    );
};

export default TimeModal;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 0,
    },
    dropDownContain: {
        justifyContent: 'space-between',
        left: 0,
        height: 46,
        borderRadius: 10,
    },
});
