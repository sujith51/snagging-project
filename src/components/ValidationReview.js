/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import Text from './Text';
import {useTheme, getAppFont} from '../utils';
import Button from './Button';

const ValidationReview = ({open, close, title, data}) => {
  const theme = useTheme();
  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={styles.container}
      animationIn="zoomIn"
      animationOut="zoomOut">
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: theme.color.primaryWhite,
          padding: 20,
          borderRadius: 10,
        }}>
        <View style={styles.viewStyle}>
          <Text
            variant="bodyDark"
            color={theme.color.red}
            size={16}
            style={{textAlign: 'center', paddingBottom: 20}}>
            {title}
          </Text>
        </View>
        <View>
          {data?.map(itm => {
            return (
              <>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    padding: 10,
                  }}
                  key={itm}>
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 5,
                      backgroundColor: theme.color.primaryBlack,
                    }}
                  />
                  <Text
                    variant="textRegular"
                    color="secondary"
                    size={16}
                    style={{bottom: 5, paddingLeft: 8}}>
                    {itm}
                  </Text>
                </View>
              </>
            );
          })}
        </View>
        <View style={styles.subBtn}>
          <Button
            title="Close"
            type="primary"
            style={styles.button1}
            titleStyle={{...getAppFont('Bold')}}
            onPress={close}
          />
        </View>
        {/* <Button
                  
                    title="Close "
                    type="primary"
                    style={styles.button1}
                    titleStyle={{ ...getAppFont('Bold') }}
                    onPress={close}
                  /> */}
        {/* <Button
          onPress={close}
          title="Close"
          style={styles.button1}
          type="primary"
          titleStyle={{ ...getAppFont('Bold') }}
        /> */}
        {/* <Button
          title="Close"
          style={
            styles.button1
          }
          titleStyle={{ color: 'red' }}
          onPress={close}
        /> */}
      </View>
    </ReactNativeModal>
  );
};

export default ValidationReview;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // minHeight: 340,
    padding: 10,
    marginHorizontal: 25,
    borderRadius: 10,
    // alignItems: 'center',
    // height: 56,
    // justifyContent: 'space-between',
  },
  viewStyle: {
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
  },
  subBtn: {
    // paddingBottom: 50,
    // alignItems: 'center',
    height: 50,
  },
  button1: {
    width: 100,
    marginRight: Platform.isPad ? 15 : 5,
    // padding: Platform.isPad ? 20 : 10,
    borderRadius: 10,
    // height: 100,
  },
});
