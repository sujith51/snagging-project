/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';

import { Delete } from '_svg';
import { check } from 'prettier';
import { useTheme } from '../utils';
import { getAppFont, monthDayYear } from '../utils/theme';

import Date from '../assets/svg_components/Date';
import Text from './Text';
import Button from './Button';
import CheckBox from './ui-elements/CheckBox';
import DropDownVendor from './DropDownVendor';
import CalendarModal from './CalendarModal';

const RequestMaterailListModal = ({
  open,
  close,
  data = [],
  openModal,
  deleteResult,
  openSuccess,
  vendorData,
  alreadyAddedData,
  saveNewMaterial,
  openList,
}) => {
  const theme = useTheme();
  const [calendar, setCalendar] = useState(false);
  const [calendarDate, setCalendarDate] = useState(null);
  const [isVendor, setIsVendor] = useState(false);
  const [localData, setLocalData] = useState(data);
  // const materialSelected = avlData?.map((da) => da);

  useEffect(() => {
    const timer = setTimeout(
      () => (isVendor ? setIsVendor(false) : null),
      2000,
    );
    return () => clearTimeout(timer);
  }, [isVendor]);

  const handleCalender = (date) => {
    setCalendar((prev) => !prev);
    setCalendarDate(date);
  };

  const handleVendorValid = () => {
    const isVendorSlected = vendorData.some((a) => a.checked);
    if (isVendorSlected) {
      setIsVendor(false);
      openSuccess();
    } else {
      setIsVendor(true);
    }
  };
  const toggleCheck = (index) => {
    const copy = [...localData];
    const backup = copy.map((data, i) => {
      return index == i ? { ...data, checked: !data.checked } : data;
    });
    setLocalData(backup);
  };
  const submit = () => {
    const checked = localData.filter((val) => val.checked);
    const final = [...checked, ...alreadyAddedData];
    saveNewMaterial(final);
    openList();
  };
  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
    >
      <ScrollView style={styles.scroolStyle}>
        <CalendarModal open={calendar} close={(date) => handleCalender(date)} />
        <Text variant="bodyBold" size={21} style={styles.headerStyle}>
          Added Material List
        </Text>
        <View>
          <Text color="primary" variant="bodyBold" style={{ paddingTop: 10 }}>
            Material not in the rate card
          </Text>
          {localData.length === 0 && (
            <Text size={12} style={{ paddingTop: 10 }}>
              Newly added materials will be shown here
            </Text>
          )}
          {localData?.map((value, index) => {
            return (
              <View key={`required-${value.item}`}>
                <View style={styles.header}>
                  <View style={{ flexDirection: 'row' }}>
                    <CheckBox
                      onPress={() => toggleCheck(index)}
                      checked={value.checked}
                    />
                    <View style={{ marginLeft: 5 }}>
                      <Text variant="bodyBold" size={15} style={styles.mt12}>
                        Material {index + 1}
                      </Text>
                    </View>
                  </View>
                  <TouchableWithoutFeedback onPress={() => deleteResult(index)}>
                    <Delete />
                  </TouchableWithoutFeedback>
                </View>
                <View
                  style={[
                    styles.card,
                    { borderColor: theme.color.primaryBouquet },
                  ]}
                >
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Item Name
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.item}
                    </Text>
                  </View>
                  {/* <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Make
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.make}
                    </Text>
                  </View> */}
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Specification
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.specification}
                    </Text>
                  </View>
                  {value.qtyOwner > 0 && (
                    <View style={styles.row}>
                      <Text variant="bodyLight" size={15}>
                        Owner Qty
                      </Text>
                      <Text variant="bodyBold" size={15}>
                        {value.qtyOwner}
                      </Text>
                    </View>
                  )}

                  {value.qtyTenant > 0 && (
                    <View style={styles.row}>
                      <Text variant="bodyLight" size={15}>
                        Tenant Qty
                      </Text>
                      <Text variant="bodyBold" size={15}>
                        {value.qtyTenant}
                      </Text>
                    </View>
                  )}
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Total Qty
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.quantity}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Description
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      {value.description}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text variant="bodyLight" size={15}>
                      Rate Card
                    </Text>
                    <Text variant="bodyBold" size={15}>
                      Unavailable
                    </Text>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
        <Button
          title="Select Material List"
          type="primary"
          titleStyle={{ ...getAppFont('Regular') }}
          style={styles.button1}
          onPress={submit}
        />
      </ScrollView>
    </ReactNativeModal>
  );
};

export default RequestMaterailListModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
    borderRadius: 20,
  },
  card: {
    borderWidth: 1,
    paddingHorizontal: 15,
    marginBottom: 10,
    paddingVertical: 10,
    // flexWrap: 'wrap',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    // backgroundColor:'red'
    flexWrap: 'wrap',
  },
  header: {
    flexDirection: 'row',
    paddingVertical: 10,
    justifyContent: 'space-between',
    paddingTop: 20,
  },
  button1: {
    marginTop: 20,
    borderRadius: 10,
    marginBottom: 30,
  },
  scroolStyle: {
    flex: 1,
    marginTop: 50,
    paddingHorizontal: 20,
    // marginBottom: 70,
  },
  headerStyle: {
    paddingTop: 5,
  },
});
