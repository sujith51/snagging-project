/* eslint-disable react-native/no-color-literals */
/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  createRef,
  useRef,
  useLayoutEffect,
  useCallback,
  useEffect,
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Platform,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { FlatList } from 'react-native-gesture-handler';
import { Transition, Transitioning } from 'react-native-reanimated';
import CheckBox from './CheckBox';
import { useTheme, config } from '../../utils';
import { getAppFont } from '../../utils/theme';
import { DropDownIcon, RadioChecked } from '../../assets/svg_components';

const maxHeight = config.DEVICE_WINDOW_HEIGHT - 300;

const DropDownMultiple = ({
  value = [],
  options,
  placeholder = 'Select',
  onOpen = () => {},
  onClose = () => {},
  placeHolderStyle = null,
  count = null,
  iconColor = null,
  labelContStyle = {},
  labelStyle = {},
  contStyle = {},
  drDownMdlwidth = null,
}) => {
  const theme = useTheme();
  // :States
  const [open, setOpen] = useState(false);
  const dropdownRef = useRef(createRef());
  const [dropdownDim, setDropDownDim] = useState(null);
  const [selected, setSelected] = useState(null);

  // :Effects
  useLayoutEffect(() => {
    dropdownRef.current.measureInWindow((x, y, width, height) => {
      if (!dropdownDim || dropdownDim.x !== x || dropdownDim.y !== y) {
        setDropDownDim({ x, y, width, height });
      }
    });
  });

  // :Functions
  const setAnimatedOpenFn = useCallback(
    (opened) => {
      if (opened) {
        setOpen(true);
      } else {
        setOpen(false);
      }
    },
    [setOpen],
  );

  const onLabelClick = () => {
    setAnimatedOpenFn(true);
    onOpen();
  };

  const requestClose = useCallback(() => {
    setAnimatedOpenFn(false);
    // onClose(selected);
  }, [setAnimatedOpenFn]);

  const onSelect = useCallback(
    (item) => {
      // setAnimatedOpenFn(false);
      setSelected(item);
      onClose(item);
    },
    [onClose, setAnimatedOpenFn],
  );
  const returnCommunity=()=>{
   let datas= value?.filter((val) => val?.value != 'All')
   let count;
   const length=datas?.length;
   console.log('length',length)
   if(length==1){
    count=datas?.filter(val=>val?.value)[0]?.label;
    

   }else{
    count=`${length} Communities Selected`
   }
   return count;
  }
  console.log({value})
  // :Render
  return (
    /*  */
    <>
      <View>
        <TouchableWithoutFeedback onPress={() => onLabelClick()}>
          <View
            style={[styles.container, contStyle]}
            ref={(el) => (dropdownRef.current = el)}
            collapsable={false}
          >
            <View>
              <Text style={[styles.label, placeHolderStyle]}>
               {returnCommunity()}
              </Text>
            </View>
            <View style={!drDownMdlwidth && { left: 5 }}>
              <DropDownIcon
                color={iconColor || theme.color.secondaryDark}
                width={12}
                height={10}
                top={3}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
      <ReactNativeModal
        isVisible={open}
        onBackdropPress={requestClose}
        onBackButtonPress={requestClose}
        backdropColor="transparent"
        style={{ padding: 0, margin: 0 }}
      >
        <View
          style={[
            styles.dropdownContainer,
            {
              top: dropdownDim?.y + dropdownDim?.height,
              left: dropdownDim?.x,
              width: drDownMdlwidth ? dropdownDim?.width : 'auto',
              backgroundColor: theme.color.primaryWhite,
              ...theme.cardShadow,
            },
          ]}
        >
          <FlatList
            // contentContainerStyle={!open && { height: 0 }}
            style={{ maxHeight }}
            data={options}
            keyExtractor={(item, index) => `dropDown-key-${index}`}
            renderItem={({ item }) => {
              const isExist = value.some((val) => val.value == item.value);

              return (
                <View>
                  <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                    <View
                      style={[
                        {
                          padding: 10,
                          paddingHorizontal: 12,
                          flexDirection: 'row',
                          alignItems: 'center',
                        },
                        labelContStyle,
                      ]}
                    >
                      <CheckBox
                        checked={isExist}
                        onPress={() => onSelect(item)}
                      />
                      <Text style={[styles.dropdownTxtStyle, { labelStyle }]}>
                        {item.label}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              );
            }}
          />
        </View>
      </ReactNativeModal>
    </>
  );
};

export default React.memo(DropDownMultiple);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 7,
    paddingHorizontal: 12,
    // borderWidth: 1.5,
    borderRadius: 6,
    alignItems: 'center',
    // justifyContent: 'space-between',
    // flex: 1,
    right: 20,
  },
  label: {
    textAlign: 'center',
    marginTop: 3,
    fontSize: Platform.isPad ? 20 : 14,
  },
  dropdownTxtStyle: {
    ...getAppFont('Medium'),
    fontSize: Platform.isPad ? 20 : 14,
    paddingLeft: 10,
    // paddingBottom: 5,
  },
  dropdownContainer: {
    position: 'absolute',
    top: 0,
    // left: 0,
    // backgroundColor: theme.colors.white,
    // ...theme.cardShadow,
    paddingBottom: 7,
    borderRadius: 5,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageStyle: {
    height: 30,
    width: 40,
    marginRight: 10,
  },
});
