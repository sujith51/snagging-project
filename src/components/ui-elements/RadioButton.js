/* eslint-disable react-native/no-color-literals */
import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text } from 'react-native';
import { RadioChecked } from '../../assets/svg_components';
import { useTheme, getAppFont } from '../../utils';

const RadioButton = ({
  checked = false,
  leftText = null,
  rigthText = null,
  textStyle = null,
  onPress = () => {},
}) => {
 
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.rowStyle}>
        <RadioChecked checked={checked} />
    </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  square: {
    width: 20,
    height: 20,
    borderWidth: 2,
    alignItems: 'center',
  },
  rowStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStyle: {
    ...getAppFont('Regular'),
    left: 6,
  },
});

export default RadioButton;


