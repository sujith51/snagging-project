import React, { useState, useRef, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Slider,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Modal from 'react-native-modal';
import { Camera } from 'expo-camera';
import { endpoints, getFullUrlPM, services, theme } from '../../utils';
import { Button } from '_components';
const { width } = Dimensions.get('window');
import * as FileSystem from 'expo-file-system';
const { height } = Dimensions.get('screen');
const CameraModal = ({
  isModalVisible = false,
  handleBarCodeScanned,
  onClose,
  passImageToParent,
  preImages = [],
  limit,
  statusPerm
}) => {
  let cameraRef = useRef(Camera);
  let camera;

  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  // const [type, setType] = useState(Camera.Constants.Type.front);
  const [capturedImage, setCapturedImage] = useState(null);
  const [startOver, setStartOver] = useState(true);
  const [cameraImages, setCameraImages] = useState([]);
  const [loading, setLoading] = useState(false);

  // console.log("Pre Images+++++ in camera +++ ", preImages)


  console.log("statusPerm in camera +++ ", statusPerm)


  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, [statusPerm]);

  //     useEffect(()=>{
  //         if(isModalVisible){
  // console.log('inside useeffect')
  //         }
  //     },[isModalVisible])
  const __takePicture = async () => {
    if (!camera) return;
    const options = { base64: true, quality: 0.5 };
    const photo = await camera.takePictureAsync(options);
    // passImageToParent(photo);
    setLoading(true);
    const base64Img = await FileSystem.readAsStringAsync(photo.uri, {
      encoding: FileSystem.EncodingType.Base64,
    });

    const data = {
      image: base64Img,
    };

    services
      .post(getFullUrlPM(endpoints.uploadBase64Image), data)
      .then((res) => {
        setLoading(false);
        if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
          const obj = {
            id: cameraImages ? cameraImages.length + 1 : 1,
            uri: res?.data?.image_url,
          };
          let final = cameraImages.concat(obj);
          setCameraImages(final);
          // console.log("Respone++++ ", res)
        } else {
          alert('Something went wrong!!');
        }
      })
      .catch((err) => {
        setLoading(false);
      });
  };

  const __closeCamera = () => {
    setStartOver(true);
  };
  if (hasPermission === null) {
    return <View />;
  }

  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }
  // onClose();
  // };
  // const __closeCamera = () => {
  //   setStartOver(true);
  // };
  // if (hasPermission === null) {
  //   return <View />;
  // }
  // if (hasPermission === false) {
  //   return <Text>No access to camera</Text>;
  // }

  const deleteImg = (id) => {
    let final = cameraImages.filter((item) => item.id !== id);
    let finalIndex = final.map((item, key) => {
      return {
        ...item,
        id: key + 1,
      };
    });
    setCameraImages(finalIndex);
  };

  return (
    <Modal
      isVisible={isModalVisible}
      useNativeDriver={true}
      style={{ margin: 0, flex: 1 }}
      onBackdropPress={onClose}
      backdropColor={theme.color.darkGray}
      backdropOpacity={0.8}
    >
      <View style={{ justifyContent: 'flex-end', flex: 1 }}>
        <Camera
          style={{ flex: 1 }}
          type={type}
          ref={(r) => {
            camera = r;
          }}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}
          >
            <View
              style={{
                position: 'absolute',
                top: '5%',
                right: '5%',
              }}
            >
              <TouchableOpacity onPress={onClose}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 20,
                    padding: 10,
                  }}
                >
                  X
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                position: 'absolute',
                bottom: 0,
                flexDirection: 'row',
                flex: 1,
                width: '100%',
                padding: 20,
                justifyContent: 'space-between',
              }}
            >
              <View
                style={{
                  alignSelf: 'center',
                  flex: 1,
                  alignItems: 'center',
                }}
              >
                {loading ? (
                  <View
                    style={{
                      // height: 120,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <ActivityIndicator size="large" color={theme.color.green} />
                  </View>
                ) : (
                  cameraImages?.length < limit && (
                    <TouchableOpacity
                      onPress={__takePicture}
                      style={{
                        width: 70,
                        height: 70,
                        bottom: 0,
                        borderRadius: 50,
                        backgroundColor: '#fff',
                      }}
                    />
                  )
                )}
              </View>
            </View>

            <View
              style={{
                flex: 1,
                position: 'absolute',
                top: '5%',
                left: 0,
                width: 110,
              }}
            >
              {/* <Text style={{
                    color: '#fff',
                    fontSize: 20,
                    padding: 10,
                  }}>Hello</Text> */}
              <ScrollView
                style={{ flex: 1, height: height - 230 }}
                persistentScrollbar
              >
                {cameraImages.map((item, key) => (
                  <View key={key} style={{ width: 60, height: 60, margin: 5 }}>
                    <Image
                      source={{ uri: item.uri }}
                      style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: 4,
                        borderWidth: 2,
                        borderColor: theme.color.primaryWhite,
                      }}
                    />
                    <TouchableOpacity
                      style={{ position: 'absolute', right: 7, top: -2 }}
                      onPress={() => deleteImg(item.id)}
                    >
                      <Text style={{ color: 'white', fontSize: 19 }}>x</Text>
                    </TouchableOpacity>
                  </View>
                ))}
              </ScrollView>
            </View>
          </View>
        </Camera>
        <View
          style={{
            backgroundColor: 'white',
            paddingTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-around',
           
            
          }}
        >
          <View style={{ width: 150 , height:50,}}>
            <Button
              disabled={loading}
              active={true}
              title={'FLIP CAMERA'}
              parentStyle={{
                marginBottom: 10,
                width: '70%',
                paddingHorizontal: 20,
                alignSelf: 'center',
              }}
              onPress={() => {
                setType(
                  type === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back,
                );
              }}
            />
          </View>
          <View style={{ width: 150, height:50, }}>
            <Button
              type="blue"
              disabled={cameraImages?.length === 0 ? true : false || loading}
              active={true}
              title={'SUBMIT'}
              parentStyle={{
                marginBottom: 10,
                width: '70%',
                paddingHorizontal: 20,
                alignSelf: 'center',
              }}
              onPress={() => {
                passImageToParent(cameraImages);
                onClose();
              }}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default CameraModal;

const styles = StyleSheet.create({});
