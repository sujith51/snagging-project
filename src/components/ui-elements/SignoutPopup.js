/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import Modal from 'react-native-modal';
// import { Button } from '_components';
import { normalizeUnits } from 'moment';
import { getAppFont, useTheme } from '../../utils';
import Button from '../Button';

const SignoutPopup = ({
  visible = false,
  title = '',
  desc = '',
  btnTitle1 = 'YES',
  btnTitle2 = 'NO',
  yes,
  close,
  deletePopup = false,
}) => {
  const theme = useTheme();

  return (
    <View>
      <Modal
        visible={visible}
        animationType="slide"
        transparent
        onRequestClose={close}
        onBackdropPress={close}
      >
        <View style={styles.container}>
          <View
            style={[
              styles.subCont,
              { backgroundColor: theme.color.primaryWhite },
              deletePopup && {
                borderRadius: 12,
                backgroundColor: theme.color.primaryLight,
              },
            ]}
          >
            <View style={styles.titleCont}>
              <Text style={[styles.titleTxt, { color: theme.color.primary }]}>
                {title}
              </Text>
            </View>

            <View style={styles.descCont}>
              <Text style={[styles.descTxt, { color: theme.color.primary }]}>
                {desc}
              </Text>
            </View>

            <View style={styles.btnCont}>
              <View style={{ height: Platform.isPad ? 75 :  50, marginRight: 24 }}>
                <Button
                  title={btnTitle1}
                  type="primary"
                  style={[
                    { width: Platform.isPad ? 150 : 125, height: 45 },
                    deletePopup && { borderRadius: 8 },
                  ]}
                  titleStyle={{ ...getAppFont('Bold') }}
                  onPress={() => yes()}
                />
              </View>
              <View style={{ height: Platform.isPad ? 75 :  50 }}>
                <Button
                  title={btnTitle2}
                  type="default"
                  style={[
                    { width: Platform.isPad ? 125 : 100, height: 45 },
                    deletePopup && { borderRadius: 8 },
                  ]}
                  titleStyle={{ ...getAppFont('Bold') }}
                  onPress={() => close()}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default SignoutPopup;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  subCont: {
    height: 260,
  },
  titleCont: {
    paddingTop: 39,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleTxt: {
    fontSize: 30,
    ...getAppFont('Bold'),
  },
  descCont: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 13,
  },
  descTxt: {
    fontSize: 16,
    ...getAppFont('Regular'),
  },
  btnCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 35,
  },
});
