
import React from 'react';
import { View, StyleSheet, ScrollView,TouchableOpacity } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { Modal, Text,CalendarComponent } from '_components';
import { useTheme, getAppFont,numberFormat } from '../../utils/index';
import Button from '../Button';

const CalendarModal = ({open, close, title='hi',calendar}) => {
    const theme = useTheme();
    return (
        <Modal
            open={open}
            close={close}
            title={title}
             body={<CalendarComponent {...calendar}/>}
        />
    );
};

export default CalendarModal;

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // minHeight: 340,
        // padding: 10,
        // marginHorizontal: 10,
        borderRadius: 10,
        // alignItems: 'center',
        height: 56,
        // justifyContent: 'space-between',
        marginTop: 30,
        // backgroundColor: 'red',
    },
    wrapper: {
        paddingBottom: 15,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        marginBottom: 10,
        borderWidth: 0.3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 3,
        paddingTop: 15,
    },
    viewStyle: {
        marginLeft: 10,
        marginRight: 10,
    },
    row:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingBottom:5
    }
});
