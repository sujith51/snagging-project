import React from 'react';
import { View, FlatList, Dimensions, StyleSheet, Animated } from 'react-native';
import { useTheme } from '../../utils';

const { width: screenWidth, height: screenHeight } = Dimensions.get('screen');

const Swiper = (props) => {
  let { data, index = 0 } = props;
  const theme = useTheme();
  const [active, setActive] = React.useState(0);
  const viewabilityConfig = React.useRef({
    viewAreaCoveragePercentThreshold: 50,
  });

  let oldIndex = React.useRef(0);

  const onViewableItemsChanged = React.useRef(({ viewableItems, changed }) => {
    setActive(viewableItems[0].index);
    props.onIndexChanged(viewableItems[0].index);
  });

  React.useEffect(() => {
    if (oldIndex !== index) {
      oldIndex = index;
    }
  }, []);
  React.useEffect(() => {
    if (oldIndex !== index) {
      oldIndex = index;
    }
  }, [index]);

  return (
    <View style={{ flex: 1 }}>
      <Animated.FlatList
        scrollToIndex={{ animated: true, index: oldIndex }}
        data={data}
        scrollEventThrottle={5}
        onScroll={props.onScroll}
        horizontal={true}
        snapToAlignment="center"
        decelerationRate={0.8}
        snapToInterval={screenWidth}
        showsHorizontalScrollIndicator={false}
        pagingEnabled={true}
        renderItem={({ item }) => (
          <View style={{ width: screenWidth }}>{item}</View>
        )}
        viewabilityConfig={viewabilityConfig.current}
        keyExtractor={(item, index) => 'swiper-key' + index}
        onViewableItemsChanged={onViewableItemsChanged.current}
      />
      <View style={styles.dotts}>
        {data.map((screen, index) => (
          <View
            key={`swiper-key${index}`}
            style={[
              styles.dot,
              { borderColor: theme.color.primaryWhite },
              active === index && { backgroundColor: theme.color.primaryWhite },
            ]}
          />
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  dotts: {
    bottom: -90,
    flexDirection: 'row',
    width: screenWidth,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dot: {
    width: 12,
    height: 12,
    borderRadius: 6,
    opacity: 1,
    marginHorizontal: 5,

    borderWidth: 1,
  },
});

export default Swiper;
