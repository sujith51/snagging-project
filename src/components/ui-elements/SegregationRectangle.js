import React, { useCallback, useState } from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
} from 'react-native';
import SegSmallRectangle from './SegSmallRectangle';
import { getAppFont, theme } from "../../utils";

const SegregationRectangle = ({
  selectedData,
  headerStyle,
  listingData = [],
  quotationLoad,
  headTextStyle,
  title = null,
}) => {
  const [toggle, toggleRadio] = useState();
  const onRadioHandle = useCallback((item) => {
    toggleRadio(item);
  });

  return (
    <SafeAreaView style={styles.flatListContainer}>
      <FlatList
        data={listingData[0] !== null ? listingData : []}
        keyExtractor={(item, index) => `list-key-${index}`}
        renderItem={({ item, index }) => (
          <SafeAreaView key={`item${index}`} style={styles.container}>
            <View style={[styles.subContainer, { paddingTop: headerStyle }]}>
              <View
                style={[styles.headTotalText, { paddingTop: headTextStyle }]}
              >
                <Text style={styles.headerText}>{title}</Text>
                {/* <Text style={styles.headerText}>{`#${1455}`}</Text> */}
                {/* item.id_property */}
              </View>
              <View style={[styles.vendorText, { paddingTop: headTextStyle }]}>
                {/* <Text style={styles.headerText}>{'Vendor'}</Text>
                <Text style={styles.headerText}> {'vendor1'}</Text> */}
                {/* item.vendor_name */}
              </View>
            </View>
            <View style={styles.borderLine} />
            <View style={{ paddingLeft: 20, paddingTop: 20 }}>
              <SegSmallRectangle
                childList={item[0]}
                // quotationLoad={quotationLoad}
                typeSelect={selectedData}
              />
              <View style={{ paddingRight: 30 }}>
                <View style={styles.headTotalText}>
                  <Text style={styles.textTotal}>{'Total Items'}</Text>
                  <Text style={styles.textTotal}>{item.count}</Text>
                </View>
                <View style={styles.headTotalText}>
                  <Text style={styles.textTotal}>{'Total Price'}</Text>
                  <Text
                    style={styles.textTotal}
                  >{`AED ${item.grand_total}`}</Text>
                </View>
              </View>
            </View>
          </SafeAreaView>
        )}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    borderRadius: 1,
    borderColor: '#B07E9C',
    borderWidth: 1,
  },
  flatListContainer: {
    flex: 1,
    // top:20
    // marginTop: StatusBar.currentHeight || 0,
  },
  subContainer: {
    paddingLeft: 20,
    paddingRight: 30,
    backgroundColor: theme.color.primary,
    // paddingTop:10,
  },

  borderLine: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },

  textTotal: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000000',
  },
  headTotalText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  vendorText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  headerText: {
    fontSize: 18,
    // fontWeight: 'bold',
    color: '#FFFFFF',
    ...getAppFont('Bold'),
  },
});

export default SegregationRectangle;
