/* eslint-disable react-native/no-color-literals */
/* eslint-disable react/no-array-index-key */
import React from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Platform } from 'react-native';
import { getAppFont, theme } from '../../utils';

const TabView = ({ tabs, selected = false, onTabChange = () => {} }) => {
  return (
    <View style={styles.tabsContainer}>
      {tabs.map((item, index) => {
        return (
          <Tab
            key={`top-tab-${index}`}
            title={item.name}
            onPress={() => onTabChange(item.value)}
            selected={item.value === selected}
            activeBarColor={item.activeBar}
            activeText={item.value === selected ? 'bold' : '200'}
            // bgColor={bgColor}
            // botBorder={botBorder}
          />
        );
      })}
    </View>
  );
};

export default TabView;

const Tab = ({
  onPress,
  title,
  selected = false,
  disabled = false,
  activeBarColor = 'primary',
  activeText = 'primaryBlack',
}) => {
  return (
    <TouchableWithoutFeedback
      disabled={disabled}
      onPress={onPress}
      style={styles.tabStyle}
    >
      <View
        style={[
          styles.tabInner,
          {
            // backgroundColor:bgColor,
            // borderBottomWidth: bgColor === 'white' ? 1 : 2,
          },
        ]}
      >
        <Text
          style={
            selected
              ? styles.tabTextSelected
              : [styles.tabText, { fontWeight: activeText }]
          }
        >
          {title}
        </Text>
        {selected && (
          <View
            style={[styles.activeBar, { backgroundColor: activeBarColor }]}
          />
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  tabsContainer: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
  },
  tabStyle: {
    flexGrow: 1,
  },
  tabInner: {
    flex: 1,
    alignItems: 'center',
    // paddingTop:80,
    borderBottomWidth: 1,
    borderBottomColor: '#B4B4B4',
  },
  tabText: {
    color: '#000000',
    fontSize: Platform.isPad ? 19 : 15,
    marginTop: 10,
    justifyContent: 'space-between',
    ...getAppFont('Regular'),
  },
  tabTextSelected: {
    color: '#000000',
    fontSize: Platform.isPad ? 19 : 15,
    textAlign: 'center',
    marginTop: 10,
    justifyContent: 'space-between',
    // fontWeight: 'bold',
    ...getAppFont('Bold'),
  },
  activeBar: {
    backgroundColor: theme.color.primary,
    height: 2,
    width: 185,
    marginTop: 10,
    // position: 'absolute',
    bottom: -1,
  },
});
