/* eslint-disable react-native/no-color-literals */
/* eslint-disable no-unused-vars */
import React from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Platform,
} from 'react-native';

const SmallRectangle = ({ childList, quotationLoad = false, typeSelect }) => {
  const listChild = childList ? Object.values(childList) : [];
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <FlatList
          data={listChild}
          keyExtractor={(item, index) => `child-key-${index}`}
          renderItem={({ item, index }) => (
            <>
              <Text style={styles.itemHeadText}>{`${
                `${index + 1}.` +
                ` ${item?.item_request ? item.item_request : '(empty)'}`
              }`}</Text>
              <View style={styles.renderText}>
                <View style={styles.itemMainHead}>
                  <Text style={styles.itemHeadText}>Quantity</Text>
                  <Text style={styles.itemText}>{item.item_quantity}</Text>
                </View>
                <View style={styles.itemMainHead}>
                  <Text style={styles.itemHeadText}>Issue Type</Text>
                  {item.task_type === '2' && (
                    <Text style={styles.itemText}>Minor</Text>
                  )}
                  {item.task_type === '1' && (
                    <Text style={styles.itemText}>Major</Text>
                  )}
                </View>
                <View style={styles.itemMainHead}>
                  <Text style={styles.itemHeadText}>Paid By</Text>
                  {item.charge_to === '3' ? (
                    <Text style={styles.itemText}>Owner</Text>
                  ) : item.charge_to === '4' ? (
                    <Text style={styles.itemText}>Tenant</Text>
                  ) : item.charge_to === '5' ? (
                    <Text style={styles.itemText}>Owner/Tenant</Text>
                  ) : (
                    <Text style={styles.itemText}>--</Text>
                  )}
                </View>
                {typeSelect == 3 ? (
                  <View style={styles.itemMainHead}>
                    <Text style={styles.itemHeadText}>Price</Text>
                    <Text style={styles.itemText}>
                      {' '}
                      {`AED ${item.charge_to}`}
                    </Text>
                  </View>
                ) : null}
              </View>
            </>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  renderText: {
    borderRadius: 1,
    borderColor: '#B07E9C',
    borderWidth: 1,
    padding: 10,
    marginVertical: 8,
    marginRight: 15,
    flexDirection: 'column',
    paddingLeft: 20,
  },
  itemHeadText: {
    fontSize: Platform.isPad ? 18 : 15,
    fontWeight: 'bold',
    color: '#000000',
  },
  itemText: {
    fontSize: Platform.isPad ? 19 : 15,
    fontWeight: 'normal',
    color: '#000000',
  },
  itemMainHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
});

export default SmallRectangle;
