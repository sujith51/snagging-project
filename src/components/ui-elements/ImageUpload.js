/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-unused-styles */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  Platform,
} from 'react-native';
// import * as ImagePicker from 'expo-image-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

// import { Button } from '_components';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as FileSystem from 'expo-file-system';
import { DeleteIcon, CameraIcon, Cancel } from '_svg';
import Modal from 'react-native-modal';
import { SignoutPopup } from '_components';
import {
  theme,
  getAppFont,
  services,
  endpoints,
  getFullUrlPM,
} from '../../utils';
import Button from '../Button';
import Spinner from './Spinner';
import CameraModal from './Camera';

const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;
const UPLOAD_FILE_SIZE = 5000000; // 5.1mb

const ImageUpload = ({
  preImages,
  onImagesUpdate,
  style = null,
  base64 = true,
  multiple = false,
  captureButtonTitle = 'Capture New',
  uploadButtonTitle = 'Choose a File',
  uploadEnable = false,
  renderImage = null,
  deletePopup = false,
  limit = 10,
  title='Upload'
}) => {
  // console.log("preImages   ++++ ", preImages)
  const [images, updateImages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [showImage, setShowImage] = useState(null);
  const [index, setIndex] = useState(0);
  const [triggerPicker, setTriggerPicker] = useState(false);
  const [statusPerm, setPermStatus] = useState(false);
  useEffect(() => {
    if (preImages) {
      updateImages([...images, ...preImages]);
    }
    if (preImages == '') {
      deleteImage(0);
    }
  }, []);

  // Remove if we get null values===
  useEffect(() => {
    if (preImages === null) {
      updateImages([]);
    }
  }, [preImages]);

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
      const result = await Permissions.getAsync(Permissions.CAMERA);
      if (permission.status !== 'granted') {
        const newPermission = await Permissions.askAsync(
          Permissions.CAMERA_ROLL,
        );
        if (newPermission.status === 'granted') {
          // its granted.
        }
      }
      if (result.status !== 'granted') {
        const newResult = await Permissions.askAsync(Permissions.CAMERA);
        if (newResult.status === 'granted') {
          // its granted.
        }
      }
    }
  };

  //   console.log("Images in upload docs++ ", images)

  useEffect(() => {
    onImagesUpdate(images.length === 0 ? null : images);
  }, [images]);

  const passImageToParent = async (result) => {
    // console.log("Images in ImageUpload+++ ", result)
    setTriggerPicker(false);
    const trim = [];
    result.map((item, key) => {
      trim.push(item.uri);
    });

    const updatedImages = [...images, ...trim];

    if (multiple) {
      onImagesUpdate(updatedImages);
    } else {
      onImagesUpdate(updatedImages[0]);
    }
    updateImages(updatedImages);
    // console.log("Images in ImageUpload+++ after trim ", trim)
    // console.log("Images in ImageUpload+++ after final ", trim)
    setTriggerPicker(false);

    // setTriggerPicker(false);
    // if (result.size > UPLOAD_FILE_SIZE) {
    //     Alert.alert(
    //         'File size is exceeding limit',
    //         `Please select file which is lesser than or equal to ${
    //         UPLOAD_FILE_SIZE / 1000000
    //             }MB`,
    //     );
    //     return;
    // }
    // const base64Img = await FileSystem.readAsStringAsync(result.uri, {
    //     encoding: FileSystem.EncodingType.Base64,
    // });
    // const data = {
    //     image: base64Img,
    // };
    // setTriggerPicker(false);
    // setLoading(true);
    // services
    //     .post(getFullUrlPM(endpoints.uploadBase64Image), data)
    //     .then((res) => {
    //         if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
    //             const updatedImages = [...images, res.data.image_url];
    //             if (multiple) {
    //                 onImagesUpdate(updatedImages);
    //             } else {
    //                 onImagesUpdate(updatedImages[0]);
    //             }
    //             updateImages(updatedImages);
    //             setLoading(false);
    //         } else {
    //             alert('Something went wrong!!');
    //             setLoading(false);
    //         }
    //     });
    // let updatedImages = [...images, base64 ? base64Img : result];
    // if (multiple) {
    //     onImagesUpdate(updatedImages);
    // } else {
    //     onImagesUpdate(updatedImages[0]);
    // }
    // updateImages(updatedImages);
  };

  const closeModal = () => {
    setShowImage(false);
  };
  const deleteImage = (index) => {
    // console.log("Index+++ ", index)
    const uImages = images.splice(0);
    uImages.splice(index, 1);
    updateImages(uImages);
    // console.log("Images for uuu==", uImages)
    // onImagesUpdate(images.length === 0 ? null : images); // Dont enable issue
  };

  const selectImage = async () => {
    if (Constants.platform.android) {
      // setCalled(false)
      const { canAskAgain } = await Permissions.getAsync(Permissions.CAMERA);
      console.log({canAskAgain})
      await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (!canAskAgain) {
        Alert.alert('please grant permission in setting and Try');
      }
    }
    await launchImageLibrary({
      mediaTypes: 'photo',
      includeBase64:true,
     
      quality: 0.5,
    },(res)=>{
      console.log(res)
      if(!res?.didCancel){
        let value=res?.assets?.length>0?res?.assets[0]:null
        console.log({value})
        if(value){
          if (value?.fileSize > UPLOAD_FILE_SIZE) {
            Alert.alert(
              'File size is exceeding limit',
              `Please select file which is lesser than or equal to ${
                UPLOAD_FILE_SIZE / 1000000
              }MB`,
            );
            return;
          }
          // const base64Img = await FileSystem.readAsStringAsync(value?.uri, {
          //   encoding: FileSystem.EncodingType.Base64,
          // });
          const data = {
            image: value?.base64,
          };
          setTriggerPicker(false);
          setLoading(true);
          services
            .post(getFullUrlPM(endpoints.uploadBase64Image), data)
            .then((res) => {
              if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
                const updatedImages = [...images, res.data.image_url];
                if (multiple) {
                  onImagesUpdate(updatedImages);
                } else {
                  onImagesUpdate(updatedImages[0]);
                }
                updateImages(updatedImages);
                setLoading(false);
              } else {
                alert('Something went wrong!!');
                setLoading(false);
              }
            });
        }
       
      }
    });
    
    if (!result.cancelled) {
      if (result.size > UPLOAD_FILE_SIZE) {
        Alert.alert(
          'File size is exceeding limit',
          `Please select file which is lesser than or equal to ${
            UPLOAD_FILE_SIZE / 1000000
          }MB`,
        );
        return;
      }
      
    } else {
      Alert.alert('Error,please try again');
    }

    //
    // const updatedImages = [...images, base64 ? base64Img : result];
    // if (multiple) {
    //   onImagesUpdate(updatedImages);
    // } else {
    //   onImagesUpdate(updatedImages[0]);
    // }
    // updateImages(updatedImages);
  };

  const captureImage = async () => {
    setShowImage(true);
    setPermStatus(!statusPerm)
    // if (Constants.platform.android) {
    //   const { canAskAgain } = await Permissions.getAsync(Permissions.CAMERA);
    //   await Permissions.askAsync(Permissions.CAMERA_ROLL);
    //   if (!canAskAgain) {
    //     Alert.alert('please grant permission in setting and Try');
    //   }
    // }
    // const result = await ImagePicker.launchCameraAsync({
    //   mediaTypes: ImagePicker.MediaTypeOptions.Images,
    //   allowsEditing: false,
    //   aspect: [4, 3],
    //   quality: 0.1,
    // });
    // // const lost=  await ImagePicker.getPendingResultAsync()
    //   // console.log('res',result,lost)
    //   if(!result.cancelled){
    //       const base64Img = await FileSystem.readAsStringAsync(result.uri, {
    //           encoding: FileSystem.EncodingType.Base64,
    //       });
    //       const data = {
    //           image: base64Img,
    //       };
    //       setTriggerPicker(false);
    //       setLoading(true);
    //       services
    //           .post(getFullUrlPM(endpoints.uploadBase64Image), data)
    //           .then((res) => {
    //               if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
    //                   const updatedImages = [...images, res.data.image_url];
    //                   if (multiple) {
    //                       onImagesUpdate(updatedImages);
    //                   } else {
    //                       onImagesUpdate(updatedImages[0]);
    //                   }
    //                   updateImages(updatedImages);
    //                   setLoading(false);
    //               } else {
    //                   alert('Something went wrong!!');
    //                   setLoading(false);
    //               }
    //           });
    //   }else{
    //       Alert.alert(
    //           'Error,please try again',
    //
    //       );
    //   }
  };

  const yesClicked = () => {
    deleteImage(index);
    setModalVisible(false);
  };
  const togglePickerModal = (show = null) => {
    if (getPermissionAsync()) {
      setTriggerPicker(show || !triggerPicker);
    } else {
      Alert.alert(
        'Please Allow Permissions',
        'Please allow all requested permissions to continue',
      );
    }
  };
  const imagePickerModal = (
    <Modal
      deviceHeight={screenHeight}
      deviceWidth={screenWidth}
      animationInTiming={300}
      animationOutTiming={300}
      backdropTransitionInTiming={300}
      backdropTransitionOutTiming={300}
      useNativeDriver
      animationIn="slideInUp"
      animationOut="slideOutDown"
      onSwipeComplete={() => setTriggerPicker(false)}
      onBackdropPress={() => setTriggerPicker(false)}
      swipeDirection="down"
      style={styles.popupWrapper}
      isVisible={triggerPicker}
    >
      <View>
        <View style={styles.popUpContainer}>
          <TouchableOpacity
            style={styles.photoTouch}
            onPress={() => captureImage()}
          >
            <View style={styles.photoView}>
              <Text style={styles.applyText}>Take a Photo</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.galleryview}>
            <TouchableOpacity
              style={styles.galleryTouch}
              onPress={() => selectImage()}
            >
              <Text style={styles.applyText}>Choose from Gallery</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.cancelView}>
          <TouchableOpacity
            style={styles.cancelTouch}
            onPress={() => setTriggerPicker(false)}
          >
            <Text style={styles.applyText}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
  return (
    <View style={[{ ...styles.uploadContainer }, style]}>
      {showImage && (
        <CameraModal
          preImages={preImages}
          isModalVisible={showImage}
          onClose={() => closeModal()}
          passImageToParent={passImageToParent}
          limit={limit}
          statusPerm={statusPerm}
        />
      )}
      {imagePickerModal}
      {renderImage?.length > 0 && renderImage?.length < 1 ? (
        <View style={{ backgroundColor: 'green', flexDirection: 'row' }}>
          {renderImage && (
            <Image
              // source={{ uri: `data:image/png;base64,${renderImage[0]}` }}
              source={{ uri: `${renderImage[0]}` }}
              style={styles.fullImageView}
            />
          )}
          <TouchableOpacity
            onPress={() => deleteImage(0)}
            // style={styles.crossBtnView}
            style={{ paddingLeft: 0.3, paddingRight: 0.3 }}
          >
            <DeleteIcon />
          </TouchableOpacity>
        </View>
      ) : (
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <View style={styles.imagePreview}>
            {/* <View>
              <View style={[styles.imageView, styles.imageView1]}>
                <View>
                  <TouchableOpacity onPress={() => selectImage()}>
                    <Image
                    //   source={require('../../../../assets/icons/upload.png')}
                    />
                    <Text style={styles.uploadText}>Upload</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View> */}
            {renderImage &&
              renderImage.map((image, index) => {
                return (
                  <View key={index}>
                    <View style={styles.eachImgView}>
                      {renderImage && (
                        <Image
                          // source={{
                          //   uri: `data:image/png;base64,${renderImage[index]}`,
                          // }}
                          source={{
                            uri: `${renderImage[index]}`,
                          }}
                          style={styles.imageView}
                        />
                      )}
                      <TouchableOpacity
                        onPress={() => {
                          setIndex(index);
                          setModalVisible(true);
                          // deleteImage(index)
                        }}
                        style={{
                          // justifyContent: 'center',
                          // alignItems: 'center',
                          // paddingLeft: 7,
                          // paddingRight: 7,
                          position: 'absolute',
                          top: 6,
                          right: 3,
                          width: 20,
                          height: 20,
                        }}
                      >
                        <Cancel color={theme.color.primaryLight} />
                      </TouchableOpacity>
                    </View>
                  </View>
                );
              })}
          </View>
        </ScrollView>
      )}
      {loading && <ActivityIndicator />}
      <View
        style={[
          {
            alignItems: 'center',
            justifyContent: 'center',
            //   padding: 30,
          },
          style,
        ]}
      >
        {/* <Image source={require('../../../../assets/icons/upload.png')} /> */}
        {/* <Text style={styles.uploadText}>Upload Image</Text> */}
        <View style={{ flexDirection: 'row' }}>
          {/* <Button */}
          {/* title="Add Photo*" */}
          {/* //   textStyle={{ fontSize: theme.typography.body2.s }} */}
          {/* style={{ width: 200, height: 49, borderRadius: 10 }} */}
          {/* titleStyle={{ ...getAppFont('Regular') }} */}
          {/* onPress={() => captureImage()} */}
          {/* icon={<CameraIcon />} */}
          {/* disabled = { renderImage?.length >= 5} */}
          {/* /> */}
          <Button
            title={title}
            //   textStyle={{ fontSize: theme.typography.body2.s }}
            style={{
              width: 200,
              height: Platform.isPad ? 70 : 49,
              borderRadius: 10,
            }}
            titleStyle={{ ...getAppFont('Regular') }}
            onPress={() => togglePickerModal(true)}
            icon={<CameraIcon />}
            disabled={renderImage?.length >= limit}
          />
        </View>
        {uploadEnable && (
          <View style={{ flexDirection: 'row' }}>
            <Button
              title={uploadButtonTitle}
              // textStyle={{ fontSize: theme.typography.body2.s }}
              style={{ marginTop: 10 }}
              onPress={() => selectImage()}
            />
          </View>
        )}
      </View>

      {modalVisible && (
        <SignoutPopup
          deletePopup
          visible={modalVisible}
          title="Delete !"
          desc="Are you sure you want to delete?"
          yes={() => yesClicked()}
          no={() => setModalVisible(false)}
          close={() => setModalVisible(false)}
        />
      )}
    </View>
  );
};

export default ImageUpload;

const styles = StyleSheet.create({
  uploadContainer: {
    // borderStyle: 'dotted',
    // borderWidth: 1,
    // borderRadius: 10,
    // borderColor: theme.color.primary,
    // backgroundColor: theme.color.white,
    // backgroundColor :'red'
  },
  uploadText: {
    // fontSize: theme.typography.subTitle.s,
    // color: theme.color.primary,
    marginTop: 10,
    textAlign: 'center',
  },
  imagePreview: {
    flexDirection: 'row',
    marginVertical: 10,
    paddingLeft: 6, // need to adjust
  },
  eachImgView: {
    position: 'relative',
    // flex: 1,
    marginRight: 20,
  },
  imageView: {
    overflow: 'hidden',
    // borderRadius: 10,
    width: 100,
    height: 100,
  },
  fullImageView: {
    // overflow: 'hidden',
    // borderRadius: 10,
    width: 106,
    height: 93,
  },
  imageView1: {
    width: 100,
    height: 100,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // borderColor: theme.color.primary,
    marginRight: 10,
  },
  photoView: {
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: theme.color.secondaryDark,
  },
  photoTouch: {
    alignSelf: 'stretch',
  },
  applyText: {
    fontSize: 20,
    color: theme.color.primary,
    fontWeight: '500',
    paddingTop: 15,
    paddingBottom: 10,
  },
  cancelView: {
    borderRadius: 20,
    height: 80,
    backgroundColor: theme.color.primaryWhite,
    // alignItems:'center',
    justifyContent: 'center',
  },
  galleryview: {
    // borderBottomWidth: 1,
    // borderBottomColor: theme.color.secondaryDark,flex:1,
    width: '100%',
  },
  cancelTouch: {
    alignItems: 'center',
  },
  galleryTouch: {
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  crossBtnView: {
    position: 'absolute',
    right: -7,
    top: -8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.color.lightGray,
    borderRadius: 10,
    width: 20,
    height: 20,
    borderColor: theme.color.primary,
    borderWidth: 1,
    padding: 10,
  },
  popupWrapper: {
    justifyContent: 'flex-end',
  },
  popUpContainer: {
    borderRadius: 20,
    height: 140,
    backgroundColor: theme.color.primaryWhite,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraIcon: {
    height: 15,
    width: 15,
  },
  cameraIconStyle: {
    position: 'absolute',
    right: 0,
    bottom: 3,
    backgroundColor: '#fff',
    borderRadius: 30 / 2,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    ...theme.cardShadow,
  },
});
