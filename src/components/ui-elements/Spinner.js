import React from 'react';
import { View, StyleSheet, Modal, ActivityIndicator } from 'react-native';


const Spinner = ({
  visible = false,
  size = 'small',
  animationType = 'slide',
}) => {
  return (
    <Modal visible={visible} animationType={animationType} transparent={true}>
      <View style={styles.container}>
        <ActivityIndicator size={size} />
      </View>
    </Modal>
  );
};

export default Spinner;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
