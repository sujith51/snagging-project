import React from 'react';
import {Text,View,StyleSheet} from 'react-native';
const ErrorText=(props)=>{
    return(
        <Text style={[style.error,props.style]}>{props.children}</Text>
    )
}
const style=StyleSheet.create({
    error:{
        fontSize:10,
        color:'red',
        paddingTop:3
    },
});
export  default ErrorText;