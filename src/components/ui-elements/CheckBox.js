/* eslint-disable react-native/no-color-literals */
import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text } from 'react-native';
import { TickIcon } from '../../assets/svg_components';
import { useTheme, getAppFont } from '../../utils';

const CheckBox = ({
  checked = false,
  leftText = null,
  rigthText = null,
  textStyle = null,
  onPress = () => {},
}) => { 
  const theme = useTheme();

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.rowStyle}>
        {leftText && (
          <Text style={[styles.textStyle, textStyle]}>{leftText}</Text>
        )}
        <View
          style={[
            styles.square,
            { borderColor: theme.color.primary },
            checked && { backgroundColor: theme.color.primary },
          ]}
        >
          {checked && <TickIcon height={15} width={20} />}
        </View>
        {rigthText && (
          <Text style={[styles.textStyle, textStyle]}>{rigthText}</Text>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default CheckBox;

const styles = StyleSheet.create({
  square: {
    width: 20,
    height: 20,
    borderWidth: 2,
    alignItems: 'center',
  },
  rowStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStyle: {
    ...getAppFont('Regular'),
    left: 6,
  },
});
