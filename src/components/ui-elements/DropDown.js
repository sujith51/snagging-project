/* eslint-disable react-native/no-color-literals */
/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  createRef,
  useRef,
  useLayoutEffect,
  useCallback,
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Platform,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { FlatList } from 'react-native-gesture-handler';

import { useTheme, config, showFixedLength } from '../../utils';
import { getAppFont } from '../../utils/theme';
import { DropDownIcon } from '../../assets/svg_components';

const maxHeight = config.DEVICE_WINDOW_HEIGHT - 300;

const Dropdown = ({
  value,
  options,
  placeholder = 'Select',
  onOpen = () => {},
  onClose = () => {},
  placeHolderStyle = null,
  count = null,
  iconColor = null,
  labelContStyle = {},
  labelStyle = null,
  contStyle = {},
  drDownMdlwidth = null,
  communityExists = true,
}) => {
  const theme = useTheme();
  // :States
  const [open, setOpen] = useState(false);
  const dropdownRef = useRef(createRef());
  const [dropdownDim, setDropDownDim] = useState(null);
  const [selected, setSelected] = useState(null);

  // :Effects
  useLayoutEffect(() => {
    dropdownRef.current.measureInWindow((x, y, width, height) => {
      if (!dropdownDim || dropdownDim.x !== x || dropdownDim.y !== y) {
        setDropDownDim({ x, y, width, height });
      }
    });
  });

  // :Functions
  const setAnimatedOpenFn = useCallback(
    (opened) => {
      if (opened) {
        setOpen(true);
      } else {
        setOpen(false);
      }
    },
    [setOpen],
  );

  const onLabelClick = () => {
    setAnimatedOpenFn(true);
    onOpen();
  };

  const requestClose = useCallback(() => {
    setAnimatedOpenFn(false);
    // onClose(selected);
  }, [setAnimatedOpenFn]);

  const onSelect = useCallback(
    (item) => {
      setAnimatedOpenFn(false);
      setSelected(item);
      onClose(item);
    },
    [onClose, setAnimatedOpenFn],
  );

  // :Render
  return (
    /*  */
    <>
      <View>
        <TouchableWithoutFeedback onPress={() => onLabelClick()}>
          <View
            style={[styles.container, contStyle]}
            ref={(el) => (dropdownRef.current = el)}
            collapsable={false}
          >
            {selected?.icon ? (
              <View style={styles.rowView}>
                <Image
                  source={selected?.icon ?? value ?? placeholder}
                  style={styles.imageStyle}
                />
                <Text style={styles.label}>
                  {selected?.label ?? value ?? placeholder}
                </Text>
              </View>
            ) : (
              <View>
                <Text style={[styles.label, placeHolderStyle]}>
                  {showFixedLength(selected?.label, 14) ?? value ?? placeholder}{' '}
                  {/* {communityExists && count && `(${count})`} */}
                </Text>
              </View>
            )}
            <View style={!drDownMdlwidth && { left: 5 }}>
              <DropDownIcon
                color={iconColor || theme.color.secondaryDark}
                width={12}
                height={10}
                top={3}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
      <ReactNativeModal
        isVisible={open}
        onBackdropPress={requestClose}
        onBackButtonPress={requestClose}
        backdropColor="transparent"
        style={{ padding: 0, margin: 0 }}
      >
        <View
          style={[
            styles.dropdownContainer,
            {
              top: dropdownDim?.y + dropdownDim?.height,
              left: dropdownDim?.x,
              width: drDownMdlwidth ? dropdownDim?.width : 'auto',
              backgroundColor: theme.color.primaryWhite,
              ...theme.cardShadow,
            },
          ]}
        >
          <FlatList
            // contentContainerStyle={!open && { height: 0 }}
            style={{ maxHeight }}
            data={options}
            keyExtractor={(item, index) => `dropDown-key-${index}`}
            renderItem={({ item }) => (
              <View>
                {item.icon ? (
                  <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                    <View style={[styles.rowView, { paddingHorizontal: 12 }]}>
                      <Image source={item.icon} style={styles.imageStyle} />
                      <Text style={styles.dropdownTxtStyle}>{item.label}</Text>
                    </View>
                  </TouchableWithoutFeedback>
                ) : (
                  <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                    <View
                      style={[
                        {
                          padding: 10,
                          paddingHorizontal: 12,
                        },
                        labelContStyle,
                      ]}
                    >
                      <Text
                        style={[
                          styles.dropdownTxtStyle,
                          labelStyle ? { labelStyle } : null,
                        ]}
                      >
                        {item.label}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              </View>
            )}
          />
        </View>
      </ReactNativeModal>
    </>
  );
};

export default React.memo(Dropdown);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 7,
    paddingHorizontal: 12,
    // borderWidth: 1.5,
    borderRadius: 6,
    alignItems: 'center',
    // justifyContent: 'space-between',
    // flex: 1,
    right: 20,
  },
  label: {
    textAlign: 'center',
    marginTop: 3,
    fontSize: Platform.isPad ? 20 : 14,
  },
  dropdownTxtStyle: {
    ...getAppFont('Medium'),
    fontSize: Platform.isPad ? 20 : 14,
  },
  dropdownContainer: {
    position: 'absolute',
    top: 0,
    // left: 0,
    // backgroundColor: theme.colors.white,
    // ...theme.cardShadow,
    paddingBottom: 7,
    borderRadius: 5,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageStyle: {
    height: 30,
    width: 40,
    marginRight: 10,
  },
});
