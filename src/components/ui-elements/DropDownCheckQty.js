/* eslint-disable import/no-unresolved */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  createRef,
  useRef,
  useLayoutEffect,
  useCallback,
  useEffect,
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  TextInput,
  ScrollView,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux';
import { usePrevious } from '_hooks';
import { config, useTheme } from '../../utils';
import { getAppFont } from '../../utils/theme';
import { Cancel, DropDownIcon, SearchIcon } from '../../assets/svg_components';
import CheckBox from './CheckBox';
import IncrementDecrement from '../IncrementDecrement';
import Button from '../Button';
import { getRateCardList } from '../../redux/actions';
import DropDown from './DropDown';
import InputText from '../InputText';

const DropDownCheckQty = ({
  value,
  options = [],
  placeholder = 'Select',
  onOpen = () => {},
  onClose = () => {},
  quantityValue = () => {},
  quantityData,
  placeHolderStyle = null,
  contStyle = {},
  tableHeadTxtStyle = {},
  contentStyle = {},
  handleCheckDropDown = () => {},
  taskDetail,
  contractor_id,
  listCategory = [],
  selectedAll = [],
  fullScreen = false,
  multiColor = false,
  SelectedItems = false,
  contractor = true,
  currentData,
}) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  // :States
  const [open, setOpen] = useState(false);

  const [materialData, setMaterialData] = useState([]);
  const [category, setCategory] = useState(null);
  const dropdownRef = useRef(createRef());
  const [dropdownDim, setDropDownDim] = useState(null);
  const [selected, setSelected] = useState([]);
  const [errorMessage, setErrorMessage] = useState(false);

  const dropdownContainerRef = useRef(null);
  const idRef = useRef(contractor_id);
  const [searchText, setsearchText] = useState('');
  const [FinalSearchData, setFinalSearchData] = useState([]);
  const [quantity, setQuantity] = useState({});
  const prevId = usePrevious(contractor_id);

  // const selectedOptions = materialData.filter((item) => item.checked === true);

  // console.log("DropDown Check ", contractor_id, materialData?.length === 0, buttonState)
  const {
    rateCardList: {
      data: materialRateCardData,
      // loading: materialRateCardLoading,
    },
  } = useSelector((state) => state.taskdetails);
  // :Effects
  useLayoutEffect(() => {
    dropdownRef.current.measureInWindow((x, y, width, height) => {
      if (!dropdownDim || dropdownDim.x !== x || dropdownDim.y !== y) {
        setDropDownDim({ x, y, width, height });
      }
    });
  });

  // :Functions

  useEffect(() => {
    if (open) {
      if (selectedAll?.length > 0 && Object.keys(quantity)?.length == 0) {
        const obj = selectedAll.reduce((acc, objs) => {
          const id = objs?.id;
          if (!acc[id]) {
            acc[id] = {};
          }
          acc[id] = {
            checked: objs.checked,
            qty: objs.qty,
            qtyOwner: objs.qtyOwner,
            qtyTenant: objs.qtyTenant,
            touched: true,
          };
          return acc;
        }, {});
        setQuantity(obj);
      }
    }
  }, [open]);
  useEffect(() => {
    const newData = materialData.filter((item) => {
      const data = item.material_name ? item.material_name.toUpperCase() : '';
      const textData = searchText.toUpperCase();
      return data.indexOf(textData) > -1;
    });
    setFinalSearchData(newData);
  }, [searchText, materialData]);

  useEffect(() => {
    if (
      contractor_id !== null &&
      currentData?.taskType !== 0 &&
      currentData?.payBy != null &&
      open &&
      contractor
    ) {
      dispatch(
        getRateCardList({
          id_community: taskDetail[0].id_community,
          id_contractor: contractor_id,
          // category,
        }),
      );
    }
  }, [category, contractor_id, dispatch, taskDetail, open]);

  useEffect(() => {
    if (materialRateCardData?.length > 0) {
      // const formatData = materialRateCardData.map((item) => {
      //   return {
      //     ...item,
      //     checked: false,
      //     qty: 1,
      //   };
      // });
      setMaterialData(materialRateCardData);
    } else {
      setMaterialData([]);
    }
  }, [materialRateCardData]);

  const setAnimatedOpenFn = (opened) => {
    if (opened) {
      setOpen(true);
    } else {
      setOpen(false);
    }
    // dropdownContainerRef.current?.animateNextTransition();
  };

  const onLabelClick = () => {
    setAnimatedOpenFn(true);
    onOpen();
  };

  const requestClose = () => {
    // if (materialRateCardData) {,

    const itemID =
      materialRateCardData &&
      materialRateCardData?.map((item) => {
        return {
          ...item,
          checked: quantity[item?.id] ? quantity[item?.id]?.checked : false,
          qtyOwner: quantity[item?.id] ? quantity[item?.id]?.qtyOwner : null,
          qtyTenant: quantity[item?.id] ? quantity[item?.id]?.qtyTenant : null,
        };
      });

    const selected = itemID && itemID?.filter((item) => item.checked === true);

    // console.log("requestClose.....", selected.some((data) => data.qtyTenant !== 0))

    if (currentData?.payBy === 3) {
      const formatData = materialRateCardData?.map((item) => {
        return {
          ...item,
          checked: quantity[item?.id] ? quantity[item?.id]?.checked : false,
          qty: quantity[item?.id] ? quantity[item?.id]?.qty : 1,
          qtyOwner: quantity[item?.id] ? quantity[item?.id]?.qty : 1,
          qtyTenant: 0,
        };
      });

      onClose(formatData);
      setsearchText('');
      setAnimatedOpenFn(false);
      setCategory(null);
    } else if (currentData?.payBy === 4) {
      const formatData = materialRateCardData?.map((item) => {
        return {
          ...item,
          checked: quantity[item?.id] ? quantity[item?.id]?.checked : false,
          qty: quantity[item?.id] ? quantity[item?.id]?.qty : 1,
          qtyOwner: 0,
          qtyTenant: quantity[item?.id] ? quantity[item?.id]?.qty : 1,
        };
      });

      onClose(formatData);
      setsearchText('');
      setAnimatedOpenFn(false);
      setCategory(null);
    } else if (currentData?.payBy === 5) {
      const isQuantityChecked =
        Object.values(quantity).length > 0
          ? Object.values(quantity).some((val) => val.checked)
          : false;
      // if quantity has no checked then simply submit
      let formatData;
      if (!isQuantityChecked) {
        formatData = materialRateCardData?.map((item) => {
          return {
            ...item,
            checked: quantity[item?.id] ? quantity[item?.id]?.checked : false,
            qty: quantity[item?.id] ? quantity[item?.id]?.qty : 1,
            qtyOwner: quantity[item?.id] ? quantity[item?.id]?.qtyOwner : 0,
            qtyTenant: quantity[item?.id] ? quantity[item?.id]?.qtyTenant : 0,
          };
        });
        onClose(formatData);
        setsearchText('');
        setAnimatedOpenFn(false);
        setCategory(null);
        return;
      }
      // if quantity has checked then do validataion
      if (
        isQuantityChecked &&
        selected.every((data) => data?.qtyOwner + data?.qtyTenant > 0)
      ) {
        formatData = materialRateCardData?.map((item) => {
          return {
            ...item,
            checked: quantity[item?.id] ? quantity[item?.id]?.checked : false,
            qty: quantity[item?.id] ? quantity[item?.id]?.qty : 1,
            qtyOwner: quantity[item?.id]
              ? quantity[item?.id]?.qtyOwner || 0
              : 0,
            qtyTenant: quantity[item?.id]
              ? quantity[item?.id]?.qtyTenant || 0
              : 0,
          };
        });
        onClose(formatData);
        setsearchText('');
        setAnimatedOpenFn(false);
        setCategory(null);
      } else {
        setErrorMessage(true);
      }
    } else {
      setErrorMessage(true);
    }
    // quantityValue(quantity)
  };

  const closeModal = () => {
    setAnimatedOpenFn(false);
    setCategory(null);
  };
  // console.log('quantifyt.....', quantity);

  const handleCategory = (cat) => {
    setCategory(cat.value);
  };

  const onSelect = useCallback(
    (item) => {
      setAnimatedOpenFn(true);
      const exist = selected.findIndex((i) => i === item);
      if (exist >= 0) {
        setSelected((prev) => [...prev.filter((a) => a !== item)]);
      } else {
        setSelected((prev) => [...prev, item]);
      }
      //   onClose(selected);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [selected],
  );

  const checkClicked = useCallback(
    (item_) => {
      const obj = {};
      if (!quantity[item_?.id]) {
        obj[item_?.id] = {
          checked: true,
          qty: 1,
          qtyOwner: 0,
          qtyTenant: 0,
          touched: false,
        };
      } else {
        obj[item_?.id] = {
          ...quantity[item_?.id],
          checked: !quantity[item_?.id].checked,
        };
      }
      setQuantity({ ...quantity, ...obj });

      // console.log('check', obj[item_?.id]);
      // const final = materialData.map((item) => {
      //   return {
      //     ...item,
      //     checked: item.id === item_.id ? !item.checked : item.checked,
      //   };
      // });
      // setMaterialData(final);
      //  handleCheckDropDown(quantity);
    },
    [quantity],
  );

  const incrementClicked = (item_) => {
    // here quantity would have existed
    const obj = {};
    obj[item_?.id] = {
      ...quantity[item_?.id],
      qty: quantity[item_?.id].qty + 1,
      qtyOwner: 0,
      qtyTenant: 0,
    };
    setQuantity({ ...quantity, ...obj });
  };

  const handletxtInput = (item_, inptval) => {
    const NumOnly = inptval.replace(/[^0-9]/g, '');

    const obj = {};
    obj[item_?.id] = {
      ...quantity[item_?.id],
      qty: Number(NumOnly),
      qtyOwner: 0,
      qtyTenant: 0,
    };
    setQuantity({ ...quantity, ...obj });
  };

  const decrementClicked = (item_) => {
    const obj = {};
    obj[item_?.id] = {
      ...quantity[item_?.id],
      qty: quantity[item_?.id].qty - 1,
      qtyOwner: 0,
      qtyTenant: 0,
    };
    setQuantity({ ...quantity, ...obj });
  };

  const handleOwnerInput = (item_, inptval) => {
    const NumOnly = inptval.replace(/[^0-9]/g, '');

    setErrorMessage(false);
    if (
      NumOnly !== '' &&
      NumOnly < quantity[item_?.id].qty &&
      quantity[item_?.id].qtyTenant === 0
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: Number(NumOnly),
        qtyTenant: quantity[item_?.id].qty - Number(NumOnly),
        touched: true,
      };
      setQuantity({ ...quantity, ...obj });
    }
    if (
      NumOnly !== '' &&
      NumOnly <= quantity[item_?.id].qty &&
      quantity[item_?.id].qtyTenant !== ''
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: Number(NumOnly),
        qtyTenant: quantity[item_?.id].qty - Number(NumOnly),
        touched: true,
      };

      setQuantity({ ...quantity, ...obj });
    } else if (
      NumOnly === '' &&
      NumOnly < quantity[item_?.id].qty &&
      quantity[item_?.id].qtyTenant !== null &&
      NumOnly <= quantity[item_?.id].qty - quantity[item_?.id].qtyTenant
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: Number(NumOnly),
        qtyTenant: Number(NumOnly),
        touched: true,
        // qtyTenant: quantity[item_?.id].qty - Number(NumOnly)
      };
      setQuantity({ ...quantity, ...obj });
    } else if (
      quantity[item_?.id].qtyTenant === null &&
      NumOnly === null &&
      item_ !== null &&
      item_ !== undefined
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: Number(NumOnly),
        qtyTenant: Number(NumOnly),
        touched: true,
      };
      setQuantity({ ...quantity, ...obj });
    } else {
      alert('Please enter less than Quantity');
    }
  };

  const handleTenantInput = (item_, inptval) => {
    const NumOnly = inptval.replace(/[^0-9]/g, '');
    setErrorMessage(false);

    if (
      NumOnly !== '' &&
      NumOnly < quantity[item_?.id].qty &&
      quantity[item_?.id].qtyOwner === 0
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: quantity[item_?.id].qty - Number(NumOnly),
        qtyTenant: Number(NumOnly),
        touched: true,
      };
      setQuantity({ ...quantity, ...obj });
    }

    if (
      NumOnly !== '' &&
      NumOnly <= quantity[item_?.id].qty &&
      quantity[item_?.id].qtyOwner !== ''
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: quantity[item_?.id].qty - Number(NumOnly),
        qtyTenant: Number(NumOnly),
        touched: true,
      };
      setQuantity({ ...quantity, ...obj });
    } else if (
      NumOnly === '' &&
      quantity[item_?.id].qtyOwner !== null &&
      NumOnly <= quantity[item_?.id].qty - quantity[item_?.id].qtyOwner
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: Number(NumOnly),
        qtyTenant: Number(NumOnly),
        touched: true,
      };
      setQuantity({ ...quantity, ...obj });
    } else if (
      NumOnly === '' &&
      NumOnly < quantity[item_?.id].qty &&
      quantity[item_?.id].qtyOwner !== null &&
      NumOnly <= quantity[item_?.id].qty - quantity[item_?.id].qtyOwner
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        // qtyOwner: quantity[item_?.id].qty - Number(NumOnly),
        qtyTenant: Number(NumOnly),
        touched: true,
      };
      setQuantity({ ...quantity, ...obj });
    } else if (
      NumOnly === null &&
      quantity[item_?.id].qtyOwner === null &&
      item_ !== null &&
      item_ !== undefined
    ) {
      const obj = {};
      obj[item_?.id] = {
        ...quantity[item_?.id],
        qtyOwner: Number(NumOnly),
        qtyTenant: Number(NumOnly),
        touched: true,
      };
      setQuantity({ ...quantity, ...obj });
      //   alert('Error Tenant')
    } else {
      alert('Please enter less than Quantity');
    }
  };

  // :Render
  return (
    /*  */
    <>
      <View>
        <TouchableWithoutFeedback onPress={() => onLabelClick()}>
          <View
            style={[styles.container, contStyle]}
            ref={(el) => (dropdownRef.current = el)}
            collapsable={false}
          >
            {selected?.icon ? (
              <View style={styles.rowView}>
                <Image
                  source={selected?.icon ?? value ?? placeholder}
                  style={styles.imageStyle}
                />
                <Text style={styles.label}>
                  {selected?.label ?? value ?? placeholder}
                </Text>
              </View>
            ) : (
              <View>
                <Text style={[styles.label, placeHolderStyle]}>
                  {placeholder}
                </Text>
              </View>
            )}
            <View style={{ left: 22 }}>
              <DropDownIcon
                color={theme.color.primaryWhite}
                width={14}
                height={7}
                top={3}
              />
            </View>
            {/* <Image
                source={<RightArrow />}
                style={[styles.dropdownIcon, { tintColor: 'orange' }]}
              /> */}
          </View>
        </TouchableWithoutFeedback>
        {!open && selectedAll?.length > 0 && (
          <View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 15,
                marginBottom: 18,
                // flex: 1,
                justifyContent: 'space-around',
                // height: 30,
                // backgroundColor: 'red'
              }}
            >
              <View style={{ flex: 0.5, textAlign: 'center' }}>
                <Text style={[tableHeadTxtStyle, { textAlign: 'center' }]}>
                  Item
                </Text>
              </View>

              <View style={{ flex: 0.5, alignItems: 'flex-end' }}>
                <Text style={tableHeadTxtStyle}>Qty</Text>
              </View>

              {/* <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <Text style={tableHeadTxtStyle}>Price</Text>
              </View> */}
            </View>
            {selectedAll?.map((item, index) => {
              return (
                <>
                  {item?.qty > 0 && (
                    <View key={index}>
                      <View
                        key={item.material_name}
                        style={{
                          flexDirection: 'row',
                          paddingTop: 5,
                          justifyContent: 'space-between',
                          // flex: 1,
                        }}
                      >
                        <View style={{ flex: 0.8 }}>
                          <Text numberOfLines={3} style={contentStyle}>
                            {item?.material_name}
                          </Text>
                        </View>

                        <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                          <Text style={[contentStyle, { right: 10 }]}>
                            {item?.qty}
                          </Text>
                        </View>

                        {/* <View style={{ flex: 1, alignItems: 'flex-end' }}>
                       <Text style={contentStyle}>{item.qty * item.price}</Text>
                        </View> */}
                      </View>
                      {/* {
                        payBy === 5 && */}
                      <View
                        style={{
                          marginTop: 10,
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}
                      >
                        {currentData?.payBy === 5 && item?.qtyOwner ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                              paddingRight: 50,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: 16,
                                fontWeight: '700',
                                color: theme.color.primary,
                              }}
                            >
                              Owner
                            </Text>
                            <Text
                              style={{
                                fontSize: 16,
                                fontWeight: 'bold',
                                color: theme.color.primary,
                              }}
                            >
                              {': '}
                              {item?.qtyOwner}
                            </Text>
                          </View>
                        ) : null}

                        {currentData?.payBy === 5 && item?.qtyTenant ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                            }}
                          >
                            <Text
                              style={{
                                fontSize: 16,
                                fontWeight: '700',
                                color: theme.color.primary,
                              }}
                            >
                              Tenant
                            </Text>
                            <Text
                              style={{
                                fontSize: 16,
                                fontWeight: 'bold',
                                color: theme.color.primary,
                              }}
                            >
                              {': '}
                              {item?.qtyTenant}
                            </Text>
                          </View>
                        ) : null}

                        {/* <Text>Both </Text> */}
                      </View>
                      {/* // } */}
                      <View
                        style={{
                          flex: 1,
                          marginTop: 10,
                          marginBottom: 5,
                          borderBottomWidth: 0.5,
                          borderBottomColor: theme.color.primary,
                        }}
                      />
                    </View>
                  )}
                </>
              );
            })}
            {/* <View
              style={{
                flex: 1,
                marginTop: 38,
                borderBottomWidth: 0.6,
                borderBottomColor: theme.color.primary,
              }}
            /> */}
          </View>
        )}
      </View>
      <ReactNativeModal
        isVisible={open}
        onBackdropPress={closeModal}
        onBackButtonPress={closeModal}
        // backdropColor="transparent"
        // style={{ padding: 0, margin: 0 }}
        style={
          [styles.container, fullScreen && { margin: 0 }]
          // { flexDirection: 'column' },
          // { backgroundColor: theme.color.primaryWhite },
        }
        animationInTiming={0.1}
        animationOutTiming={0.1}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              padding: 15,
              backgroundColor: theme.color.secondaryLight,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingTop: 50,
            }}
          >
            <Text style={{ fontSize: 18, ...getAppFont('Regular') }}>
              Select Materials
            </Text>
            <TouchableWithoutFeedback
              onPress={() => {
                closeModal();
              }}
            >
              <View>
                <Cancel color={theme.color.primary} />
              </View>
            </TouchableWithoutFeedback>
          </View>

          <View
            style={{
              backgroundColor: theme.color.primaryWhite,
              paddingBottom: 10,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                borderWidth: 0.5,
                borderRadius: 10,
                paddingLeft: 20,
                margin: 10,
              }}
            >
              <TextInput
                style={styles.input}
                placeholder="Search Materials"
                onChangeText={(txt) => setsearchText(txt)}
                underlineColorAndroid="transparent"
              />
              <View
                style={[
                  styles.searchIconSty,
                  { backgroundColor: theme.color.primary },
                ]}
              >
                <SearchIcon />
              </View>
            </View>
          </View>

          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{ backgroundColor: theme.color.primaryWhite }}
          >
            {/* <View style={{ padding: 10 }}>
              <Text
                style={{
                  fontSize: 18,
                  ...getAppFont('Regular'),
                  paddingBottom: 10,
                }}
              >
                Select category*
              </Text>
              <DropDown
                options={listCategory}
                iconColor={theme.color.primaryWhite}
                // placeholder={
                //   checjlistObj?.answer[data.id_template_mapping] &&
                //   checjlistObj.answer[data.id_template_mapping].Vendor
                // }
                placeHolderStyle={{
                  color: theme.color.primaryWhite,
                  backgroundColor: theme.color.primary,
                  ...getAppFont('Medium'),
                }}
                drDownMdlwidth
                contStyle={[
                  styles.dropDownContainer,
                  { backgroundColor: theme.color.primary },
                ]}
                onClose={(itm) => handleCategory(itm)}
              />
            </View> */}

            {/* <View style={[styles.searchSection]}>
              <TextInput
                style={styles.input}
                placeholder="Search Materials"
                onChangeText={(txt) => setsearchText(txt)}
                underlineColorAndroid="transparent"
              />
              <View
                style={[
                  styles.searchIconSty,
                  { backgroundColor: theme.color.primary },
                ]}
              >
                <SearchIcon />
              </View>
            </View> */}

            <View
              style={{
                // marginLeft : 48,
                flexDirection: 'row',
                // marginRight:30,
                // paddingTop: 10,
                paddingBottom: 10,
              }}
            >
              {/* <View style={{ flex: 0.1 }} /> */}
              {/* <View style={{ paddingLeft: 10 }}>
              <Text style={tableHeadTxtStyle}>Items</Text>
            </View> */}
              {/* <View style={{ flex: 0.3 }}>
              <Text style={tableHeadTxtStyle}>Specification</Text>
            </View> */}
              {/* <View style={{ flex: 0.2 }}>
              <Text style={tableHeadTxtStyle}>Price</Text>
            </View>
            <View style={{ flex: 0.3 }}>
              <Text style={[tableHeadTxtStyle, { left: 27 }]}>Qty</Text>
            </View> */}
            </View>
            <View
              style={
                {
                  // paddingLeft: 10,
                }
              }
            >
              {contractor_id &&
                currentData?.taskType !== 0 &&
                currentData?.payBy != null &&
                FinalSearchData?.map((item, index) => {
                  // console.log("FinalSearchData", quantity)
                  return (
                    <View key={index}>
                      <View
                        key={item.id}
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          paddingBottom: 15,
                          paddingLeft: 10,
                          alignItems: 'flex-start',
                        }}
                      >
                        <View>
                          <CheckBox
                            // checked={item.checked}
                            checked={quantity?.[item.id]?.checked}
                            onPress={() => checkClicked(item)}
                          />
                        </View>
                        <View style={{ flex: 1, paddingLeft: 10 }}>
                          <Text
                            onPress={() => checkClicked(item)}
                            style={[
                              contentStyle,
                              multiColor && {
                                backgroundColor:
                                  index % 2 === 0
                                    ? theme.color.primaryWhite
                                    : theme.color.lightGray,
                              },
                            ]}
                          >
                            {item.material_name}
                          </Text>
                        </View>
                        <View style={{ borderBottomWidth: 1 }} />
                        {/* <View style={{ flex: 0.3 }}>
                    <Text style={contentStyle}>{item.specification}</Text>
                  </View> */}

                        {/* <View style={{ flex: 0.3, alignItems: 'center' }}>
                    <IncrementDecrement
                      disable={!item.checked}
                      value={item.qty}
                      inc={() => incrementClicked(item)}
                      dec={() => item.qty > 0 && decrementClicked(item)}
                      inputQty={(inptval) => handletxtInput(item, inptval)}
                    />
                  </View> */}
                      </View>
                      <View>
                        {quantity?.[item.id]?.checked && (
                          <View>
                            <View
                              style={{
                                flexDirection: 'row',
                                flex: 1,
                                justifyContent: 'space-between',
                                padding: 10,
                                paddingTop: 0,
                                // paddingLeft: 27,
                                paddingHorizontal: 30,
                              }}
                            >
                              <View>
                                <Text style={tableHeadTxtStyle}>Price</Text>
                              </View>
                              <View>
                                <Text style={tableHeadTxtStyle}>Quantity</Text>
                              </View>
                            </View>
                            <View
                              style={{
                                flexDirection: 'row',
                                flex: 1,
                                justifyContent: 'space-between',
                                padding: 10,
                                paddingTop: 0,
                                // paddingLeft: 30,
                                paddingHorizontal: 25,
                              }}
                            >
                              <View style={{ left: 10 }}>
                                <Text
                                  style={[
                                    contentStyle,
                                    {
                                      ...getAppFont('Bold'),
                                      // color: theme.color.primaryBlack,
                                    },
                                  ]}
                                >
                                  {item.price}
                                </Text>
                              </View>
                              <View>
                                <IncrementDecrement
                                  disable={!quantity?.[item.id]?.checked}
                                  qtyValue={quantity?.[item.id]?.qty}
                                  inc={() => incrementClicked(item)}
                                  dec={() =>
                                    quantity?.[item.id]?.qty > 0 &&
                                    decrementClicked(item)
                                  }
                                  inputQty={(inptval) =>
                                    handletxtInput(item, inptval)
                                  }
                                />
                              </View>
                            </View>
                            {currentData?.payBy === 5 && (
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  paddingHorizontal: 30,
                                }}
                              >
                                <View>
                                  <Text
                                    variant="bodyBold"
                                    size={18}
                                    style={tableHeadTxtStyle}
                                  >
                                    Owner
                                  </Text>
                                  <InputText
                                    type="default"
                                    value={
                                      quantity?.[item.id]?.touched &&
                                      quantity?.[item.id]?.qtyOwner !== null
                                        ? String(quantity?.[item.id]?.qtyOwner)
                                        : ''
                                    }
                                    onChangeText={(text) =>
                                      handleOwnerInput(item, text)
                                    }
                                    textFieldContainerStyle={styles.borRadius}
                                    style={{ height: 40 }}
                                    // innerRef={specifyRef}
                                  />
                                </View>
                                <View style={{ left: 50 }}>
                                  <Text
                                    variant="bodyBold"
                                    size={18}
                                    style={tableHeadTxtStyle}
                                  >
                                    Tenant
                                  </Text>
                                  <InputText
                                    type="default"
                                    value={
                                      quantity?.[item.id]?.touched &&
                                      quantity?.[item.id]?.qtyTenant !== null
                                        ? String(quantity?.[item.id]?.qtyTenant)
                                        : ''
                                    }
                                    onChangeText={(text) =>
                                      handleTenantInput(item, text)
                                    }
                                    textFieldContainerStyle={styles.borRadius}
                                    style={{ height: 40 }}
                                    // innerRef={specifyRef}
                                  />
                                </View>
                                {/* <Text>Both </Text> */}
                              </View>
                            )}
                            {errorMessage &&
                            quantity?.[item.id]?.qtyOwner +
                              quantity?.[item.id]?.qtyTenant ==
                              0 ? (
                              <Text
                                style={{
                                  fontSize: 12,
                                  color: 'red',
                                  textAlign: 'left',
                                  marginBottom: 10,
                                  left: 30,
                                }}
                              >
                                Please enter Quantity for Owner and Tenant
                              </Text>
                            ) : null}
                            <View
                              style={{
                                height: 4,
                                backgroundColor: 'lightgray',
                              }}
                            />
                          </View>
                        )}
                      </View>
                    </View>
                  );
                })}
              {FinalSearchData?.length === 0 && (
                <View
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                  <Text style={styles.warStyle}>No results found</Text>
                </View>
              )}
              {/* {!contractor_id && (
                <View
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                  <Text style={styles.warStyle}>
                    {' '}
                    Select Vendor to see the items
                  </Text>
                </View>
              )} */}
              {(!contractor_id ||
                currentData?.taskType == 0 ||
                !currentData?.payBy) && (
                <View
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                  <Text style={styles.warStyle}>
                    Select {!contractor_id && "'Vendor'"}
                    {currentData?.taskType == 0 && " 'Issue Type'"}
                    {!currentData?.payBy && " 'Paid By'"} to see the items
                  </Text>
                </View>
              )}
              {/* {category === null && (
                <View
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                  <Text style={styles.warStyle}> Please select category</Text>
                </View>
              )} */}
            </View>
          </ScrollView>
          <View style={styles.footerStyl}>
            {contractor_id === null ||
            currentData?.taskType == 0 ||
            currentData?.payBy === null ||
            materialData?.length === 0 ? (
              <Button
                title="Close"
                style={styles.button1}
                titleStyle={{ ...getAppFont('Regular') }}
                onPress={closeModal}
              />
            ) : (
              <Button
                title="Submit"
                style={styles.button1}
                titleStyle={{ ...getAppFont('Regular') }}
                onPress={requestClose}
              />
            )}
          </View>
        </View>
      </ReactNativeModal>
    </>
  );
};

export default DropDownCheckQty;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    // paddingVertical: 7,
    // paddingHorizontal: 12,
    // flex: 1,
    height: 56,
    justifyContent: 'space-between',
    alignItems: 'center',
    // margin: 5,
    // backgroundColor: 'red'
  },
  label: { textAlign: 'center', marginTop: 3 },
  button1: {
    ...getAppFont('Regular'),
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageStyle: {
    height: 30,
    width: 40,
    marginRight: 10,
  },
  warStyle: {
    ...getAppFont('Regular'),
    paddingBottom: 20,
  },
  footerStyl: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 10,
    paddingLeft: 10,
    margin: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    ...getAppFont('Regular'),
  },
  searchIconSty: {
    padding: 10,
    marginHorizontal: 4,
    marginVertical: 2,
    borderRadius: 12,
  },
  dropDownContainer: {
    justifyContent: 'space-between',
    left: 0,
    height: 46,
    borderRadius: 10,
  },
  mt12: {
    marginTop: 12,
    marginBottom: 1,
  },
  borRadius: {
    borderRadius: 1,
    width: config.DEVICE_WINDOW_WIDTH / 2 - 100,
    paddingHorizontal: 10,
    marginTop: 2,
    // height: 30,
  },
});
