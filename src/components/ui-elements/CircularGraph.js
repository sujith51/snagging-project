import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Dimensions, Platform } from 'react-native';
// import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Circle, Text as SvgText } from 'react-native-svg';
import { useTheme } from '../../utils';

const CircularGraph = (props) => {
  const theme = useTheme();
  const {
    graphColor,
    totalFill,
    completedData,
    selectedId = true,
    totalCount = 0,
    value = 0,
  } = props;
  return (
    <View style={styles.progressChartView}>
      <View style={styles.circleView}>
        <AnimatedCircularProgress
          size={277}
          width={3}
          fill={(value / totalCount) * 100} // totalFill
          rotation={360}
          lineCap="round"
          tintColor={selectedId ? graphColor.color : null}
          backgroundColor={theme.color.darkGray}
          padding={20}
          duration={2000}
          renderCap={({ center }) => (
            <Circle
              cx={center.x}
              cy={center.y}
              r="13"
              fill={graphColor.color}
            />
          )}
        />
      </View>
      <View style={styles.statsView}>
        <View
          style={[
            styles.circleShapeView,
            { backgroundColor: graphColor.color },
          ]}
        >
          <Text
            style={
              selectedId
                ? [styles.textStatus, { color: graphColor.textColor }]
                : null
            }
          >
            Completed
          </Text>
          <Text
            style={
              selectedId
                ? [styles.textValue, { color: graphColor.textColor }]
                : null
            }
          >{`${value}/${totalCount}`}</Text>
        </View>
      </View>
    </View>
  );
};

export default CircularGraph;
const styles = StyleSheet.create({
  progressChartView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleView: {
    position: 'absolute',
  },
  statsView: {
    position: 'absolute',
    alignItems: 'center',
  },
  textStatus: {
    fontWeight: 'bold',
    fontSize: Platform.isPad ? 16 : 12,
    marginBottom: 10,
  },
  textValue: {
    fontSize: 48,
  },
  circleShapeView: {
    borderRadius: 100 / 1,
    width: 187,
    height: 187,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
