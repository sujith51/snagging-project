/* eslint-disable import/no-unresolved */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  createRef,
  useRef,
  useLayoutEffect,
  useCallback,
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { FlatList } from 'react-native-gesture-handler';
import { Transition, Transitioning } from 'react-native-reanimated';
import CheckBox from './CheckBox';
import { useTheme } from '../../utils';
import { getAppFont } from '../../utils/theme';
import { DropDownIcon } from '../../assets/svg_components';

const Dropdown = ({
  value,
  options,
  placeholder = 'Select',
  onOpen = () => {},
  onClose = () => {},
  placeHolderStyle = null,
}) => {
  const theme = useTheme();
  // :States
  const [open, setOpen] = useState(false);
  const [animatedOpen, setAnimatedOpen] = useState(false);
  const dropdownRef = useRef(createRef());
  const [dropdownDim, setDropDownDim] = useState(null);
  const [selected, setSelected] = useState([]);
  const dropdownContainerRef = useRef(null);

  // :Effects
  useLayoutEffect(() => {
    dropdownRef.current.measureInWindow((x, y, width, height) => {
      if (!dropdownDim || dropdownDim.x !== x || dropdownDim.y !== y) {
        setDropDownDim({ x, y, width, height });
      }
    });
  });

  // :Functions

  const transition = (
    <Transition.Together>
      {/* <Transition.Out type="slide-top" /> */}
      {/* <Transition.In interpolation="easeInOut" durationMs={200} /> */}
      <Transition.Change interpolation="easeInOut" durationMs={100} />
    </Transition.Together>
  );

  const setAnimatedOpenFn = (opened) => {
    if (opened) {
      setOpen(true);
      setAnimatedOpen(true);
    } else {
      setAnimatedOpen(false);
      setTimeout(() => {
        setOpen(false);
      }, 100);
    }
    dropdownContainerRef.current?.animateNextTransition();
  };

  const onLabelClick = () => {
    setAnimatedOpenFn(true);
    onOpen();
  };

  const requestClose = () => {
    setAnimatedOpenFn(false);
    onClose(selected);
  };

  const onSelect = useCallback(
    (item) => {
      setAnimatedOpenFn(true);
      const exist = selected.findIndex((i) => i === item);
      if (exist >= 0) {
        setSelected((prev) => [...prev.filter((a) => a !== item)]);
      } else {
        setSelected((prev) => [...prev, item]);
      }
      //   onClose(selected);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [selected],
  );

  // :Render
  return (
    /*  */
    <>
      <View>
        <TouchableWithoutFeedback onPress={() => onLabelClick()}>
          <View
            style={styles.container}
            ref={(el) => (dropdownRef.current = el)}
            collapsable={false}
          >
            {selected?.icon ? (
              <View style={styles.rowView}>
                <Image
                  source={selected?.icon ?? value ?? placeholder}
                  style={styles.imageStyle}
                />
                <Text style={styles.label}>
                  {selected?.label ?? value ?? placeholder}
                </Text>
              </View>
            ) : (
              <View>
                <Text style={[styles.label, placeHolderStyle]}>
                  {/* {selected?.label ?? value ?? placeholder} */}
                  Total Inspection(80)
                </Text>
              </View>
            )}
            <View style={{ left: 15 }}>
              <DropDownIcon
                color={theme.color.secondaryDark}
                width={12}
                height={10}
                top={3}
              />
            </View>
            {/* <Image
              source={<RightArrow />}
              style={[styles.dropdownIcon, { tintColor: 'orange' }]}
            /> */}
          </View>
        </TouchableWithoutFeedback>
      </View>
      <ReactNativeModal
        isVisible={open}
        onBackdropPress={requestClose}
        onBackButtonPress={requestClose}
        backdropColor="transparent"
        style={{ padding: 0, margin: 0 }}
        animationInTiming={0.1}
        animationOutTiming={0.1}
      >
        <Transitioning.View
          ref={dropdownContainerRef}
          transition={transition}
          style={[
            styles.dropdownContainer,
            {
              top: dropdownDim?.y + dropdownDim?.height,
              left: dropdownDim?.x,
              width: 'auto', // dropdownDim?.width,
              backgroundColor: theme.color.primaryWhite,
              ...theme.cardShadow,
            },
          ]}
        >
          <FlatList
            contentContainerStyle={!animatedOpen && { height: 0 }}
            data={
              //   selected
              // ? [
              //     selected,
              //     ...options.filter((f) => f.value !== selected?.value),
              //   ]
              //     :
              options
            }
            keyExtractor={(item, index) => `dropDown-key-${index}`}
            renderItem={({ item }) => (
              <View>
                {item.icon ? (
                  <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                    <View style={[styles.rowView, { paddingHorizontal: 12 }]}>
                      <Image source={item.icon} style={styles.imageStyle} />
                      <Text style={styles.dropdownTxtStyle}>{item.label}</Text>
                    </View>
                  </TouchableWithoutFeedback>
                ) : (
                  <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                    <View
                      style={{
                        padding: 10,
                        paddingHorizontal: 12,
                        flexDirection: 'row',
                      }}
                    >
                      <CheckBox
                        checked={selected.some(
                          (data) => data.value === item.value,
                        )}
                        onPress={() => onSelect(item)}
                      />
                      <Text style={styles.dropdownTxtStyle}>{item.label}</Text>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              </View>
            )}
          />
        </Transitioning.View>
      </ReactNativeModal>
    </>
  );
};

export default Dropdown;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 7,
    paddingHorizontal: 12,
    // borderWidth: 1.5,
    borderRadius: 6,
    alignItems: 'center',
    flex: 1,
  },
  label: { textAlign: 'center', marginTop: 3 },
  dropdownTxtStyle: {
    ...getAppFont('Medium'),
  },
  dropdownContainer: {
    position: 'absolute',
    top: 0,
    // left: 0,
    // backgroundColor: theme.colors.white,
    // ...theme.cardShadow,
    paddingBottom: 7,
    borderRadius: 5,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageStyle: {
    height: 30,
    width: 40,
    marginRight: 10,
  },
});
