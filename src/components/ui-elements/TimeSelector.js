import React, { useState,useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    Platform,Button,TouchableOpacity
} from 'react-native';
import { theme } from '../../utils';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

const TimeSelector = ({time,onChangeDate}) => {
    const [show, setShow] = useState(false);
    const [timeState, setTimeState] = useState(time);
    // useEffect(()=>{
    //     setTimeState(moment(time))
    // },[time])
    const timeChange = (time) => {
        let date=new Date(time.nativeEvent.timestamp)


        // setShow(Platform.OS === 'ios');
        setShow(false);
        setTimeState(date);
        onChangeDate(date)
    };

    const showDatepicker = (currentMode) => {
        setShow(true);
    };
    return (
        <>
        <TouchableOpacity  onPress={showDatepicker}  style={styles.viewcontainer}>
           <Text>
               {moment(timeState).format('LT')}
           </Text>
        </TouchableOpacity>
        {
            show &&
                <DateTimePicker
                    testID="dateTimePicker"
                    value={timeState}
                    mode='time'
                    is24Hour={true}
                    display="default"
                    onChange={time => timeChange(time)}
                />
        }

        </>
        /*<DatePicker
            mode="time"
            is24Hour={false}
            style={{ width: '100%', height: 50 }}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{ dateInput: { opacity: 0 } }}
            iconComponent={
                <View style={{ width: '100%' }}>
                    <View style={styles.subView}>
                        <View style={props.small ? styles.iconViewSmall : styles.iconView}>
                            <View>
                                <Image style={styles.orderContactIcon} source={require('../../assets/icons/form-time.png')} />
                            </View>
                        </View>
                        <Text style={styles.textStyle}>
                            {moment(time).format('LT')}
                        </Text>
                    </View>
                </View>
            }
            onDateChange={time => timeChange(time)}
        />*/
    )
}

export default TimeSelector;

const styles = StyleSheet.create({
    datePicker: {
        width: 50,
        marginTop: 20
    },
    textStyle: {
        paddingLeft: '7%',
        padding: 10,
        color: theme.color.primaryWhite,
        fontSize: 12,
    },
    subView: {
        flexDirection: "row",
        alignItems: 'center',
        flex:1,
        backgroundColor:theme.color.primary,
        borderRadius: 10
    },
    iconView: {
        backgroundColor: "white",
        width: 60,
        height: 60,
        borderRadius: 10,
        padding: 19,
        elevation: 10,
        shadowColor: '#636363',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
    },
    viewcontainer:{
        paddingHorizontal:15,
        paddingVertical:15,
        borderRadius:5,
        borderWidth:1,
        borderColor:'grey',
        justifyContent:'center',
        marginBottom:10
    },
    iconViewSmall: {
        backgroundColor: "white",
        width: 40,
        height: 40,
        justifyContent: "center",
        borderRadius: 10,
        alignItems: "center",
        elevation: 10,
        shadowColor: '#636363',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
    },
    orderContactIcon: {
        width: 22,
        height: 22,
        resizeMode: "contain"
    }
});

