/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React, { useCallback, useState } from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Platform,
} from 'react-native';
import SmallRectangle from './SmallRectangle';
import { getAppFont, theme } from '../../utils';

const LargeRectangle = ({
  selectedData,
  headerStyle,
  listingData = [],
  quotationLoad,
  headTextStyle,
  onVendorSelect = () => {},
}) => {
  const [toggle, toggleRadio] = useState();
  const onRadioHandle = useCallback((val, item) => {
    toggleRadio(val);
    const toggleData = val != undefined;
    onVendorSelect(item, toggleData);
  });

  // const listChild = childList ? Object.values(childList) : []
  return (
    <SafeAreaView style={styles.flatListContainer}>
      <FlatList
        data={listingData}
        keyExtractor={(item, index) => `list-key-${index}`}
        renderItem={({ item, index }) => (
          <SafeAreaView key={`item${index}`} style={styles.container}>
            <View style={[styles.subContainer, { paddingTop: headerStyle }]}>
              <View
                style={[styles.headTotalText, { paddingTop: headTextStyle }]}
              >
                <Text style={styles.headerText}>Unit Number</Text>
                <Text style={styles.headerText}>
                  {selectedData === 1
                    ? `#${item?.unit_key ?? ' '}`
                    : `#${item?.unit_number ?? ' '}`}
                </Text>
              </View>
              <View style={[styles.vendorText, { paddingTop: headTextStyle }]}>
                <Text style={styles.headerText}>Vendor</Text>
                <Text style={styles.headerText}> {item.vendor_name}</Text>
              </View>
            </View>
            <View style={styles.borderLine} />
            <View style={{ paddingLeft: 20, paddingTop: 20 }}>
              <SmallRectangle
                childList={item?.item_requested}
                quotationLoad={quotationLoad}
                typeSelect={selectedData}
              />
              <View style={{ paddingRight: 30 }}>
                <View style={styles.headTotalText}>
                  <Text style={styles.textTotal}>Total Items</Text>
                  <Text style={styles.textTotal}>{item.total_items}</Text>
                </View>
                {/* {
                                selectedData == 2 ? 
                                // <View style={{ flexDirection:'row', paddingBottom:5}}>
                                //     {/* onPress:={handleOnClick}  selectedTab */}
                {/* <RadioButton 
                                //         checked = {index === toggle}  
                                //         onPress={()=>onRadioHandle(index,item)}/>
                                //     <Text style={{fontSize:24,fontWeight:'bold',color:'#000000', paddingLeft:20}}>{'Preferred Vendor'}</Text> */}

                {/* </View>:null */}
                {/* } */}
              </View>
            </View>
          </SafeAreaView>
        )}
        ListEmptyComponent={
          listingData?.length === 0 &&
          !quotationLoad && (
            <View style={styles.mesContainer}>
              <Text style={styles.mesText}>
                Sorry!! No Quotation Data Available.
              </Text>
            </View>
          )
        }
      />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: StatusBar.currentHeight || 0,
    borderRadius: 1,
    borderColor: '#B07E9C',
    borderWidth: 1,
  },
  flatListContainer: {
    flex: 1,
    // top:20
    // marginTop: StatusBar.currentHeight || 0,
  },
  mesContainer: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mesText: {
    textAlign: 'center',
    fontSize: 18,
    ...getAppFont('Regular'),
    // fontFamily:'System',
    // color: '#898989',
  },
  subContainer: {
    paddingLeft: 20,
    paddingRight: 30,
    backgroundColor: theme.color.primary,
    // paddingTop:10,
  },

  borderLine: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  textTotal: {
    fontSize: Platform.isPad ? 26 : 24,
    fontWeight: 'bold',
    color: '#000000',
  },
  headTotalText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  vendorText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  headerText: {
    fontSize: Platform.isPad ? 18 : 15,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});

export default LargeRectangle;
