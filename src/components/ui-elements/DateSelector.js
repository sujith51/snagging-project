import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import { useTheme } from '../../utils';
import { theme } from '../../utils';

const DateSelector = ({
  Date,
  style,
  placeholder,
  viewStyle,
  icon,
  iconViewStyle,
  dateFontWeight,
  fontStyle,
  onChangeDate,
  dateFormat,minDate
}) => {
  const [dateState, setDateState] = useState(moment(Date).format('YYYY-MM-DD'));
  const theme = useTheme();
  const dateChange = (timeD) => {
    let date=moment(timeD).format('YYYY-MM-DD')
    setDateState(date);

    onChangeDate(date);
  };
  useEffect(() => {
    setDateState(moment(Date, 'YYYY-MM-DD'));
  }, [Date]);

  return (
    <DatePicker
      date={dateState}
      mode="date"
      placeholder={placeholder}
      style={style}
      format={dateFormat}
    
      minDate={minDate}
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      customStyles={{ dateInput: { opacity: 0 } }}
      iconComponent={
          <View style={{flexDirection:'row'}}>
          <View style={styles.subView}>
            {icon && (
              <View style={[iconViewStyle, styles.iconView]}>
                <View>
                  <Image
                    style={styles.orderContactIcon}
                    source={require('../../assets/icons/form-calendar.png')}
                  />
                </View>
              </View>
            )}
            <Text style={styles.textStyle}>
              {moment(dateState).format(dateFormat)}
            </Text>
          </View>
          </View>

      }
      onDateChange={(date) => dateChange(date)}
    />
  );
};

export default DateSelector;

const styles = StyleSheet.create({
  datePicker: {
    width: 50,
    marginTop: 20,
  },
  textStyle: {
    paddingLeft: '7%',
    padding: 10,
    fontSize: 12,
      color:theme.color.primaryWhite
  },
  subView: {
    flexDirection: 'row',
    alignItems: 'center',
      flex:1,
      backgroundColor:theme.color.primary,
      borderRadius: 10
  },
  iconView: {
    backgroundColor: 'white',
    width: 40,
    height: 40,
    borderRadius: 10,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 10,
    shadowColor: '#636363',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  orderContactIcon: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
  },
});
