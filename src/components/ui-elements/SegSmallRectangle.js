import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar } from 'react-native';

const SegSmallRectangle = ({childList,quotationLoad=false, typeSelect}) => {
    const listChild = childList ? Object.values(childList) : []
  return (
    <SafeAreaView style={styles.container}>
        <View >
            <FlatList
                data={listChild}
                keyExtractor={(item, index) => `child-key-${index}`}
                renderItem={({ item ,index}) => ( 
                    <>
                        <Text style={styles.itemHeadText}>{`${index+1 + '.' +' '+ item.item_name}`}</Text>
                        <View style={styles.renderText}>
                            <View style={styles.itemMainHead}>
                                <Text style={styles.itemHeadText}>{'QTY'}</Text>
                                <Text style={styles.itemText}>{item.item_quantity}</Text>
                            </View>
                            <View style={styles.itemMainHead}>
                                <Text style={styles.itemHeadText}>{'Issue Type'}</Text>
                                {item.task_type === '2' && <Text style={styles.itemText}>Minor</Text>}
                                {item.task_type === '1' && <Text style={styles.itemText}>Major</Text>}
                            </View>
                            <View style={styles.itemMainHead}>
                                <Text style={styles.itemHeadText}>{'Paid By'}</Text>
                                 {item.pay_by === '3' && <Text style={styles.itemText}>Owner</Text>}
                                 {item.pay_by === '4' && <Text style={styles.itemText}>Tenant</Text>}
                            </View> 
                            {typeSelect==2 ?  
                                <View style={styles.itemMainHead}>
                                    <Text style={styles.itemHeadText}>{'Price'}</Text>
                                    <Text style={styles.itemText}> {`AED ${item.price}`}</Text>
                                </View>
                                :
                                null
                            }
                           
                        </View>
                    </>
                    
                    
                )}
            />
        </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    renderText: {
        borderRadius:1,
        borderColor:'#B07E9C',
        borderWidth:1,
        padding: 10,
        marginVertical: 8,
        marginRight:15,
        flexDirection:'column',
        paddingLeft:20
    },
    itemHeadText:{
        fontSize:15,
        fontWeight:'bold',
        color:'#000000'
    },
    itemText:{
        fontSize:15,
        fontWeight:'normal' ,
        color:'#000000'
    },
    itemMainHead:{
        flexDirection:'row',
        justifyContent:'space-between', 
        paddingBottom:10
    }
});

export default SegSmallRectangle;




 