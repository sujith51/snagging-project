import React, { useState, useRef } from 'react';
import { Text, View, Dimensions, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
// import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { theme } from '../../utils/theme';
import { SuccessIcon } from '../../assets/svg_components';





const StatusPopModel = ({ message, status}) => {    
    return (
        <Modal
            isVisible={true}
            style={style.modalView}
            // deviceHeight={350}
            // deviceWidth={350}
            animationInTiming={200}
            backdropTransitionInTiming={100}
            animationOutTiming={200}
            backdropTransitionOutTiming={100}
            animationIn="slideInUp"
            animationOut="slideOutDown"
            onRequestClose={false}  //props.closeModal ,
            onBackdropPress={false}
            useNativeDriver={true}
            onSwipeComplete={() => console.log()}
            // backdropColor={'red'}
            swipeDirection="left">
            <View style={{flex: 1}}>
                <Text style={{color:'#00000'}}>I am the modal content!</Text>
            </View>
            <View style={style.container}>
        <View style={style.buttonView}>
          <View style={style.buttonSubView}>
            <View style={style.successView}>
            </View>
            <View style={{ marginVertical: 20 }}>
              <Text style={style.successTittletext}>{'testing'}</Text>
            </View>
            <View>
              <Text style={style.successMsgText}>{'testing'}</Text>
            </View>
          </View>
        </View>
      </View>
    </Modal>
    );
};

export default StatusPopModel;


const style = StyleSheet.create({
  container: {
    flex: 0,
    flexBasis: 'auto',
    minHeight: 310,
    width: 280,
    padding: 10,
    margin: 'auto',
    borderRadius: 10,
    borderWidth: 1,
    backgroundColor: theme.color.primary
  },
  modalView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonView: {
    alignItems: 'center',
  },
});
