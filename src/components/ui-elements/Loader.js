import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import ReactNativeModal from 'react-native-modal';

const Loader = ({ open, close = () => {} }) => {
  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={[styles.container, { backgroundColor: 'rgba(0,0,0,0.1)' }]}
      animationIn="zoomIn"
      animationOut="zoomOut"
    >
      <View
        style={{
          position: 'absolute',
          top: '50%',
          left: '45%',
        }}
      >
        <ActivityIndicator color="#999999" size="large" />
      </View>
    </ReactNativeModal>
  );
};
export default Loader;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // borderRadius: 20,
  },
});
