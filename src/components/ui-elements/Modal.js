
import React from 'react';
import { View, StyleSheet, ScrollView,TouchableOpacity,Image } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import Text from '../Text';
import { useTheme, getAppFont,numberFormat } from '../../utils/index';
import Button from '../Button';
import { HeaderIcon, Cancel } from '_svg';
const Modal = ({open, close, title,body}) => {
    const theme = useTheme();
    return (
        <ReactNativeModal
            isVisible={open}
            onBackdropPress={close}
            onBackButtonPress={close}
            style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
            animationIn="zoomIn"
            animationOut="zoomOut"
        >
            <View style={{flex:1}}>
               <View style={styles.header}>
                   <TouchableOpacity
                        onPress={close}

                   >
                       <Image
                           source={require('../../assets/icons/ic_close.png')}
                           style={styles.image}
                       />
                   </TouchableOpacity>
                   <Text style={styles.titleLabel}>
                       {title}
                   </Text>
               </View>
                <View>
                    {body}
                </View>
            </View>
        </ReactNativeModal>
    );
};

export default Modal;

const styles = StyleSheet.create({
    container: {
        borderRadius: 10,

        marginTop: 50,
        marginBottom:50
    },
    header:{flexDirection:'row',alignItems:'center', paddingLeft: 15,paddingVertical:15},
    image: {
        height: 15,
        width: 15,
    },
    wrapper: {
        paddingBottom: 15,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        marginBottom: 10,
        borderWidth: 0.3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 3,
        paddingTop: 15,
    },
    viewStyle: {
        marginLeft: 10,
        marginRight: 10,
    },
    row:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingBottom:5
    },
    titleLabel: {
        fontSize: 17,
        fontWeight: '500',
        textAlign:'center',flex:1,paddingLeft:5
    },
});
