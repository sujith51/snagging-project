/* eslint-disable react-native/no-color-literals */
/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  createRef,
  useRef,
  useLayoutEffect,
  useCallback,
  useEffect,
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { FlatList } from 'react-native-gesture-handler';
import { Transition, Transitioning } from 'react-native-reanimated';
import { useTheme } from '../../utils';
import { getAppFont } from '../../utils/theme';
import { DropDownIcon, MoreIcon } from '../../assets/svg_components';

const MoreMenu = ({
  value,
  options,
  // placeholder = 'Select',
  onOpen = () => {},
  onClose = () => {},
  // placeHolderStyle = null,
  isOpen = false,
}) => {
  const theme = useTheme();
  // :States
  const [open, setOpen] = useState(false);
  const [animatedOpen, setAnimatedOpen] = useState(false);
  const dropdownRef = useRef(createRef());
  const [dropdownDim, setDropDownDim] = useState(null);
  const [selected, setSelected] = useState(null);
  const dropdownContainerRef = useRef(null);
  // :Effects
  useLayoutEffect(() => {
    dropdownRef.current.measureInWindow((x, y, width, height) => {
      if (!dropdownDim || dropdownDim.x !== x || dropdownDim.y !== y) {
        setDropDownDim({ x, y, width, height });
      }
    });
  });

  useEffect(() => {
    if (isOpen) {
      onLabelClick();
      setAnimatedOpenFn(true);
    } else {
      setAnimatedOpenFn(false);
    }
  }, [isOpen]);

  // :Functions

  const transition = (
    <Transition.Together>
      {/* <Transition.Out type="slide-top" /> */}
      {/* <Transition.In interpolation="easeInOut" durationMs={200} /> */}
      <Transition.Change interpolation="easeInOut" durationMs={100} />
    </Transition.Together>
  );

  const setAnimatedOpenFn = (opened) => {
    if (opened) {
      setOpen(true);
      setAnimatedOpen(true);
    } else {
      setAnimatedOpen(false);
      setTimeout(() => {
        setOpen(false);
      }, 100);
    }
    dropdownContainerRef.current?.animateNextTransition();
  };

  const onLabelClick = () => {
    setAnimatedOpenFn(true);
    onOpen();
  };

  const requestClose = () => {
    setAnimatedOpenFn(false);
    // onClose(selected);
  };

  const onSelect = useCallback(
    (item) => {
      setAnimatedOpenFn(false);
      setSelected(item);
      onClose(item);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [selected],
  );

  // :Render
  return (
    /*  */
    <>
      <View>
        <TouchableWithoutFeedback onPress={() => onLabelClick()}>
          <View
            style={styles.container}
            ref={(el) => (dropdownRef.current = el)}
            collapsable={false}
          >
            {/* {selected?.icon ? (
              <View style={styles.rowView}>
                <Image
                  source={selected?.icon ?? value ?? placeholder}
                  style={styles.imageStyle}
                />
                <Text style={styles.label}>
                  {selected?.label ?? value ?? placeholder}
                </Text>
              </View>
            ) : (
              <View>
                <Text style={[styles.label, placeHolderStyle]}>
                  {selected?.label ?? value ?? placeholder}{' '}
                  {count && `(${count})`}
                </Text>
              </View>
            )} */}
            <View>
              <MoreIcon width={12} height={20} />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
      <ReactNativeModal
        isVisible={open}
        onBackdropPress={requestClose}
        onBackButtonPress={requestClose}
        backdropColor="transparent"
        style={{ padding: 0, margin: 0 }}
      >
        <View
          style={[
            styles.dropdownContainer,
            {
              top: dropdownDim?.y + dropdownDim?.height,
              left: dropdownDim?.x - 80,
              width: 'auto',
              backgroundColor: 'white',
              ...theme.cardShadow,
            },
          ]}
        >
          <FlatList
            // contentContainerStyle={!open && { height: 0 }}
            data={options}
            keyExtractor={(item,index) => `dropDown-key-${index}`}
            renderItem={({ item }) => (
              <View>
                {item.icon ? (
                  <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                    <View style={[styles.rowView, { paddingHorizontal: 12 }]}>
                      <Image source={item.icon} style={styles.imageStyle} />
                      <Text style={styles.dropdownTxtStyle}>{item.label}</Text>
                    </View>
                  </TouchableWithoutFeedback>
                ) : (
                  <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                    <View style={{ padding: 5 }}>
                      <Text style={styles.dropdownTxtStyle}>{item.label}</Text>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              </View>
            )}
          />
        </View>
      </ReactNativeModal>
    </>
  );
};

export default MoreMenu;

const styles = StyleSheet.create({
  container: {
    // flexDirection: 'row',
    paddingVertical: 7,
    paddingHorizontal: 12,
    // borderWidth: 1.5,
    borderRadius: 6,
    alignItems: 'center',
    // flex: 1,
    right: 10,
    top: 10,
    // backgroundColor: 'red',
  },
  label: { textAlign: 'center', marginTop: 3 },
  dropdownTxtStyle: {
    ...getAppFont('Medium'),
  },
  dropdownContainer: {
    position: 'absolute',
    // top: 40,
    // right: 20,
    // backgroundColor: theme.colors.white,
    // ...theme.cardShadow,
    paddingBottom: 7,
    borderRadius: 5,
    padding: 7,
  },
  dropdownTxtStyle: {
    ...getAppFont('Medium'),
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageStyle: {
    height: 30,
    width: 40,
    marginRight: 10,
  },
});
