/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-underscore-dangle */
import React, { useEffect,useState } from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  Animated,
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native';
import PropTypes from 'prop-types';
import { theme } from '../../utils';
import styles from './styles';
const SummaryPanel=(props)=>{
  const [title,setTitle]=useState(props.title);
  const [animation,setAnimation]=useState(new Animated.Value(props.isActive ? 1 : 0));
    const [minHeight,setMinHeight]=useState('');
    const [maxHeight,setMaxHeight]=useState('');

    useEffect(()=>{
        Animated.spring(animation, {
            toValue: props.isActive ? 1 : 0,
            useNativeDriver: false,
        }).start();
    },[props.isActive])
   const setMaxHeightView=(event) =>{
       setMaxHeight(event.nativeEvent.layout.height)
    }

    const setMinHeightView =(event)=> {
        setMinHeight(event.nativeEvent.layout.height)
    }

    const toggle=() =>{
        props.onToggle(props.title);
    }
    let height = 0;
    if (minHeight && maxHeight) {
        height = animation.interpolate({
            inputRange: [0, 1],
            outputRange: [minHeight, minHeight + maxHeight],
        });
    }
    return (
        <Animated.View
            style={[
                styles.container,
                {
                    height,
                },
            ]}
        >
          <TouchableWithoutFeedback onPress={toggle}>
            <View style={[styles.titleContainer]} onLayout={setMinHeightView}>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.title}>{title}</Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.body} onLayout={setMaxHeightView}>
              {props.children}
          </View>
        </Animated.View>
    );
}

// SummaryPanel.propTypes = {
//   title: PropTypes.string.isRequired,
//   children: PropTypes.array.isRequired,
//   onToggle: PropTypes.func.isRequired,
//   isActive: PropTypes.bool,
// };
//
// SummaryPanel.defaultProps = {
//   isActive: false,
// };

export default React.memo(SummaryPanel);
