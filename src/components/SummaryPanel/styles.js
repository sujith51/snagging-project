import { StyleSheet } from 'react-native';
import { theme } from '../../utils';

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.color.primaryWhite,
    overflow: 'hidden',
    margin: 0,
    marginBottom: 5,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 1,
    paddingLeft: 10,
    margin: 0,
    height: 33,
    alignItems: 'center',
    backgroundColor: theme.color.secondaryLight3,
    // borderWidth: 1,
    // borderColor: 'yellow',
    borderBottomWidth: 0,
    // borderRadius: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 17,
    color: theme.color.primaryWhite,
  },
  titleNormal: {
    fontSize: 17,
    color: theme.color.primaryWhite,
  },

  iconStyle: {
    margin: 10,
  },

  body: {
    padding: 0,
    paddingTop: 0,
    borderWidth: 0.5,
    borderColor: theme.color.primaryWhite,
    // borderTopColor: 'blue',
    borderTopWidth: 0,
  },
  arrowStyle: { color: theme.color.primaryWhite, marginRight: 5 },
});

export default styles;
