/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-unused-styles */
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import {
  getQuotationList,
  genSegReport,
  getCommunityList,
} from '../redux/actions';
import { useTheme, getAppFont, returnWithoutStatic } from '../utils';
import TabView from './ui-elements/TabView';
import LargeRectangle from './ui-elements/LargeRectangle';
import CommonContainer from './CommonContainer';
import Text from './Text';
import Button from './Button';
import Spinner from './ui-elements/Spinner';
import SelectCommunity from './SelectCommunity';
import { NotificationIcon } from '../assets/svg_components';

const QuotationRequested = ({ item }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { navigate } = useNavigation();

  const [selected, selectedTab] = useState(1);
  const [communities, setCommunities] = useState([]);
  const [units, setUnits] = useState([]);
  const [selectedVendor, setselectedVendor] = useState();
  const [getToggleData, setToggleData] = useState(false);
  const [inValid, setInValid] = useState(false);

  const tabTitles = [
    {
      name: 'Quotation Requested',
      value: 1,
      type: 'quotation_requested',
      activeBar: theme.color.primary,
    },
    {
      name: 'Quotation Received',
      value: 2,
      type: 'quotation_received',
      activeBar: theme.color.primary,
    },
  ];

  const {
    quotationList: { listData, quotationLoading, quotationListError },
  } = useSelector((state) => state.quotation);

  const { data, selectCommunity, communityList, selectUnit } = useSelector(
    (state) => state.user,
  );

  // : Effects
  useEffect(() => {
    if (data?.id_user && data?.id_user_type) {
      dispatch(
        getCommunityList({
          id_user: data?.id_user,
          id_user_type: data?.id_user_type,
        }),
      );
    }
  }, []);

  useEffect(() => {
    const commun = [];
    if (communityList?.data) {
      Object.keys(communityList?.data).forEach((key) =>
        commun.push({
          value: key,
          label: communityList?.data[key],
        }),
      );
      setCommunities(commun);
    }
  }, [communityList?.data]);

  useEffect(() => {
    refreshData()
    // else if (selectCommunity) {
    //   listData['accessCommunityIds[0]'] = selectCommunity?.value;
    //   listData['id_role'] = 16; //id_role
    //   listData['status'] =
    //     selected === 1 ? 'quotation_requested' : 'quotation_received';
    //   dispatch(getQuotationList(listData));
    // }
  }, [dispatch, selectCommunity, selectUnit, selected]);

 const refreshData = () =>{
  const listData = {};
  if (selectCommunity?.length > 0) {
    listData['accessCommunityIds[0]'] = returnWithoutStatic(selectCommunity);
    listData['id_role'] = 16; //id_role
    listData['status'] =
      selected === 1 ? 'quotation_requested' : 'quotation_received';
    dispatch(getQuotationList(listData));
  }
 }

  const onSubmit = () => {
    if (selected === 2) {
      if (getToggleData && !inValid) {
        const listData = {};
        listData['quote_code'] = selectedVendor?.quote_code;
        listData['id_property'] = selectUnit?.value;
        listData['unit_name'] = selectUnit?.label;
        listData['vendor_id'] = selectedVendor?.vendor_id;
        listData['id_user'] = data.id_user;
        listData['quote_status'] = '0';
        dispatch(genSegReport(listData));
        setInValid(false);
        navigate('SegregationReport', { selectedVendor, selectUnit });
      } else {
        setInValid(true);
        Alert.alert('Please select one quotation !');
      }
    } else {
      navigate('Dashboard');
    }
  };
  const onSelectVendor = (item, toggleData) => {
    setselectedVendor(item);
    setToggleData(toggleData);
  };

  const containerProps = {
    navbar: {
      title: 'Quotations',
      actions: [
        {
          icon: <NotificationIcon />,
          onPress: () => navigate('Notifications'),
        },
      ],
    },
    scrollEnabled: true,    
    onRefresh : () =>{
      refreshData()
    }
  };

  return (
    <CommonContainer {...containerProps}>
      {communities?.length > 0 && (
        <SelectCommunity units={units} communities={communities} />
      )}
      <View style={styles.container}>
        {selectCommunity?.length >0 ? (
          <View>
            <View style={{ paddingTop: 20 }}>
              <TabView
                onTabChange={selectedTab}
                tabs={tabTitles}
                selected={selected}
              />
            </View>
            <View style={{ paddingTop: 30 }}>
              <LargeRectangle
                selectedData={selected}
                headerStyle={selected == 2 ? 8 : 0}
                listingData={listData}
                quotationLoad={quotationLoading}
                headTextStyle={selected == 2 ? 10 : 4}
                onVendorSelect={(item, toggle) => onSelectVendor(item, toggle)}
              />
            </View>
            <View
              style={[
                styles.buttonView,
                selected == 1
                  ? { paddingHorizontal: 45, paddingVertical: 20 }
                  : null,
              ]}
            >
              {/* {listData != null && (
                <Button
                  titleStyle={styles.buttonTxt}
                  title={selected == 2 ? 'Generate Segregation Report' : 'Done'}
                  style={styles.buttonContainer}
                  color={theme.color.red}
                  onPress={() => onSubmit()}
                />
              )} */}
            </View>
          </View>
        ) : (
          <View style={styles.mesContainer}>
            <Text variant="textRegular" style={styles.mesText}>
              Please Select Community for Quotation Data!
            </Text>
          </View>
        )}
      </View>
      <Spinner visible={quotationLoading} size="large" animationType="none" />
    </CommonContainer>
  );
};

export default QuotationRequested;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'column',
  },
  buttonView: {
    paddingTop: 40,
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 100,
  },
  buttonTxt: {
    ...getAppFont('Regular'),
    fontSize: 18,
    // fontWeight:'bold',
    color: '#FFFFFF',
  },
  mesContainer: {
    flex: 1,
    paddingTop: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mesText: {
    textAlign: 'center',
    // fontSize: 18,
    // fontFamily:'System',
    // color: '#898989',
  },
});
