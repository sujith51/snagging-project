import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import { useFocusEffect } from '@react-navigation/native';
import InputText from './InputText';
import Text from './Text';
import Button from './Button';
import CounterWrapper from './CounterWrapper';
import { useTheme, getAppFont, config } from '../utils';
import { Ticked } from '../assets/svg_components';

const { width } = Dimensions.get('window');
const counterWidth = width * 0.5;

const RequestModal = ({
  open,
  close,
  openList,
  saveNewMaterial = () => {},
  tableHeadTxtStyle = {},
  payBy = [],
  data = [],
  openAddMaterial = () => {},
  materials,
  allMaterials = [],
}) => {
  const theme = useTheme();
  const [item, setItem] = useState('');
  const [make, setMake] = useState('');
  const [specification, setSpecification] = useState('');
  const [quantity, setQuantity] = useState(0);
  const [qtyOwner, setOwner] = useState('');
  const [qtyTenant, setTenant] = useState('');
  const [description, setDescription] = useState('');
  const [successMsg, setSuccessMsg] = useState(false);
  const [errMsg, setErrMsg] = useState(false);
  const [shouldSubmit, setShouldSubmit] = useState(false);
  const [search, setSearch] = useState(false);
  const [blured, setBlured] = useState(false);
  const [showError, setShowError] = useState(false);
  const [filterData, setFilterData] = useState([]);
  const itemRef = useRef();
  const makeRef = useRef();
  const specifyRef = useRef();
  const descriptRef = useRef();
  const [list, setList] = useState([]);

  useEffect(() => {
    if (open) {
      setList([]);
      setList(data?.newMaterial || []);
    }
  }, [open, data]);

  useEffect(() => {
    if (shouldSubmit) {
      saveMaterial(list);
      setShouldSubmit(false);
    }
  }, [list, saveMaterial, shouldSubmit]);

  useEffect(() => {
    setItem(materials?.item_request ? materials?.item_request : '');
  }, [materials]);
  useEffect(() => {
    const timer = setTimeout(
      () => (successMsg ? setSuccessMsg(false) : null),
      2000,
    );
    return () => clearTimeout(timer);
  }, [successMsg]);

  useEffect(() => {
    const timer = setTimeout(() => (errMsg ? setErrMsg(false) : null), 2000);
    return () => clearTimeout(timer);
  }, [errMsg]);

  const addQuantity = () => {
    setQuantity(quantity + 1);
  };
  const minusQuantity = () => {
    setQuantity(quantity - 1);
  };

  const handleinputQty = (inp) => {
    const NumOnly = inp.replace(/[^0-9]/g, '');
    setQuantity(Number(NumOnly));
  };

  useEffect(() => {
    // if (qtyOwner !== '' && quantity !== null && qtyTenant !== '') {
    if (quantity !== null) {
      setOwner('');
      setTenant('');
    }
    if (payBy === 3) {
      setOwner(quantity);
      setTenant(0);
    }
    if (payBy === 4) {
      setTenant(quantity);
      setOwner(0);
    }
  }, [quantity]);

  const handleOwner = (val) => {
    const NumOnly = val.replace(/[^0-9]/g, '');
    if (NumOnly !== '' && NumOnly <= quantity) {
      setOwner(Number(NumOnly));
      setTenant(quantity - Number(NumOnly));
    } else if (NumOnly == '' && (qtyTenant !== 0 || qtyTenant == '')) {
      setOwner('');
      setTenant('');
    } else {
      alert('Please enter less than Quantity');
      setOwner('');
      setTenant('');
    }
  };
  const handleTenant = (val) => {
    const NumOnly = val.replace(/[^0-9]/g, '');
    if (NumOnly !== '' && NumOnly <= quantity) {
      setTenant(Number(NumOnly));
      setOwner(quantity - Number(NumOnly));
    } else if (NumOnly == '' && (qtyOwner !== 0 || qtyOwner == '')) {
      setOwner('');
      setTenant('');
    } else {
      alert('Please enter less than Quantity');
      setOwner('');
      setTenant('');
    }
  };
  // const handleOwner = (val) => {
  //   const NumOnly = val.replace(/[^0-9]/g, '');

  //   if (NumOnly !== '' && NumOnly <= quantity && qtyTenant !== 0) {
  //     setOwner(Number(NumOnly));
  //     setTenant(quantity - Number(NumOnly));
  //   } else if (NumOnly !== '' && NumOnly <= quantity && qtyTenant === '') {
  //     setOwner(Number(NumOnly));
  //     setTenant(quantity - Number(NumOnly));
  //   } else if (
  //     NumOnly === '' &&
  //     qtyTenant !== null &&
  //     NumOnly <= quantity - qtyTenant
  //   ) {
  //     setOwner('');
  //     setTenant('');
  //   } else if (NumOnly > quantity || NumOnly >= quantity - qtyTenant) {
  //     setOwner('');
  //     alert('Please enter less than Quantity');
  //   } else {
  //     setOwner('');
  //     // alert('Please enter less than Quantity')
  //   }
  // };

  // const handleTenant = (val) => {
  //   const NumOnly = val.replace(/[^0-9]/g, '');

  //   if (NumOnly !== '' && NumOnly < quantity && qtyOwner !== 0) {
  //     setTenant(Number(NumOnly));
  //     setOwner(quantity - Number(NumOnly));
  //   } else if (NumOnly !== '' && NumOnly <= quantity && qtyOwner === '') {
  //     setTenant(Number(NumOnly));
  //     setOwner(quantity - Number(NumOnly));
  //   } else if (
  //     NumOnly === '' &&
  //     qtyOwner !== null &&
  //     NumOnly <= quantity - qtyOwner
  //   ) {
  //     setOwner('');
  //     setTenant('');
  //   } else if (NumOnly > quantity || NumOnly >= quantity - qtyOwner) {
  //     setTenant('');
  //     alert('Please enter less than Quantity');
  //   } else {
  //     setTenant('');
  //     // alert('Please enter less than Quantity')
  //   }
  // };

  const onAdd = () => {
    if (item && specification && quantity && qtyOwner && payBy === 3) {
      const obj = {
        checked: true,
        item,
        // make,
        specification,
        quantity,
        description,
        qtyOwner,
        qtyTenant,
      };
      setList([...list, obj]);
      setItem('');
      // setMake('');
      setSpecification('');
      setQuantity(0);
      setDescription('');
      setSuccessMsg(true);
      setTenant('');
      setOwner('');
      setShouldSubmit(true);
      return;
    }
    if (item && specification && quantity && qtyTenant && payBy === 4) {
      const obj = {
        checked: true,
        item,
        // make,
        specification,
        quantity,
        description,
        qtyOwner,
        qtyTenant,
      };
      setList([...list, obj]);
      setItem('');
      // setMake('');
      setSpecification('');
      setQuantity(0);
      setDescription('');
      setSuccessMsg(true);
      setTenant('');
      setOwner('');
      setShouldSubmit(true);
      return;
    }
    if (
      item &&
      specification &&
      quantity &&
      qtyTenant + qtyOwner > 0 &&
      payBy === 5
    ) {
      const obj = {
        checked: true,
        item,
        // make,
        specification,
        quantity,
        description,
        qtyOwner,
        qtyTenant,
      };
      setList([...list, obj]);
      setItem('');
      // setMake('');
      setSpecification('');
      setQuantity(0);
      setDescription('');
      setSuccessMsg(true);
      setTenant('');
      setOwner('');
      setShouldSubmit(true);
    } else {
      setErrMsg(true);
    }
  };
  const onSubmit = () => {
    openList(list);
  };

  const saveMaterial = useCallback(() => {
    saveNewMaterial(list);
  }, [list, saveNewMaterial]);

  const searchItem = (text) => {
    const isExist =
      data?.newMaterial?.length > 0 &&
      data?.newMaterial?.filter((val) => val?.item == text);
    setShowError(false);
    if (isExist?.length > 0) {
      setShowError(true);
      // return;
    }

    setItem(text);
    if (text) {
      setSearch(true);
      const setData = allMaterials?.filter((material) => {
        const data1 = material?.item_request
          ? material.item_request.toUpperCase()
          : '';
        const textData = text.toUpperCase();
        return data1.indexOf(textData) > -1;
      });
      setFilterData(setData);
    } else {
      setSearch(false);
      setFilterData(allMaterials);
    }
  };
  const setOnBlur = (va) => {
    setTimeout(() => setSearch(false), 1000);
  };
  const selectText = (val) => {
    setItem(val?.item_request);
    setSearch(false);
  };
  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
    >
      <ScrollView style={styles.scroolStyle}>
        <Text variant="bodyBold" size={21} style={styles.headerStyle}>
          Request for Material*
        </Text>
        {data?.taskType == 0 || !data?.Vendor?.value || data?.payBy == null ? (
          <Text style={styles.warStyle}>
            Select {!data?.Vendor?.value && "'Vendor'"}
            {data?.taskType == 0 && " 'Issue Type'"}
            {!data?.payBy && " 'Paid By'"} to see Material Request
          </Text>
        ) : (
          <>
            <Button
              title="Added Material List"
              type="primary"
              titleStyle={{ ...getAppFont('Regular') }}
              style={styles.button1}
              onPress={openAddMaterial}
            />
            <View style={{ height: 20 }}>
              {showError && (
                <Text variant="bodyBold" size={12} style={{ color: 'red' }}>
                  Material already exists with same name
                </Text>
              )}
            </View>

            <Text variant="bodyBold" size={18} style={styles.mt12}>
              Item Name*
            </Text>
            <View style={{ position: 'relative' }}>
              <InputText
                type="default"
                value={item}
                onChangeText={(e) => searchItem(e)}
                innerRef={itemRef}
                onSubmitEditing={() => {
                  specifyRef.current.focus();
                  setSearch(false);
                }}
                textFieldContainerStyle={styles.borRadius}
                onBlur={(e) => setOnBlur(e)}
              />
            </View>
            {search && filterData?.length > 0 ? (
              <View style={styles.absolute}>
                {filterData.map((val, index) => {
                  return (
                    <TouchableOpacity onPress={() => selectText(val)}>
                      <Text
                        style={{
                          paddingVertical: 7,
                          paddingLeft: 5,
                          backgroundColor: index % 2 == 1 ? 'white' : '#ddd',
                          height: 40,
                        }}
                      >
                        {val?.item_request}
                      </Text>
                    </TouchableOpacity>
                  )
                })}
              </View>
            ) : (
              <View>
                <Text variant="bodyBold" size={18} style={styles.mt12}>
                  Specification (Size/color/style/model)*
                </Text>
                <InputText
                  type="default"
                  value={specification}
                  onChangeText={(text) => setSpecification(text)}
                  textFieldContainerStyle={styles.borRadius}
                  innerRef={specifyRef}
                />
                <Text variant="bodyBold" size={18} style={styles.mt12}>
                  Quantity*
                </Text>
                <CounterWrapper
                  width={counterWidth}
                  quantity={quantity}
                  onIncrement={addQuantity}
                  onDecrement={minusQuantity}
                  inputQty={handleinputQty}
                />

                {payBy === 5 && (
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 20,
                      // paddingHorizontal: 30,
                    }}
                  >
                    <View>
                      <Text
                        variant="bodyBold"
                        size={18}
                        style={tableHeadTxtStyle}
                      >
                        Owner
                      </Text>
                      <InputText
                        type="default"
                        value={qtyOwner !== '' ? String(qtyOwner) : ''}
                        onChangeText={(text) => handleOwner(text)}
                        textFieldContainerStyle={styles.borRadius1}
                        style={{ height: 40 }}
                        // innerRef={specifyRef}
                      />
                    </View>
                    <View style={{ left: 50 }}>
                      <Text
                        variant="bodyBold"
                        size={18}
                        style={tableHeadTxtStyle}
                      >
                        Tenant
                      </Text>
                      <InputText
                        type="default"
                        value={qtyTenant !== '' ? String(qtyTenant) : ''}
                        onChangeText={(text) => handleTenant(text)}
                        textFieldContainerStyle={styles.borRadius1}
                        style={{ height: 40 }}
                        // innerRef={specifyRef}
                      />
                    </View>
                    {/* <Text>Both </Text> */}
                  </View>
                )}
                <Text variant="bodyBold" size={18} style={styles.mt12}>
                  Description
                </Text>

                <InputText
                  type="textArea"
                  value={description}
                  textFieldContainerStyle={styles.borRadius}
                  onChangeText={(text) => setDescription(text)}
                  innerRef={descriptRef}
                />
                {successMsg && (
                  <View style={styles.successView}>
                    <View
                      style={[
                        styles.circle,
                        { backgroundColor: theme.color.green },
                      ]}
                    >
                      <Ticked height={10} />
                    </View>
                    <Text
                      variant="bodyBold"
                      size={16}
                      style={styles.successTxt}
                      color={theme.color.green}
                    >
                      Material added successfully
                    </Text>
                  </View>
                )}
                {errMsg && (
                  <View style={styles.successView}>
                    <Text
                      variant="bodyBold"
                      size={16}
                      style={styles.successTxt}
                      color={theme.color.red}
                    >
                      *Please add all the required fields
                    </Text>
                  </View>
                )}
                <Button
                  disabled={showError}
                  title="Add"
                  type="primary"
                  titleStyle={{ ...getAppFont('Regular') }}
                  style={styles.button1}
                  onPress={onAdd}
                />
                <Button
                  title="Finish"
                  type="primary"
                  disabled={item || specification || quantity}
                  titleStyle={{ ...getAppFont('Regular') }}
                  style={[styles.button1, { marginBottom: 30 }]}
                  onPress={onSubmit}
                />
              </View>
            )}
          </>
        )}
      </ScrollView>
    </ReactNativeModal>
  );
};

export default RequestModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 20,
    marginTop: 20,
  },
  mt12: {
    marginTop: 12,
    marginBottom: 9,
  },
  absolute: {

    zIndex: 5,
    maxHeight: 240,
    overflow: 'hidden',
    borderLeftWidth: 0.5,
    borderLeftColor: 'black',
    borderRightWidth: 0.5,
    borderRightColor: 'black',
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
    borderRadius: 5,
  },
  headerStyle: {
    marginBottom: 25,
    paddingTop: 5,
  },
  button1: {
    marginBottom: 20,
    borderRadius: 10,
  },
  scroolStyle: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 20,
    // paddingBottom: 40,
  },
  successView: {
    alignItems: 'center',
    flexDirection: 'row',
    // paddingRight: 10,
  },
  successTxt: {
    marginTop: 5,
    marginBottom: 10,
    paddingLeft: 7,
    top: 2,
  },
  dropDownContainer: {
    justifyContent: 'space-between',
    left: 0,
    height: 46,
    borderRadius: 10,
  },
  circle: {
    width: 25,
    height: 25,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  borRadius: {
    borderRadius: 10,
    paddingHorizontal: 10,
  },
  borRadius1: {
    borderRadius: 8,
    width: config.DEVICE_WINDOW_WIDTH / 2 - 100,
    paddingHorizontal: 10,
    marginTop: 2,
  },
});
