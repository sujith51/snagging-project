/* eslint-disable react-native/no-inline-styles */
import React, { useState, createRef } from 'react';
import { StyleSheet, View, ActivityIndicator, Platform } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import SignatureCapture from 'react-native-signature-capture';
// import InputText from './InputText';
import Text from './Text';
import Button from './Button';
import {
  useTheme,
  getAppFont,
  services,
  getFullUrlPM,
  endpoints,
} from '../utils';

const SignModal = ({ open, close, saveSign }) => {
  const theme = useTheme();
  const sign = createRef();
  const [signatureUrl, setSignatureUrl] = useState(null);
  const [signEnable, setSignEnable] = useState(false);
  const [signLoader, setSignLoader] = useState(false);

  const signatureSaveClicked = () => {
    sign.current?.saveImage();
  };

  const resetImageClicked = () => {
    sign.current?.resetImage();
    setSignatureUrl(null);
    setSignEnable(false);
  };

  const onSaveEvent = (result) => {
    const data = {
      image: result.encoded,
    };
    if (result.encoded) {
      setSignLoader(true);
      services
        .post(getFullUrlPM(endpoints.uploadBase64Image), data)
        .then((res) => {
          if (res?.status?.toUpperCase() === 'SUCCESS' && res.data) {
            setSignatureUrl(res?.data?.image_url);
            setSignLoader(false);
            saveSign(res?.data?.image_url);
            close();
          } else {
            setSignLoader(false);
            alert('Something went wrong!!');
          }
        });
    }
  };

  const onDragEvent = () => {
    setSignEnable(true);
  };

  return (
    <ReactNativeModal
      isVisible={open}
      onBackdropPress={close}
      onBackButtonPress={close}
      style={[styles.container, { backgroundColor: theme.color.primaryWhite }]}
    >
      <Text variant="bodyBold" size={21} style={styles.headerStyle}>
        Draw Signature below
      </Text>
      <View
        style={{
          flex: 1,
          //   height: 300,
          //   padding: 20,
          borderWidth: 2,
          borderColor: theme.color.secondaryLight2,
          borderRadius: theme.borderRadius.x,
        }}
      >
        {signLoader && <ActivityIndicator style={{ paddingTop: 60 }} />}
        {!signLoader ? (
          <SignatureCapture
            style={[
              styles.signature,
              {
                flex: 1,
                // height: 300,
                // borderWidth: 2,
                //   borderColor: theme.color.secondaryLight2,
                // borderRadius: theme.borderRadius.x,
              },
            ]}
            ref={sign}
            onSaveEvent={onSaveEvent}
            onDragEvent={onDragEvent}
            showNativeButtons={false}
            //   showTitleLabel={false}
            viewMode="portrait"
          />
        ) : null}
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}
      >
        <View style={styles.buttonView}>
          <Button
            title="Save Signature"
            type="primary"
            titleStyle={{ ...getAppFont('Bold') }}
            style={styles.button1}
            onPress={signatureSaveClicked}
          />
        </View>
        <View style={styles.buttonView}>
          <Button
            title="Reset"
            style={styles.button1}
            onPress={resetImageClicked}
            type="primary"
            titleStyle={{ ...getAppFont('Bold') }}
          />
        </View>
        <View style={styles.buttonView}>
          <Button
            title="Close"
            style={styles.button1}
            onPress={close}
            type="primary"
            titleStyle={{ ...getAppFont('Bold') }}
          />
        </View>
      </View>
    </ReactNativeModal>
  );
};

export default SignModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 20,
  },
  headerStyle: {
    // marginBottom: 25,
    padding: Platform.isPad ? 22 : 15,
  },
  buttonView: {
    marginBottom: 20,
    height: 60,
  },
  button1: {
    borderRadius: 10,
    width: Platform.isPad ? 300 : 180,
    // height: 100,
    // padding: Platform.isPad ? 10 : 10,
  },
  signature: {
    flex: 1,
    // borderColor: '#000033',
    borderWidth: 1,
  },
});
